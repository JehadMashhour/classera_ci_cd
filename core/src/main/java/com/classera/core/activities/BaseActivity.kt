package com.classera.core.activities

import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.os.Bundle
import android.os.LocaleList
import android.view.View
import androidx.appcompat.widget.ContentFrameLayout
import androidx.preference.PreferenceManager
import com.classera.data.language.LanguageUtils
import com.classera.data.prefs.Prefs
import com.classera.data.prefs.PrefsImpl
import dagger.android.support.DaggerAppCompatActivity
import java.util.*
import javax.inject.Inject


/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Suppress("UNUSED_PARAMETER")
abstract class BaseActivity : DaggerAppCompatActivity() {

    var parentView: View? = null

    val prefs: Prefs by lazy { PrefsImpl(PreferenceManager.getDefaultSharedPreferences(this)) }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        findParentView()
    }

    override fun getApplicationContext(): Context {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            super.getApplicationContext()
        } else {
            LanguageUtils.applyLocalizationContext(super.getApplicationContext(), prefs)
        }
    }

    override fun getResources(): Resources {
        return when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.O -> {
                val locale = Locale(prefs.language)
                val config = super.getResources().configuration
                config.setLocale(locale)
                val localeList = LocaleList(locale)
                LocaleList.setDefault(localeList)
                config.setLocales(localeList)
                config.setLayoutDirection(locale)
                super.getResources()
            }
            else -> {
                val config = super.getResources().configuration
                config.locale = Locale(prefs.language)
                config.setLayoutDirection(Locale(prefs.language))
                val metrics = super.getResources().displayMetrics
                Resources(assets, metrics, config)
            }
        }
    }

    override fun attachBaseContext(newBase: Context) {
        val prefs: Prefs by lazy { PrefsImpl(PreferenceManager.getDefaultSharedPreferences(newBase)) }
        super.attachBaseContext(LanguageUtils.applyLocalizationContext(newBase, prefs))
    }

    private fun findParentView() {
        val content = findViewById<View>(android.R.id.content)
        if (content is ContentFrameLayout && content.childCount > 0) {
            parentView = content.getChildAt(0)
        }
        if (parentView == null) {
            parentView = content
        }
    }

    // This injection is a workaround for a known issue in Gradle build system https://github.com/google/dagger/issues/955
    @Inject
    fun setDummy(dummy: String) {
        // No impl
    }
}
