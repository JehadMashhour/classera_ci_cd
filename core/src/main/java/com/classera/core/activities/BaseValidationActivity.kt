package com.classera.core.activities

import android.content.res.Resources
import android.os.Bundle
import android.transition.TransitionManager
import android.view.ViewGroup
import android.widget.EditText
import com.classera.core.utils.android.onTextChanged
import com.google.android.material.textfield.TextInputLayout
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 09, 2019
 *
 * @author Mohamed Hamdan
 */

abstract class BaseValidationActivity : BaseBindingActivity(), Validator.ValidationListener {

    protected val validator: Validator by lazy {
        val validator = Validator(this)
        validator.setValidationListener(this)
        validator
    }

    private var lastFocusedField: TextInputLayout? = null

    override fun isBindingEnabled() = false

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        initFocusAndTextChangeListener(parentView as? ViewGroup?)
    }

    private fun initFocusAndTextChangeListener(viewGroup: ViewGroup?) {
        for (i in 0 until (viewGroup?.childCount ?: 0)) {
            val view = viewGroup?.getChildAt(i)
            if (view is ViewGroup) {
                initFocusAndTextChangeListener(view)
            } else if (view is EditText) {
                initFocusChangeListener(view)
                initTextChangeListener(view)
            }
        }
    }

    private fun initFocusChangeListener(editText: EditText) {
        editText.setOnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {
                lastFocusedField = view.parent.parent as TextInputLayout
            }
        }
    }

    private fun initTextChangeListener(editText: EditText) {
        editText.onTextChanged {
            beginDelayedTransition()
            lastFocusedField?.error = null
            lastFocusedField?.isErrorEnabled = false
            lastFocusedField?.requestLayout()
        }
    }

    override fun onValidationFailed(errors: MutableList<ValidationError>?) {
        beginDelayedTransition()
        errors?.forEach { error ->
            val errorMessageResourceName = error.getCollatedErrorMessage(this)
            val errorMessageResource = resources.getIdentifier(errorMessageResourceName, "string", this?.packageName)
            val errorMessage = this?.getString(errorMessageResource)
            if (error.view.parent.parent is TextInputLayout) {
                val paren = (error.view.parent.parent as TextInputLayout)
                paren.isErrorEnabled = true
                paren.error = errorMessage
            } else if (error.view is EditText) {
                (error.view as EditText).error = errorMessage
            }
        }
    }

    private fun getErrorMessage(key: String): String {
        val identifier = resources.getIdentifier(key, "string", packageName)
        return try {
            getString(identifier)
        } catch (ignored: Resources.NotFoundException) {
            key
        }
    }

    private fun beginDelayedTransition() {
        TransitionManager.beginDelayedTransition(parentView as ViewGroup)
    }

    fun removeError(textInputLayout: TextInputLayout?) {
        beginDelayedTransition()
        textInputLayout?.error = null
        textInputLayout?.isErrorEnabled = false
        textInputLayout?.requestLayout()
    }

    // This injection is a workaround for a known issue in Gradle build system https://github.com/google/dagger/issues/955
    @Inject
    fun setDummy3(dummy: String) {
        // No impl
    }
}
