package com.classera.core

import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
fun Context.intentTo(addressableActivity: AddressableActivity): Intent {
    return Intent(addressableActivity.action).setPackage(packageName)
}

fun Fragment.intentTo(addressableActivity: AddressableActivity): Intent {
    return Intent(addressableActivity.action).setPackage(context?.packageName)
}

val FragmentManager.currentNavigationFragment: Fragment?
    get() = primaryNavigationFragment?.childFragmentManager?.fragments?.first()

interface AddressableActivity {

    val action: String

    val className: String
}

object Activities {

    object Login : AddressableActivity {

        override val action: String = "com.classera.authentication.LoginActivity"

        override val className: String = "com.classera.authentication.login.LoginActivity"
    }


    object EssayQuesionActivity : AddressableActivity {

        override val action: String = "com.classera.assignments.create.essay.EssayQuestionActivity"

        override val className: String = "com.classera.assignments.create.essay.EssayQuestionActivity"
    }

    object ParentChildActivity : AddressableActivity {

        override val action: String = "com.classera.main.parent.ParentChildActivity"

        override val className: String = "com.classera.main.parent.ParentChildActivity"
    }


    object Main : AddressableActivity {

        override val action: String = "com.classera.main.MainActivity"

        override val className: String = "com.classera.main.MainActivity"
    }

    object Splash : AddressableActivity {

        override val action: String = "com.classera.splash.SplashActivity"

        override val className: String = "com.classera.splash.SplashActivity"

        const val NOTIFICATION_DATA_EVENT_EXTRA = "notificationDataEventExtra"
        const val NOTIFICATION_DATA_BODY_EXTRA = "notificationDataBodyExtra"
        const val NOTIFICATION_DATA_UUID_EXTRA = "notificationDataUUIDExtra"
    }

    object PublicProfile : AddressableActivity {

        override val action: String = "com.classera.profile.publicprofile.PublicProfileActivity"

        override val className: String = "com.classera.profile.publicprofile.PublicProfileActivity"
    }

    object MultipleQuestionActivity : AddressableActivity {

        override val action: String = "com.classera.assignments.mcq.MultipleQuestionActivity"

        override val className: String = "com.classera.assignments.mcq.MultipleQuestionActivity"
    }
}
