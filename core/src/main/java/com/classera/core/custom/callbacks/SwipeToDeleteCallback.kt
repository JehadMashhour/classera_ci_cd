package com.classera.core.custom.callbacks

import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Canvas
import android.graphics.Bitmap
import android.graphics.RectF
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import com.classera.core.R
import com.classera.core.adapter.BaseAdapter


class SwipeToDeleteCallback constructor(
    drag: Int,
    dir: Int,
    val context: Context,
    private val recyclerView: RecyclerView?
) : ItemTouchHelper.SimpleCallback(drag, dir) {

    private var swiped: MutableLiveData<Boolean> = MutableLiveData()
    private val paint = Paint()

    var position: Int = -1 // default

    init {
        swiped.postValue(false) // default
    }


    fun updateSwipe(value: Boolean) {
        swiped.postValue(value)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    fun getSwiped(): LiveData<Boolean>? {
        return swiped
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        position = viewHolder.adapterPosition
        if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT) {
            updateSwipe(true)
        }
    }

    override fun isItemViewSwipeEnabled(): Boolean {
        return (recyclerView?.adapter as? BaseAdapter<*>?)?.getItemsCount() != 0
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {

        val icon: Bitmap
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

            val itemView = viewHolder.itemView
            val height = itemView.bottom.toFloat() - itemView.top.toFloat()
            val width = height / THIRD_OF_PAGE

            if (dX > 0) {
                paint.color = Color.parseColor("#D32F2F")
                val background =
                    RectF(itemView.left.toFloat(), itemView.top.toFloat(), dX, itemView.bottom.toFloat())
                c.drawRect(background, paint)
                icon = drawableToBitmap(context.getDrawable(R.drawable.ic_delete)!!)
                val iconDest = RectF(
                    itemView.left.toFloat() + width,
                    itemView.top.toFloat() + width,
                    itemView.left.toFloat() + 2 * width,
                    itemView.bottom.toFloat() - width
                )
                c.drawBitmap(icon, null, iconDest, paint)
            } else {
                paint.color = Color.parseColor("#D32F2F")
                val background = RectF(
                    itemView.right.toFloat() + dX,
                    itemView.top.toFloat(),
                    itemView.right.toFloat(),
                    itemView.bottom.toFloat()
                )
                c.drawRect(background, paint)
                icon = drawableToBitmap(context.getDrawable(R.drawable.ic_delete)!!)
                val iconDest = RectF(
                    itemView.right.toFloat() - 2 * width,
                    itemView.top.toFloat() + width,
                    itemView.right.toFloat() - width,
                    itemView.bottom.toFloat() - width
                )
                c.drawBitmap(icon, null, iconDest, paint)
            }
        }
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }

    private fun drawableToBitmap(drawable: Drawable): Bitmap {

        if (drawable is BitmapDrawable) {
            return drawable.bitmap
        }

        val bitmap =
            Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)

        return bitmap
    }

}

const val THIRD_OF_PAGE = 3
