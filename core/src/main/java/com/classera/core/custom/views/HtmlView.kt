package com.classera.core.custom.views

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.util.AttributeSet
import android.view.MotionEvent
import android.webkit.WebView


/**
 * Project: Classera
 * Created: Dec 31, 2019
 *
 * @author Mohamed Hamdan
 */
@SuppressLint("ClickableViewAccessibility", "SetJavaScriptEnabled")
class HtmlView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : WebView(context, attrs, defStyleAttr, defStyleRes) {

    init {
        settings?.javaScriptEnabled = true
        isScrollContainer = false
        setOnTouchListener { _, event -> event.action == MotionEvent.ACTION_MOVE }
        setBackgroundColor(Color.TRANSPARENT)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        return false
    }

    fun setText(text: CharSequence?) {
        if (text.toString().isContainsArabic()) {
            loadDataWithBaseURL(
                null, FORMAT_ARA + text.toString() + FORMAT_END,
                MIME_TYPE, ENCODING, null
            )
            evaluateJSCode()
        } else {
            loadDataWithBaseURL(
                null, FORMAT_ENG + text.toString() + FORMAT_END,
                MIME_TYPE, ENCODING, null
            )
            evaluateJSCode()
        }
    }

    private fun evaluateJSCode() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            evaluateJavascript(JS_EVALUATE_FORMAT_CODE, null)
        } else {
            loadUrl(JS_EVALUATE_FORMAT_CODE)
        }
    }

    private fun String.isContainsArabic(): Boolean {
        return this.mapIndexed { index, _ -> this.codePointAt(index) }
            .any { it in LOWER_LETTER..UPPER_LETTER }
    }

    private companion object {
        private const val MIME_TYPE = "text/html"
        private const val ENCODING = "UTF-8"
        private const val LOWER_LETTER = 0x0600
        private const val UPPER_LETTER = 0x06E0
        private const val FORMAT_ENG =
            "<head><meta name='viewport' content='width=device-width, initial-scale=1.0," +
                    " maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'>" +
                    "<style>table,a,img {width: 100% !important; word-break: break-all;" +
                    "height:auto !important;}" +
                    "</style> </head><body><div id='height'>"
        private const val FORMAT_END = "</div></body>"
        private const val FORMAT_ARA =
            "<head><meta name='viewport' content='width=device-width, initial-scale=1.0," +
                    " maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'>" +
                    "<style>table,a,img {width: 100% !important; word-break: break-all;" +
                    "height:auto !important;}</style> " +
                    "</head><body dir='rtl'><div id='height'>"
        private const val JS_EVALUATE_FORMAT_CODE =
            "var images = document.getElementsByTagName('img'); \n" +
                    "var srcList = [];\n" +
                    "for(var i = 0; i < images.length; i++) {\n" +
                    "    srcList.push(images[i].style.height= 'auto !important');\n" +
                    "    srcList.push(images[i].style.width= '100% !important');\n" +
                    "}"
    }
}
