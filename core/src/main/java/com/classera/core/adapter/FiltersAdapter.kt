package com.classera.core.custom.views

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.ClickableAdapter
import com.classera.core.databinding.RowQuickFilterBinding

/**
 * Project: Classera
 * Created: Dec 24, 2019
 *
 * @author Mohamed Hamdan
 */
internal class FiltersAdapter(
    private val values: Array<String?>?,
    private val onFilterSelected: (position: Int) -> Unit
) : BaseAdapter<FiltersAdapter.ViewHolder>(), ClickableAdapter {

    var selectedPosition: Int = 0

    init {
        onFilterSelected(selectedPosition)
    }

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowQuickFilterBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun onItemClicked(view: View, holder: ViewHolder) {
        val previousPosition = this.selectedPosition
        val currentPosition = holder.adapterPosition
        if (currentPosition != RecyclerView.NO_POSITION) {
            this.selectedPosition = currentPosition
        }
        notifyItemChanged(previousPosition)
        notifyItemChanged(selectedPosition)
        onFilterSelected(selectedPosition)
    }

    override fun getItemsCount(): Int {
        return values?.size ?: 0
    }

    inner class ViewHolder(binding: RowQuickFilterBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowQuickFilterBinding> {
                this.currentPosition = position
                this.selectedPosition = this@FiltersAdapter.selectedPosition
                this.text = values?.get(position)
            }
        }
    }
}
