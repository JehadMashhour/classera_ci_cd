package com.classera.core.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationSet
import android.view.animation.AnimationUtils
import android.view.animation.OvershootInterpolator
import android.view.animation.TranslateAnimation
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.R
import com.classera.core.custom.views.BaseRecyclerView

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Suppress("unused")
abstract class BaseAdapter<V : BaseViewHolder> : RecyclerView.Adapter<V>() {

    private var onItemClickListener: ((view: View, position: Int) -> Unit)? = null

    private var disableEmptyIcon: Boolean = false
    private var animationEnabled: Boolean = true

    private var clickEnabled = true
    protected var context: Context? = null
    protected var inflater: LayoutInflater? = null
    protected var recyclerView: RecyclerView? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
        context = recyclerView.context
        inflater = LayoutInflater.from(context)

        if (recyclerView.layoutManager is GridLayoutManager) {
            val layoutManager = recyclerView.layoutManager as GridLayoutManager
            layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {

                override fun getSpanSize(position: Int): Int {
                    return if (getItemViewType(position) == EMPTY_ITEM_VIEW_TYPE) {
                        layoutManager.spanCount
                    } else {
                        1
                    }
                }
            }
        }

        handleAnimations()
    }

    private fun handleAnimations() {
        if (animationEnabled) {
            val manager = recyclerView?.layoutManager
            if (manager is GridLayoutManager) {
                handleGridAnimations(manager)
            } else if (manager is LinearLayoutManager) {
                handleLinearAnimations(manager)
            }
            if (itemCount > 0) {
                recyclerView?.post { recyclerView?.scheduleLayoutAnimation() }
            }
        }
    }

    private fun handleGridAnimations(manager: GridLayoutManager?) {
        val animationSet = if (manager?.orientation == GridLayoutManager.VERTICAL) {
            getBottomAnimationSet()
        } else {
            getEndAnimationSet()
        }
        val controller = AnimationUtils.loadLayoutAnimation(context, R.anim.grid_layout_animation)
        controller.animation = animationSet
        recyclerView?.layoutAnimation = controller
    }

    private fun handleLinearAnimations(manager: LinearLayoutManager?) {
        val animationSet = if (manager?.orientation == GridLayoutManager.VERTICAL) {
            getBottomAnimationSet()
        } else {
            getEndAnimationSet()
        }
        val controller = AnimationUtils.loadLayoutAnimation(context, R.anim.linear_layout_animation)
        controller.animation = animationSet
        recyclerView?.layoutAnimation = controller
    }

    private fun getBottomAnimationSet(): AnimationSet {
        val animationSet = AnimationSet(true)
        animationSet.addAnimation(getTranslateAnimation(0f, DEFAULT_DELTA))
        animationSet.addAnimation(getAlphaAnimation())

        return animationSet
    }

    private fun getEndAnimationSet(): AnimationSet {
        val animationSet = AnimationSet(true)
        animationSet.addAnimation(getTranslateAnimation(DEFAULT_DELTA, 0f))
        animationSet.addAnimation(getAlphaAnimation())
        return animationSet
    }

    private fun getAlphaAnimation(): Animation {
        val alphaAnimation = AlphaAnimation(START_ALPHA, END_ALPHA)
        alphaAnimation.interpolator = OvershootInterpolator()
        alphaAnimation.duration = DEFAULT_DURATION
        return alphaAnimation
    }

    private fun getTranslateAnimation(fromXDelta: Float, fromYDelta: Float): Animation {
        val translateAnimation = TranslateAnimation(
            Animation.RELATIVE_TO_PARENT, fromXDelta,
            Animation.RELATIVE_TO_PARENT, 0f,
            Animation.RELATIVE_TO_PARENT, fromYDelta,
            Animation.RELATIVE_TO_PARENT, 0f
        )
        translateAnimation.interpolator = OvershootInterpolator()
        translateAnimation.duration = DEFAULT_DURATION
        return translateAnimation
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        this.recyclerView = null
        this.onItemClickListener = null
        this.inflater = null
        this.context = null
    }

    @Suppress("UNCHECKED_CAST")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): V {
        if (viewType == EMPTY_ITEM_VIEW_TYPE) {
            val view = inflater?.inflate(R.layout.row_empty, parent, false)
            return EmptyViewHolder(view) as V
        }
        return getViewHolder(parent, viewType)
    }

    abstract fun getViewHolder(parent: ViewGroup, viewType: Int): V

    override fun onBindViewHolder(holder: V, position: Int) {
        if ((onItemClickListener != null || this is ClickableAdapter)
            && holder::class.java != EmptyViewHolder::class.java
        ) {
            handleClickListener(holder)
        }

        bind(holder, position)
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0 && getItemsCount() == 0) {
            return EMPTY_ITEM_VIEW_TYPE
        }
        return super.getItemViewType(position)
    }

    open fun bind(holder: V, position: Int) {
        holder.bind(position)
    }

    final override fun getItemCount(): Int {
        return if (getItemsCount() > 0) getItemsCount() else if (disableEmptyIcon) 0 else 1
    }

    abstract fun getItemsCount(): Int

    private fun handleClickListener(holder: V) {
        holder.itemView.setOnClickListener { view ->
            if (!clickEnabled) {
                return@setOnClickListener
            }
            view.isEnabled = false
            clickEnabled = false
            onItemClicked(view, holder)
            view.postDelayed({
                clickEnabled = true
                view.isEnabled = true
            }, DISABLE_VIEW_DURATION)
        }
    }

    open fun onItemClicked(view: View, holder: V) {
        val clickedPosition = holder.adapterPosition
        if (clickedPosition != RecyclerView.NO_POSITION) {
            onItemClickListener?.invoke(view, clickedPosition)
        }
    }

    fun setOnItemClickListener(onItemClickListener: (view: View, position: Int) -> Unit) {
        this.onItemClickListener = onItemClickListener
    }

    fun getOnItemClickListener(): ((view: View, position: Int) -> Unit)? {
        return onItemClickListener
    }

    fun disableAnimations() {
        animationEnabled = false
    }

    fun disableEmptyView() {
        disableEmptyIcon = true
    }

    fun scheduleLayoutAnimation() {
        if (animationEnabled) {
            recyclerView?.scheduleLayoutAnimation()
        }
    }

    private inner class EmptyViewHolder(view: View?) : BaseViewHolder(view) {

        private var textViewMessage: TextView? = null

        init {
            textViewMessage = itemView.findViewById(R.id.text_view_row_empty_message)
        }

        override fun bind(position: Int) {
            if (recyclerView is BaseRecyclerView) {
                val emptyMessage = (recyclerView as BaseRecyclerView).emptyMessage
                if (emptyMessage != null) {
                    textViewMessage?.text = emptyMessage
                }
            }
        }
    }


    private companion object {

        private const val EMPTY_ITEM_VIEW_TYPE = 135
        private const val DEFAULT_DELTA = 0.7f
        private const val DEFAULT_DURATION = 350L
        private const val DISABLE_VIEW_DURATION = 500L
        private const val START_ALPHA = 0.2f
        private const val END_ALPHA = 1f
    }
}
