package com.classera.core.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Odai Nazzal on 12/28/2019.
 * Classera
 *
 * o.nazzal@classera.com
 */
abstract class BaseSingleSelectionAdapter<V : BaseViewHolder> : BaseAdapter<V>(), ClickableAdapter {

    open var selectedPosition: Int = -1

    override fun onItemClicked(view: View, holder: V) {
        super.onItemClicked(view, holder)
        val clickedPosition = holder.adapterPosition
        if (clickedPosition != RecyclerView.NO_POSITION) {
            if (selectedPosition != -1) {
                notifyItemChanged(selectedPosition)
            }
            selectedPosition = holder.adapterPosition
            notifyItemChanged(selectedPosition)
        }
    }
}
