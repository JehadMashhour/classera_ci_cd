package com.classera.core.utils

fun Boolean?.toInt(): Int {
    return if (this == true) 1 else 0
}
