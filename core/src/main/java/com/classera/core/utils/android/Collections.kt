package com.classera.core.utils.android

/**
 * Project: Classera
 * Created: Jun 04, 2020
 *
 * @author Mohamed Hamdan
 */
fun <K, V> Map<K, V>.removeNull(): Map<K, V?> {
    val result = mutableMapOf<K, V>()
    forEach {
        if (it.value != null) {
            result[it.key] = it.value
        }
    }
    return result
}
