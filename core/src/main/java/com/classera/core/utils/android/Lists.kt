package com.classera.core.utils.android

import androidx.lifecycle.MutableLiveData

/**
 * Project: Classera
 * Created: 7/5/2021
 *
 * @author Jehad Abdalqader
 */

inline fun <T> List<T>.contains(predicate: (T) -> Boolean): Boolean {
    return this.find(predicate) != null
}

fun <T> MutableLiveData<T>.notifyChanged() {
    this.value = this.value
}

fun <T> MutableLiveData<T>.notifyChangedBackground() {
    this.postValue(this.value)
}
