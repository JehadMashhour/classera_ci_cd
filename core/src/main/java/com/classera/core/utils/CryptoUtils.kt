package com.classera.core.utils

import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.InputStream
import java.security.Key
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec

const val ALGORITHM: String = "AES";
const val TRANSFORMATION: String = "AES";

fun encrypt(key: String, inputStream: InputStream) : InputStream{
    return doCrypto(Cipher.ENCRYPT_MODE, key, inputStream).inputStream()
}

fun decrypt(key: String, inputStream: InputStream) : InputStream {
    return doCrypto(Cipher.DECRYPT_MODE, key, inputStream).inputStream()
}

private fun doCrypto(cipherMode: Int, key: String, inputStream: InputStream) : ByteArray {
    try {
        val secretKey: Key = SecretKeySpec(key.toByteArray(), ALGORITHM)
        val cipher: Cipher = Cipher.getInstance(TRANSFORMATION)
        cipher.init(cipherMode, secretKey)

        return cipher.doFinal(inputStream.readBytes())

    } catch (e: Exception) {
        throw  java.lang.Exception("Error encrypting/decrypting file", e)
    }
}
