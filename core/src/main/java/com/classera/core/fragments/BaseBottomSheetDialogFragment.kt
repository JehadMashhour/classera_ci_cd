package com.classera.core.fragments

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.findNavController
import com.classera.core.R
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Suppress("UNUSED_PARAMETER")
abstract class BaseBottomSheetDialogFragment : BottomSheetDialogFragment() {

    lateinit var behavior: BottomSheetBehavior<FrameLayout>

    private var bottomSheetCallback = object : BottomSheetBehavior.BottomSheetCallback() {

        override fun onSlide(bottomSheet: View, slideOffset: Float) {
            // No impl
        }

        override fun onStateChanged(bottomSheet: View, newState: Int) {
            if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                behavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        }
    }

    var imageViewClose: ImageView? = null

    protected abstract val layoutId: Int

    open fun enableDependencyInjection(): Boolean {
        return true
    }

    override fun onAttach(context: Context) {
        if (enableDependencyInjection()) {
            AndroidSupportInjection.inject(this)
        }
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.BottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findView()
        initListeners()
    }


    private fun findView() {
        imageViewClose = view?.findViewById(R.id.image_view_bottom_sheet_close)
    }

    private fun initListeners() {
        imageViewClose?.setOnClickListener {
            behavior.state = BottomSheetBehavior.STATE_HIDDEN
        }
    }

    protected fun <T> setResult(key: String, value: T) {
        findNavController().previousBackStackEntry?.savedStateHandle?.set(key, value)
    }

    protected fun <T> backWithResult(key: String, value: T) {
        findNavController().apply {
            previousBackStackEntry?.savedStateHandle?.set(key, value)
            popBackStack()
        }
    }

    protected fun <T> onResult(key: String): LiveData<T>? {
        return findNavController()
            .currentBackStackEntry
            ?.savedStateHandle
            ?.getLiveData(key)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        dialog.setCanceledOnTouchOutside(isCancelable)
        dialog.setOnShowListener {
            val bottomSheet =
                dialog.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)
            behavior = BottomSheetBehavior.from(bottomSheet!!)
            behavior.isHideable = isHideAble()
            behavior.isDraggable = isDraggable()
            behavior.state = BottomSheetBehavior.STATE_COLLAPSED
        }
        return dialog
    }

    open fun isHideAble(): Boolean {
        return true
    }

    open fun isDraggable(): Boolean {
        return true
    }

    fun preventHiding() {
        imageViewClose?.isEnabled = false
        behavior.addBottomSheetCallback(bottomSheetCallback)
    }

    fun allowHiding() {
        imageViewClose?.isEnabled = true
        behavior.removeBottomSheetCallback(bottomSheetCallback)
    }

    // This injection is a workaround for a known issue in Gradle build system https://github.com/google/dagger/issues/955
    @Inject
    fun setDummy(dummy: String) {
        // No impl
    }
}
