package com.classera.core.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.findNavController
import com.classera.core.Screen
import com.flurry.android.FlurryAgent
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Suppress("UNUSED_PARAMETER")
abstract class BaseFragment : DaggerFragment() {

    protected abstract val layoutId: Int

    private var disabledViews: MutableList<View?>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val screenName = this::class.java.getAnnotation(Screen::class.java)?.value
        FlurryAgent.logEvent("screen_opened", mapOf("screen_name" to screenName))
    }

    protected fun disableAllViews() {
        disableAllViews(view as ViewGroup)
    }

    private fun disableAllViews(viewGroup: ViewGroup?) {
        for (i in 0 until (viewGroup?.childCount ?: 0)) {
            if (viewGroup?.getChildAt(i) is ViewGroup) {
                disableView(viewGroup)
                addViewToDisabledViews(viewGroup)
                disableAllViews(viewGroup.getChildAt(i) as? ViewGroup?)
            } else if (viewGroup?.getChildAt(i)?.isEnabled == true) {
                val view = viewGroup.getChildAt(i)
                disableView(view)
                addViewToDisabledViews(view)
            }
        }
    }

    protected fun enableViews() {
        enableAllViews()
        clearDisabledViews()
    }

    protected fun <T> setResult(key: String, value: T) {
        findNavController().previousBackStackEntry?.savedStateHandle?.set(key, value)
    }

    protected fun <T> backWithResult(key: String, value: T) {
        findNavController().apply {
            previousBackStackEntry?.savedStateHandle?.set(key, value)
            popBackStack()
        }
    }

    protected fun <T> onResult(key: String): LiveData<T>? {
        return findNavController()
            .currentBackStackEntry
            ?.savedStateHandle
            ?.getLiveData(key)
    }

    private fun disableView(view: View?) {
        view?.isEnabled = false
    }

    private fun addViewToDisabledViews(view: View?) {
        disabledViews?.add(view)
    }

    private fun enableAllViews() {
        disabledViews?.forEach { it?.isEnabled = true }
    }

    private fun clearDisabledViews() {
        disabledViews?.clear()
    }


    // This injection is a workaround for a known issue in Gradle build system https://github.com/google/dagger/issues/955
    @Inject
    fun setDummy(dummy: String) {
        // No impl
    }
}
