package com.classera.core.fragments

import android.os.Bundle
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AutoCompleteTextView
import android.widget.EditText
import android.widget.TextView
import com.classera.core.utils.android.hideKeyboard
import com.classera.core.utils.android.onTextChanged
import com.google.android.material.textfield.TextInputLayout
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 09, 2019
 *
 * @author Mohamed Hamdan
 */
abstract class BaseValidationFragment : BaseBindingFragment(), Validator.ValidationListener {
    protected lateinit var validator: Validator

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setupValidator()
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    private fun setupValidator() {
        validator = Validator(this)
        validator.setValidationListener(this)
    }

    private var lastFocusedField: TextInputLayout? = null
    override fun isBindingEnabled() = false
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initFocusAndTextChangeListener(view as? ViewGroup?)
        handelAutoCompleteTextViewClickListener(view as? ViewGroup?)
    }

    private fun handelAutoCompleteTextViewClickListener(viewGroup: ViewGroup?) {
        for (i in 0 until (viewGroup?.childCount ?: 0)) {
            val view = viewGroup?.getChildAt(i)
            if (view is ViewGroup) {
                handelAutoCompleteTextViewClickListener(view)
            } else if (view is AutoCompleteTextView) {
                view.setOnClickListener { hideKeyboard() }
            }
        }
    }

    private fun initFocusAndTextChangeListener(viewGroup: ViewGroup?) {
        for (i in 0 until (viewGroup?.childCount ?: 0)) {
            val view = viewGroup?.getChildAt(i)
            if (view is ViewGroup) {
                initFocusAndTextChangeListener(view)
            } else if (view is EditText) {
                initFocusChangeListener(view)
                initTextChangeListener(view)
            } else if (view is TextView) {
                //initFocusChangeListener(view)
                initTextChangeListener(view)
            }
        }
    }

    private fun initFocusChangeListener(mView: View) {
        mView.setOnFocusChangeListener { view, hasFocus ->
            when (view) {
                is EditText -> {
                    if (hasFocus && view.parent.parent is TextInputLayout) {
                        lastFocusedField = view.parent.parent as TextInputLayout
                    }
                }
            }

        }
    }


    private fun initTextChangeListener(view: View) {
        when (view) {
            is EditText -> {
                view.onTextChanged {
                    removeError(lastFocusedField)
                }
            }

            is TextView -> {
                view.onTextChanged {
                    removeError(view)
                }
            }
        }

    }


    fun removeError(view: View?) {
        when (view) {
            is TextInputLayout -> {
                beginDelayedTransition()
                view.error = null
                view.isErrorEnabled = false
                view.requestLayout()
            }

            is TextView -> {
                view.error = null
                view.clearFocus()
            }
        }

    }


    override fun onValidationFailed(errors: MutableList<ValidationError>?) {
        beginDelayedTransition()
        errors?.forEach { error ->
            val errorMessageResourceName = error.getCollatedErrorMessage(requireContext())
            val errorMessageResource =
                resources.getIdentifier(errorMessageResourceName, "string", context?.packageName)
            val errorMessage = context?.getString(errorMessageResource)
            when {
                error.view.parent.parent is TextInputLayout -> {
                    val paren = (error.view.parent.parent as TextInputLayout)
                    paren.isErrorEnabled = true
                    paren.error = errorMessage
                }
                error.view is EditText -> {
                    (error.view as EditText).error = errorMessage
                    (error.view as EditText).requestFocus()
                }
                error.view is TextView -> {
                    (error.view as TextView).error = errorMessage
                    (error.view as TextView).requestFocus()
                }
            }
        }
    }

    private fun beginDelayedTransition() {
        TransitionManager.beginDelayedTransition(view as ViewGroup)
    }

    // This injection is a workaround for a known issue in Gradle build system https://github.com/google/dagger/issues/955
    @Inject
    fun setDummy5(dummy: String) {
        // No impl
    }
}
