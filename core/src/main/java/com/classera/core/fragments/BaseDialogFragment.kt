package com.classera.core.fragments

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import androidx.appcompat.app.AppCompatDialog
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.findNavController
import com.classera.core.R
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Suppress("UNUSED_PARAMETER")
abstract class BaseDialogFragment : AppCompatDialogFragment() {


    var imageViewClose: ImageView? = null

    protected abstract val layoutId: Int

    open fun enableDependencyInjection(): Boolean {
        return true
    }

    override fun onAttach(context: Context) {
        if (enableDependencyInjection()) {
            AndroidSupportInjection.inject(this)
        }
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId, container);
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findView()
        initListeners()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
    }

    private fun findView() {
        imageViewClose = view?.findViewById(R.id.image_view_bottom_sheet_close)
    }

    private fun initListeners() {
        imageViewClose?.setOnClickListener {
            dismiss()
        }
    }

    protected fun <T> setResult(key: String, value: T) {
        findNavController().previousBackStackEntry?.savedStateHandle?.set(key, value)
    }

    protected fun <T> backWithResult(key: String, value: T) {
        findNavController().apply {
            previousBackStackEntry?.savedStateHandle?.set(key, value)
            popBackStack()
        }
    }

    protected fun <T> onResult(key: String): LiveData<T>? {
        return findNavController()
            .currentBackStackEntry
            ?.savedStateHandle
            ?.getLiveData(key)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as AppCompatDialog
        dialog.window?.attributes?.windowAnimations = R.style.enter_exit_animate

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setStyle(STYLE_NO_FRAME, android.R.style.Theme);
        dialog.setCanceledOnTouchOutside(isCancelable)
        return dialog
    }

    fun preventHiding() {
        imageViewClose?.isEnabled = false
    }

    fun allowHiding() {
        imageViewClose?.isEnabled = true
    }

    // This injection is a workaround for a known issue in Gradle build system https://github.com/google/dagger/issues/955
    @Inject
    fun setDummy(dummy: String) {
        // No impl
    }
}
