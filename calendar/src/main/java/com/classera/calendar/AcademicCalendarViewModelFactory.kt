package com.classera.calendar

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.calendar.add.AddEventViewModel
import com.classera.calendar.student.AcademicCalenderStudentViewModel
import com.classera.calendar.teacher.AcademicCalenderTeacherViewModel
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.calendar.CalendarRepository
import com.classera.data.repositories.lectures.LecturesRepository
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
class AcademicCalendarViewModelFactory @Inject constructor(
    private val lecturesRepository: LecturesRepository,
    private val academicCalendarRepository: CalendarRepository,
    private val prefs: Prefs
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            AcademicCalenderStudentViewModel::class.java -> {
                AcademicCalenderStudentViewModel(academicCalendarRepository, prefs) as T
            }
            AcademicCalenderTeacherViewModel::class.java -> {
                AcademicCalenderTeacherViewModel(academicCalendarRepository, prefs) as T
            }
            else -> {
                throw IllegalAccessException("There is no view model called $modelClass")
            }
        }
    }
}
