package com.classera.calendar.student

import com.classera.calendar.AcademicCalendarViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.calendar.AcademicCalendarEvent
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.calendar.CalendarRepository

/**
 * Created by Rawan Al-Theeb on 1/23/2020.
 * Classera
 * r.altheeb@classera.com
 */
class AcademicCalenderStudentViewModel(private val calendarRepository: CalendarRepository, prefs: Prefs) :
    AcademicCalendarViewModel(prefs) {

    override suspend fun getEvents(start: String?, end: String?): BaseWrapper<List<AcademicCalendarEvent>> {
        return calendarRepository.getUserEvents(start, end)
    }

    override fun deleteEvent(position: Int) {
        throw IllegalAccessException("The student should not be able to delete a AcademicCalender")
    }


}
