package com.classera.calendar.student

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.calendar.AcademicCalendarViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * Created by Rawan Al-Theeb on 1/23/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class AcademicCalenderStudentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: AcademicCalendarViewModelFactory
        ): AcademicCalenderStudentViewModel {
            return ViewModelProvider(fragment, factory)[AcademicCalenderStudentViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideAcademicCalendarAdapter(viewModel: AcademicCalenderStudentViewModel): AcademicCalenderStudentAdapter{
            return AcademicCalenderStudentAdapter(viewModel)
        }
    }

    @Binds
    abstract fun bindFragment(fragment: AcademicCalenderStudentFragment): Fragment
}
