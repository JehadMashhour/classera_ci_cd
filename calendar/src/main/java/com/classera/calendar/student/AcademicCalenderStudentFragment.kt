package com.classera.calendar.student

import com.classera.calendar.AcademicCalendarAdapter
import com.classera.calendar.AcademicCalendarFragment
import com.classera.calendar.AcademicCalendarViewModel
import javax.inject.Inject


/**
 * Created by Rawan Al-Theeb on 1/23/2020.
 * Classera
 * r.altheeb@classera.com
 */
class AcademicCalenderStudentFragment : AcademicCalendarFragment() {

    @Inject
    lateinit var viewModel: AcademicCalenderStudentViewModel

    @Inject
    lateinit var adapter: AcademicCalenderStudentAdapter


    override fun getCalenderViewModel(): AcademicCalendarViewModel {
        return viewModel
    }

    override fun getCalenderAdapter(): AcademicCalendarAdapter<*> {
        return adapter
    }

    override fun handleAdapterListeners() {
        // No impl
    }
}
