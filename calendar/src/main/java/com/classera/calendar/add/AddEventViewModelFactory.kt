package com.classera.calendar.add

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.calendar.CalendarRepository
import com.classera.data.repositories.lectures.LecturesRepository
import javax.inject.Inject


/**
 * Created by Rawan Al-Theeb on 2/9/2020.
 * Classera
 * r.altheeb@classera.com
 */
class AddEventViewModelFactory @Inject constructor(
    private val calendarRepository: CalendarRepository,
    private val lecturesRepository: LecturesRepository,
    private val addEventFragmentArgs: AddEventFragmentArgs
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AddEventViewModel(lecturesRepository,calendarRepository, addEventFragmentArgs) as T
    }
}
