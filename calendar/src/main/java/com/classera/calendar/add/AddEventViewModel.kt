package com.classera.calendar.add

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.selection.Selectable
import com.classera.data.models.vcr.Lecture
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.calendar.CalendarRepository
import com.classera.data.repositories.lectures.LecturesRepository
import com.classera.data.toDate
import com.classera.data.toString

/**
 * Created by Rawan Al-Theeb on 1/27/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Suppress("LongParameterList")
class AddEventViewModel(
    private val lecturesRepository: LecturesRepository,
    private val calendarRepository: CalendarRepository,
    private val addEventFragmentArgs: AddEventFragmentArgs
) : BaseViewModel() {
    var lectures: Array<out Lecture>? = null
    private var selectedLectures: Array<out Selectable>? = null
    private var selectedUsers: Array<out Selectable>? = null

    fun getLectures() = liveData {
        emit(Resource.Loading(show = true))
        val resource =
            tryResource { lecturesRepository.getLectures() }
        lectures = resource.element<BaseWrapper<List<Lecture>>>()?.data?.toTypedArray()
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun onSaveClicked(
        title: String?,
        startingDate: String?,
        startingTime: String?,
        endingDate: String?,
        endingTime: String?,
        description: String?,
        allDay: String?
    ) = liveData {
        emit(Resource.Loading(show = true))
        val resource = if (addEventFragmentArgs.event == null) {
            startAddingEvent(title, startingDate, startingTime, endingDate, endingTime, allDay, description)
        } else {
            startEditingEvent(
                title,
                addEventFragmentArgs.event!!.id,
                startingDate,
                startingTime,
                endingDate,
                endingTime,
                allDay,
                description
            )
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private suspend fun startAddingEvent(
        title: String?,
        startingDate: String?,
        startingTime: String?,
        endingDate: String?,
        endingTime: String?,
        allDay: String?,
        description: String?
    ): Resource =
        tryNoContentResource {
            calendarRepository.teacherAddEvent(
                title = title,
                startingDate = "$startingDate ${startingTime.toDate("hh:mm a").toString("HH:mm")}",
                endingDate = "$endingDate ${endingTime.toDate("hh:mm a").toString("HH:mm")}",
                allDay = allDay,
                description = description,
                lecturesIds = selectedLectures?.filter { it.selected }?.map { it.id },
                usersIds = selectedUsers?.filter { it.selected }?.map { it.id }
            )
        }

    private suspend fun startEditingEvent(
        title: String?,
        eventId: String?,
        startingDate: String?,
        startingTime: String?,
        endingDate: String?,
        endingTime: String?,
        allDay: String?,
        description: String?
    ): Resource =
        tryNoContentResource {
            calendarRepository.teacherEditEvent(
                title,
                eventId,
                "$startingDate ${startingTime.toDate("hh:mm a").toString("HH:mm")}",
                "$endingDate ${endingTime.toDate("hh:mm a").toString("HH:mm")}",
                allDay,
                description
            )
        }

    fun setSelectedLectures(lectures: Array<out Selectable>?) {
        this.selectedLectures = lectures
    }

    fun setSelectedUsers(users: Array<out Selectable>?) {
        this.selectedUsers = users
    }
}


