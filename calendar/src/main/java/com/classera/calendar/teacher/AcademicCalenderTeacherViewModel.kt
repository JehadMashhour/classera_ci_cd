package com.classera.calendar.teacher

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.classera.calendar.AcademicCalendarViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.calendar.AcademicCalendarEvent
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.calendar.CalendarRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


/**
 * Created by Rawan Al-Theeb on 1/23/2020.
 * Classera
 * r.altheeb@classera.com
 */
class AcademicCalenderTeacherViewModel(private val calendarRepository: CalendarRepository, prefs: Prefs) :
    AcademicCalendarViewModel(prefs) {

    private val _notifyItemRemovedLiveData = MutableLiveData<Int>()
    val notifyItemRemovedLiveData: LiveData<Int> = _notifyItemRemovedLiveData

    private val _notifyItemRemoveErrorLiveData = MutableLiveData<String>()
    val notifyItemRemoveErrorLiveData: LiveData<String> = _notifyItemRemoveErrorLiveData

    override suspend fun getEvents(start: String?, end: String?): BaseWrapper<List<AcademicCalendarEvent>> {
        return calendarRepository.getUserEvents(start, end)
    }

    override fun deleteEvent(position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val event = getEvent(position)
            event?.deleting = true

            val eventId = event?.id
            val resource = tryNoContentResource {
                calendarRepository.teacherDeleteEvent(eventId)
            }
            if (resource.isSuccess()) {
                deleteItem(position)
                _notifyItemRemovedLiveData.postValue(position)
                return@launch
            } else {
                _notifyItemRemoveErrorLiveData.postValue((resource as Resource.Error).error.message)
            }
            event?.deleting = false
        }
    }


}
