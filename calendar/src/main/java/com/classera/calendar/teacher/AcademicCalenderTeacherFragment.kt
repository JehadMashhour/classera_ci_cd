package com.classera.calendar.teacher

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.PopupMenu
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.classera.calendar.AcademicCalendarAdapter
import com.classera.calendar.AcademicCalendarFragment
import com.classera.calendar.AcademicCalendarViewModel
import com.classera.calendar.R
import com.classera.core.utils.android.observe
import com.google.android.material.floatingactionbutton.FloatingActionButton
import javax.inject.Inject


/**
 * Created by Rawan Al-Theeb on 1/23/2020.
 * Classera
 * r.altheeb@classera.com
 */
class AcademicCalenderTeacherFragment : AcademicCalendarFragment() {

    @Inject
    lateinit var viewModel: AcademicCalenderTeacherViewModel

    @Inject
    lateinit var adapter: AcademicCalenderTeacherAdapter

    private var floatingActionButton: FloatingActionButton? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initAddEventFab()
        initViewModelListeners()
    }

    private fun findViews() {
        floatingActionButton = view?.findViewById(R.id.floating_action_button_fragment_calender_add)
    }

    private fun initAddEventFab() {
        floatingActionButton?.visibility = View.VISIBLE
    }

    override fun getCalenderViewModel(): AcademicCalendarViewModel {
        return viewModel
    }

    override fun getCalenderAdapter(): AcademicCalendarAdapter<*> {
        return adapter
    }

    override fun handleAdapterListeners() {
        adapter.setOnItemClickListener { view, position ->
            when (view.id) {
                R.id.image_view_row_academic_calender_more -> {
                    handleMoreClicked(view, position)
                }
            }
        }

        floatingActionButton?.setOnClickListener {
            navigateToAddEvent()
        }

    }

    private fun navigateToAddEvent() {
        val event = null
        val direction = AcademicCalenderTeacherFragmentDirections.addEventDirection(
            resources.getString(R.string.title_fragment_add_event_calender_title),
            event
        )
        view?.findNavController()?.navigate(direction)
    }

    private fun handleMoreClicked(view: View, position: Int) {
        val menu = PopupMenu(requireContext(), view)
        menu.menuInflater.inflate(R.menu.menu_calender_teacher_actions, menu.menu)
        menu.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.item_menu_calender_teacher_actions_edit -> {
                    handleEditItemClicked(position)
                }
                R.id.item_menu_calender_teacher_actions_delete -> {
                    handleDeleteItemClicked(position)
                }
            }
            return@setOnMenuItemClickListener true
        }
        menu.show()
    }

    private fun handleEditItemClicked(position: Int) {
        val event = viewModel.getEvent(position)
        event?.start = viewModel.getEventById(event?.id)?.start
        event?.end = viewModel.getEventById(event?.id)?.end
        val title = event?.title
        val direction = AcademicCalenderTeacherFragmentDirections.addEventDirection(title, event)
        findNavController().navigate(direction)
    }

    private fun handleDeleteItemClicked(position: Int) {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.title_delete_calender_event_dialog)
            .setMessage(getString(R.string.message_delete_calender_event_dialog))
            .setPositiveButton(R.string.button_positive_delete_calender_event_dialog) { _, _ ->
                viewModel.deleteEvent(position)
            }
            .setNegativeButton(R.string.button_negative_delete_calender_event_dialog, null)
            .show()
    }

    private fun initViewModelListeners() {
        viewModel.notifyItemRemovedLiveData.observe(this) {
            adapter.notifyItemRemoved(it)
            refreshAcademicCalender()
        }
        viewModel.notifyItemRemoveErrorLiveData.observe(this) { errorMessage ->
            Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()
        }
    }
}
