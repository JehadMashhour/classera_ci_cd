package com.classera.asessments

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.asessments.assessmentdetails.AssessmentDetailsViewModel
import com.classera.data.repositories.assessments.AssessmentsRepoistory
import javax.inject.Inject

class AssessmentsModelFactory @Inject constructor(
    private val assessmentsRepository: AssessmentsRepoistory
) : ViewModelProvider.Factory{

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            AssessmentsViewModel::class.java -> AssessmentsViewModel(assessmentsRepository) as T
            AssessmentDetailsViewModel::class.java -> AssessmentDetailsViewModel(assessmentsRepository) as T
            else -> throw IllegalAccessException("There is no view model called $modelClass")
        }

    }
}
