package com.classera.asessments

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.ProgressBar
import android.widget.SearchView
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.onDebounceQueryTextChange
import com.classera.data.models.assessments.Assessment
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import javax.inject.Inject

@Screen("Assessments")
class AssessmentsFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: AssessmentsViewModel

    private var recyclerView: RecyclerView? = null
    private var progressBar: ProgressBar? = null
    private var adapter: AssessmentsAdapter? = null
    private var errorView: ErrorView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var searchView: SearchView? = null

    override val layoutId: Int = R.layout.fragment_assessment

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initListeners()
    }

    private fun initViews() {
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_assessments)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_assessments)
        errorView = view?.findViewById(R.id.error_view_fragment_assessment)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_assessments)
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener { refreshAssessments() }
        getAssessments()
    }

    private fun getAssessments(pageNumber: Int = DEFAULT_PAGE) {
        if (pageNumber == DEFAULT_PAGE) {
            adapter?.resetPaging()
        }
        viewModel.getAssessment(searchView?.query, pageNumber).observe(this, this::handleResource)
    }

    private fun refreshAssessments() {
        viewModel.refreshAssessment(searchView?.query).observe(this, this::handleResource)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val searchMenuItem = menu.findItem(R.id.item_menu_fragment_assessments_search)
        searchView = (searchMenuItem.actionView as? SearchView?)
        searchView?.onDebounceQueryTextChange { getAssessments() }

        searchView?.setOnCloseListener {
            getAssessments()
            return@setOnCloseListener false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_assessments, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun initItemClickListener() {
        adapter?.setOnItemClickListener { view, position ->
            navigateToAssessmentDetails(view, viewModel.getAssessment(position))
        }
    }

    private fun navigateToAssessmentDetails(view: View, assessment: Assessment?) {
        val id = assessment?.id
        val title = assessment?.title
        view.findNavController().navigate(AssessmentsFragmentDirections.assessmentDetailsDirection(id, title))
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        if (adapter == null) {
            initAdapter()
        } else {
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()
    }

    private fun initAdapter() {
        adapter = AssessmentsAdapter(viewModel)
        recyclerView?.adapter = adapter
        adapter?.setOnLoadMoreListener(::getAssessments)
        initItemClickListener()
    }

    private fun handleErrorResource(resource: Resource.Error) {
        if ((adapter?.getItemsCount() ?: 0) > 0) {
            Toast.makeText(context, resource.error.resourceMessage, Toast.LENGTH_LONG).show()
            return
        }
        errorView?.visibility = View.VISIBLE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getAssessments() }
        adapter?.finishLoading()
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    override fun onDestroyView() {
        errorView?.setOnRetryClickListener(null)
        errorView = null
        progressBar = null
        recyclerView?.adapter = null
        recyclerView = null
        adapter = null
        swipeRefreshLayout = null
        searchView = null
        super.onDestroyView()
    }
}
