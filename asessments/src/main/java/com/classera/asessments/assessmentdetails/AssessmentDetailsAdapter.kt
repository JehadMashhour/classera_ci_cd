package com.classera.asessments.assessmentdetails

import android.view.ViewGroup
import com.classera.asessments.databinding.RowAssessmentDetailsBinding
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter

class AssessmentDetailsAdapter(
    private val viewModel: AssessmentDetailsViewModel
) : BasePagingAdapter<BaseBindingViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowAssessmentDetailsBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getAssessmentCount()
    }

    inner class ViewHolder(binding: RowAssessmentDetailsBinding) : BaseBindingViewHolder(binding) {
        override fun bind(position: Int) {
            bind<RowAssessmentDetailsBinding> {
                assessmentDetails = viewModel.getAssessment(position)
            }
        }
    }
}
