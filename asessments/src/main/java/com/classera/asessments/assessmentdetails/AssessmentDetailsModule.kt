package com.classera.asessments.assessmentdetails

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.asessments.AssessmentsModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class AssessmentDetailsModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: AssessmentsModelFactory
        ): AssessmentDetailsViewModel {
            return ViewModelProvider(fragment, factory)[AssessmentDetailsViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: AssessmentsDetailsFragment): Fragment
}
