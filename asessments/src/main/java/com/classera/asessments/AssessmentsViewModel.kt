package com.classera.asessments

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.assessments.Assessment
import com.classera.data.models.assessments.AssessmentResponse
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.assessments.AssessmentsRepoistory
import kotlinx.coroutines.Dispatchers

class AssessmentsViewModel(private val assessmentsRepository: AssessmentsRepoistory) : BaseViewModel() {

    private var assessments: MutableList<Assessment> = mutableListOf()

    fun getAssessment(position: Int): Assessment? {
        return assessments[position]
    }

    fun getAssessmentCount(): Int {
        return assessments.size
    }

    fun getAssessment(text: CharSequence?, pageNumber: Int): LiveData<Resource> {
        return getAssessment(text, pageNumber, pageNumber == DEFAULT_PAGE)
    }

    fun refreshAssessment(text: CharSequence?) = getAssessment(text, DEFAULT_PAGE, false)

    private fun getAssessment(text: CharSequence?, pageNumber: Int, showProgress: Boolean) = liveData(Dispatchers.IO) {
        if (showProgress) {
            emit(Resource.Loading(show = true))
        }

        val resource = tryResource {
            assessmentsRepository.getAssessmentsList(
                pageNumber,
                text = text
            )
        }

        if (pageNumber == DEFAULT_PAGE) {
            assessments.clear()
        }

        assessments.addAll(
            resource.element<BaseWrapper<AssessmentResponse>>()?.data?.assessments ?: mutableListOf()
        )

        emit(resource)
        emit(Resource.Loading(show = false))
    }
}
