package com.classera.asessments

import android.view.ViewGroup
import com.classera.asessments.databinding.RowAssessmentBinding
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter

class AssessmentsAdapter(private val viewModel: AssessmentsViewModel) : BasePagingAdapter<BaseBindingViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowAssessmentBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getAssessmentCount()
    }

    inner class ViewHolder(binding: RowAssessmentBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowAssessmentBinding> { assessments = viewModel.getAssessment(position) }
        }
    }
}

