package com.classera.digitallibrary

import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.core.utils.android.getBackgroundColorBasedOn
import com.classera.digitallibrary.databinding.RowDigitalLibraryBinding

/**
 * Project: Classera
 * Created: Dec 23, 2019
 *
 * @author Mohamed Hamdan
 */
class DigitalLibraryAdapter(
    private val viewModel: DigitalLibraryViewModel
) : BasePagingAdapter<DigitalLibraryAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowDigitalLibraryBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getLibrariesCount()
    }

    inner class ViewHolder(binding: RowDigitalLibraryBinding) : BaseBindingViewHolder(binding) {

        private var linearLayoutLike: View? = null
        private var linearLayoutRate: View? = null
        private var buttonOpenInBrowser: Button? = null

        init {
            linearLayoutLike = itemView.findViewById(R.id.linear_layout_row_digital_library_like)
            linearLayoutRate = itemView.findViewById(R.id.linear_layout_row_digital_library_rate)
            buttonOpenInBrowser = itemView.findViewById(R.id.button_row_digital_library_open_in_browser)
        }

        override fun bind(position: Int) {
            linearLayoutLike?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    onItemClicked(it, this)
                }
            }

            linearLayoutRate?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    onItemClicked(it, this)
                }
            }

            buttonOpenInBrowser?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    onItemClicked(it, this)
                }
            }

            bind<RowDigitalLibraryBinding> {
                this.library = viewModel.getLibrary(position)
                this.library?.backgroundColor = getBackgroundColorBasedOn(position)
            }
        }
    }
}
