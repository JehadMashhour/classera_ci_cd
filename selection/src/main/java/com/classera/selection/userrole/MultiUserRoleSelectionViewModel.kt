package com.classera.selection.userrole

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.calendar.teacher.eventusers.UserSelectionRole
import com.classera.data.models.selection.Selectable
import com.classera.data.models.user.User
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.user.UserRepository

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
class MultiUserRoleSelectionViewModel(
    private val userRepository: UserRepository? = null,
    private val url: String? = null
) : BaseViewModel() {

    private var selectables: Array<out Selectable?>? = null
    private var userSelectionRoles: List<UserSelectionRole>? = null

    private var _notifyAdapterLiveData = MutableLiveData<Unit>()
    var notifyAdapterLiveData: LiveData<Unit> = _notifyAdapterLiveData

    fun getItemsCount(): Int {
        return selectables?.size ?: 0
    }

    fun getSelectable(position: Int): Selectable? {
        return selectables?.get(position)
    }

    fun toggleFilter(position: Int) {
        val selectable = selectables?.get(position)
        selectable?.selected = selectable?.selected?.not() == true
    }

    fun getSelectedItems(): Array<out Selectable?>? {
        return selectables?.filter { it?.selected == true }?.toTypedArray()
    }

    fun getUserRoles() = liveData {
        emit(Resource.Loading(show = true))
        val resource =
            tryResource { userRepository!!.getUserRoles(url) }
        if (resource.isSuccess()) {
            val element = resource.element<BaseWrapper<List<UserSelectionRole>>>()
            userSelectionRoles = element?.data
            selectables = userSelectionRoles?.firstOrNull()?.user?.toTypedArray()
            _notifyAdapterLiveData.postValue(Unit)
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getQuickFilterValues(): Pair<Array<String?>, Array<String?>> {
        val entryValues = userSelectionRoles?.map { it.title }?.toTypedArray() ?: arrayOf()
        val entries = userSelectionRoles?.map { it.id }?.toTypedArray() ?: arrayOf()
        return entries to entryValues
    }

    fun onFilterSelected(role: String) {
        selectables = userSelectionRoles?.firstOrNull { it.id == role }?.user?.toTypedArray()
        _notifyAdapterLiveData.postValue(Unit)
    }

    fun getSelectedUsers(): List<User?>? {
        return userSelectionRoles?.flatMap { role -> role.user?.filter { it?.selected == true } ?: listOf() }
    }
}
