package com.classera.selection.userrole

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.user.UserRepository
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
class MultiUserRoleSelectionViewModelFactory @Inject constructor(
    private val userRepository: UserRepository,

    @Url
    private val url: String? = null
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MultiUserRoleSelectionViewModel(userRepository, url) as T
    }
}
