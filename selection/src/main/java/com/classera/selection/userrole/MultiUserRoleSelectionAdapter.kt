package com.classera.selection.userrole

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.selection.databinding.RowMultiSelectionBinding

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
class MultiUserRoleSelectionAdapter(
    private val viewModel: MultiUserRoleSelectionViewModel
) : BaseAdapter<MultiUserRoleSelectionAdapter.ViewHolder>() {

    init {
        disableAnimations()
    }

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowMultiSelectionBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getItemsCount()
    }

    inner class ViewHolder(binding: RowMultiSelectionBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowMultiSelectionBinding> { selectable = viewModel.getSelectable(position) }
            itemView.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    viewModel.toggleFilter(clickedPosition)
                    notifyDataSetChanged()
                }
            }
        }
    }
}
