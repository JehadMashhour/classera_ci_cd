package com.classera.switchroles

import android.view.ViewGroup
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.switchroles.databinding.RowSwitchRolesBinding

class SwitchRolesAdapter(private val viewModel: SwitchRolesViewModel) :
    BasePagingAdapter<SwitchRolesAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowSwitchRolesBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getSwitchRolesListCount()
    }

    inner class ViewHolder(binding: RowSwitchRolesBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowSwitchRolesBinding> {
                switchRolesItem = viewModel.getRole(position)
                currentRole = viewModel.getCurrentRole()
            }
        }
    }
}
