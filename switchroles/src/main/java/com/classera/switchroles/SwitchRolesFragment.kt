package com.classera.switchroles

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.ProgressBar
import android.widget.SearchView
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.onDebounceQueryTextChange
import com.classera.data.network.errorhandling.Resource
import javax.inject.Inject


@Screen("Switch Roles")
class SwitchRolesFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: SwitchRolesViewModel

    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var adapter: SwitchRolesAdapter? = null
    private var errorView: ErrorView? = null
    private var searchView: SearchView? = null

    override val layoutId: Int = R.layout.fragment_switch_roles

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_switch_roles)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_switch_roles)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_switch_roles)
        errorView = view?.findViewById(R.id.error_view_fragment_switch_roles)
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            adapter?.resetPaging()
            getRolesList()
        }
        getRolesList()
    }

    private fun getRolesList() {
        viewModel.getRolesList().observe(this, this::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        if (adapter == null) {
            initAdapter()
        } else {
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()
    }

    private fun initAdapter() {
        adapter = SwitchRolesAdapter(viewModel)
        adapter?.setOnItemClickListener { _, position ->
            val roleId = viewModel.getRole(position).id
            roleId?.let { viewModel.generateNewAuth(it).observe(this, this::handleAuthResponse) }
           
        }
        recyclerView?.adapter = adapter
    }


    private fun handleAuthResponse(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                val i =
                    activity?.baseContext?.packageName?.let {
                        activity?.baseContext?.packageManager?.getLaunchIntentForPackage(
                            it
                        )
                    }
                  i?.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                  startActivity(i)


                handleGenerateNewSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleGenerateNewSuccessResource() {

        //update the AuthToken and refresh the APP.

    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getRolesList() }
        adapter?.finishLoading()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_switch_roles, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val searchMenuItem = menu.findItem(R.id.item_menu_fragment_switch_roles_search)
        searchView = (searchMenuItem.actionView as? SearchView?)
        searchView?.onDebounceQueryTextChange {
            viewModel.currentSearchValue = it.toString().trim()
            viewModel.getFilteredRolesList()
            handleSuccessResource()
        }

        searchView?.setOnCloseListener {
            getRolesList()
            return@setOnCloseListener false
        }
    }

    override fun onDestroyView() {
        progressBar = null
        recyclerView = null
        adapter = null
        errorView = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }
}
