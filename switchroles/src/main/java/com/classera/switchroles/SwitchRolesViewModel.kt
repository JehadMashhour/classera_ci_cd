package com.classera.switchroles

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.switchroles.Role
import com.classera.data.models.switchschools.School
import com.classera.data.models.user.UserRole
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.switchroles.SwitchRolesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SwitchRolesViewModel(
    private val switchRolesRepository: SwitchRolesRepository,
    private val prefs: Prefs
) : BaseViewModel() {

    private var switchRolesList: MutableList<Role> = mutableListOf()
    var currentSearchValue: String = ""
    private var filteredRolesList: List<Role> = listOf()

    fun getRolesList() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource =
            tryResource { switchRolesRepository.getSwitchRolesList() }
        switchRolesList.clear()
        switchRolesList.addAll(
            resource.element<BaseWrapper<List<Role>>>()?.data ?: mutableListOf()
        )
        getFilteredRolesList()
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getRole(position: Int): Role {
        return filteredRolesList[position]
    }

    fun getSwitchRolesListCount(): Int {
        return filteredRolesList.size
    }

    fun generateNewAuth(role: UserRole): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            switchRolesRepository.generateNewAuth(role)
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getCurrentRole(): UserRole? {
        return prefs.userRole
    }

    fun getFilteredRolesList() {
        viewModelScope.launch {
            filteredRolesList = if (currentSearchValue.isEmpty())
                switchRolesList.filter {
                    it.title?:"" == it.title
                }
            else {
                switchRolesList.filter {
                    it.title?.contains(currentSearchValue, true)?: false
                }
            }

        }
    }
}
