package com.classera.switchroles

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.switchroles.SwitchRolesRepository
import javax.inject.Inject

class SwitchRolesViewModelFactory @Inject constructor(
    private val switchRolesRepository: SwitchRolesRepository,
    private val prefs: Prefs
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SwitchRolesViewModel(switchRolesRepository, prefs) as T
    }
}
