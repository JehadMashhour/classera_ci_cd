package com.calssera.vcr.teacherlist

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class TeacherListModule {

    @Module
    companion object {


        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: TeacherViewModelFactory
        ): TeacherListViewModel {
            return ViewModelProvider(fragment, factory)[TeacherListViewModel::class.java]
        }


        @Provides
        @JvmStatic
        fun provideTeacherListFragmentArgs(fragment: Fragment): TeacherListFragmentArgs{
            val args by fragment.navArgs<TeacherListFragmentArgs>()
            return args
        }


        @Provides
        @JvmStatic
        fun provideTeacherListAdapter(teacherListViewModel: TeacherListViewModel): TeacherListAdapter {
            return TeacherListAdapter(teacherListViewModel)
        }
    }

    @Binds
    abstract fun bindFragment(fragment: TeacherListFragment): Fragment

}
