package com.calssera.vcr.add

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.calssera.vcr.R
import com.calssera.vcr.databinding.FragmentAddVcrBinding
import com.calssera.vcr.lecturefragment.LectureFragment
import com.classera.core.Screen
import com.classera.core.fragments.BaseValidationFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.vcr.Lecture
import com.classera.data.models.vcr.sharewith.ShareWithStatus
import com.classera.data.models.vcr.vendor.Duration
import com.classera.data.network.errorhandling.Resource
import com.classera.utils.views.dialogs.datepickerbottomdialog.DateBottomSheetFragment
import com.classera.utils.views.dialogs.listbottomdialog.ListBottomSheetFragment
import com.classera.utils.views.dialogs.messagebottomdialog.MessageBottomSheetFragment
import com.classera.utils.views.dialogs.timepickerbottomdialog.TimeBottomSheetFragment
import com.google.android.material.textfield.TextInputEditText
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import java.util.*
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
@Screen("Add Vcr")
class AddVcrFragment : BaseValidationFragment() {

    @Inject
    lateinit var viewModel: AddVcrViewModel

    private val args: AddVcrFragmentArgs by navArgs()

    @NotEmpty(message = "validation_fragment_add_vcr_title")
    private var editTextTitle: TextInputEditText? = null

    @NotEmpty(message = "validation_fragment_add_vcr_external_link")
    private var editTextExternalLink: TextInputEditText? = null

    @NotEmpty(message = "validation_fragment_add_vcr_zoom_link")
    private var editTextZoomLink: TextInputEditText? = null

    private var editTextPasscode: TextInputEditText? = null

    @NotEmpty(message = "validation_fragment_add_vcr_lectures")
    private var lecturesTextView: TextView? = null

    @NotEmpty(message = "validation_fragment_add_vcr_starting_date")
    private var dateTextView: TextView? = null

    @NotEmpty(message = "validation_fragment_add_vcr_starting_time")
    private var timeTextView: TextView? = null

    @NotEmpty(message = "validation_fragment_add_vcr_duration")
    private var durationTextView: TextView? = null

    @NotEmpty(message = "validation_fragment_add_vcr_sharing_status")
    private var shareWithTextView: TextView? = null

    @NotEmpty(message = "validation_fragment_add_vcr_repeat_until")
    private var repeatUntilTextView: TextView? = null

    private var buttonApply: Button? = null
    private var progressBarApply: ProgressBar? = null


    override val layoutId: Int = R.layout.fragment_add_vcr

    override fun isBindingEnabled(): Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        showTitle()
        bindData()
    }

    private fun findViews() {
        editTextTitle = view?.findViewById(R.id.fragment_add_vcr_input_edit_text_title)
        editTextExternalLink =
            view?.findViewById(R.id.fragment_add_vcr_input_edit_text_externalLink)
        editTextZoomLink = view?.findViewById(R.id.fragment_add_vcr_input_edit_text_zoomLink)
        editTextPasscode = view?.findViewById(R.id.fragment_add_vcr_input_edit_text_passcode)
        lecturesTextView = view?.findViewById(R.id.fragment_add_vcr_txt_view_lecture_value)
        dateTextView = view?.findViewById(R.id.fragment_add_vcr_txt_view_date_value)
        timeTextView = view?.findViewById(R.id.fragment_add_vcr_txt_view_time_value)
        durationTextView = view?.findViewById(R.id.fragment_add_vcr_txt_view_duration_value)
        shareWithTextView = view?.findViewById(R.id.fragment_add_vcr_txt_view_shareWith_value)
        repeatUntilTextView = view?.findViewById(R.id.fragment_add_vcr_txt_view_repeatUntil_value)
        buttonApply = view?.findViewById(R.id.fragment_add_vcr_button_apply)
        progressBarApply = view?.findViewById(R.id.fragment_add_vcr_progress_bar_apply)
    }


    private fun showTitle() {
        if (args.vcr != null) {
            (activity as AppCompatActivity).supportActionBar?.title = args.vcr?.title
        } else {
            (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.create_vcr)
        }
    }

    private fun bindData() {
        bind<FragmentAddVcrBinding> {
            this?.addVcrFragment = this@AddVcrFragment
            this?.viewModel = viewModel
        }
    }

    private fun handleApplyResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleApplyLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleApplySuccessResource(resource as Resource.Success<NoContentBaseWrapper>)
            }
            is Resource.Error -> {
                handleApplyErrorResource(resource)
            }
        }
    }

    private fun handleApplyLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBarApply?.visibility = View.VISIBLE
            buttonApply?.text = ""
            buttonApply?.isEnabled = false
        } else {
            progressBarApply?.visibility = View.GONE
            buttonApply?.setText(R.string.button_fragment_add_vcr_save)
            buttonApply?.isEnabled = true
        }
    }

    private fun handleApplySuccessResource(resource: Resource.Success<NoContentBaseWrapper>) {
        MessageBottomSheetFragment.show(
            fragment = this,
            image = R.drawable.ic_info,
            title = getString(R.string.message_fragment_vcr_success_message),
            message = resource.data?.message,
            buttonLeftText = getString(R.string.close)
        ) {
            findNavController().popBackStack()
        }
    }

    private fun handleApplyErrorResource(resource: Resource.Error) {
        MessageBottomSheetFragment.show(
            fragment = this,
            image = R.drawable.ic_info,
            message = resource.error.message ?: resource.error.cause?.message,
            buttonLeftText = getString(R.string.close)
        )
    }


    fun onTitleTextChanged(text: CharSequence, start: Int, before: Int, count: Int) {
        viewModel.vcrResponse?.setTitleValue(text.toString())
    }

    fun onExternalLinkTextChanged(text: CharSequence, start: Int, before: Int, count: Int) {
        viewModel.vcrResponse?.setExternalLinkValue(text.toString())
    }

    fun onZoomLinkTextChanged(text: CharSequence, start: Int, before: Int, count: Int) {
        viewModel.vcrResponse?.setZoomLinkValue(text.toString())
    }

    fun onPasscodeTextChanged(text: CharSequence, start: Int, before: Int, count: Int) {
        viewModel.vcrResponse?.setPasscodeValue(text.toString())
    }

    fun on5MinutesBeforeVcrStartChanged(checked: Boolean) {
        viewModel.vcrResponse?.setAllowStudentStartVcrValue(checked)
    }

    fun onWeeklyRepeatCheckedChanged(checked: Boolean) {
        viewModel.vcrResponse?.setWeeklyRepeatValue(checked)
    }

    fun onRecurringChanged(checked: Boolean) {
        viewModel.vcrResponse?.setRecurringValue(checked)
    }

    fun onLecturesClicked() {
        LectureFragment.show(
            fragment = this,
            selectedLectures = viewModel.vcrResponse?.lectures?.toMutableList()
        ) { _: String, lectures: Array<Lecture?> ->
            viewModel.vcrResponse?.setSelectedLectures(
                lectures.toList(), getString(
                    R.string.selected
                )
            )
        }

    }

    fun onDateClicked() {
        DateBottomSheetFragment.show(
            fragment = this,
            title = getString(R.string.select_date),
            selectedDate = viewModel.vcrResponse?.dateValue,
            minimumDate = Date()
        ) { _, date ->
            viewModel.vcrResponse?.setDate(date)
        }
    }

    fun onTimeClicked() {
        TimeBottomSheetFragment.show(
            fragment = this,
            title = getString(R.string.select_time),
            selectedTime = viewModel.vcrResponse?.timeValue
        ) { _, time ->

            viewModel.vcrResponse?.setTime(time)
        }
    }

    fun onDurationClicked() {
        viewModel.vcrResponse?.getDurationList(getString(R.string.minute))?.let {
            ListBottomSheetFragment.show<Duration>(
                fragment = this,
                title = getString(R.string.hint_fragment_edit_vcr_duration),
                selectableList = it.toTypedArray(),
                selectedId = viewModel.vcrResponse?.duration
            ) { _, duration ->
                viewModel.vcrResponse?.setDuration(duration, getString(R.string.minute))
            }
        }
    }

    fun onShareWithClicked() {
        ListBottomSheetFragment.show<ShareWithStatus>(
            fragment = this,
            title = getString(R.string.hint_fragment_edit_vcr_share_with),
            selectableList = viewModel.getShareWithList().toTypedArray(),
            selectedId = viewModel.vcrResponse?.sharingStatus?.id.toString()
        ) { _, shareWithStatus ->
            viewModel.vcrResponse?.setShareWithText(
                requireContext(),
                shareWithStatus.shareWithStatusTypes
            )
        }
    }


    fun onRepeatUntilClicked() {
        DateBottomSheetFragment.show(
            fragment = this,
            title = getString(R.string.select_date),
            selectedDate = viewModel.vcrResponse?.repeatUntil,
            minimumDate = Date()
        ) { _, date ->
            viewModel.vcrResponse?.setRepeatUntilText(date)
        }
    }


    fun onApplyButtonClicked() {
        if (editTextTitle?.text.toString().trim().isEmpty()) {
            editTextTitle?.error = "Cannot be empty"
        } else {
            validator.validate()
        }
    }

    override fun onValidationSucceeded() {
        viewModel.vcrResponse?.let {
            viewModel.onApplyClicked().observe(viewLifecycleOwner, ::handleApplyResource)
        }

    }

    private companion object {

        private const val ALLOW_START = "1"
        private const val DO_NOT_ALLOW_START = "0"
    }
}
