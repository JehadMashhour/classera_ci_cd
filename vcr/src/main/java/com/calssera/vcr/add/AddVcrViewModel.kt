package com.calssera.vcr.add

import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.calssera.vcr.R
import com.classera.core.BaseViewModel
import com.classera.core.utils.toInt
import com.classera.data.models.BaseWrapper
import com.classera.data.models.vcr.sharewith.ShareWithStatus
import com.classera.data.models.vcr.Lecture
import com.classera.data.models.vcr.sharewith.ShareWithStatusTypes
import com.classera.data.models.vcr.VcrResponse
import com.classera.data.models.vcr.vendor.Duration
import com.classera.data.models.vcr.vendor.VcrStatusWrapper
import com.classera.data.models.vcr.vendor.Vendor
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.vcr.VcrRepository
import com.classera.data.toDate
import com.classera.data.toString
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.reflect.Field
import java.util.*

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
@Suppress("LongParameterList")
class AddVcrViewModel(
    private val fragment: Fragment,
    private val vcrRepository: VcrRepository,
    private val addVcrFragmentArgs: AddVcrFragmentArgs
) : BaseViewModel() {

    var vcrResponse: VcrResponse? = addVcrFragmentArgs.vcr?.apply {
        vendor?.vcrStatusWrapper?.settings?.weeklyRepeated = false
        vendor?.vcrStatusWrapper?.settings?.recurring = false
    } ?: VcrResponse().apply {
        this.vendor = addVcrFragmentArgs.vendor
        this.teacher = addVcrFragmentArgs.teacher
    }
        private set


    fun getShareWithList() = ShareWithStatusTypes.getList(fragment.context)

    init {
        initData()
    }


    private fun initData() {
        vcrResponse?.setSelectedLectures(suffix = fragment.getString(R.string.selected))
        vcrResponse?.setDuration(suffix = fragment.getString(R.string.minute))
        vcrResponse?.setVendorLink()
        vcrResponse?.setDate()
        vcrResponse?.setTime()
        vcrResponse?.setShareWithText(fragment.requireContext())
        vcrResponse?.setRepeatUntilText()
        vcrResponse?.setAllowStudentStartVcrValue()
        vcrResponse?.setWeeklyRepeatValue()
        vcrResponse?.setRecurringValue()
    }

    fun onApplyClicked() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = if (addVcrFragmentArgs.vcr == null) {
            startAddingVcr()
        } else {
            startEditingVcr()
        }

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private suspend fun startAddingVcr(): Resource =
        tryNoContentResource {
            vcrRepository.addVcr(
                vendor = vcrResponse?.vendor?.id,
                teacherId = vcrResponse?.teacher?.id,
                title = vcrResponse?.title,
                zoomLink = vcrResponse?.zoomLink,
                externalLink = vcrResponse?.externalLink,
                passcode = vcrResponse?.passcode,
                lectures = vcrResponse?.lectures,
                startingTime = "${vcrResponse?.dateValue.toString("yyyy-MM-dd")} ${
                    this.vcrResponse?.timeValue.toString(
                        "HH:mm"
                    )
                }",
                duration = vcrResponse?.duration,
                sharingStatus = vcrResponse?.sharingStatus?.id.toString(),
                allowStart = vcrResponse?.allowStudentStartVcr?.let { it.toInt().toString() },
                weeklyRepeat = vcrResponse?.weeklyRepeat?.let { it.toInt().toString() },
                repeatUntil = vcrResponse?.repeatUntil?.toString("yyyy-MM-dd"),
                recurring = vcrResponse?.recurring?.let { it.toInt().toString() }
            )
        }

    private suspend fun startEditingVcr(): Resource =
        tryNoContentResource {
            vcrRepository.editVcr(
                id = vcrResponse?.id,
                vendor = vcrResponse?.vendor?.id,
                title = vcrResponse?.title,
                zoomLink = vcrResponse?.zoomLink,
                externalLink = vcrResponse?.externalLink,
                passcode = vcrResponse?.passcode,
                lectures = vcrResponse?.lectures,
                startingTime = "${vcrResponse?.dateValue.toString("yyyy-MM-dd")} ${
                    this.vcrResponse?.timeValue.toString(
                        "HH:mm"
                    )
                }",
                duration = vcrResponse?.duration,
                sharingStatus = vcrResponse?.sharingStatus?.id.toString(),
                allowStart = vcrResponse?.allowStudentStartVcr?.let { it.toInt().toString() },
                weeklyRepeat = vcrResponse?.weeklyRepeat?.let { it.toInt().toString() },
                repeatUntil = vcrResponse?.repeatUntil?.toString("yyyy-MM-dd"),
                recurring = vcrResponse?.recurring?.let { it.toInt().toString() }
            )
        }


}
