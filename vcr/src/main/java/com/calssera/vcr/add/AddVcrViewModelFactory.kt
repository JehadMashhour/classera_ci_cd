package com.calssera.vcr.add

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.lectures.LecturesRepository
import com.classera.data.repositories.vcr.VcrRepository
import javax.inject.Inject


/**
 * Created by Rawan Al-Theeb on 12/19/2019.
 * Classera
 * r.altheeb@classera.com
 */
class AddVcrViewModelFactory @Inject constructor(
    private val fragment: Fragment,
    private val vcrRepository: VcrRepository,
    private val addVcrFragmentArgs: AddVcrFragmentArgs
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AddVcrViewModel(fragment,vcrRepository, addVcrFragmentArgs) as T
    }
}
