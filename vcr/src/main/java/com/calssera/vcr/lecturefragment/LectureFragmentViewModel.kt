package com.calssera.vcr.lecturefragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseViewModel
import com.classera.core.utils.android.notifyChanged
import com.classera.data.models.BaseWrapper
import com.classera.data.models.vcr.Lecture
import com.classera.data.models.vcr.lecturetimeslotpreparation.LectureTimeslotPreparation
import com.classera.data.models.vcr.lecturetimeslotpreparation.Preparation
import com.classera.data.models.vcr.lecturetimeslotpreparation.Timeslot
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.lectures.LecturesRepository
import com.classera.data.repositories.vcr.VcrRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LectureFragmentViewModel(
    private val vcrRepository: VcrRepository,
    private val lecturesRepository: LecturesRepository,
    private val lectureFragmentArgs: LectureFragmentArgs
) : BaseViewModel() {

    private val _lecturesList = MutableLiveData<MutableList<Lecture>>()
    val lecturesList: LiveData<MutableList<Lecture>> = _lecturesList

    private var _timeslots = MutableLiveData<Map<Int, List<Timeslot>?>>()
    val timeslots: LiveData<Map<Int, List<Timeslot>?>> =
        _timeslots


    private var _preparations = MutableLiveData<Map<Int, List<Preparation>?>>()
    val preparations: LiveData<Map<Int, List<Preparation>?>> =
        _preparations


    private val _changedPosition = MutableLiveData<Int>()
    val changedPosition: LiveData<Int> = _changedPosition


    fun getLectures() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource =
            tryResource { lecturesRepository.getLectures() }
        _lecturesList.postValue(selectLectures(resource.element<BaseWrapper<List<Lecture>>>()?.data?.toMutableList()))

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private fun selectLectures(loadedList: MutableList<Lecture>?): MutableList<Lecture>? {
        loadedList?.forEachIndexed { index, lecture ->
            lectureFragmentArgs.selectedLectures?.firstOrNull { it.id == lecture.id }?.let {
                loadedList[index] = it
                loadedList[index].selected = true
            }
        }
        return loadedList
    }

    private suspend fun getTimeslots(position: Int) {
        val resource =
            tryResource { vcrRepository.getLectureTimeSlotsPreparations(listOf(getLecture(position)?.id)) }

        var lectureTimeslotPreparationList =
            resource.element<BaseWrapper<List<LectureTimeslotPreparation>>>()?.data

        lectureTimeslotPreparationList.isNullOrEmpty().run {
            if (this) {
                lectureTimeslotPreparationList = mutableListOf(LectureTimeslotPreparation())
            }
        }

        _timeslots.postValue(
            mapOf(
                position to (lectureTimeslotPreparationList?.get(0)?.timeslots ?: mutableListOf())
            )
        )

    }


    private suspend fun getPreparations(position: Int) {
        val resource =
            tryResource { vcrRepository.getLectureTimeSlotsPreparations(listOf(getLecture(position)?.id)) }


        var lectureTimeslotPreparationList =
            resource.element<BaseWrapper<List<LectureTimeslotPreparation>>>()?.data

        lectureTimeslotPreparationList.isNullOrEmpty().run {
            if (this) {
                lectureTimeslotPreparationList = mutableListOf(LectureTimeslotPreparation())
            }
        }


        _preparations.postValue(
            mapOf(
                position to (lectureTimeslotPreparationList?.get(0)?.preparations
                    ?: mutableListOf())
            )
        )

    }


    fun getItemsCount(): Int {
        return _lecturesList.value?.size ?: 0
    }

    fun getLecture(position: Int): Lecture? {
        return _lecturesList.value?.get(position)
    }

    fun getSelectedLectures(): Array<Lecture?>? {
        return _lecturesList.value?.filter {
            it.selected
        }?.map {
            it.apply { it.expanded = false }
        }?.toTypedArray()
    }

    fun toggleSelected(position: Int) {
        val selectable = getLecture(position)
        selectable?.apply {
            selected = selected.not()
            timeslot = if (timeslot != null) null else timeslot
            preparation = if (preparation != null) null else preparation
        }
        _changedPosition.value = position
        this._lecturesList.notifyChanged()
    }

    fun selectAll() {
        this._lecturesList.value?.forEachIndexed { index, lecture ->
            lecture.selected = true
            _changedPosition.value = index
        }
        this._lecturesList.notifyChanged()
    }

    fun unSelectAll() {
        this._lecturesList.value?.forEachIndexed { index, lecture ->
            lecture.selected = false
            lecture.timeslot = if (lecture.timeslot != null) null else lecture.timeslot
            lecture.preparation = if (lecture.preparation != null) null else lecture.preparation
            _changedPosition.value = index
        }
        this._lecturesList.notifyChanged()
    }


    fun toggleExpanded(position: Int) {
        val selectable = getLecture(position)
        selectable?.apply {
            expanded = expanded.not()
        }
        _changedPosition.value = position
    }

    fun onNewTimeSlotSelected(position: Int, timeslot: Timeslot) {
        getLecture(position)?.timeslot = timeslot
        _changedPosition.postValue(position)
    }

    fun onNewPreparationSelected(position: Int, preparation: Preparation) {
        getLecture(position)?.preparation = preparation
        _changedPosition.postValue(position)
    }

    fun onTimeSlotViewClicked(position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            getTimeslots(position)
        }
    }

    fun onPreparationViewClicked(position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            getPreparations(position)
        }

    }


}
