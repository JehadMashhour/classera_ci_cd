package com.calssera.vcr.lecturefragment

import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import com.calssera.vcr.R
import com.calssera.vcr.add.AddVcrFragmentDirections
import com.calssera.vcr.databinding.FragmentLectureBinding
import com.classera.utils.views.dialogs.listbottomdialog.ListBottomSheetFragment
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseBindingFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.vcr.Lecture
import com.classera.data.models.vcr.lecturetimeslotpreparation.Preparation
import com.classera.data.models.vcr.lecturetimeslotpreparation.Timeslot
import com.classera.data.network.errorhandling.Resource
import javax.inject.Inject


class LectureFragment : BaseBindingFragment(), LectureHandlers {

    override val layoutId: Int = R.layout.fragment_lecture

    @Inject
    lateinit var lectureFragmentViewModel: LectureFragmentViewModel

    @Inject
    lateinit var lectureFragmentAdapter: LectureFragmentAdapter

    private val args by navArgs<LectureFragmentArgs>()

    private lateinit var recyclerView: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var errorView: ErrorView
    private lateinit var parentListConstraintLayout: ConstraintLayout
    private lateinit var selectAllCheckBox: CheckBox
    private lateinit var selectAllTextView: TextView
    private lateinit var constraintLayoutSelectAll: ConstraintLayout

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findViews()
        bindData()
        setTitle()
        setupRecyclerView()
        initChangedPositionListener()
        initLecturesListener()
        initTimeslotsListeners()
        initPreparationsListeners()
        getLectures()

    }

    private fun findViews() {
        view?.run {
            recyclerView = findViewById(R.id.fragment_lecture_recycler_view)
            progressBar = findViewById(R.id.fragment_lecture_progressBar)
            errorView = findViewById(R.id.fragment_lecture_error_view)
            parentListConstraintLayout =
                findViewById(R.id.fragment_lecture_constraintLayout_parent_list)
            selectAllCheckBox = findViewById(R.id.fragment_lecture_checkbox_select_all)
            selectAllTextView = findViewById(R.id.fragment_lecture_txtView_select_all)
            constraintLayoutSelectAll =
                findViewById(R.id.fragment_lecture_constraintLayout_select_all)
        }
    }

    private fun bindData() {
        bind<FragmentLectureBinding>
        {
            this?.lectureHandlers = this@LectureFragment
        }
    }

    private fun setTitle() {
        (activity as AppCompatActivity).supportActionBar?.title =
            getString(R.string.hint_fragment_edit_vcr_lectures)
    }

    private fun setupRecyclerView() {
        recyclerView.adapter = lectureFragmentAdapter
    }


    private fun initChangedPositionListener() {
        lectureFragmentViewModel.changedPosition.observe(this) {
            lectureFragmentAdapter.notifyDataSetChanged()
        }
    }

    private fun initTimeslotsListeners() {
        lectureFragmentViewModel.timeslots.observe(this) { map ->
            val entry: Map.Entry<Int, List<Timeslot>?> = map.entries.iterator().next()
            ListBottomSheetFragment.show<Timeslot>(
                this,
                entry.key.toString(),
                getString(R.string.choose_time_slot),
                entry.value!!.toTypedArray(),
                lectureFragmentViewModel.getLecture(entry.key)?.timeslot?.id
            ) { position, t ->
                lectureFragmentViewModel.onNewTimeSlotSelected(position.toInt(), t)
            }
        }
    }

    private fun initPreparationsListeners() {
        lectureFragmentViewModel.preparations.observe(this) { map ->
            val entry: Map.Entry<Int, List<Preparation>?> = map.entries.iterator().next()
            ListBottomSheetFragment.show<Preparation>(
                this,
                entry.key.toString(),
                getString(R.string.choose_preparation),
                entry.value!!.toTypedArray(),
                lectureFragmentViewModel.getLecture(entry.key)?.preparation?.id
            ) { position, t ->
                lectureFragmentViewModel.onNewPreparationSelected(position.toInt(), t)
            }
        }
    }

    private fun getLectures() {
        lectureFragmentViewModel.getLectures().observe(this) { resource ->
            when (resource) {
                is Resource.Loading -> {
                    handleLoadingResource(resource)
                }
                is Resource.Success<*> -> {
                    errorView.visibility = View.GONE
                    lectureFragmentAdapter.notifyDataSetChanged()
                }

                is Resource.Error -> {
                    handleErrorResource(resource)
                }
            }
        }
    }


    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar.visibility = View.VISIBLE
            parentListConstraintLayout.visibility = View.GONE
        } else {
            progressBar.visibility = View.GONE
            parentListConstraintLayout.visibility = View.VISIBLE
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView.visibility = View.VISIBLE
        errorView.setError(resource)
        errorView.setOnRetryClickListener { getLectures() }
    }

    private fun initLecturesListener() {
        lectureFragmentViewModel.lecturesList.observe(viewLifecycleOwner)
        { it ->
            if (it.size > 0) {
                val isAllChecked = it.all { it.selected }
                if (isAllChecked) {
                    constraintLayoutSelectAll.tag = 1
                    selectAllTextView.text = getString(R.string.deselect_all)
                } else {
                    constraintLayoutSelectAll.tag = 0
                    selectAllTextView.text = getString(R.string.select_all)
                }
                selectAllCheckBox.isChecked = isAllChecked

            }
        }
    }

    override fun onApplyButtonClicked() {
        setFragmentResult(
            args.key,
            bundleOf(DATA to lectureFragmentViewModel.getSelectedLectures())
        )
        findNavController().navigateUp()
    }

    companion object {
        const val DATA = "data"
        const val LECTURES_FRAGMENT_REQUEST_KEY = "lectures_fragment_request_key"

        @Suppress("UNCHECKED_CAST")
        inline fun show(
            fragment: Fragment,
            selectedLectures: MutableList<Lecture?>? = null,
            crossinline listener: ((requestKey: String, t: Array<Lecture?>) -> Unit)
        ) {
            fragment.setFragmentResultListener(LECTURES_FRAGMENT_REQUEST_KEY) { key, bundle ->
                listener(key, bundle.get(ListBottomSheetFragment.DATA) as Array<Lecture?>)
            }

            fragment.findNavController().navigate(
                AddVcrFragmentDirections.lecturesActionDirection(
                    LECTURES_FRAGMENT_REQUEST_KEY
                ).apply {
                    this.selectedLectures = selectedLectures?.filterNotNull()?.toTypedArray()
                }
            )
        }
    }

    override fun onSelectAllClicked(view: View) {

        when (view.tag == null || view.tag == 0) {
            true -> {
                lectureFragmentViewModel.selectAll()
            }
            false -> {
                lectureFragmentViewModel.unSelectAll()
            }
        }
    }
}
