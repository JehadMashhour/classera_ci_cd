package com.calssera.vcr.student

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.calssera.vcr.VcrViewModelFactory
import com.classera.data.prefs.Prefs
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
@Module
abstract class VcrStudentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: VcrViewModelFactory
        ): VcrStudentViewModel {
            return ViewModelProvider(fragment, factory)[VcrStudentViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideVcrAdapter(
            viewModel: VcrStudentViewModel,
            prefs: Prefs
        ): VcrStudentAdapter {
            return VcrStudentAdapter(viewModel,prefs)
        }
    }

    @Binds
    abstract fun bindFragment(fragment: VcrStudentFragment): Fragment
}
