package com.calssera.vcr.vendorbottomsheet

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.calssera.vcr.R
import com.calssera.vcr.teacher.VcrTeacherFragmentDirections
import com.classera.utils.views.dialogs.messagebottomdialog.MessageBottomSheetFragment
import com.classera.core.fragments.BaseBottomSheetDialogFragment
import com.classera.core.utils.android.getStringOrElse
import com.classera.data.models.BaseWrapper
import com.classera.data.models.vcr.vendor.VcrStatusWrapper
import com.classera.data.network.errorhandling.Resource
import javax.inject.Inject

class VendorBottomSheetFragment : BaseBottomSheetDialogFragment() {

    @Inject
    lateinit var viewModel: VendorBottomSheetViewModel

    @Inject
    lateinit var vendorBottomSheetAdapter: VendorBottomSheetAdapter

    private val args by navArgs<VendorBottomSheetFragmentArgs>()

    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null

    override val layoutId: Int = R.layout.fragment_vendor_bottom_sheet

    override fun enableDependencyInjection(): Boolean {
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findViews()
        setupRecyclerView()
        getVendors()
    }

    private fun findViews() {
        recyclerView = view?.findViewById(R.id.fragment_add_vcr_recycler_view)
        progressBar = view?.findViewById(R.id.fragment_add_vcr_progress_bar)
    }


    private fun setupRecyclerView() {
        recyclerView?.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
        recyclerView?.adapter = vendorBottomSheetAdapter
        vendorBottomSheetAdapter.setOnItemClickListener { view, position ->
            handleItemClicked(view, position)
        }
    }

    private fun handleItemClicked(view: View, position: Int) {
        viewModel.getVcrStatus(viewModel.getVendor(position).id?.toInt())
            .observe(viewLifecycleOwner, Observer { handleVcrStatusResource(it, position) })
    }


    private fun getVendors() {
        viewModel.getVendors().observe(viewLifecycleOwner, this::handleVendorResource)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleVendorResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleVendorLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleVendorSuccessResource()
            }
            is Resource.Error -> {
                handleVendorErrorResource(resource)
            }
        }
    }

    private fun handleVendorLoadingResource(resource: Resource.Loading) {
        progressBar?.visibility = if (resource.show) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    private fun handleVendorSuccessResource() {
        recyclerView?.adapter?.notifyDataSetChanged()
    }

    private fun handleVendorErrorResource(resource: Resource.Error) {
        val message =
            context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }


    @Suppress("UNCHECKED_CAST")
    private fun handleVcrStatusResource(resource: Resource, vendorPosition: Int) {
        when (resource) {
            is Resource.Loading -> {
                handleVcrStatusLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleVcrStatusSuccessResource(
                    resource as Resource.Success<BaseWrapper<VcrStatusWrapper>>,
                    vendorPosition
                )
            }
            is Resource.Error -> {
                handleVcrStatusErrorResource(resource)
            }
        }
    }

    private fun handleVcrStatusLoadingResource(resource: Resource.Loading) {
        progressBar?.visibility = if (resource.show) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    private fun handleVcrStatusSuccessResource(
        success: Resource.Success<BaseWrapper<VcrStatusWrapper>>,
        vendorPosition: Int
    ) {
        success.data?.data?.canProceed?.let {
            if (it) {
                navigateToAddVcr(success, vendorPosition)
            } else {
                showMessageBottomSheetFragment(success)
            }
        }
    }

    private fun navigateToAddVcr(
        success: Resource.Success<BaseWrapper<VcrStatusWrapper>>,
        vendorPosition: Int
    ) {
        val direction = VcrTeacherFragmentDirections.addVcrDirection(
            getString(R.string.create_vcr)
        ).apply {
            vendor = viewModel.getVendor(vendorPosition)
                .apply { vcrStatusWrapper = success.data?.data }
            teacher = args.teacher
        }
        findNavController().navigate(direction)
    }

    private fun showMessageBottomSheetFragment(success: Resource.Success<BaseWrapper<VcrStatusWrapper>>) {
        MessageBottomSheetFragment.show(
            fragment = this,
            image = R.drawable.ic_info,

            message = success.data?.data?.message,
            buttonLeftText = getString(R.string.close)
        )
    }

    private fun handleVcrStatusErrorResource(resource: Resource.Error) {
        val message =
            context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)

        MessageBottomSheetFragment.show(
            fragment = this,
            image = R.drawable.ic_info,

            message = message,
            buttonLeftText = getString(R.string.close)
        )
    }

}
