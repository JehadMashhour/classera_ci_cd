package com.calssera.vcr.vendorbottomsheet

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.vcr.vendor.VendorResponse
import com.classera.data.models.vcr.vendor.Vendor
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.vcr.VcrRepository
import kotlinx.coroutines.Dispatchers

class VendorBottomSheetViewModel(private val vcrRepository: VcrRepository) : BaseViewModel() {
    private var vendors: MutableList<Vendor> = mutableListOf()

    fun getVcrStatus(vendor: Int?): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource =
            tryResource { vcrRepository.getVcrStatus(vendor) }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getVendors(): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource { vcrRepository.getVendors() }
        vendors.addAll(
            resource.element<BaseWrapper<VendorResponse>>()?.data?.vendors ?: mutableListOf()
        )
        emit(resource)
        emit(Resource.Loading(show = false))
    }


    fun getVendor(position: Int): Vendor {
        return vendors[position]
    }

    fun getVendorsCount(): Int {
        return vendors.size
    }
}
