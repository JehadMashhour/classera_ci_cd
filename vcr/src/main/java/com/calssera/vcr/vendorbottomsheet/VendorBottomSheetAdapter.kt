package com.calssera.vcr.vendorbottomsheet

import android.view.View
import android.view.ViewGroup
import com.calssera.vcr.databinding.RowVendorBinding
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.data.models.vcr.VendorTypes

class VendorBottomSheetAdapter(private val vendorBottomSheetViewModel: VendorBottomSheetViewModel) :
    BaseAdapter<VendorBottomSheetAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowVendorBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return vendorBottomSheetViewModel.getVendorsCount()
    }

    inner class ViewHolder(binding: RowVendorBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            val vendor = vendorBottomSheetViewModel.getVendor(position)

            if (context?.packageName == MOE_IRAQ_PACKAGE_NAME && vendor.id == VendorTypes.WIZIQ.id.toString()) {
                itemView.visibility = View.GONE
            }

            bind<RowVendorBinding> {
                this.vendor = vendor
            }
        }
    }

    private companion object {
        private const val MOE_IRAQ_PACKAGE_NAME = "com.app.classera.moeiraq"
    }


}
