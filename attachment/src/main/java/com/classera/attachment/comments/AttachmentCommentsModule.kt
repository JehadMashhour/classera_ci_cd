package com.classera.attachment.comments

import android.app.Application
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.calssera.vcr.vendorbottomsheet.VendorBottomSheetFragmentArgs
import com.classera.attachment.AttachmentDetailsActivityArgs
import com.classera.attachment.AttachmentDetailsViewModelFactory
import com.classera.data.repositories.attachment.AttachmentRepository
import com.classera.data.repositories.user.UserRepository
import com.classera.discussionrooms.addroom.AddDiscussionFragmentArgs
import com.classera.storage.Storage
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class AttachmentCommentsModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: AttachmentCommentsViewModelFactory
        ): AttachmentCommentsViewModel {
            return ViewModelProvider(fragment, factory)[AttachmentCommentsViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideAttachmentDetailsViewModelFactory(
            application: Application,
            fragment: Fragment,
            attachmentRepository: AttachmentRepository,
            userRepository: UserRepository
        ): AttachmentCommentsViewModelFactory {
            return AttachmentCommentsViewModelFactory(
                application,
                fragment.arguments?.get(AttachmentCommentsBottomSheetFragment.ATTACHMENT_ID) as String?,
                attachmentRepository,
                userRepository
            )
        }

        @Provides
        @JvmStatic
        fun provideAttachmentCommentsAdapter(
            viewModel: AttachmentCommentsViewModel
        ): AttachmentCommentsAdapter {
            return AttachmentCommentsAdapter(viewModel)
        }

    }

    @Binds
    abstract fun bindFragment(fragment: AttachmentCommentsBottomSheetFragment): Fragment
}
