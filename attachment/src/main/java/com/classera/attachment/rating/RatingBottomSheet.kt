package com.classera.attachment.rating

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.FragmentManager
import com.classera.attachment.R
import com.classera.core.fragments.BaseBottomSheetDialogFragment
import me.zhanghai.android.materialratingbar.MaterialRatingBar

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
class RatingBottomSheet private constructor() : BaseBottomSheetDialogFragment() {

    private var callback: ((rating: Float) -> Unit)? = null
    private var ratingBar: MaterialRatingBar? = null
    private var buttonSubmit: Button? = null

    override val layoutId: Int = R.layout.bottom_sheet_rating

    override fun enableDependencyInjection(): Boolean {
        return false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
    }

    private fun findViews() {
        ratingBar = view?.findViewById(R.id.rating_bar_bottom_sheet_rating)
        buttonSubmit = view?.findViewById(R.id.button_bottom_sheet_rating_submit)
    }

    private fun initListeners() {
        ratingBar?.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
            if (fromUser) {
                ratingBar.rating = rating.toInt().toFloat()
            }
        }

        buttonSubmit?.setOnClickListener {
            callback?.invoke(ratingBar?.rating ?: 0f)
            imageViewClose?.callOnClick()
        }
    }

    companion object {

        fun show(supportFragmentManager: FragmentManager, callback: (rating: Float) -> Unit) {
            val ratingBottomSheet = RatingBottomSheet()
            ratingBottomSheet.callback = callback
            ratingBottomSheet.show(supportFragmentManager, "")
        }
    }
}
