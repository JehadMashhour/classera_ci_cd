package com.classera.attachment.add

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.attachment.AttachmentDetailsActivityArgs
import com.classera.data.repositories.attachment.AttachmentRepository
import com.classera.data.repositories.user.UserRepository
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
class AddAttachmentViewModelFactory @Inject constructor(
    private val attachmentRepository: AttachmentRepository
) : ViewModelProvider.Factory{

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AddAttachmentViewModel(attachmentRepository) as T
    }
}
