@file:Suppress("DEPRECATION")

package com.classera.switchsemester

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.ProgressBar
import android.widget.SearchView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Activities
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.intentTo
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.onDebounceQueryTextChange
import com.classera.data.network.errorhandling.Resource
import javax.inject.Inject

@Screen("Switch Semester")
class SwitchSemesterFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: SwitchSemesterViewModel

    private var progressDialog: ProgressDialog? = null

    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var adapter: SwitchSemesterAdapter? = null
    private var errorView: ErrorView? = null
    private var searchView: SearchView? = null

    override val layoutId: Int = R.layout.fragment_switch_semester

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        initProgressDialog()
    }

    private fun initProgressDialog() {
        progressDialog = ProgressDialog(requireContext())
        progressDialog?.setMessage(getString(R.string.please_wait))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
    }

    private fun findViews() {
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_switch_semester)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_switch_semester)
        errorView = view?.findViewById(R.id.error_view_fragment_switch_semester)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_switch_semester)
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            adapter?.resetPaging()
            getSemesterList()
        }
        getSemesterList()

        viewModel.notifyAdapterLiveData.observe(this) { adapter?.notifyDataSetChanged() }
    }

    private fun getSemesterList() {
        viewModel.getSemesterList().observe(this, this::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        if (adapter == null) {
            initAdapter()
        } else {
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getSemesterList() }
        adapter?.finishLoading()
    }

    private fun initAdapter() {
        adapter = SwitchSemesterAdapter(viewModel)
        adapter?.setOnItemClickListener { _, position ->
            val semesterId = viewModel.getSemester(position).semesterId
            semesterId?.let { viewModel.generateSemester(it).observe(this, this::handleAuthResponse) }
        }
        recyclerView?.adapter = adapter
    }

    private fun handleAuthResponse(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleAuthLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleAuthSuccessResource()
            }
            is Resource.Error -> {
                handleAuthErrorResource(resource)
            }
        }
    }

    private fun handleAuthLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressDialog?.show()
        } else {
            progressDialog?.hide()
        }
    }

    private fun handleAuthSuccessResource() {
        val intent = intentTo(Activities.Splash)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    private fun handleAuthErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.resourceMessage, Toast.LENGTH_LONG).show()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_switch_semester, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val searchMenuItem = menu.findItem(R.id.item_menu_fragment_switch_semester_search)
        searchView = (searchMenuItem.actionView as? SearchView?)
        searchView?.onDebounceQueryTextChange {
            viewModel.currentSearchValue = it.toString().trim()
            viewModel.getFilteredSemesterList()
            handleSuccessResource()
        }

        searchView?.setOnCloseListener {
            getSemesterList()
            return@setOnCloseListener false
        }
    }

    override fun onDestroyView() {
        progressBar = null
        recyclerView = null
        adapter = null
        errorView = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }
}
