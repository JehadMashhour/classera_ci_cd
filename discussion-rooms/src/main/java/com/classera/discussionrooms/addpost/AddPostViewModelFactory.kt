package com.classera.discussionrooms.addpost

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.discussions.DiscussionRoomsRepository
import javax.inject.Inject


/**
 * Created by Rawan Al-Theeb on 3/3/2020.
 * Classera
 * r.altheeb@classera.com
 */
class AddPostViewModelFactory @Inject constructor(private val discussionRoomsRepository: DiscussionRoomsRepository) :
    ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AddPostViewModel(discussionRoomsRepository) as T
    }
}
