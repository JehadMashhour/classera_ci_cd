package com.classera.discussionrooms.addpost

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.classera.core.Screen
import com.classera.core.fragments.BaseValidationFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.network.errorhandling.Resource
import com.classera.discussionrooms.R
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import javax.inject.Inject

/**
 * Created by Rawan Al-Theeb on 3/3/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Screen("Add Post")
class AddPostFragment : BaseValidationFragment() {
    @Inject
    lateinit var viewModel: AddPostViewModel
    private val args: AddPostFragmentArgs by navArgs()

    @NotEmpty(message = "validation_fragment_add_post_content")
    private var editTextText: EditText? = null

    private var buttonCreate: Button? = null
    private var progressBarCreate: ProgressBar? = null
    private var linearLayoutParent: LinearLayout? = null

    override val layoutId: Int = R.layout.fragment_add_post

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initCreateListener()
    }

    private fun findViews() {
        buttonCreate = view?.findViewById(R.id.button_fragment_add_post_create)
        editTextText = view?.findViewById(R.id.edit_text_fragment_add_post_text)
        progressBarCreate = view?.findViewById(R.id.progress_bar_fragment_add_post_create)
        linearLayoutParent = view?.findViewById(R.id.linear_layout_fragment_add_post_parent)
    }

    private fun initCreateListener() {
        buttonCreate?.setOnClickListener {
            validator.validate()
        }
    }

    override fun onValidationSucceeded() {
        viewModel.onSaveClicked(
            args.roomId,
            editTextText?.text?.toString()
        ).observe(this, ::handleCreateResource)
    }

    private fun handleCreateResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleCreateLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleCreateSuccessResource(resource as Resource.Success<NoContentBaseWrapper>)
            }
            is Resource.Error -> {
                handleCreateErrorResource(resource)
            }
        }
    }

    private fun handleCreateLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBarCreate?.visibility = View.VISIBLE
            buttonCreate?.text = ""
            buttonCreate?.isEnabled = false
        } else {
            progressBarCreate?.visibility = View.GONE
            buttonCreate?.setText(R.string.title_button_fragment_add_post_create)
            buttonCreate?.isEnabled = true
        }
    }

    private fun handleCreateSuccessResource(resource: Resource.Success<NoContentBaseWrapper>) {
        if (resource.data?.message?.isNotBlank() == true) {
            Toast.makeText(requireContext(), resource.data?.message, Toast.LENGTH_LONG).show()
        }
        findNavController().popBackStack()
    }

    private fun handleCreateErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }
}
