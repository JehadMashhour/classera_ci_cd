package com.classera.discussionrooms.addroom

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Button
import android.widget.Switch
import android.widget.Toast
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseValidationFragment
import com.classera.core.utils.android.getStringOrElse
import com.classera.core.utils.android.hideKeyboard
import com.classera.core.utils.android.observe
import com.classera.core.utils.toInt
import com.classera.data.models.BaseWrapper
import com.classera.data.models.discussions.Lecture
import com.classera.data.models.discussions.RoomDetails
import com.classera.data.models.discussions.Student
import com.classera.data.models.selection.Selectable
import com.classera.data.network.errorhandling.Resource
import com.classera.discussionrooms.R
import com.classera.discussionrooms.databinding.FragmentAddDiscussionBinding
import com.classera.selection.MultiSelectionActivity
import com.google.android.material.textfield.TextInputLayout
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import javax.inject.Inject

/**
 * Created by Rawan Al-Theeb on 2/26/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Screen("Add Discussion room")
class AddDiscussionFragment : BaseValidationFragment() {

    @Inject
    lateinit var viewModel: AddDiscussionViewModel

    @NotEmpty(message = "validation_fragment_add_post_title")
    private var editTextTitle: EditText? = null

    @NotEmpty(message = "validation_fragment_add_post_instructions")
    private var editTextInstructions: EditText? = null

    @NotEmpty(message = "validation_fragment_add_post_lectures")
    private var editTextLectures: EditText? = null

    @NotEmpty(message = "validation_fragment_add_post_students")
    private var editTextStudents: EditText? = null

    private var buttonCreate: Button? = null
    private var switchHideComments: Switch? = null
    private var hideComments: String? = null
    private var progressBarCreate: ProgressBar? = null
    private var progressBar: ProgressBar? = null
    private var errorView: ErrorView? = null
    private var linearLayoutParent: LinearLayout? = null
    private var studentsIds: List<String?>? = null
    private var lecturesIds: List<String?>? = null

    private val args: AddDiscussionFragmentArgs by navArgs()

    override val layoutId: Int = R.layout.fragment_add_discussion

    override fun isBindingEnabled(): Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findViews()
        showTitle()
        hideTheKeyboardListener()
        initCreateListener()
        initHideCommentsListener()
        initStudentsListener()
        initLecturesListener()
        initErrorViewListener()
        getStudents()
        bindData()
    }

    private fun findViews() {
        editTextTitle = view?.findViewById(R.id.edit_text_fragment_discussion_room_title)
        editTextInstructions = view?.findViewById(R.id.edit_text_fragment_add_discussion_room_instructions)
        editTextLectures = view?.findViewById(R.id.edit_text_fragment_add_discussion_room_lectures)
        editTextStudents = view?.findViewById(R.id.edit_text_fragment_add_discussion_room_students)
        buttonCreate = view?.findViewById(R.id.button_fragment_add_discussion_room_create)
        switchHideComments = view?.findViewById(R.id.switch_fragment_add_discussion_room_hide_comments)
        progressBarCreate = view?.findViewById(R.id.progress_bar_fragment_add_discussion_room_create)
        linearLayoutParent = view?.findViewById(R.id.linear_layout_fragment_add_discussion_room_parent)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_add_discussion_room)
        errorView = view?.findViewById(R.id.error_view_fragment_add_discussion_room)
    }

    private fun showTitle() {
        if (args.room != null) {
            (activity as AppCompatActivity).supportActionBar?.title = args.room?.title
            viewModel.getRoomDetails(args.room!!.id).observe(this , this::handleRoomDetailsResource)
            buttonCreate?.text = resources.getString(R.string.pd_name_save)
        } else {
            (activity as AppCompatActivity).
                supportActionBar?.title = getString(R.string.title_fragment_add_discussion_room)
        }
    }

    private fun hideTheKeyboardListener() {
        editTextLectures?.setOnClickListener {
            editTextLectures?.hideKeyboard()
        }

        editTextLectures?.setOnClickListener {
            editTextLectures?.hideKeyboard()
        }
    }

    private fun bindData() {
        bind<FragmentAddDiscussionBinding> {
            this?.room = args.room
        }
    }

    private fun initCreateListener() {
        buttonCreate?.setOnClickListener {
            validator.validate()
        }
    }

    private fun initHideCommentsListener() {
        if (args.room == null) {
            switchHideComments?.isChecked = false
            hideComments = HIDE_COMMENT_FALSE
        } else {
            switchHideComments?.isChecked = args.room?.hideComments ?: false
            hideComments = args.room?.hideComments.toInt().toString()
        }

        switchHideComments?.setOnCheckedChangeListener { _, isChecked ->
            hideComments = if (isChecked) {
                HIDE_COMMENT_TRUE
            } else {
                HIDE_COMMENT_FALSE
            }
        }
    }

    private fun initStudentsListener() {
        editTextStudents?.setOnClickListener {
            MultiSelectionActivity.start(
                fragment = this,
                filters = viewModel.students,
                selectedFilter = viewModel.getSelectedStudents(),
                key = STUDENTS_REQUEST_CODE
            )
        }
    }

    private fun initLecturesListener() {
        editTextLectures?.setOnClickListener {
            MultiSelectionActivity.start(
                fragment = this,
                filters = viewModel.lectures?.toTypedArray(),
                selectedFilter = viewModel.getSelectedLectures(),
                key = LECTURES_REQUEST_CODE
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (MultiSelectionActivity.isDone(requestCode, resultCode, data, STUDENTS_REQUEST_CODE)) {
            addStudentsChips(MultiSelectionActivity.getSelectedFilterFromIntent(data))
            editTextLectures?.setText(viewModel.getSelectedLectureTitles())
            removeError(editTextLectures?.parent?.parent as TextInputLayout)
        } else if (MultiSelectionActivity.isDone(requestCode, resultCode, data, LECTURES_REQUEST_CODE)) {
            addLectureChips(MultiSelectionActivity.getSelectedFilterFromIntent(data))
            editTextStudents?.setText(viewModel.getSelectedStudentTitles())
            removeError(editTextStudents?.parent?.parent as TextInputLayout)
        }
    }

    private fun addStudentsChips(students: Array<Selectable>?) {
        viewModel.setSelectedStudents(students)
        editTextStudents?.setText(students?.joinToString(separator = " | ") { it.title ?: "" })
        removeError(editTextStudents?.parent?.parent as TextInputLayout)
    }

    private fun addLectureChips(lectures: Array<Selectable>?) {
        viewModel.setSelectedLectures(lectures)
        editTextLectures?.setText(lectures?.joinToString(separator = " | ") { it.title ?: "" })
        removeError(editTextLectures?.parent?.parent as TextInputLayout)
    }

    private fun getStudents() {
        viewModel.getStudents().observe(this, ::handleStudentsResource)
    }

    private fun handleStudentsResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleStudentsLoadingResource(resource)
            }
            is Resource.Error -> {
                handleStudentsErrorResource(resource)
            }
        }
    }

    private fun handleStudentsLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            linearLayoutParent?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            linearLayoutParent?.visibility = View.VISIBLE
        }
    }

    private fun handleStudentsErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
    }

    override fun onValidationSucceeded() {
        studentsIds = viewModel.selectedStudents?.filter { it.selected }?.map { it.id }
        lecturesIds = viewModel.selectedLectures?.filter { it.selected }?.map { it.id }

        viewModel.onSaveClicked(
            editTextTitle?.text?.toString(),
            editTextInstructions?.text?.toString(),
            hideComments,
            studentsIds,
            lecturesIds
        ).observe(this, ::handleCreateResource)
    }

    private fun handleCreateResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleCreateLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleCreateSuccessResource()
            }
            is Resource.Error -> {
                handleCreateErrorResource(resource)
            }
        }
    }

    private fun handleCreateLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBarCreate?.visibility = View.VISIBLE
            buttonCreate?.text = ""
            buttonCreate?.isEnabled = false
        } else {
            progressBarCreate?.visibility = View.GONE
            if (args.room != null){
                buttonCreate?.text = resources.getString(R.string.pd_name_save)
            }else{
                buttonCreate?.setText(R.string.title_button_fragment_add_discussion_room_create)
            }
            buttonCreate?.isEnabled = true
        }
    }

    private fun handleCreateSuccessResource() {
        findNavController().popBackStack()
    }

    private fun handleCreateErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    private fun initErrorViewListener() {
        errorView?.setOnRetryClickListener {
            getStudents()
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleRoomDetailsResource(resource: Resource) {
        when (resource) {

            is Resource.Success<*> -> {
                handleRoomDetailsSuccessResource(resource as Resource.Success<BaseWrapper<RoomDetails>>)
            }
            is Resource.Error -> {
                handleRoomDetailsErrorResource(resource)
            }
        }
    }

    private fun handleRoomDetailsSuccessResource(success: Resource.Success<BaseWrapper<RoomDetails>>) {
        addStudentsChips(success.data?.data?.selectedStudents?.map {
            it?.selected = true
            it as Selectable
        }?.toTypedArray())

        var selectedLectures: MutableSet<Lecture?>? = mutableSetOf()
        var selectedStudents: List<Student?>? = success.data?.data?.selectedStudents

        for (student in selectedStudents!!) {
            student?.lectures?.forEach { lecture ->
                selectedLectures?.add(lecture!!)
            }
        }
        addLectureChips(selectedLectures?.map { it as Selectable }?.toTypedArray())
    }

    private fun handleRoomDetailsErrorResource(resource: Resource.Error) {
        val message =
            context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    private companion object {
        private const val HIDE_COMMENT_TRUE = "1"
        private const val HIDE_COMMENT_FALSE = "0"
        private const val STUDENTS_REQUEST_CODE = 101
        private const val LECTURES_REQUEST_CODE = 102
    }
}
