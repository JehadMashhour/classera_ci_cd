package com.classera.discussionrooms.addroom

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.discussions.DiscussionRoomsRepository
import javax.inject.Inject


/**
 * Created by Rawan Al-Theeb on 2/26/2020.
 * Classera
 * r.altheeb@classera.com
 */
class AddDiscussionViewModelFactory @Inject constructor(
    private val discussionRoomsRepository: DiscussionRoomsRepository,
    private val addDiscussionFragmentArgs: AddDiscussionFragmentArgs
) :
    ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AddDiscussionViewModel(discussionRoomsRepository, addDiscussionFragmentArgs) as T
    }
}
