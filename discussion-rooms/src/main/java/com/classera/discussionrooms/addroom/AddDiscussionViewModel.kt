package com.classera.discussionrooms.addroom

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.discussions.Lecture
import com.classera.data.models.discussions.Student
import com.classera.data.models.selection.Selectable
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.discussions.DiscussionRoomsRepository
import kotlinx.coroutines.Dispatchers

/**
 * Created by Rawan Al-Theeb on 2/26/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Suppress("LongParameterList")
class AddDiscussionViewModel(
    private val discussionRoomsRepository: DiscussionRoomsRepository,
    private val addDiscussionFragmentArgs: AddDiscussionFragmentArgs
) : BaseViewModel() {

    var lectures: MutableSet<Lecture>? = mutableSetOf()
    private var fullyLectures: MutableSet<Lecture>? = mutableSetOf()
    var students: Array<out Student>? = null
    var selectedLectures: MutableSet<Selectable>? = mutableSetOf()
    var selectedStudents: MutableSet<Selectable>? = mutableSetOf()

    fun getStudents() = liveData {
        emit(Resource.Loading(show = true))
        val resource = tryResource { discussionRoomsRepository.getStudents() }
        students = resource.element<BaseWrapper<List<Student>>>()?.data?.toTypedArray()
        emit(resource)
        emit(Resource.Loading(show = false))
        if (resource.isSuccess())
            getLectures(students)
    }

    private fun getLectures(students: Array<out Student>?) {
        for (student in students!!) {
            student.lectures?.forEach { lecture ->
                fullyLectures?.add(lecture!!)
            }
        }

        lectures = fullyLectures?.toMutableSet()
    }

    fun onSaveClicked(
        title: String?,
        instructions: String?,
        hideComments: String?,
        studentsIds: List<String?>?,
        lecturesIds: List<String?>?
    ) = liveData {
        emit(Resource.Loading(show = true))
        val resource = if(addDiscussionFragmentArgs.room == null ){
            startAddingDiscussionRoom(title, instructions, hideComments)
        }else{
            startEditingDiscussionRoom(
                title,
                instructions,
                hideComments,
                addDiscussionFragmentArgs.room?.id,
                studentsIds,
                lecturesIds
            )
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private suspend fun startAddingDiscussionRoom(
        title: String?,
        instructions: String?,
        hideComments: String?
    ): Resource = tryNoContentResource {
        discussionRoomsRepository.addRoom(
            title = title,
            instructions = instructions,
            hideComments = hideComments,
            lectures = selectedLectures?.filter { it.selected }?.map { it.id },
            users = selectedStudents?.filter { it.selected }?.map { it.id }
        )
    }

    private suspend fun startEditingDiscussionRoom(
        title: String?,
        instructions: String?,
        hideComments: String?,
        roomId: String?,
        studentsIds: List<String?>?,
        lecturesIds: List<String?>?
    ): Resource = tryNoContentResource {
        discussionRoomsRepository.editRoom(
            title = title,
            instructions = instructions,
            hideComments = hideComments,
            roomId = roomId,
            lectures = lecturesIds,
            users = studentsIds
        )
    }

    fun setSelectedLectures(lectures: Array<Selectable>?) {
        val newSelectedLectures = lectures?.toMutableSet()
        newSelectedLectures?.removeAll(this.lectures ?: setOf())

        students?.filter {
            it.lectures?.any { lecture ->
                newSelectedLectures?.map { selectedLecture -> selectedLecture.id }?.contains(lecture?.id) == true
            } == true
        }?.forEach { selectedStudents?.add(it.apply { it.selected = true }) }
        selectedLectures = lectures?.toMutableSet()
    }

    fun setSelectedStudents(students: Array<Selectable>?) {
        val newSelectedStudents = students?.toMutableSet()
        newSelectedStudents?.removeAll(selectedStudents ?: setOf())
        newSelectedStudents?.flatMap { (it as Student).lectures ?: mutableListOf() }
            ?.filterNotNull()
            ?.forEach { selectedLectures?.add(it.apply { it.selected = true }) }

        selectedStudents = students?.toMutableSet()
    }

    fun getSelectedStudents(): Array<Selectable>? {
        return selectedStudents?.toTypedArray()
    }

    fun getSelectedLectures(): Array<Selectable>? {
        return selectedLectures?.toTypedArray()
    }

    fun getSelectedLectureTitles(): String? {
        return selectedLectures?.map { it.title }?.joinToString(separator = "|") { it ?: "" }
    }

    fun getSelectedStudentTitles(): String? {
        return selectedStudents?.map { it.title }?.joinToString(separator = "|") { it ?: "" }
    }

    fun getRoomDetails(roomId: String?) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            discussionRoomsRepository.getRoomDetails(roomId)
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }
}
