package com.classera.discussionrooms.details

import android.os.Bundle
import android.text.InputType
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.PopupMenu
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.getStringOrElse
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.onDebounceQueryTextChange
import com.classera.data.models.BaseWrapper
import com.classera.data.models.discussions.RoomDetails
import com.classera.data.models.user.UserRole
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.prefs.Prefs
import com.classera.discussionrooms.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textview.MaterialTextView
import javax.inject.Inject

@Screen("Discussion Details")
@Suppress("TooManyFunctions")
class DiscussionDetailsFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: DiscussionDetailsViewModel

    @Inject
    lateinit var prefs: Prefs

    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null

    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var searchView: SearchView? = null
    private var adapter: DiscussionDetailsAdapter? = null
    private var errorView: ErrorView? = null
    private var floatingActionButton: FloatingActionButton? = null
    private var textViewDiscussionInstruction: MaterialTextView? = null

    override val layoutId: Int = R.layout.fragment_discussion_details

    private var closedValue: String? = null
    private var approveValue: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        lifecycle.addObserver(viewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findViews()
        if (prefs.userRole == UserRole.STUDENT || prefs.userRole == UserRole.GUARDIAN) {
            initListeners()
        } else {
            viewModel.getRoomDetails().observe(this, this::handleRoomDetailsResource)
        }

        initVisibility()
        initViewModelListeners()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_discussion_rooms, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)

        val searchMenuItem = menu.findItem(R.id.item_menu_fragment_discussion_rooms_search)
        searchView = (searchMenuItem.actionView as? SearchView?)

        searchView?.inputType = (
                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD)

        searchView?.onDebounceQueryTextChange {
            getDiscussionRooms()
        }

        searchView?.setOnCloseListener {
            getDiscussionRooms()
            return@setOnCloseListener false
        }

        // Handle actions for room details
        val approveMenuItem = menu.findItem(R.id.item_menu_fragment_discussion_rooms_actions_approve)
        val disapproveMenuItem = menu.findItem(R.id.item_menu_fragment_discussion_rooms_actions_disapprove)
        val closeMenuItem = menu.findItem(R.id.item_menu_fragment_discussion_rooms_actions_close)

        if (prefs.userRole == UserRole.STUDENT || prefs.userRole == UserRole.GUARDIAN) {
            approveMenuItem.isVisible = false
            disapproveMenuItem.isVisible = false
            closeMenuItem.isVisible = false
        } else {
            approveMenuItem.isVisible = true
            disapproveMenuItem.isVisible = true
            closeMenuItem.isVisible = true
        }

        if (closedValue == CLOSE)
            closeMenuItem.title = resources.getString(R.string.title_open_topics_comments)
        else
            closeMenuItem.title = resources.getString(R.string.title_stop_topics_comments)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_menu_fragment_discussion_rooms_actions_approve -> {
                approveValue = APPROVE
                viewModel.editRoomSettings(closedValue, APPROVE).observe(this, this::handleApproveRoomResource)
            }

            R.id.item_menu_fragment_discussion_rooms_actions_disapprove -> {
                approveValue = DIS_APPROVE
                viewModel.editRoomSettings(closedValue, DIS_APPROVE).observe(this, this::handleApproveRoomResource)
            }

            R.id.item_menu_fragment_discussion_rooms_actions_close -> {
                if (closedValue == CLOSE) {
                    item.title = resources.getString(R.string.title_open_topics_comments)
                    viewModel.editRoomSettings(OPEN, approveValue)
                            .observe(this, this::handleCloseRoomResource)

                } else {
                    item.title = resources.getString(R.string.title_stop_topics_comments)
                    viewModel.editRoomSettings(CLOSE, approveValue)
                            .observe(this, this::handleCloseRoomResource)
                }

            }

        }
        return super.onOptionsItemSelected(item)
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_discussion_details)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_discussion_details)
        swipeRefreshLayout = view?.findViewById(
                R.id.swipe_refresh_layout_fragment_discussion_details)
        errorView = view?.findViewById(R.id.error_view_fragment_discussion_details)
        floatingActionButton = view?.findViewById(R.id.floating_action_button_fragment_add_post)
        textViewDiscussionInstruction = view?.findViewById(
                R.id.label_fragment_discussion_room_instructions)
    }

    private fun initVisibility() {
        if (prefs.studentsCreateDiscussions == NOT_ALLOWED || prefs.userRole == UserRole.GUARDIAN)
            floatingActionButton?.visibility = View.GONE

        if (closedValue.equals(CLOSE)) {
            floatingActionButton?.visibility = View.GONE
        }

        if (viewModel.instruction?.isNotBlank() == true
                || viewModel.instruction?.isNotEmpty() == true) {
            textViewDiscussionInstruction?.text = viewModel.instruction
        } else {
            textViewDiscussionInstruction?.visibility = View.GONE
        }

    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            refreshDiscussionRooms()
        }

        floatingActionButton?.setOnClickListener {
            navigateToAddPost()
        }

        getDiscussionRooms()
    }

    private fun navigateToAddPost() {
        val direction =
                DiscussionDetailsFragmentDirections.addPostDirection(
                        resources.getString(R.string.title_fragment_add_post_discussion_room),
                        viewModel.roomId
                )
        view?.findNavController()?.navigate(direction)
    }

    private fun getDiscussionRooms(pageNumber: Int = DEFAULT_PAGE) {
        if (pageNumber == DEFAULT_PAGE) {
            adapter?.resetPaging()
        }
        viewModel.getPosts(searchView?.query, pageNumber).observe(this, this::handleResource)
    }

    private fun refreshDiscussionRooms() {
        viewModel.refreshPosts(searchView?.query).observe(this, this::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        if (adapter == null) {
            initAdapter()
            initAdapterListeners()
        } else {
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()
    }

    private fun initAdapter() {
        adapter = DiscussionDetailsAdapter(viewModel, closedValue, prefs)
        recyclerView?.adapter = adapter
        adapter?.setOnLoadMoreListener(::getDiscussionRooms)
    }

    private fun initAdapterListeners() {
        adapter?.setOnItemClickListener { view, position ->
            when (view.id) {
                R.id.image_view_row_discussion_post_more -> {
                    handleMoreClicked(view, position)
                }

                R.id.comment_layout -> {
                    goToPostComments(position)
                }

                R.id.comment -> {
                    goToPostComments(position)
                }

            }
        }
    }

    private fun goToPostComments(position: Int) {
        val post = viewModel.getPost(position)
        val title = post?.fullName
        val id = post?.id
        val direction = DiscussionDetailsFragmentDirections
                .discussionRoomCommentsDirection(title, id, closedValue, approveValue)

        findNavController().navigate(direction)
    }

    private fun handleErrorResource(resource: Resource.Error) {
        if ((adapter?.getItemsCount() ?: 0) > 0) {
            Toast.makeText(context, resource.error.resourceMessage, Toast.LENGTH_LONG).show()
            return
        }
        errorView?.visibility = View.VISIBLE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getDiscussionRooms() }
        adapter?.finishLoading()
    }

    override fun onDestroyView() {
        progressBar = null
        recyclerView = null
        adapter = null
        errorView = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }

    private fun handleMoreClicked(view: View, position: Int) {
        val menu = PopupMenu(requireContext(), view)
        menu.menuInflater.inflate(R.menu.menu_discussion_post_actions, menu.menu)

        if (prefs.userRole == UserRole.STUDENT || prefs.userRole == UserRole.GUARDIAN) {
            menu.menu.getItem(2).isVisible = false
        }

        val post = viewModel.getPost(position)
        val approvePostValue: String

        if (post?.approved == true) {
            approvePostValue = APPROVE
            menu.menu.getItem(2).title = resources.getString(R.string.title_row_discussion_post_dis_approve_post)
        } else {
            approvePostValue = DIS_APPROVE
            menu.menu.getItem(2).title = resources.getString(R.string.title_row_discussion_post_approve_post)
        }

        menu.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.item_menu_discussion_post_actions_delete -> {
                    handleDeleteItemClicked(position)
                }

                R.id.item_menu_discussion_post_actions_approve_post -> {
                    if (approvePostValue == (APPROVE))
                        viewModel.approveTopic(position, DIS_APPROVE).observe(this, this::handleApprovePostResource)
                    else
                        viewModel.approveTopic(position, APPROVE).observe(this, this::handleApprovePostResource)
                }
            }
            return@setOnMenuItemClickListener true
        }
        menu.show()
    }

    private fun handleDeleteItemClicked(position: Int) {

        AlertDialog.Builder(requireContext(), R.style.AppTheme_AlertDialog)
                .setTitle(R.string.title_delete_post_dialog)
                .setMessage(getString(R.string.message_delete_post_dialog))
                .setPositiveButton(R.string.button_positive_delete_post_dialog) { _, _
                    -> viewModel.deletePost(position) }
                .setNegativeButton(R.string.button_negative_delete_post_dialog, null)
                .show()
    }

    private fun initViewModelListeners() {
        viewModel.notifyItemRemovedLiveData.observe(this) { adapter?.notifyItemRemoved(it) }
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleRoomDetailsResource(resource: Resource) {
        when (resource) {

            is Resource.Success<*> -> {
                handleRoomDetailsSuccessResource(resource as Resource.Success<BaseWrapper<RoomDetails>>)
            }
            is Resource.Error -> {
                handleRoomDetailsErrorResource(resource)
            }
        }
    }

    private fun handleRoomDetailsSuccessResource(success: Resource.Success<BaseWrapper<RoomDetails>>) {
        closedValue = if (success.data?.data?.closed == true) {
            CLOSE
        } else {
            OPEN
        }

        initCloseVisibility()
        initListeners()

    }

    private fun initCloseVisibility() {
        if (closedValue.equals(CLOSE)) {
            floatingActionButton?.visibility = View.GONE
        } else {
            floatingActionButton?.visibility = View.VISIBLE
        }

    }

    private fun handleRoomDetailsErrorResource(resource: Resource.Error) {
        val message =
                context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleApprovePostResource(resource: Resource) {
        when (resource) {
            is Resource.Success<*> -> {
                handleApprovePostSuccessResource()
            }
            is Resource.Error -> {
                handleApprovePostErrorResource(resource)
            }
        }
    }

    private fun handleApprovePostSuccessResource() {
        refreshDiscussionRooms()
    }

    private fun handleApprovePostErrorResource(resource: Resource.Error) {
        val message =
                context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleApproveRoomResource(resource: Resource) {
        when (resource) {
            is Resource.Success<*> -> {
                handleApproveRoomSuccessResource()
            }
            is Resource.Error -> {
                handleApproveRoomErrorResource(resource)
            }
        }
    }

    private fun handleApproveRoomSuccessResource() {
        refreshDiscussionRooms()
    }

    private fun handleCloseRoomErrorResource(resource: Resource.Error) {
        val message =
                context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleCloseRoomResource(resource: Resource) {
        when (resource) {
            is Resource.Success<*> -> {
                handleCloseRoomSuccessResource()
            }
            is Resource.Error -> {
                handleCloseRoomErrorResource(resource)
            }
        }
    }

    private fun handleCloseRoomSuccessResource() {
        closedValue = if (closedValue.equals(CLOSE)) {
            OPEN
        } else {
            CLOSE
        }
        initCloseVisibility()
        initAdapter()
        initAdapterListeners()
    }

    private fun handleApproveRoomErrorResource(resource: Resource.Error) {
        val message =
                context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    companion object {
        private const val NOT_ALLOWED = "0"
        const val APPROVE = "1"
        const val DIS_APPROVE = "0"
        const val CLOSE = "1"
        const val OPEN = "0"
    }
}
