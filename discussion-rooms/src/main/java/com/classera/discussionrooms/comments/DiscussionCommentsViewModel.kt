package com.classera.discussionrooms.comments

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.discussions.DiscussionComment
import com.classera.data.models.discussions.DiscussionCommentsWrapper
import com.classera.data.moshi.timeago.TimeAgoAdapter
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.discussions.DiscussionRoomsRepository
import com.classera.data.repositories.user.UserRepository
import com.classera.data.toString
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import java.util.*

/**
 * Created by Rawan Al-Theeb on 12/19/2019.
 * Classera
 * r.altheeb@classera.com
 */
class DiscussionCommentsViewModel(
    private val discussionDetailsFragmentArgs: DiscussionCommentsFragmentArgs,
    private val discussionRoomsRepository: DiscussionRoomsRepository,
    private val userRepository: UserRepository,
    private val prefs: Prefs
) : BaseViewModel(), LifecycleObserver {

    private var comments: MutableList<DiscussionComment?> = mutableListOf()

    private var _notifyCommentsAdapterLiveData = MutableLiveData<Boolean>()
    var notifyCommentsAdapterLiveData: LiveData<Boolean> = _notifyCommentsAdapterLiveData

    private var _notifyAddCommentsReachMaxLiveData = MutableLiveData<String>()
    var notifyAddCommentsReachMaxLiveData: LiveData<String> = _notifyAddCommentsReachMaxLiveData

    private val _notifyItemRemovedLiveData = MutableLiveData<Int>()
    val notifyItemRemovedLiveData: LiveData<Int> = _notifyItemRemovedLiveData

    fun getComments(pageNumber: Int): LiveData<Resource> {
        return getComments(pageNumber, pageNumber == DEFAULT_PAGE)
    }

    fun getSettings(): Prefs {
        return prefs
    }

    fun refreshComments() = getComments(DEFAULT_PAGE, false)

    private fun getComments(pageNumber: Int, showProgress: Boolean) =
        liveData(Dispatchers.IO) {
            if (showProgress) {
                emit(Resource.Loading(show = true))
            }

            val postId = discussionDetailsFragmentArgs.postId
            val resource = tryResource {
                discussionRoomsRepository.getDiscussionComments(
                    postId,
                    pageNumber
                )
            }
            if (pageNumber == DEFAULT_PAGE) {
                comments.clear()
            }
            comments.addAll(
                resource.element<BaseWrapper<DiscussionCommentsWrapper>>()?.data?.comments ?: mutableListOf()
            )
            emit(resource)
            emit(Resource.Loading(show = false))
        }

    fun getCommentCount(): Int {
        return comments.size
    }

    fun getComment(position: Int): DiscussionComment? {
        return comments.get(position)
    }

    fun addComment(comment: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            val id = System.currentTimeMillis().toString()
            val user = userRepository.getLocalUser().first()?.firstOrNull()
            val discussionComment = DiscussionComment(
                localId = id,
                postContent = comment,
                created = getCreatedAt(),
                fullName = user?.firstName,
                postUserPhoto = user?.imageUrl
            )
            comments.add(0, discussionComment)

            startCommentRequest(discussionComment)
        }
    }

    private suspend fun startCommentRequest(discussionComment: DiscussionComment?) {
        val index = comments.indexOfFirst { it?.localId == discussionComment?.localId }
        if (index != -1) {
            comments[index] = discussionComment?.copy(isLoading = true, isFailed = false)
            _notifyCommentsAdapterLiveData.postValue(false)
            val comment = discussionComment?.postContent
            val postId = discussionDetailsFragmentArgs.postId
            val resource = tryNoContentResource {
                discussionRoomsRepository.addComment(postId, comment)
            }

            if (resource is Resource.Error) {
                if (resource.error.message?.contains("You've reached the maximum number of replies", true) == true) {
                    comments.removeAt(index)
                    _notifyAddCommentsReachMaxLiveData.postValue(resource.error.message)
                } else {
                    comments[index] = discussionComment?.copy(isLoading = false, isFailed = true)
                }
            } else {
                comments[index] = discussionComment?.copy(isLoading = false, isFailed = false)
            }

            _notifyCommentsAdapterLiveData.postValue(true)
        }
    }

    private fun getCreatedAt(): String? {
        val date = Date()
        return TimeAgoAdapter().fromJson(date.toString("yyyy-MM-dd HH:mm:ss"))
    }

    fun deleteDiscussionComment(position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val comment = getComment(position)
            comment?.deleting = true

            val commentId = comment?.id
            val resource = tryNoContentResource {
                discussionRoomsRepository.deleteDiscussionComment(commentId, "1")
            }
            if (resource.isSuccess()) {
                deleteItem(position)
                _notifyItemRemovedLiveData.postValue(position)
                return@launch
            }
            comment?.deleting = false
        }
    }

    private fun deleteItem(position: Int) {
        comments?.removeAt(position)
    }

    fun onCommentRetryClicked(position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val discussionComment = comments[position]
            startCommentRequest(discussionComment)
        }
    }

    fun approveTopic(position: Int, approve: String?) = liveData(Dispatchers.IO) {

        val comment = getComment(position)
        val commentId = comment?.id

        emit(Resource.Loading(show = true))
        val resource = tryNoContentResource {
            discussionRoomsRepository.approveTopic(commentId, approve)
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }
}
