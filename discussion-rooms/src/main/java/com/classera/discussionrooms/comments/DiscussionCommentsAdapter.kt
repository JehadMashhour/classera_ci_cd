package com.classera.discussionrooms.comments

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.data.models.user.UserRole
import com.classera.data.prefs.Prefs
import com.classera.discussionrooms.R
import com.classera.discussionrooms.databinding.RowDiscussionCommentBinding
import com.classera.discussionrooms.details.DiscussionDetailsFragment
import com.classera.discussionrooms.details.DiscussionDetailsFragment.Companion.CLOSE

/**
 * Created by Odai Nazzal on 12/31/2019.
 * Classera
 *
 * o.nazzal@classera.com
 */
class DiscussionCommentsAdapter(
    private val activity: Activity,
    private val viewModel: DiscussionCommentsViewModel,
    private val prefs: Prefs
) : BasePagingAdapter<DiscussionCommentsAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowDiscussionCommentBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getCommentCount()
    }

    inner class ViewHolder(binding: RowDiscussionCommentBinding) : BaseBindingViewHolder(binding) {

        private var imageViewError: ImageView? = null
        private var imageViewMore: ImageView? = null
        private var imageViewApproved: ImageView? = null
        private var textViewApproved: TextView? = null

        init {
            imageViewError = itemView.findViewById(R.id.image_view_row_discussion_comment_error)
            imageViewMore = itemView.findViewById(R.id.image_view_row_discussion_comment_more)
            imageViewApproved = itemView.findViewById(R.id.image_view_row_discussion_comment_approved)
            textViewApproved = itemView.findViewById(R.id.text_view_row_discussion_comment_approved)

            if (viewModel.getSettings().userRole == UserRole.GUARDIAN ||
                viewModel.getSettings().userRole == UserRole.STUDENT) {

                imageViewApproved?.visibility = View.GONE
                textViewApproved?.visibility = View.GONE
            }
        }

        override fun bind(position: Int) {
            imageViewMore?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    onItemClicked(it, this)
                }
            }

            imageViewError?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    viewModel.onCommentRetryClicked(clickedPosition)
                }
            }

            bind<RowDiscussionCommentBinding> {
                this.comment = viewModel.getComment(position)
                this.pref = viewModel.getSettings()
            }

            if (viewModel.getComment(position)?.creatorId.toString() == prefs.userId
                || prefs.userRole == UserRole.TEACHER
                || prefs.userRole == UserRole.TEACHER_SUPERVISOR
            )
                imageViewMore?.visibility = View.VISIBLE
            else
                imageViewMore?.visibility = View.GONE

        }
    }
}
