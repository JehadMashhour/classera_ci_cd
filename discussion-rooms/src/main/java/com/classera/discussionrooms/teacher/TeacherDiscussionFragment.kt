package com.classera.discussionrooms.teacher

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.PopupMenu
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.classera.core.utils.android.observe
import com.classera.discussionrooms.DiscussionsAdapter
import com.classera.discussionrooms.DiscussionsFragment
import com.classera.discussionrooms.DiscussionsViewModel
import com.classera.discussionrooms.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import javax.inject.Inject

class TeacherDiscussionFragment : DiscussionsFragment() {

    @Inject
    lateinit var viewModel: TeacherDiscussionViewModel

    @Inject
    lateinit var adapter: TeacherDiscussionAdapter

    private var floatingActionButton: FloatingActionButton? = null

    override fun getDiscussionViewModel(): DiscussionsViewModel {
        return viewModel
    }

    override fun getDiscussionAdapter(): DiscussionsAdapter<*> {
        return adapter
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initVisibility()

        initListeners()
        initViewModelListeners()
    }

    private fun findViews() {
        floatingActionButton =
            view?.findViewById(R.id.floating_action_button_fragment_add_discussion)
    }

    private fun initVisibility() {
        floatingActionButton?.visibility = View.VISIBLE
    }

    private fun initListeners() {
        handleAddListeners()
    }

    override fun handleAddListeners() {
        floatingActionButton?.setOnClickListener {
            navigateToAddDiscussionRoom()
        }
    }

    private fun navigateToAddDiscussionRoom() {
        val direction =
            TeacherDiscussionFragmentDirections.addDiscussionRoomDirection(
                resources.getString(R.string.title_fragment_add_discussion_room)
            )

        view?.findNavController()?.navigate(direction)
    }

    override fun initAdapterListeners() {

        adapter.setOnItemClickListener { view, position ->
            when (view.id) {
                R.id.image_view_row_discussion_room_more -> {
                    handleMoreClicked(view, position)
                }
                else -> {

                    val room = viewModel.getDiscussionRoom(position)
                    val id = room?.id
                    val title = room?.title
                    val instruction = room?.instruction
                    findNavController().navigate(
                        TeacherDiscussionFragmentDirections.discussionRoomDetailsDirection(
                            title,
                            id
                        ).apply {
                            this.instruction = instruction
                        }
                    )
                }
            }
        }
    }

    private fun handleMoreClicked(view: View, position: Int) {
        val menu = PopupMenu(requireContext(), view)
        menu.menuInflater.inflate(R.menu.menu_discussion_room_actions, menu.menu)
        menu.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.item_menu_discussion_post_actions_edit -> {
                    handleEditItemClicked(position)
                }
                R.id.item_menu_discussion_post_actions_delete -> {
                    handleDeleteItemClicked(position)
                }
            }
            return@setOnMenuItemClickListener true
        }
        menu.show()
    }

    private fun handleEditItemClicked(position: Int) {
        val room = viewModel.getDiscussionRoom(position)
        val title = room?.title
        val direction =
            TeacherDiscussionFragmentDirections.addDiscussionRoomDirection(title).setRoom(room)
        findNavController().navigate(direction)
    }

    private fun handleDeleteItemClicked(position: Int) {
        AlertDialog.Builder(requireContext(), R.style.AppTheme_AlertDialog)
            .setTitle(R.string.title_delete_room_dialog)
            .setMessage(getString(R.string.message_delete_room_dialog))
            .setPositiveButton(R.string.button_positive_delete_room_dialog) { _, _ ->
                viewModel.deleteRoom(
                    position
                )
            }
            .setNegativeButton(R.string.button_negative_delete_room_dialog, null)
            .show()
    }

    private fun initViewModelListeners() {
        viewModel.notifyItemRemovedLiveData.observe(this) {
            adapter.notifyItemRemoved(it)
            adapter.notifyDataSetChanged()
        }
    }
}
