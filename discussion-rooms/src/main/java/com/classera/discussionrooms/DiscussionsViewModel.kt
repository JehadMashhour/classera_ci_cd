package com.classera.discussionrooms

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.discussions.DiscussionRoom
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.discussions.DiscussionRoomsRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job

/**
 * Created by Rawan Al-Theeb on 12/19/2019.
 * Classera
 * r.altheeb@classera.com
 */
abstract class DiscussionsViewModel(private val discussionRoomsRepository: DiscussionRoomsRepository) :
    BaseViewModel() {

    private var discussionRoomsList: MutableList<DiscussionRoom?>? = mutableListOf()
    open var currentPage: Int = DEFAULT_PAGE

    private var job = Job()

    fun getDiscussionRooms(text: CharSequence?, pageNumber: Int): LiveData<Resource> {
        return getDiscussionRooms(text, pageNumber, pageNumber == DEFAULT_PAGE)
    }

    fun refreshDiscussionRooms(text: CharSequence?) = getDiscussionRooms(text, DEFAULT_PAGE, false)

    private fun getDiscussionRooms(text: CharSequence?, pageNumber: Int, showProgress: Boolean): LiveData<Resource> {
        
        job.cancel()
        job = Job()

        return liveData(CoroutineScope(job + Dispatchers.IO).coroutineContext) {

            currentPage = pageNumber
            if (showProgress) {
                emit(Resource.Loading(show = true))
            }

            val resource = tryResource {
                discussionRoomsRepository.getStudentDiscussions(pageNumber, text = text)
            }
            if (pageNumber == DEFAULT_PAGE) {
                discussionRoomsList?.clear()
            }
            discussionRoomsList?.addAll(
                resource.element<BaseWrapper<List<DiscussionRoom>>>()?.data ?: mutableListOf()
            )
            emit(resource)
            emit(Resource.Loading(show = false))
        }
    }

    fun getDiscussionRoomCount(): Int {
        return discussionRoomsList?.size ?: 0
    }

    fun getDiscussionRoom(position: Int): DiscussionRoom? {
        return discussionRoomsList?.get(position)
    }

    fun deleteItem(position: Int) {
        discussionRoomsList?.removeAt(position)
    }

    abstract fun deleteRoom(position: Int)

}
