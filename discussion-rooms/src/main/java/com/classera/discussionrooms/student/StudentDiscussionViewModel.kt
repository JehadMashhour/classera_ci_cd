package com.classera.discussionrooms.student

import com.classera.data.repositories.discussions.DiscussionRoomsRepository
import com.classera.discussionrooms.DiscussionsViewModel


/**
 * Created by Rawan Al-Theeb on 2/25/2020.
 * Classera
 * r.altheeb@classera.com
 */
class StudentDiscussionViewModel(discussionRoomsRepository: DiscussionRoomsRepository) :
    DiscussionsViewModel(discussionRoomsRepository) {

    override fun deleteRoom(position: Int) {
        throw IllegalAccessException("The student should not be able to delete a Room")
    }
}
