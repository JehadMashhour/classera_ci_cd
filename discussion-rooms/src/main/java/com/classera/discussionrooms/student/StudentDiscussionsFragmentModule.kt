package com.classera.discussionrooms.student

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.discussionrooms.DiscussionsViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * Created by Rawan Al-Theeb on 12/19/2019.
 * Classera
 * r.altheeb@classera.com
 */

@Module
abstract class StudentDiscussionsFragmentModule {
    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: DiscussionsViewModelFactory
        ): StudentDiscussionViewModel {
            return ViewModelProvider(fragment, factory)[StudentDiscussionViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideDiscussionAdapter(
            viewModel: StudentDiscussionViewModel
        ): StudentDiscussionAdapter {
            return StudentDiscussionAdapter(viewModel)
        }
    }

    @Binds
    abstract fun bindActivity(activity: StudentDiscussionFragment): Fragment
}
