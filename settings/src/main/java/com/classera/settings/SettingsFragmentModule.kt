package com.classera.settings

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides


@Module
abstract class SettingsFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: SettingsViewModelFactory
        ): SettingsViewModel {
            return ViewModelProvider(fragment, factory)[SettingsViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: SettingsFragment): Fragment
}
