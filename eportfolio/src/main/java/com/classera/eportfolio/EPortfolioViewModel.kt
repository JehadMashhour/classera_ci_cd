package com.classera.eportfolio

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.eportfolio.EPortfolio
import com.classera.data.models.eportfolio.EPortfolioWraper
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.eportfolios.EPortfolioRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Created by Odai Nazzal on 1/14/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
class EPortfolioViewModel(private val ePortfolioRepository: EPortfolioRepository, private val prefs: Prefs) :
    BaseViewModel() {


    private var ePortfolios: MutableList<EPortfolioWraper> = mutableListOf()

    private val _notifyItemRemovedLiveData = MutableLiveData<Int>()
    val notifyItemRemovedLiveData: LiveData<Int> = _notifyItemRemovedLiveData

    private val _notifyItemRemovedSuccessMessageLiveData = MutableLiveData<String>()
    val notifyItemRemovedSuccessMessageLiveData = _notifyItemRemovedSuccessMessageLiveData

    fun getEPortfolios(pageNumber: Int = DEFAULT_PAGE) =
        liveData(Dispatchers.IO) {
            if (pageNumber == DEFAULT_PAGE) {
                emit(Resource.Loading(show = true))
            }

            val resource = tryResource {
                ePortfolioRepository.getStudentEPortfolios(pageNumber)
            }
            if (pageNumber == DEFAULT_PAGE) {
                ePortfolios.clear()
            }
            ePortfolios.addAll(resource.element<BaseWrapper<List<EPortfolioWraper>>>()?.data ?: mutableListOf())
            emit(resource)
            emit(Resource.Loading(show = false))
        }

    fun getEPortfolio(position: Int): EPortfolio? {
        return ePortfolios[position].eportfolio
    }

    fun getEPortfoliosCount(): Int {
        return ePortfolios.size
    }

    fun getSettings(): Prefs {
        return prefs
    }

    fun deleteEport(position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val eport = getEPortfolio(position)

            val postId = eport?.id

            val resource = tryNoContentResource {
                ePortfolioRepository.deleteEport(postId)
            }
            if (resource.isSuccess()) {
                deleteItem(position)
                _notifyItemRemovedLiveData.postValue(position)
                _notifyItemRemovedSuccessMessageLiveData.postValue(resource.element<NoContentBaseWrapper>()?.message)
                return@launch
            }
        }
    }

    private fun deleteItem(position: Int) {
        ePortfolios.removeAt(position)
    }


}
