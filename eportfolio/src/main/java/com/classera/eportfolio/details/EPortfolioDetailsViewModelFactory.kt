package com.classera.eportfolio.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.eportfolios.EPortfolioRepository
import com.classera.data.repositories.user.UserRepository

/**
 * Created by Odai Nazzal on 1/16/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
class EPortfolioDetailsViewModelFactory(
    private val args: EPortfolioDetailsFragmentArgs?,
    private val ePortfolioRepository: EPortfolioRepository,
    private val userRepository: UserRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return EPortfolioDetailViewModel(
            args?.ePortfolioId,
            ePortfolioRepository,
            userRepository,
            args?.imageBackground
        ) as T
    }
}
