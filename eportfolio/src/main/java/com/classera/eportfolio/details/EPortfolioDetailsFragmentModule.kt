package com.classera.eportfolio.details

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.classera.data.repositories.eportfolios.EPortfolioRepository
import com.classera.data.repositories.user.UserRepository
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Created by Odai Nazzal on 1/16/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
@Module
abstract class EPortfolioDetailsFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: EPortfolioDetailsViewModelFactory
        ): EPortfolioDetailViewModel {
            return ViewModelProvider(fragment, factory)[EPortfolioDetailViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideEPortfolioDetailsViewModelFactory(
            fragment: Fragment,
            ePortfolioRepository: EPortfolioRepository,
            userRepository: UserRepository
        ): EPortfolioDetailsViewModelFactory {
            val args by fragment.navArgs<EPortfolioDetailsFragmentArgs>()
            return EPortfolioDetailsViewModelFactory(args, ePortfolioRepository, userRepository)
        }
    }

    @Binds
    abstract fun bindFragment(activity: EPortfolioDetailsFragment): Fragment
}
