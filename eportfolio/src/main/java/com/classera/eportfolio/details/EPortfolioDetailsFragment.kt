package com.classera.eportfolio.details


import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseBindingFragment
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.onLoadMore
import com.classera.data.models.BackgroundColor
import com.classera.data.models.BaseWrapper
import com.classera.data.models.eportfolio.details.EPortfolioDetailsWraper
import com.classera.data.network.errorhandling.Resource
import com.classera.eportfolio.R
import com.classera.eportfolio.databinding.FragmentEPotfolioDetailsBinding
import javax.inject.Inject
import com.classera.eportfolio.details.EPortfolioDetailsFragmentDirections.actionNavigationFragmentMainActivityAttachmentDetails as attachmentDetails

/**
 * A simple [Fragment] subclass.
 */
@Screen("EPortfolio Details")
class EPortfolioDetailsFragment : BaseBindingFragment() {

    override val layoutId: Int = R.layout.fragment_e_potfolio_details

    @Inject
    lateinit var viewModel: EPortfolioDetailViewModel

    private var canLoadMoreComments = false
    private var commentsAdapter: EPortfolioCommentsAdapter? = null

    private var recyclerViewComments: RecyclerView? = null
    private var cardViewRoot: CardView? = null
    private var progressBar: ProgressBar? = null
    private var errorView: ErrorView? = null
    private var relativeLayoutAddCommentSubmit: View? = null
    private var editTextAddComment: EditText? = null
    private var scrollView: NestedScrollView? = null
    private var viewActionsLine: View? = null
    private var buttonViewAttachment: View? = null
    private var imageViewRowEPortfolioImage: ImageView? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initBackgroundColor()
        initViewsListeners()
        initViewModelListeners()
        getDetails()

    }

    private fun findViews() {
        imageViewRowEPortfolioImage = view?.findViewById(R.id.imageViewRowEPortfolioImage)
        cardViewRoot = view?.findViewById(R.id.card_view_fragment_e_portfolio_details_root)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_e_portfolio_details)
        errorView = view?.findViewById(R.id.error_view_fragment_e_portfolio_details)
        recyclerViewComments =
            view?.findViewById(R.id.recycler_view_fragment_e_portfolio_details_comments)
        editTextAddComment =
            view?.findViewById(R.id.edit_text_fragment_e_portfolio_details_add_comment)
        scrollView = view?.findViewById(R.id.scroll_view_fragment_e_portfolio_details)
        viewActionsLine = view?.findViewById(R.id.view_fragment_e_portfolio_details_actions_line)
        buttonViewAttachment =
            view?.findViewById(R.id.button_fragment_e_portfolio_details_view_attachment)
        relativeLayoutAddCommentSubmit =
            view?.findViewById(R.id.relative_layout_fragment_e_portfolio_details_add_comment_submit_parent)
    }

    private fun initBackgroundColor() {
        viewModel.getBackgroundColor().observe(this, ::setBackgroundColor)
    }

    private fun setBackgroundColor(backgroundColor: BackgroundColor?) {
        imageViewRowEPortfolioImage?.setBackgroundResource(backgroundColor?.resId ?: 0)
    }

    private fun initViewsListeners() {
        errorView?.setOnRetryClickListener(::getDetails)
        relativeLayoutAddCommentSubmit?.setOnClickListener {
            if (editTextAddComment?.text?.isNotEmpty() == true && editTextAddComment?.text?.isNotBlank() == true) {
                viewModel.addComment(editTextAddComment?.text?.toString())
                editTextAddComment?.text = null
            }
        }

        scrollView?.onLoadMore {
            if (canLoadMoreComments) {
                canLoadMoreComments = false
                getComments()
            }
        }

        buttonViewAttachment?.setOnClickListener {
            if (viewModel.ePortfolio?.attachmentId == null) {
                Toast.makeText(
                    context,
                    getString(R.string.toast_fragment_eportfolio_details_no_attachment),
                    Toast.LENGTH_LONG
                ).show()
            } else {
                val attachmentId = viewModel.ePortfolio?.attachmentId
                val title = viewModel.ePortfolio?.title
                findNavController().navigate(
                    attachmentDetails(
                        attachmentId,
                        BackgroundColor.GRAY
                    ).apply {
                        this.title = title
                    }
                )
            }
        }
    }

    private fun initViewModelListeners() {
        viewModel.notifyCommentsAdapterLiveData.observe(this) {
            commentsAdapter?.notifyDataSetChanged()

            scrollView?.post { scrollView?.smoothScrollTo(0, viewActionsLine?.y?.toInt() ?: 0) }
        }
    }

    private fun getDetails() {
        viewModel.getDetails().observe(this, ::handleDetailsResource)
        getComments()
    }

    private fun getComments() {
        viewModel.getComments().observe(this, ::handleCommentsResource)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleDetailsResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleDetailsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleDetailsSuccessResource(resource as Resource.Success<BaseWrapper<EPortfolioDetailsWraper>>)
            }
            is Resource.Error -> {
                handleDetailsErrorResource(resource)
            }
        }
    }

    private fun handleDetailsLoadingResource(resource: Resource.Loading) {
        progressBar?.post {
            progressBar?.visibility = if (resource.show) View.VISIBLE else View.GONE
            cardViewRoot?.visibility = if (!resource.show) View.VISIBLE else View.GONE
        }
    }

    private fun handleDetailsSuccessResource(resource: Resource.Success<BaseWrapper<EPortfolioDetailsWraper>>) {
        bind<FragmentEPotfolioDetailsBinding> {
            resource.data?.data?.eportfolio?.let { ePortfolio ->
                this?.ePortfolio = ePortfolio
            }
        }
    }

    private fun handleDetailsErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleCommentsResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleCommentsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleCommentsSuccessResource()
            }
        }
    }

    private fun handleCommentsLoadingResource(resource: Resource.Loading) {
        canLoadMoreComments = !resource.show
    }

    private fun handleCommentsSuccessResource() {
        if (commentsAdapter == null) {
            initAdapter()
            return
        }
        commentsAdapter?.notifyDataSetChanged()
    }

    private fun initAdapter() {
        commentsAdapter = EPortfolioCommentsAdapter(viewModel)
        recyclerViewComments?.adapter = commentsAdapter
    }

}
