package com.classera.eportfolio.details

import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.eportfolio.R
import com.classera.eportfolio.databinding.RowEPortfolioCommentBinding

/**
 * Created by Odai Nazzal on 1/16/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
class EPortfolioCommentsAdapter(
    private val viewModel: EPortfolioDetailViewModel
) : BaseAdapter<EPortfolioCommentsAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowEPortfolioCommentBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getCommentCount()
    }

    inner class ViewHolder(binding: RowEPortfolioCommentBinding) : BaseBindingViewHolder(binding) {

        private var imageViewError: ImageView? = null

        init {
            imageViewError = itemView.findViewById(R.id.image_view_row_attachment_comment_error)
        }

        override fun bind(position: Int) {
            bind<RowEPortfolioCommentBinding> {
                comment = viewModel.getComment(position)
            }

            imageViewError?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    viewModel.onCommentRetryClicked(clickedPosition)
                }
            }
        }
    }
}
