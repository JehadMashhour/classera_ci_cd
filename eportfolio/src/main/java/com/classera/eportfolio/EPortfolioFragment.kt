package com.classera.eportfolio


import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.BackgroundColor
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.prefs.Prefs
import com.google.android.material.floatingactionbutton.FloatingActionButton
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
@Screen("EPortfolio")
class EPortfolioFragment : BaseFragment() {

    override val layoutId: Int = R.layout.fragment_e_portfolio

    @Inject
    lateinit var viewModel: EPortfolioViewModel

    @Inject
    lateinit var prefs: Prefs

    private var progressBar: ProgressBar? = null
    private var adapter: EPortfolioAdapter? = null
    private var errorView: ErrorView? = null

    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null

    private var floatingActionButtonAdd: FloatingActionButton? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
        initViewModelListeners()
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_ePortfolio)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_ePortfolio)
        errorView = view?.findViewById(R.id.error_view_fragment_ePortfolio)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_ePortfolio)
        floatingActionButtonAdd = view?.findViewById(R.id.floating_action_button_fragment_eport_add)

    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            getEPortfolios()
        }
        getEPortfolios()

        floatingActionButtonAdd?.setOnClickListener {
            findNavController().navigate(EPortfolioFragmentDirections.addEportfolio())
        }
    }

    private fun getEPortfolios(pageNumber: Int = DEFAULT_PAGE) {
        if (pageNumber == DEFAULT_PAGE) {
            adapter?.resetPaging()
        }
        viewModel.getEPortfolios(pageNumber).observe(this, this::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        if (adapter == null) {
            initAdapter()
            initAdapterListeners()
        } else {
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()
    }

    private fun initAdapter() {
        adapter = EPortfolioAdapter(viewModel)
        recyclerView?.adapter = adapter
        adapter?.setOnLoadMoreListener(::getEPortfolios)
    }

    private fun initAdapterListeners() {
        adapter?.setOnItemClickListener { view, position ->
            when (view.id) {
                R.id.image_view_row_eport_more -> {
                    handleMoreClicked(view, position)
                }
                else -> {
                    val ePortfolio = viewModel.getEPortfolio(position)
                    val title = ePortfolio?.title
                    val id = ePortfolio?.id
                    val imageBackground = ePortfolio?.backgroundColor ?: BackgroundColor.GRAY
                    val direction =
                        EPortfolioFragmentDirections.
                        actionNavigationFragmentMainEPortfoliosDetails(title, id, imageBackground)
                    findNavController().navigate(direction)
                }
            }
        }
    }

    private fun handleMoreClicked(view: View, position: Int) {
        val menu = PopupMenu(requireContext(), view)
        menu.menuInflater.inflate(R.menu.menu_eport_actions, menu.menu)
        menu.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.item_menu_eportfolio_actions_delete -> {
                    handleDeleteItemClicked(position)
                }
                R.id.item_menu_eportfolio_actions_edit -> {
                    val ePortfolio = viewModel.getEPortfolio(position)?.copy()
                    val direction =
                        EPortfolioFragmentDirections.addEportfolio().apply {
                            this.eportfolio = ePortfolio
                        }
                    findNavController().navigate(direction)
                }
            }
            return@setOnMenuItemClickListener true
        }
        menu.show()
    }


    private fun handleDeleteItemClicked(position: Int) {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.title_delete_eport_dialog)
            .setMessage(
                getString(
                    R.string.message_delete_eport_dialog
                )
            )
            .setPositiveButton(R.string.button_positive_delete_eport_dialog) { _, _ ->
                viewModel.deleteEport(position)
            }
            .setNegativeButton(R.string.button_negative_delete_eport_dialog, null)
            .show()
    }

    private fun initViewModelListeners() {
        viewModel.notifyItemRemovedLiveData.observe(this) {
            adapter?.notifyItemRemoved(it)
        }

        viewModel.notifyItemRemovedSuccessMessageLiveData.observe(this) { successMessage ->
            Toast.makeText(context, successMessage, Toast.LENGTH_LONG).show()
        }
    }


    private fun handleErrorResource(resource: Resource.Error) {
        if ((adapter?.getItemsCount() ?: 0) > 0) {
            Toast.makeText(context, resource.error.resourceMessage, Toast.LENGTH_LONG).show()
            return
        }
        errorView?.visibility = View.VISIBLE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getEPortfolios() }
        adapter?.finishLoading()
    }

    override fun onDestroyView() {
        progressBar = null
        adapter = null
        errorView = null
        recyclerView = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }

}
