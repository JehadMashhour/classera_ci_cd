package com.classera.eportfolio

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.courses.CoursesRepository
import com.classera.data.repositories.eportfolios.EPortfolioRepository
import com.classera.eportfolio.add.AddEportfolioViewModel
import javax.inject.Inject

/**
 * Created by Odai Nazzal on 1/14/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
class EPortfolioViewModelFactory @Inject constructor(
    private val ePortfolioRepository: EPortfolioRepository,
    private val coursesRepository: CoursesRepository,
    private val prefs: Prefs
    ) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            EPortfolioViewModel::class.java -> {
                EPortfolioViewModel(ePortfolioRepository,prefs) as T
            }
            AddEportfolioViewModel::class.java -> {
                AddEportfolioViewModel(ePortfolioRepository, coursesRepository) as T
            }
            else -> {
                throw IllegalAccessException("There is no view model called $modelClass")
            }
        }

    }
}
