package com.classera.announcements

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.ProgressBar
import android.widget.SearchView
import android.widget.Toast
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.onDebounceQueryTextChange
import com.classera.data.models.announcements.AnnouncementWrapper
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import javax.inject.Inject

@Screen("Announcements")
class AnnouncementsFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: AnnouncementsViewModel

    private var searchView: SearchView? = null
    private var progressBar: ProgressBar? = null
    private var adapter: AnnouncementsAdapter? = null
    private var errorView: ErrorView? = null

    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null

    override val layoutId: Int = R.layout.fragment_announcements

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_announcements, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val searchMenuItem = menu.findItem(R.id.item_menu_fragment_announcement_search)
        searchView = (searchMenuItem.actionView as? SearchView?)

        searchView?.onDebounceQueryTextChange {
            getAnnouncementsList()
        }

        searchView?.setOnCloseListener {
            getAnnouncementsList()
            return@setOnCloseListener false
        }
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_announcements)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_announcements)
        errorView = view?.findViewById(R.id.error_view_fragment_announcements)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_announcements)
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            getAnnouncementsList()
        }

        viewModel.notifyAdapterItemLiveData.observe(this) { adapter?.notifyItemChanged(it) }

        viewModel.toastLiveData.observe(this) { message ->
            val stringMessage = if (message is Int) getString(message) else message as String
            Toast.makeText(context, stringMessage, Toast.LENGTH_LONG).show()
        }
        getAnnouncementsList()
    }

    private fun getAnnouncementsList(pageNumber: Int = DEFAULT_PAGE) {
        if (pageNumber == DEFAULT_PAGE) {
            adapter?.resetPaging()
        }
        viewModel.getAnnouncementsList(
                searchView?.query,
                pageNumber
            )
            .observe(this, this::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        if (adapter == null) {
            initAdapter()
            initAdapterListeners()
        } else {
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()
    }

    private fun initAdapter() {
        adapter = AnnouncementsAdapter(viewModel)
        recyclerView?.adapter = adapter
        adapter?.setOnLoadMoreListener(::getAnnouncementsList)
    }

    private fun initAdapterListeners() {
        adapter?.setOnItemClickListener { _, position ->
            navigateToAnnouncementDetails(viewModel.getAnnouncement(position))
        }
    }

    private fun navigateToAnnouncementDetails(announcementWrapper: AnnouncementWrapper?) {
        announcementWrapper?.let { announcement ->
            val title = announcement.announcement?.title ?: ""
            findNavController().navigate(
                AnnouncementsFragmentDirections.announcementDetailsDirection(announcement, title)
            )
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        if ((adapter?.getItemsCount() ?: 0) > 0) {
            Toast.makeText(context, resource.error.resourceMessage, Toast.LENGTH_LONG).show()
            return
        }
        errorView?.visibility = View.VISIBLE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getAnnouncementsList() }
        adapter?.finishLoading()
    }

    override fun onDestroyView() {
        progressBar = null
        recyclerView = null
        adapter = null
        errorView = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }
}
