package com.classera.announcements

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.announcements.AnnouncementWrapper
import com.classera.data.models.announcements.AnnouncementsWrapper
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.announcements.AnnouncementsRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job


/**
 * Created by Rawan Al-Theeb on 1/1/2020.
 * Classera
 * r.altheeb@classera.com
 */
class AnnouncementsViewModel(private val announcementsRepository: AnnouncementsRepository) :
    BaseViewModel() {
    private val _notifyAdapterItemLiveData = MutableLiveData<Int>()
    val notifyAdapterItemLiveData: LiveData<Int> = _notifyAdapterItemLiveData

    private val _toastLiveData = MutableLiveData<Any>()
    val toastLiveData: LiveData<Any> = _toastLiveData

    private var announcemets: MutableList<AnnouncementWrapper> = mutableListOf()

    private var job = Job()

    fun getAnnouncementsList(text: CharSequence?, pageNumber: Int): LiveData<Resource> {
        return getAnnouncementsList(text, pageNumber, showProgress = pageNumber == DEFAULT_PAGE)
    }

    private fun getAnnouncementsList(
        text: CharSequence?,
        pageNumber: Int,
        showProgress: Boolean
    ): LiveData<Resource> {
        job.cancel()
        job = Job()

        return liveData(CoroutineScope(job + Dispatchers.IO).coroutineContext) {
            if (showProgress) {
                emit(Resource.Loading(show = true))
            }

            val resource = tryResource {
                announcementsRepository.getAnnouncementsList(text, pageNumber)
            }

            if (pageNumber == DEFAULT_PAGE) {
                announcemets.clear()
            }

            announcemets.addAll(
                resource.element<BaseWrapper<AnnouncementsWrapper>>()?.data?.announcements
                    ?: mutableListOf()
            )
            emit(resource)
            emit(Resource.Loading(show = false))
        }
    }

    fun getAnnouncement(position: Int): AnnouncementWrapper? {
        return announcemets[position]
    }

    fun getAnnouncementCount(): Int {
        return announcemets.size
    }
}
