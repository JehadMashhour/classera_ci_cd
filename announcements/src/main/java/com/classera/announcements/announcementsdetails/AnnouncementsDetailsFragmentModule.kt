package com.classera.announcements.announcementsdetails

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: June 30, 2020
 *
 * @author Khaled Mohammad
 */
@Module
abstract class AnnouncementsDetailsFragmentModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: AnnouncementDetailsViewModelFactory
        ): AnnouncementDetailsViewModel {
            return ViewModelProvider(fragment, factory)[AnnouncementDetailsViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: AnnouncementDetailsFragment): Fragment
}
