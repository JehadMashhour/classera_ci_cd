package com.classera.announcements.announcementsdetails

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.announcements.AnnouncementsRepository
import kotlinx.coroutines.Dispatchers

/**
 * Project: Classera
 * Created: June 30, 2020
 *
 * @author Khaled Mohammad
 */
class AnnouncementDetailsViewModel(
    private val announcementsRepository: AnnouncementsRepository
) : BaseViewModel() {

    fun getAnnouncementDetails(announcementId: String?) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource =
            tryResource { announcementsRepository.getAnnouncementDetails(announcementId) }
        emit(resource)
        emit(Resource.Loading(show = false))
    }
}
