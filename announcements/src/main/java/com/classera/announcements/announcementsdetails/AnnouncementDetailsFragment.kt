package com.classera.announcements.announcementsdetails

import android.os.Bundle
import android.view.View
import android.webkit.WebView
import androidx.navigation.fragment.navArgs
import com.classera.announcements.R
import com.classera.core.Screen
import com.classera.core.fragments.BaseFragment

/**
 * Created by Rawan Al-Theeb on 01/02/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Screen("Announcement Details")
class AnnouncementDetailsFragment : BaseFragment() {

    override val layoutId: Int = R.layout.fragment_announcement_details

    private val args: AnnouncementDetailsFragmentArgs by navArgs()

    private var webView: WebView? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        loadDetails()
    }

    private fun findViews(){
        webView = view?.findViewById(R.id.text_view_announcement_details)
    }

    private fun loadDetails(){
        webView?.loadData( args.announcement.announcement?.details,
            "text/html; charset=UTF-8", null)
    }
}
