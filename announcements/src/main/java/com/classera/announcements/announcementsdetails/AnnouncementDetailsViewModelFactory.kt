package com.classera.announcements.announcementsdetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.announcements.AnnouncementsRepository
import javax.inject.Inject

/**
 * Project: Classera
 * Created: June 30, 2020
 *
 * @author Khaled Mohammad
 */
class AnnouncementDetailsViewModelFactory @Inject constructor(
    private val announcementsRepository: AnnouncementsRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AnnouncementDetailsViewModel(announcementsRepository) as T
    }
}

