package com.classera.chat.chatmessages

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.models.chat.Message
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.chat.ChatBlockedUsersRepository
import com.classera.data.repositories.chat.ChatRepository
import com.classera.data.repositories.chat.ChatServiceRepository
import com.classera.data.socket.MessagesSocket
import com.squareup.moshi.Moshi
import javax.inject.Inject

/**
 * Created on 26/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
class ChatMessagesViewModelFactory @Inject constructor(
    private val application: Application,
    private val chatMessagesFragmentArgs: ChatMessagesFragmentArgs,
    private val chatBlockedUsersRepository: ChatBlockedUsersRepository,
    private val chatServiceRepository: ChatServiceRepository,
    private val chatRepository: ChatRepository,
    private val prefs: Prefs,
    private val moshi: Moshi,
    private val socket: MessagesSocket
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val messageAdapter = moshi.adapter(Message::class.java)
        return ChatMessagesViewModel(
            application,
            chatMessagesFragmentArgs,
            chatBlockedUsersRepository,
            chatServiceRepository,
            chatRepository,
            prefs,
            messageAdapter,
            socket
        ) as T
    }
}
