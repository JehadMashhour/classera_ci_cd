package com.classera.chat.chatmessages

import android.app.Application
import android.media.MediaPlayer
import android.webkit.MimeTypeMap
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.liveData
import com.classera.chat.R
import com.classera.core.BaseAndroidViewModel
import com.classera.data.BuildConfig
import com.classera.data.getFormalUserName
import com.classera.data.models.BaseWrapper
import com.classera.data.models.chat.Block
import com.classera.data.models.chat.Message
import com.classera.data.models.chat.MessageBody
import com.classera.data.models.chat.MessageMediaType
import com.classera.data.models.chat.MessageTypeEnum
import com.classera.data.models.chat.MessagesWrapper
import com.classera.data.moshi.chattime.ChatTimeAdapter
import com.classera.data.moshi.duration.DurationAdapter
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.network.errorhandling.tryThirdPartyResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.chat.ChatBlockedUsersRepository
import com.classera.data.repositories.chat.ChatRepository
import com.classera.data.repositories.chat.ChatServiceRepository
import com.classera.data.socket.MessagesSocket
import com.squareup.moshi.JsonAdapter
import kotlinx.coroutines.Dispatchers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import java.io.File
import java.util.*

/**
 * Created by Odai Nazzal on 1/11/2020, Modified by Lana Manaseer & Mohammad Hamdan
 * Classera
 *
 * o.nazzal@classera.com
 **/
class ChatMessagesViewModel(
    application: Application,
    private val chatMessagesFragmentArgs: ChatMessagesFragmentArgs,
    private val chatBlockedUsersRepository: ChatBlockedUsersRepository,
    private val chatServiceRepository: ChatServiceRepository,
    private val chatRepository: ChatRepository,
    private val prefs: Prefs,
    private val messageAdapter: JsonAdapter<Message>,
    private val messagesSocket: MessagesSocket
) : BaseAndroidViewModel(application), LifecycleObserver {

    private var pageState: String? = "0"
    private var creatorId: String? = null
    var threadId: String? = null
    var isGroup: Boolean? = null

    private var isUploading = false
    private var messages: MutableList<Message> = mutableListOf()
    private val userIDsSet = mutableSetOf<String>()
    var userBlockMeStatus = MutableLiveData<Boolean>()
    private val _newMessageLiveData = MutableLiveData<Unit>()
    val newMessageLiveData: LiveData<Unit> = _newMessageLiveData

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        pageState = "0"
        threadId = chatMessagesFragmentArgs.threadId
        isGroup = chatMessagesFragmentArgs.isGroup
        creatorId = chatMessagesFragmentArgs.creatorID

        startListenToMessages()
    }

    private fun startListenToMessages() {
        messagesSocket.onMessageReceived { message ->
            if (message?.threadId != threadId) {
                return@onMessageReceived
            }
            if (isUploading && message?.fromId == prefs.userId && message?.type == MessageTypeEnum.MEDIA) {
                isUploading = false
                messages.filter { it.uploading }.forEach { it.uploading = false }
                return@onMessageReceived
            }
            if (message != null && message.type != MessageTypeEnum.REMOVED && message.type != MessageTypeEnum.ADDED) {
                messages.add(0, message)
                _newMessageLiveData.postValue(Unit)
            }
        }
    }

    fun getListMessages() = liveData(Dispatchers.IO) {
        if (pageState == null) {
            return@liveData
        }
        emit(Resource.Loading(show = true))

        val resource = tryThirdPartyResource { chatServiceRepository.getListMessages(threadId, pageState!!) }
        pageState = resource.element<MessagesWrapper>()?.pageState

        val allMessages = resource.element<MessagesWrapper>()?.result ?: mutableListOf<Message>()

        allMessages.forEach {
            it.fromId?.let { fromId -> userIDsSet.add(fromId) }

            it.apply {
                it.group = isGroup!!
            }
            messagesSocket.updateSeen(it.messageId, threadId)
        }

        handleUsersProfilePictures(allMessages)

        messages.addAll(allMessages.filter { it.body != null })
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private suspend fun handleUsersProfilePictures(allMessages: List<Message>) {

        val users = chatRepository.getUsersBasicDetails(userIDsSet.toList()).data

        allMessages.forEach {
            if (userIDsSet.contains(it.fromId)) {
                val user = users?.firstOrNull { user -> user.userId == it.fromId }
                it.apply {
                    it.senderPicture = user?.photoFileName
                }
            }
        }
    }

    fun getMessage(position: Int): Message? {
        return messages[position]
    }

    fun getMessageCount(): Int {
        return messages.size
    }

    fun checkBlockedUser() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource = tryResource { chatBlockedUsersRepository.checkBlockedUser(creatorId) }

        if (resource.isSuccess()) {
            val isBlockMe = resource.element<BaseWrapper<Block>>()?.data?.isBlockMe
            userBlockMeStatus.postValue(isBlockMe)
        } else {
            userBlockMeStatus.postValue(false)
        }

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun toggleUserBlockStatus() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource = tryResource {
            chatBlockedUsersRepository.toggleUserBlockStatus(creatorId)
        }

        val userStatus = resource.element<BaseWrapper<Block>>()?.message?.contains(
            application.applicationContext.resources.getString(R.string.message_chat_check_blocked_status))
        prefs.isBlocked = resource.isSuccess() && userStatus == true

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getCreatorID(): String {
        return if (creatorId != null) creatorId!! else prefs.creatorId!!
    }

    fun isSender(fromId: String?): Boolean {
        return prefs.userId == fromId
    }

    fun isBlockedUser(): Boolean {
        return prefs.isBlocked
    }

    fun resetIsBlocked(): Boolean {
        prefs.isBlocked = false
        return prefs.isBlocked
    }

    fun addNewMessage(messageText: String) {
        if (messageText.isNotEmpty()) {
            val message = createMessage(MessageTypeEnum.TEXT, MessageBody(text = messageText))

            val messageJson = JSONObject(messageAdapter.toJson(message))
            messageJson.remove("message")
            messageJson.put("message", messageText)
            messagesSocket.sendMessage(messageJson)
        }
    }

    fun removeBrokenAttachment(errorMessage: String) {
        messages.filter { it.uploading }.forEach {
            it.error = true
            it.errorMessage = errorMessage
        }
    }

    fun removeMessage(position: Int) {
        messages.removeAt(position)
    }

    fun retryUpload(position: Int) = liveData {
        isUploading = true
        emit(Resource.Loading(show = true))

        val message = messages[position]
        message.error = false
        message.uploading = true
        val file = File(message.body?.url ?: "")
        val isAudio = message.body?.getMediaType() == MessageMediaType.AUDIO
        if (isAudio) {
            message.apply { this.duration = DurationAdapter().fromJson(file.getDuration().toString()) }
        }

        val resource = executeUploadAttachmentResource(file, isAudio)

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun uploadAttachment(file: File) = liveData {
        isUploading = true
        emit(Resource.Loading(show = true))

        val message = createMessage(MessageTypeEnum.MEDIA, MessageBody(url = file.absolutePath))
        val isAudio = message.body?.getMediaType() == MessageMediaType.AUDIO
        if (isAudio) {
            message.apply { this.duration = DurationAdapter().fromJson(file.getDuration().toString()) }
        }
        messages.add(0, message)

        val resource = executeUploadAttachmentResource(file, isAudio)

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private suspend fun executeUploadAttachmentResource(file: File, isAudio: Boolean): Resource {
        val userIdPart = MultipartBody.Part.createFormData("user_id", prefs.userId ?: "")
        val threadIdPart = MultipartBody.Part.createFormData("thread_id", threadId ?: "")
        val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(file.extension)
        val fileBody = RequestBody.create(MediaType.parse(mimeType ?: ""), file)
        val filePart = MultipartBody.Part.createFormData("files", file.name, fileBody)
        val sourcePart = MultipartBody.Part.createFormData("source", BuildConfig.flavorData.socketSource)
        val durationPart = if (isAudio) {
            MultipartBody.Part.createFormData("duration", file.getDuration().toString())
        } else {
            null
        }
        val resource = tryThirdPartyResource {
            chatServiceRepository.uploadAttachment(
                listOfNotNull(
                    userIdPart,
                    threadIdPart,
                    filePart,
                    sourcePart,
                    durationPart
                )
            )
        }

        return resource
    }

    private fun createMessage(type: MessageTypeEnum, body: MessageBody): Message {
        val lastSentMessageTime = System.currentTimeMillis()
        return Message(
            messageId = UUID.randomUUID().toString(),
            threadId = threadId,
            fromId = prefs.userId,
            body = body,
            sentDate = ChatTimeAdapter().fromJson(lastSentMessageTime.toString()),
            type = type,
            senderPicture = prefs.userProfilePicture,
            senderName = prefs.userFullName.getFormalUserName()
        ).apply {
            uploading = true
        }
    }

    fun reset() {
        messages.clear()
        pageState = "0"
    }
}

private fun File.getDuration(): Int {
    val mediaPlayer = MediaPlayer()
    mediaPlayer.setDataSource(absolutePath)
    mediaPlayer.prepare()
    return mediaPlayer.duration
}
