package com.classera.chat.groupusers

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Created on 25/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
@Module
abstract class ChatGroupUsersFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: ChatGroupUsersViewModelFactory
        ): ChatGroupUsersViewModel {
            return ViewModelProvider(fragment, factory)[ChatGroupUsersViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideChatGroupUsersFragmentArgs(fragment: Fragment): ChatGroupUsersFragmentArgs {
            val args by fragment.navArgs<ChatGroupUsersFragmentArgs>()
            return args
        }
    }

    @Binds
    abstract fun bindFragment(fragmentChat: ChatGroupUsersFragment): Fragment
}
