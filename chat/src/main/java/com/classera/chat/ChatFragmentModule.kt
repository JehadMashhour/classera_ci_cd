package com.classera.chat

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
@Module
abstract class ChatFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: ChatViewModelFactory
        ): ChatViewModel {
            return ViewModelProvider(fragment, factory)[ChatViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: ChatFragment): Fragment
}
