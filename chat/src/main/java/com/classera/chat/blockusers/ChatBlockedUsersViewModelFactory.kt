package com.classera.chat.blockusers

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.chat.ChatBlockedUsersRepository
import javax.inject.Inject

/**
 * Created on 20/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
class ChatBlockedUsersViewModelFactory @Inject constructor(
    private val chatRepository: ChatBlockedUsersRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ChatBlockedUsersViewModel(
            chatRepository
        ) as T
    }
}
