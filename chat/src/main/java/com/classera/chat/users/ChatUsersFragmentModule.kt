package com.classera.chat.users

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Created on 18/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
@Module
abstract class ChatUsersFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: ChatUsersViewModelFactory
        ): ChatUsersViewModel {
            return ViewModelProvider(fragment, factory)[ChatUsersViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: ChatUsersFragment): Fragment
}
