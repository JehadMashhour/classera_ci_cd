package com.classera.chat.users

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.SearchView
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.chat.R
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.onDebounceQueryTextChange
import com.classera.data.models.chat.Thread
import com.classera.data.network.errorhandling.Resource
import com.classera.filter.FilterActivity
import javax.inject.Inject

/**
 * Created on 14/02/2020.
 * Classera
 *
 * @author Lana Manaseer, Modified by Mohammad Hamdan
 */
@Screen("Chat Users")
class ChatUsersFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: ChatUsersViewModel

    private var searchView: SearchView? = null
    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null
    private var adapter: ChatUsersAdapter? = null
    private var errorView: ErrorView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null

    override val layoutId: Int = R.layout.fragment_chat_users

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findViews()
        initListeners()
        getThreads(CHAT_LIMIT)
        getRoles()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_chat_users, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)

        val searchMenuItem = menu.findItem(R.id.item_menu_fragment_chat_search)
        searchView = (searchMenuItem.actionView as? SearchView?)
        searchView?.onDebounceQueryTextChange {
            viewModel.refreshUsers(CHAT_LIMIT, searchName = searchView?.query)
                .observe(this, ::handleResource)
        }

        searchView?.setOnCloseListener {
            viewModel.refreshUsers(CHAT_LIMIT, searchName = searchView?.query)
                .observe(this, ::handleResource)
            return@setOnCloseListener false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_menu_fragment_chat_filter -> {
                FilterActivity.start(this, viewModel.getFilterRoles())
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (FilterActivity.isDone(requestCode, resultCode, data)) {
            val filter = FilterActivity.getSelectedFilter(data)
            viewModel.refreshUsers(
                    CHAT_LIMIT,
                    searchName = searchView?.query,
                    roleID = filter?.filterId
                )
                .observe(this, ::handleResource)
        } else if (FilterActivity.isAll(requestCode, resultCode, data)) {
            viewModel.refreshUsers(CHAT_LIMIT, searchName = searchView?.query)
                .observe(this, ::handleResource)
        }
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_chat_users)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_chat_users)
        errorView = view?.findViewById(R.id.error_view_fragment_chat_users)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_chat_users)
    }

    private fun initListeners() {
        errorView?.setOnRetryClickListener { getThreads(CHAT_LIMIT) }

        swipeRefreshLayout?.setOnRefreshListener {
            getThreads(CHAT_LIMIT)
        }
    }

    private fun getRoles() {
        viewModel.getRoles().observe(this) {}
    }

    private fun getThreads(
        limit: String, pagination: String? = null,
        searchName: CharSequence? = "", roleID: String? = ""
    ) {
        viewModel.getUsers(limit, pagination, searchName, roleID)
            .observe(this, ::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
            else -> {

            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleSuccessResource() {
        initAdapter()
    }

    private fun initAdapter() {
        adapter = ChatUsersAdapter(viewModel)
        initAdapterListener()
        recyclerView?.adapter = adapter
    }

    private fun navigateToChatMessages(user: Thread?) {
        findNavController().navigate(
            ChatUsersFragmentDirections.chatMessagesDirection(user?.fullName, user?.threadId, false, user?.userId)
        )
    }

    private fun initAdapterListener() {
        adapter?.setOnItemClickListener { _, position ->
            navigateToChatMessages(viewModel.getUser(position))
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
    }

    override fun onDestroyView() {
        searchView = null
        progressBar = null
        recyclerView = null
        adapter = null
        errorView = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }

    companion object {
        const val CHAT_LIMIT = "1000"
    }
}
