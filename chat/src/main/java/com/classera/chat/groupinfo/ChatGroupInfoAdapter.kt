package com.classera.chat.groupinfo

import android.app.AlertDialog
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import com.classera.chat.R
import com.classera.chat.databinding.RowThreadGroupInfoUserBinding
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder

/**
 * Created on 25/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
class ChatGroupInfoAdapter(private val viewModel: ChatGroupInfoViewModel) :
    BaseAdapter<ChatGroupInfoAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowThreadGroupInfoUserBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getUserCount()
    }

    inner class ViewHolder(binding: RowThreadGroupInfoUserBinding) :
        BaseBindingViewHolder(binding) {

        private var deleteImageButton: ImageButton? = null

        init {
            deleteImageButton = itemView.findViewById(R.id.text_view_row_thread_group_info_delete)
        }

        override fun bind(position: Int) {
            bind<RowThreadGroupInfoUserBinding> {
                threadGroupInfoUser = viewModel.getUser(position)
            }

            if (viewModel.isCreator == false) {
                deleteImageButton?.visibility = View.INVISIBLE
            }
            deleteImageButton?.setOnClickListener {
                showDeleteUser(position)
            }
        }
    }

    private fun showDeleteUser(position: Int) {
        AlertDialog.Builder(context)
            .setTitle(context?.resources?.getString(R.string.title_message_to_delete_user))
            .setMessage(context?.resources?.getString(R.string.body_message_to_delete_user))
            .setPositiveButton(android.R.string.yes) { _, _ ->
                viewModel.deleteUser(position)
                notifyDataSetChanged()
            }
            .show()
    }

}
