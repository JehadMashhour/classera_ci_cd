package com.classera.chat.groupinfo

import android.app.AlertDialog
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.classera.chat.R
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.hideKeyboard
import com.classera.core.utils.android.observe
import com.classera.data.models.chat.GroupWrapper
import com.classera.data.network.errorhandling.Resource
import com.google.android.material.floatingactionbutton.FloatingActionButton
import javax.inject.Inject

/**
 * Created on 25/02/2020.
 * Classera
 *
 * @author Lana Manaseer, Modified by Mohammad Hamdan
 */
@Screen("Chat Group Info")
class ChatGroupInfoFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: ChatGroupInfoViewModel

    private var groupCreatedName: String? = null

    private var progressBar: ProgressBar? = null
    private var groupName: AppCompatEditText? = null
    private var recyclerView: RecyclerView? = null
    private var adapter: ChatGroupInfoAdapter? = null
    private var errorView: ErrorView? = null
    private var floatingButtonChatAddGroupInfo: FloatingActionButton? = null

    override val layoutId: Int = R.layout.fragment_chat_group_info

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycle.addObserver(viewModel)
        findViews()
        initListeners()
        checkEditability()
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_chat_group_info)
        groupName = view?.findViewById(R.id.edit_text_fragment_chat_group_info_name)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_chat_group_info_users)
        errorView = view?.findViewById(R.id.error_view_fragment_chat_group_info)
        floatingButtonChatAddGroupInfo =
            view?.findViewById(R.id.floating_action_button_fragment_chat_add_group_info)
    }

    private fun initListeners() {
        errorView?.setOnRetryClickListener { }

        floatingButtonChatAddGroupInfo?.setOnClickListener {
            if (!viewModel.threadId.isNullOrEmpty()) {
                if (groupName?.text?.isNotEmpty() == true && viewModel.users.isNotEmpty()) {
                    activity?.hideKeyboard()
                    saveEditedGroupInfo()
                    navigateToChatMessages(viewModel.threadId)
                } else {
                    showCheckToasts()
                }
            } else {
                if (groupName?.text?.isNotEmpty() == true && viewModel.users.isNotEmpty()) {
                    activity?.hideKeyboard()
                    createGroup(groupName?.text?.toString())
                } else {
                    showCheckToasts()
                }
            }
        }
    }

    private fun showCheckToasts() {
        when {
            (groupName?.text?.isEmpty() == true) -> {
                Toast.makeText(
                    context,
                    getString(R.string.required_field_group_users_info_name),
                    Toast.LENGTH_SHORT
                ).show()
            }
            (viewModel.users.isEmpty()) -> {
                Toast.makeText(
                    context,
                    getString(R.string.required_field_group_users_info_users),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun checkEditability() {
        groupCreatedName = viewModel.groupName
        floatingButtonChatAddGroupInfo?.setImageResource(R.drawable.ic_done)
        if (!viewModel.threadId.isNullOrEmpty()) {
            groupName?.setText(viewModel.groupName)
            getListGroupParticipants()
        } else {
            initAdapter()
        }

        if (viewModel.isCreator == false) {
            groupName?.visibility = View.GONE
            floatingButtonChatAddGroupInfo?.visibility = View.GONE
        }
    }

    private fun saveEditedGroupInfo() {
        if (!viewModel.removedUsersList.isNullOrEmpty()) {
            viewModel.deleteParticipants(viewModel.removedUsersList).observe(this) {}
        }
        if (viewModel.addedParticipants != null) {
            viewModel.addParticipants().observe(this) {}
        }
        if (groupCreatedName != groupName?.text.toString()) {
            viewModel.updateGroupName(groupName?.text?.toString()).observe(this) {}
        }
    }

    private fun createGroup(name: String?) {
        viewModel.createGroup(name).observe(this, ::handleResource)
    }

    private fun getListGroupParticipants() {
        viewModel.getListGroupParticipants().observe(this, ::handleParticipantsResource)
    }

    private fun handleParticipantsResource(resource: Resource) {
        when (resource) {
            is Resource.Success<*> -> {
                handleSuccessParticipantResource()
            }
            else -> {
            }
        }
    }

    private fun handleSuccessParticipantResource() {
        initAdapter()
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource(resource as Resource.Success<GroupWrapper>)
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
        } else {
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource(resource: Resource.Success<GroupWrapper>) {
        navigateToChatMessages(resource.data?.threadId)
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
    }

    private fun navigateToChatMessages(threadId: String?) {
        findNavController().navigate(
            ChatGroupInfoFragmentDirections.chatMessagesDirection(
                groupName?.text?.toString(),
                threadId,
                true,
                null
            )
        )
    }

    private fun initAdapter() {
        adapter = ChatGroupInfoAdapter(viewModel)
        recyclerView?.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (!viewModel.threadId.isNullOrEmpty() && viewModel.isCreator == true) {
            inflater.inflate(R.menu.menu_fragment_chat_edit_group_info, menu)
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_menu_fragment_chat_message_remove_group -> {
                deleteGroup()
            }

            R.id.item_menu_fragment_chat_message_add_group_users -> {
                addParticipants()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun deleteGroup() {
        AlertDialog.Builder(requireContext())
            .setTitle(getString(R.string.title_message_to_delete_group))
            .setMessage(getString(R.string.body_message_to_delete_group))
            .setPositiveButton(android.R.string.yes) { _, _ ->
                viewModel.deleteGroup().observe(this, ::handleDeleteGroupResource)
            }
            .show()
    }

    private fun handleDeleteGroupResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleDeleteGroupSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleDeleteGroupSuccessResource() {
        navigateToMainChatFragment()
    }

    private fun navigateToMainChatFragment() {
        findNavController().navigate(ChatGroupInfoFragmentDirections.mainChatDirection())
    }

    private fun addParticipants() {
        navigateToAddParticipants()
    }

    private fun navigateToAddParticipants() {
        val userIds = viewModel.users.map { it.userId ?: "" }.toTypedArray()
        findNavController().navigate(
            ChatGroupInfoFragmentDirections.chatGroupUsersDirection(userIds).apply {
                isEditMode = true
            }
        )
    }

    override fun onDestroyView() {
        progressBar = null
        groupName = null
        recyclerView = null
        adapter = null
        errorView = null
        floatingButtonChatAddGroupInfo = null
        lifecycle.removeObserver(viewModel)
        super.onDestroyView()
    }
}
