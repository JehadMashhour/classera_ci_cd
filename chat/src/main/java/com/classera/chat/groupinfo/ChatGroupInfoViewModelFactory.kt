package com.classera.chat.groupinfo

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.chat.ChatRepository
import com.classera.data.repositories.chat.ChatServiceRepository
import javax.inject.Inject

/**
 * Created on 25/02/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
class ChatGroupInfoViewModelFactory @Inject constructor(
    private val chatServiceRepository: ChatServiceRepository,
    private val chatRepository: ChatRepository,
    private val prefs: Prefs,
    private val chatGroupInfoFragmentArgs: ChatGroupInfoFragmentArgs
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ChatGroupInfoViewModel(
            chatServiceRepository,
            chatRepository,
            prefs,
            chatGroupInfoFragmentArgs
        ) as T
    }
}
