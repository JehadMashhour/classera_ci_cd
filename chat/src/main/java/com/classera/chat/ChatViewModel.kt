package com.classera.chat

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BasePaginationWrapper
import com.classera.data.models.BaseWrapper
import com.classera.data.models.chat.Role
import com.classera.data.models.chat.Thread
import com.classera.data.models.chat.UserStatus
import com.classera.data.models.filter.Filterable
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.network.errorhandling.tryThirdPartyResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.chat.ChatRepository
import com.classera.data.repositories.chat.ChatServiceRepository
import kotlinx.coroutines.Dispatchers
import java.util.*

/**
 * Created by Odai Nazzal on 1/11/2020, Modified by Lana Manaseer
 * Classera
 *
 * o.nazzal@classera.com
 */
class ChatViewModel(
    private val chatRepository: ChatRepository,
    private val chatServiceRepository: ChatServiceRepository,
    private val prefs: Prefs
) : BaseViewModel() {

    private val threads = mutableListOf<Thread>()
    private var filterRoles: List<Role?>? = null

    var chatUserStatus: MutableLiveData<String> = MutableLiveData()
    var chatUserInverseStatus: MutableLiveData<String> = MutableLiveData()

    fun getThreads(
        limit: String,
        pagination: String? = null,
        searchName: CharSequence? = "",
        roleID: String? = ""
    ) = getThreads(limit, pagination, searchName, roleID, true)

    fun refreshThreads(
        limit: String,
        pagination: String? = null,
        searchName: CharSequence? = "",
        roleID: String? = ""
    ) = getThreads(limit, pagination, searchName, roleID, false)

    private fun getThreads(
        limit: String,
        pagination: String? = null,
        searchName: CharSequence? = "",
        roleID: String? = "",
        showProgress: Boolean
    ) = liveData(Dispatchers.IO) {
        if (showProgress) {
            emit(Resource.Loading(show = true))
        }

        val resource = tryThirdPartyResource {
            chatRepository.getFilteredThreads(
                limit,
                pagination,
                searchName,
                roleID
            )
        }

        if (pagination == null) {
            threads.clear()
        }

        threads.addAll(resource.element<BasePaginationWrapper<List<Thread>>>()?.data ?: mutableListOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getThreadCount(): Int {
        return threads.size
    }

    fun getThread(position: Int): Thread? {
        return threads[position]
    }

    fun getCreatorID(position: Int): String? {
        prefs.creatorId = getThread(position)?.userId
        return getThread(position)?.userId
    }

    fun getRoles() = liveData(Dispatchers.IO) {
        val resource = tryResource { chatRepository.getRoles() }

        filterRoles = resource.element<BaseWrapper<List<Role>>>()?.data ?: mutableListOf<Role>()

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getFilterRoles(): Array<out Filterable?>? {
        return filterRoles?.toTypedArray()
    }

    fun getUserStatus() = liveData(Dispatchers.IO) {
        val resource = tryThirdPartyResource { chatServiceRepository.getUserStatus() }

        if (resource.isSuccess()) {
            chatUserInverseStatus.postValue(userStatusInverse())
        }

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun updateUserStatus() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource = tryThirdPartyResource { chatServiceRepository.updateUserStatus(userStatusInverse()) }

        if (resource.isSuccess()) {
            prefs.userStatus = userStatusInverse()
            chatUserStatus.postValue(prefs.userStatus)
            chatUserInverseStatus.postValue(userStatusInverse())
        }

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private fun userStatusInverse(): String {
        var status = prefs.userStatus
        when (status?.toLowerCase(Locale.ENGLISH)) {
            UserStatus.ONLINE.name.toLowerCase() -> {
                status = UserStatus.OFFLINE.name
            }
            UserStatus.OFFLINE.name.toLowerCase() -> {
                status = UserStatus.ONLINE.name
            }
        }
        return status.toString()
    }

}
