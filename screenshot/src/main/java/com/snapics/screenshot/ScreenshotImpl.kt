package com.snapics.screenshot

import android.graphics.Bitmap
import android.graphics.Rect
import android.os.Build
import android.os.Handler
import android.view.PixelCopy
import android.view.View
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.classera.storage.Storage
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream

/**
 * Project: Classera
 * Created: Jan 03, 2020
 *
 * @author Mohamed Hamdan
 */
@RequiresApi(Build.VERSION_CODES.O)
class ScreenshotImpl(private val storage: Storage) : Screenshot {

    override fun take(fragment: Fragment, view: View) {
        val window = fragment.requireActivity().window
        val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val locationOfViewInWindow = IntArray(2)
        view.getLocationInWindow(locationOfViewInWindow)

        PixelCopy.request(
            window,
            Rect(
                locationOfViewInWindow[0],
                locationOfViewInWindow[1],
                locationOfViewInWindow[0] + (view.width),
                locationOfViewInWindow[1] + (view.height)
            ),
            bitmap,
            {
                val byteArrayOutputStream = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream)
                val bitmapData: ByteArray = byteArrayOutputStream.toByteArray()
                val byteArrayInputStream = ByteArrayInputStream(bitmapData)
                if (storage.canProceedWithDownload()) {
                    storage.saveFile("${view.hashCode()}${System.currentTimeMillis()}.png", byteArrayInputStream, true)
                }
            },
            Handler()
        )
    }
}
