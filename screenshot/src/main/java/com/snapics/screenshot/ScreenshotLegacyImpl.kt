package com.snapics.screenshot

import android.graphics.Bitmap
import android.view.View
import android.view.View.MeasureSpec
import androidx.fragment.app.Fragment
import com.classera.storage.Storage
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream

/**
 * Project: Classera
 * Created: Jan 03, 2020
 *
 * @author Mohamed Hamdan
 */
@Suppress("DEPRECATION")
class ScreenshotLegacyImpl(private val storage: Storage) : Screenshot {

    override fun take(fragment: Fragment, view: View) {
        view.measure(
            MeasureSpec.makeMeasureSpec(view.width, MeasureSpec.EXACTLY),
            MeasureSpec.makeMeasureSpec(view.height, MeasureSpec.EXACTLY)
        )
        view.layout(
            view.x.toInt(),
            view.y.toInt(),
            view.x.toInt() + view.measuredWidth,
            view.y.toInt() + view.measuredHeight
        )

        view.isDrawingCacheEnabled = true
        view.buildDrawingCache(true)
        val bitmap = Bitmap.createBitmap(view.drawingCache)
        view.isDrawingCacheEnabled = false

        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream)
        val bitmapData: ByteArray = byteArrayOutputStream.toByteArray()
        val byteArrayInputStream = ByteArrayInputStream(bitmapData)
        if (storage.canProceedWithDownload()) {
            storage.saveFile("${view}${System.currentTimeMillis()}.png", byteArrayInputStream, true)
        }
    }
}
