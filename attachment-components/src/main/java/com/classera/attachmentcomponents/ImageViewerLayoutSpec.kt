package com.classera.attachmentcomponents

import android.content.Intent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.request.RequestOptions
import com.classera.attachmentcomponents.youtubeview.YoutubeActivity
import com.classera.data.glide.GlideApp
import com.facebook.litho.ClickEvent
import com.facebook.litho.Column
import com.facebook.litho.Component
import com.facebook.litho.ComponentContext
import com.facebook.litho.annotations.*
import com.stfalcon.imageviewer.StfalconImageViewer


@LayoutSpec
object ImageViewerLayoutSpec {

    @JvmStatic
    @OnCreateLayout
    fun onCreateLayout(
        componentContext: ComponentContext,
        @Prop imageUrl: String?
    ): Component {
        return Column.create(componentContext)
            .child(ImageViewerMount.create(componentContext).imageUrl(imageUrl).build())
            .clickHandler(ImageViewerLayout.onImageClicked(componentContext))
            .build()
    }

    @OnEvent(ClickEvent::class)
    fun onImageClicked(
        context: ComponentContext?,
        @FromEvent view: View?,
        @Prop imageUrl: String?
    ) {

        val activity = (view?.parent as View).context
        StfalconImageViewer.Builder<String>(
            activity,
            listOf(imageUrl)
        ) { imageView, image ->
            GlideApp.with(activity).load(image).into(imageView)
        }.show()
    }
}

