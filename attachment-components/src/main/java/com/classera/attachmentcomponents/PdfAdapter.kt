package com.classera.attachmentcomponents

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.classera.data.glide.GlideApp
import com.necistudio.pdfvigerengine.R
import com.necistudio.vigerpdf.utils.PhotoViewAttacher

/**
 * Project: Classera
 * Created: Jan 01, 2020
 *
 * @author Mohamed Hamdan
 */
class PdfAdapter : RecyclerView.Adapter<PdfAdapter.ViewHolder>() {

    private val images: MutableList<Bitmap> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView: View = LayoutInflater.from(parent.context).inflate(R.layout.pdf_item, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return images.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val imageView = holder.imageView!!
        GlideApp.with(imageView).load(images[position]).centerCrop().into(imageView)
        PhotoViewAttacher(imageView)
    }

    fun add(bitmap: Bitmap) {
        images.add(bitmap)
        notifyDataSetChanged()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var imageView: ImageView? = null

        init {
            imageView = itemView.findViewById(R.id.imgData) as ImageView
        }
    }
}
