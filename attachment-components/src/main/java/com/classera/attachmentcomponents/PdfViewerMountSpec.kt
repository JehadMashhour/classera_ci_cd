package com.classera.attachmentcomponents

import android.content.Context
import android.graphics.Bitmap
import androidx.viewpager2.widget.ViewPager2
import com.facebook.litho.ComponentContext
import com.facebook.litho.ComponentLayout
import com.facebook.litho.Size
import com.facebook.litho.annotations.MountSpec
import com.facebook.litho.annotations.OnCreateMountContent
import com.facebook.litho.annotations.OnMeasure
import com.facebook.litho.annotations.OnMount
import com.facebook.litho.annotations.OnUnmount
import com.facebook.litho.annotations.Prop
import com.facebook.litho.utils.MeasureUtils
import com.necistudio.vigerpdf.VigerPDF
import com.necistudio.vigerpdf.manage.OnResultListener

/**
 * Project: Classera
 * Created: Dec 27, 2019
 *
 * @author Mohamed Hamdan
 */
@MountSpec
object PdfViewerMountSpec {

    @OnMeasure
    fun onMeasureLayout(c: ComponentContext, layout: ComponentLayout, widthSpec: Int, heightSpec: Int, size: Size) {
        MeasureUtils.measureWithEqualDimens(widthSpec, heightSpec, size)
    }

    @OnCreateMountContent
    fun onCreateMountContent(context: Context): ViewPager2 {
        return ViewPager2(context)
    }

    @OnMount
    fun onMount(c: ComponentContext, viewPager: ViewPager2, @Prop pdfUrl: String?) {
        val adapter = PdfAdapter()
        viewPager.adapter = adapter
        VigerPDF(c.androidContext, pdfUrl).initFromFile(object : OnResultListener {

            override fun resultData(data: Bitmap) {
                adapter.add(data)
            }

            override fun progressData(progress: Int) {

            }

            override fun failed(throwable: Throwable?) {

            }
        })
    }

    @OnUnmount
    fun onUnmount(c: ComponentContext, viewPager: ViewPager2) {
        viewPager.adapter = null
    }
}
