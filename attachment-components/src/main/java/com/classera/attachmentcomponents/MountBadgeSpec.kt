package com.classera.attachmentcomponents

import android.content.Context
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.request.RequestOptions
import com.classera.data.R
import com.classera.data.glide.GlideApp
import com.classera.data.models.digitallibrary.Attachment
import com.classera.data.models.digitallibrary.AttachmentTypes
import com.facebook.litho.ComponentContext
import com.facebook.litho.ComponentLayout
import com.facebook.litho.Size
import com.facebook.litho.annotations.*
import com.facebook.litho.utils.MeasureUtils

@MountSpec
object MountBadgeSpec {

    @OnMeasure
    fun onMeasureLayout(
        c: ComponentContext,
        layout: ComponentLayout,
        widthSpec: Int,
        heightSpec: Int,
        size: Size
    ) {
        MeasureUtils.measureWithEqualDimens(widthSpec, heightSpec, size)
    }

    @OnCreateMountContent
    fun onCreateMountContent(context: Context): ImageView {
        return ImageView(context)
    }

    @OnMount
    fun onMount(c: ComponentContext, badgeImageView: ImageView, @Prop attachment: Attachment?) {

        var resId = when (attachment?.contentType) {
            AttachmentTypes.WEBSITE -> R.drawable.ic_website
            AttachmentTypes.DOCUMENT -> R.drawable.ic_doc
            AttachmentTypes.SHOW -> R.drawable.ic_presentation
            AttachmentTypes.VIDEO,
            AttachmentTypes.VIMEO,
            AttachmentTypes.YOUTUBE,
            AttachmentTypes.EXTERNAL_RESOURCES -> null
            AttachmentTypes.GAME -> R.drawable.ic_game
            AttachmentTypes.FLASH -> R.drawable.ic_flash
            AttachmentTypes.PDF -> R.drawable.ic_doc
            AttachmentTypes.IMAGE -> R.drawable.ic_image
            AttachmentTypes.AUDIO -> R.drawable.ic_headset
            AttachmentTypes.PROGRAM -> R.drawable.ic_program
            AttachmentTypes.CODE -> R.drawable.ic_code
            AttachmentTypes.UNKNOWN -> R.drawable.ic_material
            else -> R.drawable.ic_material
        }

        if (attachment?.attachmentType?.equals("bgame", true) == true ||
            attachment?.attachmentType?.equals("Bgame", true) == true ||
            attachment?.type?.equals("Bgame", true) == true
        ) {
            resId = R.drawable.ic_game
        }

        if (resId == null) {
            badgeImageView.visibility = View.GONE
        } else {
            badgeImageView.visibility = View.VISIBLE
            GlideApp.with(c.applicationContext)
                .load(resId)
                .apply(RequestOptions().centerCrop().placeholder(null).error(null))
                .into(badgeImageView)
        }
    }
}
