package com.classera.storage
import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.classera.core.utils.android.startAppSettingsIntent
import com.classera.storage.Storage.Companion.APP_FOLDER_NAME
import com.classera.storage.Storage.Companion.BUFFER_SIZE
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream

/**
 * Project: Classera
 * Created: Jan 03, 2020
 *
 * @author Mohamed Hamdan
 */
@Suppress("DEPRECATION")
class StorageLegacyImpl(private val context: Activity) : Storage {

    override fun saveFile(fileName: String?, inputStream: InputStream, showToast: Boolean): Uri? {
        val mimeType = getMimeType(fileName)
        return when {
            isPhoto(mimeType) -> {
                writeToDisk(inputStream, getPath(fileName), showToast)
            }
            isVideo(mimeType) -> {
                writeToDisk(inputStream, getPath(fileName), showToast)
            }
            isAudio(mimeType) -> {
                writeToDisk(inputStream, getPath(fileName), showToast)
            }
            else -> {
                writeToDisk(inputStream, getPath(fileName), showToast)
            }
        }
    }

    private fun isPhoto(mimeType: String?): Boolean {
        return mimeType?.startsWith("image") == true
    }

    private fun isVideo(mimeType: String?): Boolean {
        return mimeType?.startsWith("video") == true
    }

    private fun isAudio(mimeType: String?): Boolean {
        return mimeType?.startsWith("audio") == true
    }

    private fun getPath(fileName: String?): String {
        val mimeType = getMimeType(fileName)
        val directory = when {
            isPhoto(mimeType) -> {
                Environment.DIRECTORY_PICTURES
            }
            isVideo(mimeType) -> {
                Environment.DIRECTORY_MOVIES
            }
            isAudio(mimeType) -> {
                Environment.DIRECTORY_MUSIC
            }
            else -> {
                Environment.DIRECTORY_DOWNLOADS
            }
        }
        val publicDirectory = Environment.getExternalStoragePublicDirectory(directory)
        val appFolderPath = publicDirectory.absolutePath + File.separator + APP_FOLDER_NAME
        val appFolderFile = File(appFolderPath)
        if (!appFolderFile.exists()) {
            appFolderFile.mkdir()
        }
        return appFolderPath + File.separator + fileName
    }

    private fun getMimeType(fileName: String?): String? {
        val extension = fileName?.substring(fileName.lastIndexOf('.') + 1)
        val mimeTypeMap = MimeTypeMap.getSingleton()
        return mimeTypeMap.getMimeTypeFromExtension(extension)
    }

    private fun writeToDisk(inputStream: InputStream, path: String, showToast: Boolean): Uri? {
        val file = File(path)
        try {
            val outputStream = FileOutputStream(file)
            val buffer = ByteArray(BUFFER_SIZE)
            var read: Int
            while (inputStream.read(buffer).also { read = it } != -1) {
                outputStream.write(buffer, 0, read)
            }
            outputStream.flush()
            outputStream.close()
            val values = ContentValues()
            values.put(MediaStore.Images.Media.DATA, file.absolutePath)
            values.put(MediaStore.Images.Media.MIME_TYPE, getMimeType(file.nameWithoutExtension))
            context.contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        } catch (e: Exception) {
            Log.e(Storage::class.java.simpleName, e.message, e)
        } finally {
            inputStream.close()
        }

        if (showToast) {
            Toast.makeText(
                context,
                context.getString(R.string.exo_download_completed), Toast.LENGTH_LONG
            ).show()
        }

        return FileProvider.getUriForFile(context, "${context.packageName}.files_provider", file)
    }

    override fun checkIfExist(fileName: String?): Boolean {
        return File(getPath(fileName)).exists()
    }

    override fun getFileUri(fileName: String?): Uri? {
        val file = File(getPath(fileName))
        return FileProvider.getUriForFile(context, "${context.packageName}.files_provider", file)
    }

    override fun canProceedWithDownload(): Boolean {
        val permission = Manifest.permission.WRITE_EXTERNAL_STORAGE
        return when {
            ContextCompat.checkSelfPermission(
                context,
                permission
            ) == PackageManager.PERMISSION_GRANTED -> {
                true
            }
            ActivityCompat.shouldShowRequestPermissionRationale(context, permission) -> {
                AlertDialog.Builder(context)
                    .setTitle(R.string.title_storage_permission)
                    .setMessage(R.string.message_storage_permission)
                    .setPositiveButton(R.string.allow) { _, _ ->
                        context.startAppSettingsIntent()
                    }
                    .setNegativeButton(android.R.string.cancel, null)
                    .show()
                false
            }
            else -> {
                ActivityCompat.requestPermissions(
                    context,
                    arrayOf(permission),
                    REQUEST_CODE_PERMISSION
                )
                false
            }
        }
    }

    private companion object {

        private const val REQUEST_CODE_PERMISSION = 10
    }
}
