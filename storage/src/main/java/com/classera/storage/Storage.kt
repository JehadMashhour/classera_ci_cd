package com.classera.storage
import android.net.Uri
import java.io.InputStream

/**
 * Project: Classera
 * Created: Jan 04, 2020
 *
 * @author Mohamed Hamdan
 */
interface Storage {

    fun saveFile(fileName: String?, inputStream: InputStream, showToast: Boolean): Uri?

    fun checkIfExist(fileName: String?): Boolean

    fun getFileUri(fileName: String?): Uri?

    fun canProceedWithDownload(): Boolean

    companion object {

        const val APP_FOLDER_NAME = "Classera"
        const val BUFFER_SIZE = 4 * 1024
    }
}
