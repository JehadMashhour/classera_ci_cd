package com.classera.homelocation

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.homelocation.ThreeWordsWraper
import com.classera.data.models.homelocation.w3w.W3WResponse
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.network.errorhandling.tryThirdPartyResource
import com.classera.data.repositories.homelocation.HomeLocationRepository
import com.google.android.gms.maps.model.Marker

/**
 * Created by Rawan Al-Theeb on 1/7/2020.
 * Classera
 * r.altheeb@classera.com
 */
class HomeLocationViewModel(private val homeLocationRepository: HomeLocationRepository) : BaseViewModel() {

    fun getCoordinates() = liveData {
        emit(Resource.Loading(show = true))
        var resource =
            tryResource { homeLocationRepository.getW3W() }
        if (resource.isSuccess()) {
            val threeWords = resource.element<BaseWrapper<ThreeWordsWraper>>()?.data?.threeWord?.w3w
            if (threeWords?.split('.')?.count() == WORDS_COUNT) {
                resource = tryThirdPartyResource {
                    homeLocationRepository.convertToCoordinates(threeWords)
                }
                emit(resource)
            }
        } else {
            emit(resource)
        }
        emit(Resource.Loading(show = false))
    }

    fun saveCoordinates(marker: Marker?) = liveData {
        emit(Resource.Loading(show = true))

        val coordinates = "%f,%f".format(marker?.position?.latitude, marker?.position?.longitude)
        val w3wResource = tryThirdPartyResource {
            homeLocationRepository.convertTo3wa(coordinates)
        }

        if (w3wResource.isSuccess()) {
            val response = w3wResource.element<W3WResponse>()
            val threeWords = response?.words
            val resource = tryNoContentResource {
                homeLocationRepository.saveW3W(threeWords)
            }
            if (resource is Resource.Success<*>) {
                emit(Resource.Success(response))
            } else {
                emit(resource)
            }
        } else {
            emit(w3wResource)
        }

        emit(Resource.Loading(show = false))
    }

    private companion object {

        private const val WORDS_COUNT = 3
    }
}
