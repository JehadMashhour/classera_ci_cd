package com.classera.support

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.support.SupportRepository
import com.classera.data.repositories.switchschools.SwitchSchoolsRepository
import com.classera.support.add.AddSupportTicketViewModel
import javax.inject.Inject

class SupportViewModelFactory @Inject constructor(
    private val supportRepository: SupportRepository,
    private val switchSchoolsRepository: SwitchSchoolsRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            SupportViewModel::class.java -> SupportViewModel(supportRepository) as T
            AddSupportTicketViewModel::class.java -> {
                AddSupportTicketViewModel(supportRepository,switchSchoolsRepository) as T
            }
            else -> throw IllegalAccessException("There is no view model called $modelClass")
        }
    }
}
