package com.classera.support

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class SupportFragmentModule {
    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: SupportViewModelFactory
        ): SupportViewModel {
            return ViewModelProvider(fragment, factory)[SupportViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindActivity(activity: SupportFragment): Fragment
}
