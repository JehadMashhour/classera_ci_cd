package com.classera.support

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.support.SupportWrapper
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.support.SupportRepository
import kotlinx.coroutines.Dispatchers

class SupportViewModel (private val supportRepository: SupportRepository) : BaseViewModel() {

    private var supportTicketsList: MutableList<SupportWrapper?>? = mutableListOf()

    fun getSupport(
        searchText: CharSequence?,
        text: CharSequence?,
        pageNumber: Int
    ): LiveData<Resource> {
        return getSupport(searchText, text, pageNumber, pageNumber == DEFAULT_PAGE)
    }

    private fun getSupport(
        searchText: CharSequence?,
        text: CharSequence?,
        pageNumber: Int,
        showProgress: Boolean
    ) =
        liveData(Dispatchers.IO) {
            if (showProgress) {
                emit(Resource.Loading(show = true))
            }
            val resource = tryResource {
                supportRepository.getSupport(searchText, pageNumber, text = text)
            }
            if (pageNumber == DEFAULT_PAGE) {
                supportTicketsList?.clear()
            }
            supportTicketsList?.addAll(
                resource.element<BaseWrapper<List<SupportWrapper>>>()?.data ?: mutableListOf()
            )
            emit(resource)
            emit(Resource.Loading(show = false))
        }

    fun getSupportCount(): Int {
        return supportTicketsList?.size ?: 0
    }

    fun getSupportTicket(position: Int): SupportWrapper? {
        return supportTicketsList?.get(position)
    }



}
