package com.classera.support.add

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.support.SupportDataWrapper
import com.classera.data.models.switchschools.School
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.network.errorhandling.tryThirdPartyResource
import com.classera.data.repositories.support.SupportRepository
import com.classera.data.repositories.switchschools.SwitchSchoolsRepository
import kotlinx.coroutines.Dispatchers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class AddSupportTicketViewModel(
    private val supportRepository: SupportRepository,
    private val switchSchoolsRepository: SwitchSchoolsRepository
) : BaseViewModel() {

    private var selectedTypePosition = 0
    private val type = mutableListOf<SupportDataWrapper>()

    private var selectedModulesPosition = 0
    private val modules = mutableListOf<SupportDataWrapper>()

    private var selectedPriorityPosition = 0
    private val priorities = mutableListOf<SupportDataWrapper>()

    private var selectedSchoolPosition = 0
    private var schools: MutableList<School> = mutableListOf()

    fun getSchoolsList() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource =
            tryResource { switchSchoolsRepository.getSwitchSchoolsList() }

        schools.clear()
        schools.addAll(resource.element<BaseWrapper<List<School>>>()?.data ?: mutableListOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSupportSchoolsTitles(): List<String> {
        return schools.mapNotNull { it.schoolName }
    }

    fun getSelectedSupportSchoolId(): String? {
        return if (schools.isEmpty()) "-1"
        else schools[selectedSchoolPosition].id.toString()
    }

    fun getSupportTypes() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource =
            tryResource { supportRepository.getSupportTypes() }

        type.clear()
        type.addAll(resource.element<BaseWrapper<List<SupportDataWrapper>>>()?.data ?: listOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSupportTypeTitles(): List<String> {
        return type.mapNotNull { it.title }
    }

    fun getSelectedSupportTypeId(): String? {
        return if (type.isEmpty()) "-1"
        else type[selectedTypePosition].id.toString()
    }

    fun getSupportModules() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource =
            tryResource { supportRepository.getSupportModules() }

        modules.clear()
        modules.addAll(resource.element<BaseWrapper<List<SupportDataWrapper>>>()?.data ?: listOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSupportModulesTitles(): List<String> {
        return modules.mapNotNull { it.title }
    }

    fun getSelectedSupportModulesId(): String? {
        return if (modules.isEmpty()) "-1"
        else modules[selectedModulesPosition].id.toString()
    }

    fun getSupportPriorities() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))

        val resource =
            tryResource { supportRepository.getSupportPriority() }

        priorities.clear()
        priorities.addAll(resource.element<BaseWrapper<List<SupportDataWrapper>>>()?.data ?: listOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSupportPrioritiesTitles(): List<String> {
        return priorities.mapNotNull { it.title }
    }

    fun getSelectedSupportPrioritiesId(): String? {
        return if (priorities.isEmpty()) "-1"
        else priorities[selectedPriorityPosition].id.toString()
    }

    fun addSupportTicket(
        data: Map<String, Any?>,
        attachmentFileList: ArrayList<String>,
        attachmentTypeList: ArrayList<String>
    ) = liveData {
        emit(Resource.Loading(show = true))
        val paramsList = ArrayList<MultipartBody.Part>()

        dataPart(paramsList, data)
        filesParts(attachmentFileList, attachmentTypeList, paramsList)

        val resource = tryThirdPartyResource { supportRepository.addSupportTicket(paramsList) }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private fun filesParts(
        attachmentFileList: ArrayList<String>,
        attachmentFileTypeList: ArrayList<String>,
        paramsList: ArrayList<MultipartBody.Part>
    ) {
        attachmentFileList.forEachIndexed { index, _ ->
            val attachmentFile = File(attachmentFileList[index])
            val attachmentRequestBody =
                RequestBody.create(MediaType.parse(attachmentFileTypeList[index]), attachmentFile)
            paramsList.add(
                MultipartBody.Part.createFormData(
                    "attachments[$index]",
                    attachmentFile.name,
                    attachmentRequestBody
                )
            )
        }
    }

    private fun dataPart(
        paramsList: ArrayList<MultipartBody.Part>,
        data: Map<String, Any?>
    ) {
        paramsList.add(MultipartBody.Part.createFormData("url", data["url"].toString()))
        paramsList.add(MultipartBody.Part.createFormData("title", data["title"].toString()))
        paramsList.add(MultipartBody.Part.createFormData("body", data["body"].toString()))
        paramsList.add(MultipartBody.Part.createFormData("general_problem", data["general_problem"].toString()))
        paramsList.add(MultipartBody.Part.createFormData("type", data["type"].toString()))
        paramsList.add(MultipartBody.Part.createFormData("module", data["module"].toString()))
        if (data["priority"].toString().isNotBlank() || data["priority"].toString().isNotEmpty()) {
            paramsList.add(MultipartBody.Part.createFormData("priority", data["priority"].toString()))
        }
        if (data["school_id"].toString().isNotBlank() || data["school_id"].toString().isNotEmpty()) {
            paramsList.add(MultipartBody.Part.createFormData("school_id", data["school_id"].toString()))
        }
    }
}

