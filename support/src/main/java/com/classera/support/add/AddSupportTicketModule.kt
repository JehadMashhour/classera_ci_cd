package com.classera.support.add

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.support.SupportViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class AddSupportTicketModule{
    @Module

    companion object {
        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factoryAdd: SupportViewModelFactory
        ): AddSupportTicketViewModel {
            return ViewModelProvider(fragment, factoryAdd)[AddSupportTicketViewModel::class.java]
        }

    }

    @Binds
    abstract fun bindActivity(fragmentAdd: AddSupportTicketFragment): Fragment
}
