package com.classera.support

import android.view.ViewGroup
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.support.databinding.RowSupportBinding

class SupportAdapter (
    private val viewModel: SupportViewModel
) : BasePagingAdapter<SupportAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowSupportBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getSupportCount()
    }

    inner class ViewHolder(binding: RowSupportBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowSupportBinding> {
                this.supportTickets = viewModel.getSupportTicket(position)
            }
        }
    }
}
