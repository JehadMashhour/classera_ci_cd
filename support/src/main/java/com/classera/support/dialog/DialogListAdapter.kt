package com.classera.support.dialog

import android.content.ActivityNotFoundException
import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.storage.Storage
import com.classera.support.R
import com.classera.support.databinding.RowSupportAttachmentBinding
import com.github.abdularis.buttonprogress.DownloadButtonProgress
import com.koushikdutta.ion.Ion
import java.io.File

class DialogListAdapter(
    private val commentAttachment: List<Attachment>,
    private val storage: Storage
) : BasePagingAdapter<DialogListAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowSupportAttachmentBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return commentAttachment.size
    }

    inner class ViewHolder(binding: RowSupportAttachmentBinding) : BaseBindingViewHolder(binding) {

        private var linearLayoutDownload: LinearLayout? = null
        private var buttonDownload: View? = null

        init {
            linearLayoutDownload = itemView.findViewById(R.id.linear_layout_row_digital_library_download)
            buttonDownload = itemView.findViewById(R.id.button_activity_attachment_details_download)
        }

        override fun bind(position: Int) {
            bind<RowSupportAttachmentBinding> {
                this.attachment = commentAttachment[position]
            }

            linearLayoutDownload?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    openFileIfExistOrStartDownload(clickedPosition)
                }
            }

            buttonDownload?.setOnClickListener {
                linearLayoutDownload?.callOnClick()
            }
        }

        private fun openFileIfExistOrStartDownload(position: Int) {
            if (storage.checkIfExist(commentAttachment[position].name)) {
                openFile(position)
            } else {
                startDownload(position)
            }
        }

        private fun openFile(position: Int) {
            val uri = storage.getFileUri(commentAttachment[position].name)
            val mimeTypeMap = MimeTypeMap.getSingleton()
            val mimeType = mimeTypeMap.getMimeTypeFromExtension(File(uri.toString()).extension)

            val intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(uri, mimeType)
            intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

            try {
                context?.startActivity(intent)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(context, context?.getString(R.string.there_is_no_application), Toast.LENGTH_LONG).show()
            }
        }

        private fun startDownload(position: Int) {
            if (storage.canProceedWithDownload()) {
                val attachment = commentAttachment[position]
                val url = attachment.url
                attachment.state = DownloadButtonProgress.STATE_INDETERMINATE
                val downloadFuture = Ion.with(context)
                    .load(url)
                    .progress { downloaded, total ->
                        if (attachment.state != DownloadButtonProgress.STATE_DETERMINATE) {
                            attachment.state = DownloadButtonProgress.STATE_DETERMINATE
                        }
                        attachment.progress =
                            ((downloaded.toFloat() / total.toFloat()) * 100f).toInt()
                    }
                    .asInputStream()
                downloadFuture?.setCallback { error, result ->
                    attachment.state = DownloadButtonProgress.STATE_FINISHED
                    if (error == null) {
                        storage.saveFile(attachment.name, result, true)
                    }
                }
            }
        }
    }
}
