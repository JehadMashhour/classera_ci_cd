package com.classera.support.details

import android.app.Activity
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.support.R
import com.classera.support.databinding.RowSupportCommentBinding
import com.classera.support.dialog.CustomAlertDialog


class SupportDetailsCommentsAdapter(
    private val activity: Activity,
    private val viewModel: SupportDetailsViewModel
) : BaseAdapter<SupportDetailsCommentsAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowSupportCommentBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getCommentCount()
    }

    inner class ViewHolder(binding: RowSupportCommentBinding) : BaseBindingViewHolder(binding) {

        private var imageViewError: ImageView? = null
        private var linearLayoutDownload: LinearLayout? = null

        init {
            imageViewError = itemView.findViewById(R.id.image_view_row_support_comment_error)
            linearLayoutDownload = itemView.findViewById(R.id.linear_layout_row_support_download)
        }

        override fun bind(position: Int) {
            bind<RowSupportCommentBinding> {
                comment = viewModel.getComment(position)
            }

            imageViewError?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    viewModel.onCommentRetryClicked(clickedPosition)
                }
            }

            linearLayoutDownload?.setOnClickListener {
                val dialog = CustomAlertDialog(activity, viewModel.getCommentAttachmentList(position)!!)
                dialog.setTitle(context!!.getString(R.string.download_attachment))
                dialog.show()
            }
        }
    }
}
