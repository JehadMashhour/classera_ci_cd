package com.classera.schedule

import android.graphics.Color
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.schedule.Schedule
import com.classera.data.models.schedule.ScheduleSlot
import com.classera.data.models.schedule.local.LocalSchedule
import com.classera.data.models.schedule.local.LocalScheduleDay
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.schedule.ScheduleRepository
import kotlinx.coroutines.Dispatchers
import kotlin.random.Random

/**
 * Project: Classera
 * Created: Dec 9, 2019
 * @author Odai Nazzal
 */
class ScheduleViewModel(private val scheduleRepository: ScheduleRepository) : BaseViewModel() {

    private var days: MutableList<LocalScheduleDay?>? = mutableListOf()

    fun getSchedules() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource =
            tryResource { scheduleRepository.getStudentSchedule() }
        val schedules = resource.element<BaseWrapper<List<Schedule>>>()?.data
        val days = schedules?.flatMap { it.timeSlots ?: listOf() }?.map { it?.day?.capitalize() }?.distinct()

        days?.forEach { name ->
            val schedule = schedules.map { schedule ->
                val slotName = schedule.timeSlotName
                val slots = schedule.timeSlots
                    ?.filter { it?.day?.equals(name, true) == true }
                    ?.flatMap { it?.slots ?: listOf() }

                slots?.forEach {
                    val randomR = Random.nextInt(MAX_RGB)
                    val randomG = Random.nextInt(MAX_RGB)
                    val randomB = Random.nextInt(MAX_RGB)
                    it?.color = Color.rgb(randomR, randomG, randomB)
                }

                LocalSchedule(slotName, slots)
            }
            this@ScheduleViewModel.days?.add(LocalScheduleDay(name, schedule))
        }

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getDaysCount(): Int {
        return days?.size ?: 0
    }

    fun getDay(position: Int): String? {
        return days?.get(position)?.name
    }

    fun getScheduleCountBy(dayPosition: Int): Int {
        return days?.get(dayPosition)?.data?.size ?: 0
    }

    fun getScheduleBy(dayPosition: Int, position: Int): LocalSchedule? {
        return days?.get(dayPosition)?.data?.get(position)
    }

    fun getSlotCountBy(dayPosition: Int, schedulePosition: Int): Int {
        return days?.get(dayPosition)?.data?.get(schedulePosition)?.schedule?.size ?: 0
    }

    fun getSlotBy(dayPosition: Int, schedulePosition: Int, position: Int): ScheduleSlot? {
        return days?.get(dayPosition)?.data?.get(schedulePosition)?.schedule?.get(position)
    }

    private companion object {

        private const val MAX_RGB = 255
    }
}
