package com.classera.schedule

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.schedule.databinding.RowScheduleBinding
import com.classera.schedule.slots.SlotsAdapter

/**
 * Created by Odai Nazzal on 12/28/2019.
 * Classera
 *
 * o.nazzal@classera.com
 */
class SchedulesAdapter(
    private val viewModel: ScheduleViewModel,
    private val selectedDayPosition: Int
) : BaseAdapter<SchedulesAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowScheduleBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getScheduleCountBy(selectedDayPosition)
    }

    inner class ViewHolder(binding: RowScheduleBinding) : BaseBindingViewHolder(binding) {

        private var recyclerView: RecyclerView? = null

        init {
            recyclerView = itemView.findViewById(R.id.recycler_view_row_schedule)
        }

        override fun bind(position: Int) {
            bind<RowScheduleBinding> {
                this.schedule = viewModel.getScheduleBy(selectedDayPosition, position)
            }
            recyclerView?.adapter = SlotsAdapter(viewModel, selectedDayPosition, position)
        }
    }
}
