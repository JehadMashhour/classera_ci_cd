package com.classera.schedule

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
@Module
abstract class ScheduleFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: ScheduleViewModelFactory
        ): ScheduleViewModel {
            return ViewModelProvider(fragment, factory)[ScheduleViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: ScheduleFragment): Fragment
}
