package com.classera.schedule.days

import android.view.ViewGroup
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BaseSingleSelectionAdapter
import com.classera.schedule.ScheduleViewModel
import com.classera.schedule.databinding.RowScheduleWeekDayBinding

/**
 * Created by Odai Nazzal on 12/28/2019.
 * Classera
 * o.nazzal@classera.com
 */
class DaysAdapter(
    private val viewModel: ScheduleViewModel
) : BaseSingleSelectionAdapter<DaysAdapter.ViewHolder>() {

    override var selectedPosition: Int = 0

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowScheduleWeekDayBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getDaysCount()
    }

    inner class ViewHolder(binding: RowScheduleWeekDayBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowScheduleWeekDayBinding> {
                this.selectedPosition = this@DaysAdapter.selectedPosition
                this.currentPosition = position
                this.text = viewModel.getDay(position)
            }
        }
    }
}
