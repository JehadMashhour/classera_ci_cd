package com.classera.reportcards

import android.app.Application
import androidx.browser.customtabs.CustomTabsIntent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.reportcards.ReportCardsRepository
import com.classera.storage.Storage
import javax.inject.Inject

class ReportCardsViewModelFactory @Inject constructor(
    private val reportCardsRepository: ReportCardsRepository,
    private val storage: Storage,
    private val application: Application,
    private val prefs: Prefs,
    private val customTabsIntent: CustomTabsIntent
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ReportCardsViewModel(application, reportCardsRepository, storage, prefs, customTabsIntent) as T
    }
}
