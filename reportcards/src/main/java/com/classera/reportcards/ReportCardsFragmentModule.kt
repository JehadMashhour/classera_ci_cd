package com.classera.reportcards

import android.os.Build
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.storage.Storage
import com.classera.storage.StorageImpl
import com.classera.storage.StorageLegacyImpl
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class ReportCardsFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: ReportCardsViewModelFactory
        ): ReportCardsViewModel {
            return ViewModelProvider(fragment, factory)[ReportCardsViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideStorage(fragment: Fragment): Storage {
            return if (Build.VERSION.SDK_INT == Build.VERSION_CODES.Q) {
                StorageImpl(fragment.requireContext())
            } else {
                StorageLegacyImpl(fragment.requireActivity())
            }
        }
    }

    @Binds
    abstract fun bindFragment(fragment: ReportCardsFragment): Fragment
}
