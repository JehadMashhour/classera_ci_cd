package com.classera.reportcards.reportcardsdetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.reportcards.ReportCardsRepository

class ReportCardDetailsViewModelFactory(
    private val reportCardsDetailsFragmentArgs: ReportCardDetailsFragmentArgs,
    private val reportCardsRepository: ReportCardsRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ReportCardDetailsViewModel(reportCardsDetailsFragmentArgs, reportCardsRepository) as T
    }
}
