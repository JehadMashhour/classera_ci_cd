package com.classera.reportcards.reportcardsdetails

import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.ProgressBar
import com.classera.attachmentcomponents.ReportCardLayout
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseBindingFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.BaseWrapper
import com.classera.data.models.reportcards.reportcardsdetails.ReportCardDetails
import com.classera.data.network.errorhandling.Resource
import com.classera.reportcards.R
import com.classera.reportcards.databinding.FragmentReportCardDetailsBinding
import com.facebook.litho.ComponentContext
import com.facebook.litho.LithoView
import javax.inject.Inject

@Screen("Report Card Details")
class ReportCardDetailsFragment : BaseBindingFragment() {

    @Inject
    lateinit var viewModel: ReportCardDetailsViewModel

    private var args: ReportCardDetailsFragmentArgs? = null

    private var progressBar: ProgressBar? = null
    private var errorView: ErrorView? = null
    private var frameLayoutContent: FrameLayout? = null

    override val layoutId: Int = R.layout.fragment_report_card_details

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        args = ReportCardDetailsFragmentArgs.fromBundle(arguments!!)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        getDetails()
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_report_card_details)
        errorView = view?.findViewById(R.id.error_view_fragment_report_card_details)
        frameLayoutContent = view?.findViewById(R.id.frame_layout_fragment_report_card_details_content)
    }

    private fun getDetails() {
        viewModel.getReportCardDetails().observe(this, this::handleResource)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource(resource as Resource.Success<BaseWrapper<ReportCardDetails>>)
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
        } else {
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleSuccessResource(success: Resource.Success<BaseWrapper<ReportCardDetails>>) {
        success.data?.data?.let {
            bind<FragmentReportCardDetailsBinding> {
                this?.report = it
                val componentContext = ComponentContext(context)
                val reportCardLayoutSpec = ReportCardLayout.create(componentContext)
                    .type(args?.type)
                    .id(args?.id)
                    .reportCardDetails(it)
                    .build()
                val view = LithoView.create(componentContext, reportCardLayoutSpec)
                frameLayoutContent?.addView(view)
            }
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener(::getDetails)
    }
}
