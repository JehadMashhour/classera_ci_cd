package com.classera.reportcards.reportcardsdetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.reportcards.ReportCardsRepository
import kotlinx.coroutines.Dispatchers

class ReportCardDetailsViewModel(
    private val reportCardsDetailsFragmentArgs: ReportCardDetailsFragmentArgs,
    private val reportCardsRepository: ReportCardsRepository
) : BaseViewModel() {

    fun getReportCardDetails(): LiveData<Resource> = liveData(Dispatchers.IO) {
        val url = reportCardsDetailsFragmentArgs.type.url
        val reportId = reportCardsDetailsFragmentArgs.id
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            reportCardsRepository.getReportCardDetails(
                url,
                reportId
            )
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }
}
