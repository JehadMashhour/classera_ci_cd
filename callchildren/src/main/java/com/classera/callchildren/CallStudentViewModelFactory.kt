package com.classera.callchildren


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.callchildren.bottomsheet.CallStudentBottomSheetViewModel
import com.classera.callchildren.driver.DriverCallStudentViewModel
import com.classera.callchildren.guardian.GuardianCallStudentViewModel
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.callchild.CallChildRepository
import java.net.Socket
import javax.inject.Inject

class CallStudentViewModelFactory @Inject constructor(
    private val callChildRepository: CallChildRepository,
    private val prefs: Prefs
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            GuardianCallStudentViewModel::class.java -> {
                GuardianCallStudentViewModel(callChildRepository, prefs) as T
            }
            DriverCallStudentViewModel::class.java -> {
                DriverCallStudentViewModel(callChildRepository, prefs) as T
            }
            CallStudentBottomSheetViewModel::class.java -> {
                CallStudentBottomSheetViewModel(callChildRepository) as T
            }
            else -> {
                throw IllegalAccessException("There is no view model called $modelClass")
            }
        }
    }
}
