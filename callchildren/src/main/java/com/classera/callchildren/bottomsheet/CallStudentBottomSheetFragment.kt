package com.classera.callchildren.bottomsheet

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.classera.callchild.R
import com.classera.callchild.databinding.FragmentCallStudentBottomSheetBinding
import com.classera.core.fragments.BaseBottomSheetDialogBindingFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.BaseWrapper
import com.classera.data.models.callchildren.BaseStudent
import com.classera.data.models.callchildren.CallStatus
import com.classera.data.models.callchildren.CallStatusTypes
import com.classera.data.network.errorhandling.Resource
import com.classera.utils.views.dialogs.messagebottomdialog.MessageBottomSheetFragment
import java.util.*
import javax.inject.Inject
import kotlin.concurrent.fixedRateTimer


/**
 * Project: Classera
 * Created: 7/14/2021
 *
 * @author Jehad Abdalqader
 */
class CallStudentBottomSheetFragment : BaseBottomSheetDialogBindingFragment(),
    CallStudentBottomSheetHandler {
    override val layoutId: Int = R.layout.fragment_call_student_bottom_sheet

    @Inject
    lateinit var viewModel: CallStudentBottomSheetViewModel

    private var progressBarButton: ProgressBar? = null

    private var button: Button? = null

    val args by navArgs<CallStudentBottomSheetFragmentArgs>()


    override fun enableDependencyInjection(): Boolean {
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findViews()
        bindData()
        getCallStatusFrequently()
    }


    private fun findViews() {
        progressBarButton = view?.findViewById(R.id.fragment_call_student_progress_bar)
        button = view?.findViewById(R.id.fragment_call_student_bttn_confirm_left)
    }

    private fun bindData() {
        bind<FragmentCallStudentBottomSheetBinding> {
            this?.callStudentBottomSheetHandler = this@CallStudentBottomSheetFragment
            this?.callStudentBottomSheetFragmentArgs = args
        }
    }


    private fun getCallStatusFrequently() {
        if (args.baseStudent.isCalling) {
            fixedRateTimer("timer", false, INITIAL_DELAY, TIME_PERIOD) {
                activity?.runOnUiThread {
                    getCallStatus()
                }
            }
        }
    }


    override fun onButtonClicked(view: View) {
        when ((view as Button).text.toString()) {
            getString(R.string.cancel) -> {
                changeCallStatus(
                    CallStatusTypes.CANCELED.id.toString()
                )
            }

            getString(R.string.arrived) -> {
                changeCallStatus(
                    CallStatusTypes.ARRIVED.id.toString()
                )
            }
        }

    }

    private fun setResult(errorMessage: String? = null) {
        setFragmentResult(
            DEFAULT_REQUEST_KEY,
            bundleOf(
                ERROR_MESSAGE to errorMessage,
                BASE_STUDENT to args.baseStudent
            )
        )
    }


    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        setFragmentResult(
            DISMISS_REQUEST_KEY,
            bundleOf(IS_DISMISSED to true)
        )
    }


    private fun changeCallStatus(status: String?) {
        viewModel.changeCallStatus(args.baseStudent.requestId, status).observe(this, {
            when (it) {
                is Resource.Loading -> {
                    if (it.show) {
                        showButtonLoadingUI()
                    }
                }
                is Resource.Success<*> -> {
                    getCallStatus()
                }
                is Resource.Error -> {
                    findNavController().navigateUp()
                    setResult(it.error.message ?: it.error.cause?.message)
                }
            }
        })
    }


    private fun getCallStatus() {
        viewModel.getCallStatus(args.baseStudent.requestId).observe(this, {
            when (it) {
                is Resource.Success<*> -> {
                    it.element<BaseWrapper<CallStatus>>()?.data?.let { t ->

                        if (args.baseStudent.status != t.status) {
                            args.baseStudent.status = t.status
                            setResult()
                        }

                        viewDataBinding?.invalidateAll()

                        if (!args.baseStudent.isCalling && !args.baseStudent.isDismissed) {
                            findNavController().navigateUp()

                        }

                    }
                }
                is Resource.Error -> {
                    findNavController().navigateUp()
                    setResult(it.error.message ?: it.error.cause?.message)
                }
            }
        })
    }


    private fun showButtonLoadingUI() {
        progressBarButton?.visibility = View.VISIBLE
        button?.text = ""
        button?.isEnabled = false
    }

    companion object {
        private const val DEFAULT_REQUEST_KEY = "default_request_key"
        private const val DISMISS_REQUEST_KEY = "dismiss_request_key"
        private const val ERROR_MESSAGE = "error_message"
        private const val BUTTON_CLICKED_NAME = "buttonClicked"
        private const val BASE_STUDENT = "BaseStudent"
        private const val IS_DISMISSED = "is_dismissed"
        private const val TIME_PERIOD: Long = 25000
        private const val INITIAL_DELAY: Long = 0L

        fun show(
            fragment: Fragment,
            navDirections: NavDirections,
            dismissListener: ((dismissed: Boolean) -> Unit)? = null,
            listener: ((errorMessage: String?, baseStudent: BaseStudent) -> Unit)? = null
        ) {

            fragment.setFragmentResultListener(DEFAULT_REQUEST_KEY) { _, bundle ->
                listener?.invoke(
                    bundle.get(ERROR_MESSAGE) as String?,
                    bundle.get(BASE_STUDENT) as BaseStudent
                )
            }

            fragment.setFragmentResultListener(DISMISS_REQUEST_KEY) { _, bundle ->
                dismissListener?.invoke(bundle.get(IS_DISMISSED) as Boolean)
            }

            fragment.findNavController().navigate(navDirections)
        }
    }

}
