package com.classera.callchildren.guardian

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.classera.callchild.R
import com.classera.callchild.databinding.RowCallStudentListBinding
import com.classera.callchildren.base.BaseCallStudentListAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.google.android.material.button.MaterialButton


class GuardianCallStudentListAdapter(private val viewModel: GuardianCallStudentViewModel) :
    BaseCallStudentListAdapter<GuardianCallStudentListAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowCallStudentListBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getChildListCount()
    }

    inner class ViewHolder(binding: RowCallStudentListBinding) : BaseBindingViewHolder(binding) {

        private var arrivedButton: MaterialButton? = null

        init {
            arrivedButton = itemView.findViewById(R.id.fragment_call_student_arrived_button)
        }


        override fun bind(position: Int) {
            arrivedButton?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    viewModel.getChild(position).isLoading = true
                    notifyItemChanged(position)

                    onItemClicked(it, this)
                }
            }

            bind<RowCallStudentListBinding> {
                student = viewModel.getChild(position)
            }
        }
    }
}
