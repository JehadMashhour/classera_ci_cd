package com.classera.callchildren.guardian

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.callchildren.CallStudentViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class GuardianCallStudentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: CallStudentViewModelFactory
        ): GuardianCallStudentViewModel {
            return ViewModelProvider(fragment, factory)[GuardianCallStudentViewModel::class.java]
        }


    }

    @Binds
    abstract fun bindFragment(fragment: GuardianCallStudentFragment): Fragment
}
