package com.classera.assignments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.browser.customtabs.CustomTabsIntent
import androidx.lifecycle.observe
import androidx.navigation.fragment.navArgs
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Screen
import com.classera.core.custom.views.BaseRecyclerView
import com.classera.core.custom.views.ErrorView
import com.classera.core.custom.views.QuickFilterView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.onDebounceQueryTextChange
import com.classera.data.BuildConfig
import com.classera.data.models.assignments.AssignmentStatus
import com.classera.data.models.user.UserRole
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.prefs.Prefs
import com.classera.filter.FilterActivity
import javax.inject.Inject

@Suppress("ComplexMethod")
@Screen("Assignments")
class AssignmentsFragment : BaseFragment() {

    @Inject
    lateinit var prefs: Prefs

    @Inject
    lateinit var viewModel: AssignmentsViewModel

    @Inject
    lateinit var customTabsIntent: CustomTabsIntent

    private val args by navArgs<AssignmentsFragmentArgs>()

    private var menuItemFilter: MenuItem? = null
    private var searchView: SearchView? = null
    private var progressBar: ProgressBar? = null
    private var recyclerView: BaseRecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var adapter: AssignmentsAdapter? = null
    private var errorView: ErrorView? = null
    private var filterView: QuickFilterView? = null

    private var menuItem: Menu? = null

    override val layoutId: Int = R.layout.fragment_assignments

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initFilter()
        initListeners()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_assignments, menu)
        this.menuItem = menu
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val searchMenuItem = menu.findItem(R.id.item_menu_fragment_assignments_search)
        menuItemFilter = menu.findItem(R.id.item_menu_fragment_assignments_filter)
        searchView = (searchMenuItem.actionView as? SearchView?)
        searchView?.onDebounceQueryTextChange {
            getAssignments()
        }

        searchView?.setOnCloseListener {
            getAssignments()
            return@setOnCloseListener false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_menu_fragment_assignments_filter -> {
                FilterActivity.start(this, viewModel.getFilters(), viewModel.selectedFilter)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (FilterActivity.isDone(requestCode, resultCode, data)) {
            val filter = FilterActivity.getSelectedFilter(data)
            swipeRefreshLayout?.isRefreshing = true
            viewModel.getAssignmentsListByFilter(filter)?.observe(this, ::handleResource)
            menuItem?.getItem(1)?.setIcon(R.drawable.menu_item_with_badge)
        } else if (FilterActivity.isAll(requestCode, resultCode, data)) {
            viewModel.selectedFilter = null
            adapter?.resetPaging()
            getAssignments()
            menuItem?.getItem(1)?.setIcon(R.drawable.ic_filter)
        }
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_assignments)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_assignments)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_assignments)
        errorView = view?.findViewById(R.id.error_view_fragment_assignments)
        filterView = view?.findViewById(R.id.filter_view_fragment_assignments)
    }

    private fun initFilter() {
        filterView?.setAdapter(
            R.array.assignments_filter_entries,
            R.array.assignments_filter_entry_values
        )
        filterView?.setSelectedFilter(args.selectedFilter)
        filterView?.setOnFilterSelectedListener {
            adapter?.resetPaging()
            recyclerView?.scrollToPosition(0)
            getAssignments(1)
        }
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            adapter?.resetPaging()
            getAssignments()
        }
    }

    private fun getAssignments(pageNumber: Int = DEFAULT_PAGE) {
        viewModel.getAssignmentsList(
            filterView?.getSelectedFilterKey(),
            searchView?.query,
            pageNumber
        )
            .observe(this, this::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        menuItemFilter?.isEnabled = true
        if (adapter == null) {
            initAdapter()
            initAdapterListeners()
        } else {
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()
    }

    private fun initAdapter() {
        adapter = AssignmentsAdapter(viewModel)
        recyclerView?.adapter = adapter
        adapter?.setOnLoadMoreListener(::getAssignments)
    }

    private fun initAdapterListeners() {
        adapter?.setOnItemClickListener { view, position ->
            when (view.id) {
                R.id.button_row_assignment_second_action -> {
                    if (prefs.userRole != UserRole.TEACHER)
                        handleOtherStatuses(position, "true")
                }
                R.id.button_row_assignment_action -> {
                    handleItemClicked(position)
                }
            }
            // handleItemClicked(position)
        }
    }

    private fun handleItemClicked(position: Int) {
        when (viewModel.getAssignment(position)?.status) {
            AssignmentStatus.MISSED -> {
                handleAssignmentErrorStatus(
                    getString(R.string.title_dialog_missed_exam),
                    getString(R.string.message_dialog_missed_exam)
                )
            }
            AssignmentStatus.NOT_ATTEMPTED -> {
                handleAssignmentErrorStatus(
                    getString(R.string.title_dialog_not_attempted_exam),
                    getString(R.string.message_dialog_not_attempted_exam)
                )
            }
            AssignmentStatus.SUBMISSION_GRADE -> {
                if (prefs.userRole != UserRole.TEACHER)
                    handleOtherStatuses(position, "true")
            }
            AssignmentStatus.START_AGAIN -> {
                if (prefs.userRole == UserRole.STUDENT) {
                    handleOtherStatuses(position, "false")
                } else {
                    showMessageYourNotAllowed()
                }
            }
            else -> {
                if (prefs.userRole == UserRole.STUDENT) {
                    handleOtherStatuses(position, "false")
                } else {
                    showMessageYourNotAllowed()
                }
            }
        }
    }

    private fun showMessageYourNotAllowed() {
        Toast.makeText(
            requireContext(), getString(R.string.your_not_allowed_message),
            Toast.LENGTH_LONG
        ).show()
    }

    private fun handleAssignmentErrorStatus(title: String, message: String) {
        AlertDialog.Builder(requireContext())
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok, null)
            .show()
    }

    private fun handleOtherStatuses(position: Int, type: String) {
        val assignment = viewModel.getAssignment(position)
        customTabsIntent.launchUrl(
            context,
            Uri.parse(
                getExamsLink().format(
                    assignment?.id,
                    prefs.authenticationToken,
                    type,
                    prefs.childId
                )
            )
        )
    }

    private fun handleErrorResource(resource: Resource.Error) {
        if (viewModel.getAssignmentsCount() == 0) {
            Toast.makeText(context, resource.error.message, Toast.LENGTH_LONG).show()
            errorView?.visibility = View.VISIBLE
            errorView?.setError(resource)
            errorView?.setOnRetryClickListener { getAssignments() }
            adapter?.finishLoading()
            return
        }
    }

    fun getExamsLink(): String =
        "${BuildConfig.flavorData.webBaseUrl}/courses/redirect_from_mobile?" +
                "assignment_id=%s&Authtoken=%s&grade=%s&user_id=%s"

    override fun onDestroyView() {
        progressBar = null
        recyclerView = null
        adapter = null
        errorView = null
        filterView = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }
}
