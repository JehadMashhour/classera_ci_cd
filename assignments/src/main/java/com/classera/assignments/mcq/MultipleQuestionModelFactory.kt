package com.classera.assignments.mcq

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.assignments.AssignmentsRepository
import javax.inject.Inject

/**
 * Created on 18/05/2020.
 * Classera
 *
 * @author Saeed Halawani
 */
class MultipleQuestionModelFactory @Inject constructor(
    private val assignmentsRepository: AssignmentsRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MultipleQuestionViewModel(assignmentsRepository) as T
    }
}
