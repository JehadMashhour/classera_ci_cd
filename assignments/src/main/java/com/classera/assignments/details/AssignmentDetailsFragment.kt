package com.classera.assignments.details

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.RatingBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.navigation.fragment.findNavController
import com.airbnb.lottie.LottieAnimationView
import com.classera.assignments.R
import com.classera.assignments.databinding.FragmentAssignmentDetailsBinding
import com.classera.assignments.rating.RatingBottomSheet
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseBindingFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.BaseWrapper
import com.classera.data.models.assignments.Assignment
import com.classera.data.models.assignments.AssignmentStatus
import com.classera.data.models.assignments.QuestionDraft
import com.classera.data.models.user.UserRole
import com.classera.data.network.errorhandling.Resource
import com.classera.data.prefs.Prefs
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Feb 16, 2020
 *
 * @author Mohamed Hamdan
 */
@Screen(" Assignment Details")
class AssignmentDetailsFragment : BaseBindingFragment() {

    @Inject
    lateinit var viewModel: AssignmentDetailsViewModel

    @Inject
    lateinit var prefs: Prefs

    private var errorView: ErrorView? = null
    private var progressBar: ProgressBar? = null
    private var linearLayoutContent: LinearLayout? = null
    private var linearLayoutRating: LinearLayout? = null
    private var ratingBar: RatingBar? = null
    private var animationView: LottieAnimationView? = null
    private var textViewCongrats: TextView? = null
    private var buttonAction: Button? = null

    override val layoutId: Int = R.layout.fragment_assignment_details

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
        getDetails()
    }

    private fun findViews() {
        linearLayoutContent = view?.findViewById(R.id.linear_layout_fragment_assignment_details_content)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_assignment_details)
        errorView = view?.findViewById(R.id.error_view_fragment_assignment_details)
        linearLayoutRating = view?.findViewById(R.id.linear_layout_fragment_assignment_details_rating)
        ratingBar = view?.findViewById(R.id.rating_bar_fragment_assignment_details_rating)
        animationView = view?.findViewById(R.id.animation_view_fragment_assignment_details)
        textViewCongrats = view?.findViewById(R.id.text_view_fragment_assignment_details_congrats)
        buttonAction = view?.findViewById(R.id.button_fragment_assignment_details_action)

        if (prefs.userRole == UserRole.GUARDIAN || viewModel.getAssignment()?.status == AssignmentStatus.LAUNCH) {
            linearLayoutRating?.visibility = View.GONE
        }
    }

    private fun initListeners() {
        errorView?.setOnRetryClickListener { getDetails() }

        linearLayoutRating?.setOnClickListener {
            RatingBottomSheet.show(childFragmentManager) {
            }
        }

        animationView?.setOnClickListener {
            animationView?.visibility = View.GONE
            textViewCongrats?.visibility = View.GONE
        }

        buttonAction?.setOnClickListener {
            if (viewModel.getAssignment()?.status == AssignmentStatus.LAUNCH || viewModel.canShowAnswers()) {
                startExamOrShowAnswers()
            } else {
                showAnswersAccessDeniedMessage()
            }
        }
    }

    private fun startExamOrShowAnswers() {
        //TODO This commented to be added later
//        val assignment = viewModel.getAssignment()?.copy()
//        val direction = AssignmentDetailsFragmentDirections.solveAssignmentDirection(assignment?.title, assignment)
//        findNavController().navigate(direction)
    }

    private fun showAnswersAccessDeniedMessage() {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.title_dialog_answers_access_denied)
            .setMessage(R.string.message_dialog_answers_access_denied)
            .setPositiveButton(android.R.string.ok, null)
            .show()
    }

    private fun getDetails() {
        viewModel.getAssignmentDetails().observe(this, ::handleResource)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource(resource as Resource.Success<BaseWrapper<Assignment>>)
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            linearLayoutContent?.visibility = View.GONE
        } else {
            linearLayoutContent?.visibility = View.VISIBLE
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleSuccessResource(resource: Resource.Success<BaseWrapper<Assignment>>) {
        bind<FragmentAssignmentDetailsBinding> {
            this?.assignment = resource.data?.data
        }
        if (viewModel.canShowAnswers()) {
            buttonAction?.setText(R.string.button_show_answers)
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        errorView = null
        progressBar = null
        linearLayoutContent = null
        linearLayoutRating = null
        ratingBar = null
        animationView = null
        textViewCongrats = null
        buttonAction = null
    }
}
