package com.classera.assignments.details

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.classera.data.models.assignments.Assignment
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Feb 16, 2020
 *
 * @author Mohamed Hamdan
 */
@Module
abstract class AssignmentDetailsFragmentModule {
    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: AssignmentDetailsModelFactory
        ): AssignmentDetailsViewModel {
            return ViewModelProvider(fragment, factory)[AssignmentDetailsViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideAssignment(fragment: Fragment): Assignment? {
            val args by fragment.navArgs<AssignmentDetailsFragmentArgs>()
            return args.assignment
        }
    }

    @Binds
    abstract fun bindFragment(fragment: AssignmentDetailsFragment): Fragment
}
