package com.classera.assignments.solve.types.adapters

import android.view.ViewGroup
import com.classera.assignments.R
import com.classera.assignments.databinding.RowFillBlankBinding
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.utils.android.onDebounceTextChange
import com.classera.core.utils.android.onTextChanged
import com.classera.data.models.assignments.SubQuestions
import com.google.android.material.textfield.TextInputEditText

/**
 * Project: Classera
 * Created: Mar 10, 2020
 *
 * @author Mohamed Hamdan
 */
class FillBlankAdapter(private val questions: SubQuestions?) :
    BaseAdapter<FillBlankAdapter.ViewHolder>() {

    private var onItemClickedListener: ((text: String, position: Int) -> Unit)? = null

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowFillBlankBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return questions?.getAllQuestions()?.size ?: 0
    }


    fun setonItemClickedListener(onItemClickedListener: (text: String, position: Int) -> Unit) {
        this.onItemClickedListener = onItemClickedListener
    }

    inner class ViewHolder(binding: RowFillBlankBinding) : BaseBindingViewHolder(binding) {

        private var autoCompleteTextViewAnswers: TextInputEditText? = null

        init {
            autoCompleteTextViewAnswers = itemView.findViewById(R.id.auto_complete_text_view_row_matching_answers)
        }

        override fun bind(position: Int) {
            autoCompleteTextViewAnswers?.onDebounceTextChange {
                onItemClickedListener?.invoke(it.toString(), position)
            }

            bind<RowFillBlankBinding> {
                this.position = position
                this.question = questions?.getAllQuestions()?.get(position)
            }
        }
    }

}
