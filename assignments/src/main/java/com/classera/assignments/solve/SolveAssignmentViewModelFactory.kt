package com.classera.assignments.solve

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.assignments.AssignmentsRepository
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Feb 18, 2020
 *
 * @author Mohamed Hamdan
 */
class SolveAssignmentViewModelFactory @Inject constructor(
    private val assignmentsRepository: AssignmentsRepository,
    private val solveAssignmentFragmentArgs: SolveAssignmentFragmentArgs
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val assignment = solveAssignmentFragmentArgs.assignment
        return SolveAssignmentViewModel(assignmentsRepository, assignment) as T
    }
}
