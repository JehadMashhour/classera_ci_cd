package com.classera.assignments.solve.types.hotspottype

import androidx.fragment.app.Fragment
import dagger.Binds
import dagger.Module


/**
 * Created by Rawan Al-Theeb on 6/17/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class HotspotFragmentModule {

    @Binds
    abstract fun bindFragment(hotspotFragment: HotspotFragment) : Fragment
}
