package com.classera.assignments.solve

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Feb 18, 2020
 *
 * @author Mohamed Hamdan
 */
@Module
abstract class SolveAssignmentFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(fragment: Fragment, factory: SolveAssignmentViewModelFactory): SolveAssignmentViewModel {
            return ViewModelProvider(fragment, factory)[SolveAssignmentViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideSolveAssignmentFragmentArgs(fragment: Fragment): SolveAssignmentFragmentArgs {
            return SolveAssignmentFragmentArgs.fromBundle(fragment.arguments!!)
        }
    }

    @Binds
    abstract fun bindFragment(fragment: SolveAssignmentFragment): Fragment
}
