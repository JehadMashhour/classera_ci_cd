package com.classera.assignments.solve.types.truefalsetype

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.annotation.Keep
import androidx.recyclerview.widget.RecyclerView
import com.classera.assignments.R
import com.classera.assignments.solve.types.BaseQuestionTypeFragment
import com.classera.assignments.solve.types.adapters.MultipleChoicesAdapter
import com.classera.core.Screen
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.removeNull
import dagger.android.support.AndroidSupportInjection

/**
 * Project: Classera
 * Created: Mar 10, 2020
 *
 * @author Mohamed Hamdan
 */
@Screen("True and False question")
@Keep
class TrueFalseFragment : BaseQuestionTypeFragment() {

    override var layoutResource: Int = R.layout.fragment_true_false

    private val choices by lazy { question?.getSelectableChoices() }
    private var recyclerView: RecyclerView? = null
    private val adapter by lazy { MultipleChoicesAdapter(choices) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initAdapter()
        setDraftAnswer()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    private fun setDraftAnswer() {

        question?.let { question ->
            val answer = questionAnswer?.firstOrNull { it.questionId.toString() == question.id }
            question.draftAnswer = answer?.answer
            question.studentAnswer = answer?.answer
            answer?.answer?.toInt()?.minus(1)?.let { adapter.setSelectedPosition(it) }
        }
    }

    private fun initAdapter() {
        adapter.setOnItemClickListener { _, position ->
            adapter.setSelectedPosition(position)
            val data = this.getData().removeNull()
            viewModel.saveAsDraft(data).observe(viewLifecycleOwner) {}
        }
        recyclerView?.adapter = adapter
    }

    private fun findViews() {
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_true_false)
    }

    override fun getData(): Map<String, Any?> {
        return mapOf(
            "assignment_id" to assignment?.id,
            "question_id" to question?.id,
            "submission_id" to settings?.submissionId,
            "question_number" to saveQuestionNumber,
            "answer" to (choices?.indexOfFirst { it.selected } ?: -1) + 1
        )
    }

    override fun getSubmitData(mapToBeFilled: MutableMap<String, Any?>) {
        val answer = (choices?.indexOfFirst { it.selected } ?: -1) + 1
        if (answer != 0) {
            mapToBeFilled["answers[$submitQuestionNumber][question_id]"] = question?.id
            mapToBeFilled["answers[$submitQuestionNumber][text]"] = answer
        }
    }
}
