package com.classera.assignments.solve.types.multipleselecttype

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.annotation.Keep
import androidx.recyclerview.widget.RecyclerView
import com.classera.assignments.R
import com.classera.assignments.solve.types.BaseQuestionTypeFragment
import com.classera.assignments.solve.types.adapters.MultipleSelectAdapter
import com.classera.core.Screen
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.removeNull
import dagger.android.support.AndroidSupportInjection

/**
 * Project: Classera
 * Created: Mar 10, 2020
 *
 * @author Mohamed Hamdan
 */
@Screen("Multiple Select question")
@Keep
class MultipleSelectFragment : BaseQuestionTypeFragment() {

    override var layoutResource: Int = R.layout.fragment_multiple_select
    private val choices by lazy { question?.getSelectableChoices() }
    private var recyclerView: RecyclerView? = null
    private val adapter by lazy {  MultipleSelectAdapter(choices)}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initAdapter()
        setDraftAnswer()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    private fun setDraftAnswer() {

        question?.let { question ->
            val answer = questionAnswer?.firstOrNull { it.questionId.toString() == question.id }
            val answers = answer?.answer?.split(", ")?.map { it.toInt() }
            question.draftAnswer = answer?.answer
            question.studentAnswer = answer?.answer

            answers?.forEach { adapter.setSelectedPosition(it -1) }

        }

    }

    private fun initAdapter() {
        adapter.setOnItemClickListener { _, position ->
            adapter.setSelectedPosition(position)
            val data = this.getDraftData().removeNull()
            viewModel.saveAsDraft(data).observe(viewLifecycleOwner) {}
        }

        recyclerView?.adapter = adapter
    }

    private fun findViews() {
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_multi_select)
    }

    private fun getDraftData(): Map<String, Any?> {
        val answers = choices?.filter { it.selected }?.map { it.number }
        val data = mutableMapOf(
            "assignment_id" to assignment?.id,
            "question_id" to question?.id,
            "submission_id" to settings?.submissionId,
            "question_number" to saveQuestionNumber
        )
        data["answer"] = answers?.joinToString()
        return data
    }

    override fun getData(): Map<String, Any?> {
        val studentAnswers = choices?.filter { it.selected }?.map { it.number }
        val data = mutableMapOf(
            "assignment_id" to assignment?.id,
            "question_id" to question?.id,
            "submission_id" to settings?.submissionId,
            "question_number" to saveQuestionNumber
        )
        studentAnswers?.forEachIndexed { index, s -> data["answer[$index]"] = s }
        return data
    }

    override fun getSubmitData(mapToBeFilled: MutableMap<String, Any?>) {
        if (choices?.any { it.selected } == true) {
            mapToBeFilled["answers[${submitQuestionNumber}][question_id]"] = question?.id
        }

        var keyIndex = 0
        choices?.forEachIndexed { index, choice ->
            if (choice.selected) {
                mapToBeFilled["answers[${submitQuestionNumber}][text][$keyIndex]"] = index + 1
                keyIndex++
            }
        }
    }
}
