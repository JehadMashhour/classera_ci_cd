package com.classera.assignments.solve

import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.classera.assignments.solve.types.BaseQuestionTypeFragment

/**
 * Project: Classera
 * Created: Mar 10, 2020
 *
 * @author Mohamed Hamdan
 */
class QuestionTabsAdapter(
    fragment: Fragment,
    private val viewModel: SolveAssignmentViewModel
) : FragmentStateAdapter(fragment) {

    private var fragments = mutableListOf<Fragment>()

    override fun getItemCount(): Int {
        return viewModel.getQuestionCount()
    }

    override fun createFragment(position: Int): Fragment {
        val fragment = (viewModel.getQuestionType(position).className.newInstance() as Fragment)
        fragments.add(fragment)
        return fragment.apply {

            arguments = bundleOf(
                "question" to viewModel.getQuestion(position),
                "settings" to viewModel.getSettings(),
                "assignment" to viewModel.getAssignment(),
                "saveQuestionNumber" to (position + 1),
                "submitQuestionNumber" to viewModel.getQuestionNumber(position),
                "questionAnswer" to ArrayList(viewModel.draftQuestions)
            )
        }
    }


    fun getFragment(position: Int): BaseQuestionTypeFragment? {
        return fragments.getOrNull(position) as? BaseQuestionTypeFragment?
    }
}
