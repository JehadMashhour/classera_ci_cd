package com.classera.assignments.add

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: May 18, 2020
 *
 * @author Kahled Mohammad
 */
@Module
abstract class AddAssignmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: AddAssignmentViewModelFactory
        ): AddAssignmentViewModel {
            return ViewModelProvider(fragment, factory)[AddAssignmentViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: AddAssignmentFragment): Fragment
}
