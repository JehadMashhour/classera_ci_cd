package com.classera.assignments.create.essay

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.assignments.AssignmentsRepository
import javax.inject.Inject

class EssayQuestionViewModelFactory @Inject constructor(
    private val assignmentsRepository: AssignmentsRepository
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return EssayQuestionViewModel(assignmentsRepository) as T
    }
}
