package com.classera.teacher

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.assignments.listquestions.Question
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.assignments.AssignmentsRepository
import kotlinx.coroutines.Dispatchers


/**
 * Created by Rawan Al-Theeb on 3/21/2020.
 * Classera
 * r.altheeb@classera.com
 */
class QuestionsListViewModel(private val assignmentsRepository: AssignmentsRepository) :
    BaseViewModel() {

    private val _notifyAdapterItemLiveData = MutableLiveData<Int>()
    val notifyAdapterItemLiveData: LiveData<Int> = _notifyAdapterItemLiveData

    private val _toastLiveData = MutableLiveData<Any>()
    val toastLiveData: LiveData<Any> = _toastLiveData

    private var questions: MutableList<Question>? = mutableListOf()

    fun getList(assignmentId: String?) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource { getQuestionsList(assignmentId) }
        questions = resource.element<BaseWrapper<List<Question>>>()?.data?.toMutableList()
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun refreshList(assignmentId: String?) = liveData(Dispatchers.IO) {
        val resource = tryResource { getQuestionsList(assignmentId)  }
        questions = resource.element<BaseWrapper<List<Question>>>()?.data?.toMutableList()
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    suspend fun getQuestionsList(assignmentId: String?): BaseWrapper<List<Question>> {
        return assignmentsRepository.getQuestionList(assignmentId)
    }

    fun getQuestion(position: Int): Question? {
        return questions?.get(position)
    }

    fun getQuestionsCount(): Int {
        return questions?.size ?: 0
    }

    fun deleteItem(position: Int) {
        questions?.removeAt(position)
    }
}
