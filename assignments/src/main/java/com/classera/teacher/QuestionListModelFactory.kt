package com.classera.teacher

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.assignments.AssignmentsRepository
import javax.inject.Inject


/**
 * Created by Rawan Al-Theeb on 3/21/2020.
 * Classera
 * r.altheeb@classera.com
 */
class QuestionListModelFactory @Inject constructor(
    private val assignmentsRepository: AssignmentsRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return QuestionsListViewModel(assignmentsRepository) as T
    }
}
