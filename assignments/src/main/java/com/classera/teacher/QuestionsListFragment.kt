package com.classera.teacher

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.assignments.R
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.observe
import com.classera.data.network.errorhandling.Resource
import com.google.android.material.floatingactionbutton.FloatingActionButton
import javax.inject.Inject

class QuestionsListFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: QuestionsListViewModel

    private var recyclerView: RecyclerView? = null
    private var progressBar: ProgressBar? = null
    private var adapter: QuestionsListAdapter? = null
    private var errorView: ErrorView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var addQuestion: FloatingActionButton? = null

    private val args by navArgs<QuestionsListFragmentArgs>()

    override val layoutId: Int = R.layout.fragment_questions_list

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initListeners()


    }

    private fun initViews() {
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_fragment_questions_list)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_fragment_questions_list)
        errorView = view?.findViewById(R.id.error_view_fragment_fragment_questions_list)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_questions_list)
        addQuestion = view?.findViewById(R.id.floating_action_button_fragment_questions_list_add_question)
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener { refreshQuestionsList() }
        getQuestionsList()

        addQuestion?.setOnClickListener {
            findNavController().navigate(QuestionsListFragmentDirections.questionsTypesDirection())
        }
    }

    private fun getQuestionsList() {
        viewModel.getList(args.assignmentId).observe(this, this::handleResource)
    }

    private fun refreshQuestionsList() {
        viewModel.refreshList(args.assignmentId).observe(this, this::handleResource)
    }


    private fun initItemClickListener() {
        adapter?.setOnItemClickListener { view, position ->
            // No impl
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        initAdapter()
    }

    private fun initAdapter() {
        adapter = QuestionsListAdapter(viewModel)
        recyclerView?.adapter = adapter
        initItemClickListener()
    }

    private fun handleErrorResource(resource: Resource.Error) {
        if ((adapter?.getItemsCount() ?: 0) > 0) {
            Toast.makeText(context, resource.error.resourceMessage, Toast.LENGTH_LONG).show()
            return
        }
        errorView?.visibility = View.VISIBLE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getQuestionsList() }
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    override fun onDestroyView() {
        errorView?.setOnRetryClickListener(null)
        errorView = null
        progressBar = null
        recyclerView?.adapter = null
        recyclerView = null
        adapter = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }
}
