package com.classera.teacher

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * Created by Rawan Al-Theeb on 3/21/2020.
 * Classera
 * r.altheeb@classera.com
 */

@Module
abstract class QuestionListFragmentModule {
    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: QuestionListModelFactory
        ): QuestionsListViewModel {
            return ViewModelProvider(fragment, factory)[QuestionsListViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: QuestionsListFragment): Fragment
}
