package com.classera.authentication.forgot.username

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import com.classera.authentication.R
import com.classera.authentication.forgot.sms.VerificationCodeBottomSheet
import com.classera.core.Screen
import com.classera.core.fragments.BaseBottomSheetValidationDialogFragment
import com.classera.core.utils.android.getStringOrElse
import com.classera.core.utils.android.hideKeyboard
import com.classera.core.utils.android.observe
import com.classera.data.network.errorhandling.Resource
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Screen("Forgot Username")
class ForgotUsernameBottomSheet : BaseBottomSheetValidationDialogFragment() {

    @Inject
    lateinit var viewModel: ForgetUserNameViewModel

    private var buttonSubmit: Button? = null
    private var progressBar: ProgressBar? = null

    @NotEmpty(message = "validation_bottom_sheet_forgot_username_id_number")
    private var editTextIdNumber: EditText? = null

    override val layoutId: Int = R.layout.bottom_sheet_forgot_username

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
    }

    private fun findViews() {
        buttonSubmit = view?.findViewById(R.id.button_bottom_sheet_forgot_username_submit)
        editTextIdNumber = view?.findViewById(R.id.edit_text_bottom_sheet_forgot_username_username)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_submit)
    }

    private fun initListeners() {
        buttonSubmit?.setOnClickListener {
            validator.validate()
        }
    }

    override fun onValidationSucceeded() {
        activity?.hideKeyboard()
        viewModel.forgetUsername(editTextIdNumber?.text?.toString()).observe(this, this::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            buttonSubmit?.text = null
            buttonSubmit?.isEnabled = false
        } else {
            progressBar?.visibility = View.GONE
            buttonSubmit?.setText(R.string.button_bottom_sheet_forgot_password)
            buttonSubmit?.isEnabled = true
        }
    }

    private fun handleSuccessResource() {
        VerificationCodeBottomSheet().show(childFragmentManager, "")
    }

    private fun handleErrorResource(resource: Resource.Error) {
        Toast.makeText(context, resource.error.message, Toast.LENGTH_LONG).show()
    }
}
