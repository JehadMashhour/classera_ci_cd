package com.classera.authentication.forgot.username

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.authentication.AuthenticationViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Created by Odai Nazzal on 12/11/2019.
 * Classera
 * o.nazzal@classera.com
 */

@Module
abstract class ForgetUserNameModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: AuthenticationViewModelFactory
        ): ForgetUserNameViewModel {
            return ViewModelProvider(fragment, factory)[ForgetUserNameViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindActivity(activity: ForgotUsernameBottomSheet): Fragment
}
