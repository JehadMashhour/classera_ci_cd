package com.classera.authentication.forgot.sms

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.authentication.AuthenticationRepository
import kotlinx.coroutines.Dispatchers

/**
 * Created by Odai Nazzal on 12/11/2019.
 * Classera
 * o.nazzal@classera.com
 */

class VerificationCodeViewModel(private val authenticationRepository: AuthenticationRepository) : BaseViewModel() {

    fun checkVerificationCode(code: String?, username: String?): LiveData<Resource> =
        liveData(Dispatchers.IO) {
            emit(Resource.Loading(show = true))
            val resource = tryResource {
                authenticationRepository.checkVerificationCode(
                    code,
                    username
                )
            }
            emit(resource)
            emit(Resource.Loading(show = false))
        }
}
