package com.classera.authentication

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.authentication.forgot.password.ForgetPasswordViewModel
import com.classera.authentication.forgot.sms.VerificationCodeViewModel
import com.classera.authentication.forgot.username.ForgetUserNameViewModel
import com.classera.authentication.login.LoginViewModel
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.authentication.AuthenticationRepository
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
class AuthenticationViewModelFactory @Inject constructor(
    private val application: Application,
    private val authenticationRepository: AuthenticationRepository,
    private val prefs: Prefs
) : ViewModelProvider.AndroidViewModelFactory(application) {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            LoginViewModel::class.java -> LoginViewModel(application, authenticationRepository, prefs) as T
            ForgetPasswordViewModel::class.java -> ForgetPasswordViewModel(authenticationRepository) as T
            VerificationCodeViewModel::class.java -> VerificationCodeViewModel(authenticationRepository) as T
            ForgetUserNameViewModel::class.java -> ForgetUserNameViewModel(authenticationRepository) as T
            else -> throw IllegalAccessException("There is no view model called $modelClass")
        }
    }
}
