package com.classera.authentication.login

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.authentication.R
import com.classera.core.BaseAndroidViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.authentication.login.LoginResponse
import com.classera.data.models.user.UserRole
import com.classera.data.network.errorhandling.NetworkException
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.authentication.AuthenticationRepository
import kotlinx.coroutines.Dispatchers

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
class LoginViewModel(
    application: Application,
    private val authenticationRepository: AuthenticationRepository,
    private val prefs: Prefs
) : BaseAndroidViewModel(application) {

    fun startLogin(username: String?, password: String?): LiveData<Resource> =
        liveData(Dispatchers.IO) {
            emit(Resource.Loading(show = true))
            val resource = tryResource { authenticationRepository.loginBy(username, password) }
            if (resource.isSuccess()) {
                val user = resource.element<BaseWrapper<LoginResponse>>()?.data?.userData?.user
                if (user?.isNotAllowedToLogin() == true) {
                    emit(
                        Resource.Error(
                            NetworkException.Business(
                                application.getString(R.string.access_denied),
                                0
                            )
                        )
                    )
                } else {
                    emit(resource)
                }
            } else {
                emit(resource)
            }
            emit(Resource.Loading(show = false))
        }

    fun getCurrentRole(): UserRole? {
        return prefs.userRole
    }

    fun googleLogin(android: String?): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource { authenticationRepository.googleLogin(android) }
        if (resource.isSuccess()) {
            val user = resource.element<BaseWrapper<LoginResponse>>()?.data?.userData?.user
            if (user?.isNotAllowedToLogin() == true) {
                emit(
                    Resource.Error(
                        NetworkException.Business(
                            application.getString(R.string.access_denied),
                            0
                        )
                    )
                )
            } else {
                emit(resource)
            }
        } else {
            emit(resource)
        }
        emit(Resource.Loading(show = false))
    }

    fun googleLoginVisibilitySettings(): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource =
            tryResource { authenticationRepository.googleLoginSettings() }
        emit(resource)
        emit(Resource.Loading(show = false))
    }
}
