package com.classera.authentication.login

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.classera.authentication.AuthenticationViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Module
abstract class LoginActivityModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            activity: AppCompatActivity,
            factory: AuthenticationViewModelFactory
        ): LoginViewModel {
            return ViewModelProvider(activity, factory)[LoginViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindActivity(activity: LoginActivity): AppCompatActivity
}
