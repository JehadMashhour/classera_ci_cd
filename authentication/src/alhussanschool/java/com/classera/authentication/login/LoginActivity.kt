package com.classera.authentication.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.widget.AppCompatImageView
import androidx.lifecycle.observe
import com.classera.authentication.R
import com.classera.authentication.forgot.password.ForgotPasswordBottomSheet
import com.classera.core.Activities
import com.classera.core.Screen
import com.classera.core.activities.BaseValidationActivity
import com.classera.core.intentTo
import com.classera.core.utils.android.hideKeyboard
import com.classera.core.utils.android.onDoneClicked
import com.classera.data.BuildConfig
import com.classera.data.models.BaseWrapper
import com.classera.data.models.authentication.settings.GoogleRestrictionSettings
import com.classera.data.models.user.UserRole
import com.classera.data.network.errorhandling.Resource
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import com.mobsandgeeks.saripaar.annotation.Password
import java.util.*
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Screen("Login")
class LoginActivity : BaseValidationActivity() {

    private lateinit var googleSignInClient: GoogleSignInClient

    @Inject
    lateinit var viewModel: LoginViewModel

    private var validationMessage: Snackbar? = null

    private var buttonLogin: Button? = null
    private var buttonGoogleLogin: AppCompatImageView? = null
    private var progressBar: ProgressBar? = null
    private var autoCompleteTextViewSection: AutoCompleteTextView? = null
    private var textInputLayoutSection : TextInputLayout? = null
    private var selectedSectionPosition = -1

    private lateinit var fireBaseAuth: FirebaseAuth

    @NotEmpty(message = "validation_bottom_sheet_forgot_password_username")
    private var editTextUsername: EditText? = null

    @Password(message = "invalid_password", min = 1)
    private var editTextPassword: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        findViews()
        initSectionsAdapter()
        handleGoogleLoginSettings()
        initListeners()
        findViewById<View>(R.id.button_activity_login_forgot_password).setOnClickListener {
            ForgotPasswordBottomSheet().show(supportFragmentManager, "")
        }

        editTextPassword?.onDoneClicked { buttonLogin?.callOnClick() }

        saveNotificationUUID()
    }

    private fun removeSnackBar() {
        editTextPassword?.setOnFocusChangeListener { v, hasFocus -> validationMessage?.dismiss() }
        editTextUsername?.setOnFocusChangeListener { v, hasFocus -> validationMessage?.dismiss() }
    }

    private fun initGoogleLogin() {
        fireBaseAuth = FirebaseAuth.getInstance()
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(BuildConfig.flavorData.googleServicesClientId)
            .requestServerAuthCode(BuildConfig.flavorData.googleServicesClientId)
            .requestEmail()
            .requestId()
            .requestProfile()
            .build()

        googleSignInClient = GoogleSignIn.getClient(this, gso)
        googleSignInClient.revokeAccess()
    }

    private fun findViews() {
        buttonLogin = findViewById(R.id.button_activity_login_submit)
        progressBar = findViewById(R.id.progress_bar_activity_login)
        editTextUsername = findViewById(R.id.edit_text_activity_login_username)
        editTextPassword = findViewById(R.id.edit_text_activity_login_password)
        buttonGoogleLogin = findViewById(R.id.button_activity_login_google)
        autoCompleteTextViewSection =
            findViewById(R.id.auto_complete_text_view_activity_login_section)

        textInputLayoutSection = findViewById(R.id.text_input_layout_login_activity_section)
    }

    private fun initListeners() {
        buttonLogin?.setOnClickListener { validator.validate(true) }
        buttonGoogleLogin?.setOnClickListener {
            googleSignIn()
        }

        autoCompleteTextViewSection?.setOnClickListener { hideKeyboard() }
    }

    private fun initSectionsAdapter() {
        val adapter = ArrayAdapter(
            this,
            R.layout.dropdown_menu_popup_item,
            resources.getStringArray(R.array.array_login_activity_sections)
        )
        autoCompleteTextViewSection?.setAdapter(adapter)
        autoCompleteTextViewSection?.setOnItemClickListener { _, _, position, _ ->
            selectedSectionPosition = position
        }
    }

    private fun googleSignIn() {
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(
            signInIntent,
            RC_SIGN_IN
        )
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                fireBaseAuthWithGoogle(account!!)
            } catch (e: ApiException) {
                Log.e("ex", e.message, e)
            }
        }
    }

    private fun fireBaseAuthWithGoogle(acct: GoogleSignInAccount) {
        viewModel.googleLogin(acct.idToken).observe(this, this::handleResource)
    }

    override fun onValidationSucceeded() {
        hideKeyboard()

        if (selectedSectionPosition == -1) {
            Toast.makeText(this, R.string.error_login_activity_select_postion, Toast.LENGTH_SHORT)
                .show()
            return
        }
        var username = editTextUsername?.text?.toString()
        val password = editTextPassword?.text?.toString()

        username = if (selectedSectionPosition == 0) {
            USER_NAME_NATIONAL_PREFIX + username
        } else {
            USER_NAME_INTERNATIONAL_PREFIX + username
        }

        viewModel.startLogin(username, password)
            .observe(this, this::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            buttonLogin?.text = null
            buttonLogin?.isEnabled = false
        } else {
            progressBar?.visibility = View.GONE
            buttonLogin?.setText(R.string.button_activity_login)
            buttonLogin?.isEnabled = true
        }
    }

    private fun handleSuccessResource() {

        // if the user parent go to child list activity
        if (viewModel.getCurrentRole() == UserRole.GUARDIAN) {
            val intent = intentTo(Activities.ParentChildActivity)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        } else {
            val intent = intentTo(Activities.Main)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        val message = resource.error.message ?: ""
        validationMessage = Snackbar.make(parentView!!, message, Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.button_view_error_retry) { buttonLogin?.callOnClick() }
        validationMessage?.view?.findViewById<TextView>(
            com.google.android.material.R.id.snackbar_text
        ) ?.maxLines = SNACK_BAR_MAX_LINES
        validationMessage?.show()
        removeSnackBar()
    }

    private fun saveNotificationUUID() {
        if (prefs.uuid.isNullOrEmpty() || prefs.uuid.isNullOrBlank())
            prefs.uuid = UUID.randomUUID().toString()
    }

    private fun handleGoogleLoginSettings() {
        viewModel.googleLoginVisibilitySettings().observe(this, this::handleGoogleSettingsResource)
    }

    private fun handleGoogleSettingsResource(resource: Resource) {
        when (resource) {
            is Resource.Success<*> -> {
                handleGoogleSettingsSuccessResource(
                    resource as Resource.Success<BaseWrapper<GoogleRestrictionSettings>>)
            }
            is Resource.Error -> {
                handleGoogleSettingsErrorResource(resource)
            }
        }
    }

    private fun handleGoogleSettingsSuccessResource(success: Resource.Success<BaseWrapper<GoogleRestrictionSettings>>) {
        success.data?.data?.hideGoogleLogin?.let { visible ->
            if (visible) {
                buttonGoogleLogin?.visibility = View.VISIBLE
                initGoogleLogin()
            } else {
                buttonGoogleLogin?.visibility = View.GONE
            }

        }
    }

    private fun handleGoogleSettingsErrorResource(resource: Resource.Error) {
        buttonGoogleLogin?.visibility = View.GONE
    }

    companion object {

        private const val RC_SIGN_IN = 9001
        private const val USER_NAME_NATIONAL_PREFIX = "HNS"
        private const val USER_NAME_INTERNATIONAL_PREFIX = "HIS"
        private const val SNACK_BAR_MAX_LINES = 5
    }
}
