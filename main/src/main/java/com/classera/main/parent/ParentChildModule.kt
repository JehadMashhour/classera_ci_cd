package com.classera.main.parent

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class ParentChildModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideViewModel(
            activity: AppCompatActivity,
            factory: ParentChildViewModelFactory
        ): ParentChildViewModel{
            return ViewModelProvider(activity, factory)[ParentChildViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindActivity(activity: ParentChildActivity): AppCompatActivity
}
