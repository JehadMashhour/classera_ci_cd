package com.classera.main.parent.adapter

import android.view.ViewGroup
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.main.databinding.RowParentChildListBinding
import com.classera.main.parent.ParentChildViewModel

class ParentChildListAdapter(private val viewModel: ParentChildViewModel) :
    BasePagingAdapter<ParentChildListAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowParentChildListBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getChildListCount()
    }

    inner class ViewHolder(binding: RowParentChildListBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowParentChildListBinding> {
                child = viewModel.getChild(position)
            }
        }
    }
}
