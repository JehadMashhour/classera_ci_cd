package com.classera.main

import android.view.Menu
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseViewModel
import com.classera.data.models.notification.NotificationWrapper
import com.classera.data.models.user.UserRole
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.network.errorhandling.tryThirdPartyResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.authentication.AuthenticationRepository
import com.classera.data.repositories.notification.NotificationRepository
import com.classera.data.repositories.settings.SettingsRepository
import com.classera.data.repositories.user.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
class MainViewModel(
    private val userRepository: UserRepository,
    private val authenticationRepository: AuthenticationRepository,
    private val prefs: Prefs,
    private val notificationRepository: NotificationRepository,
    private val settingsRepository: SettingsRepository
) : BaseViewModel() {

    var notificationUnseenCount: Int? = 0

    fun getUserInfo(): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource { userRepository.getUserInfo() }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getNavigationViewActionsMenu(): Int {
        return if (prefs.userRole?.actionsMenu != Menu.NONE) {
            prefs.userRole!!.actionsMenu
        } else {
            prefs.previousUserRole!!.actionsMenu
        }
    }

    fun getNavigationViewMenu(): Int {
        return prefs.userRole!!.menu
    }

    fun getBottomNavigationViewNavigationResource(): String {
        return prefs.userRole!!.bottomNavigationViewNavigation
    }

    fun getNavigationViewNavigationResource(): String {
        return prefs.userRole!!.navigationViewNavigation
    }

    fun logout() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(true))
        prefs.language = Locale.getDefault().language
        val resource = tryNoContentResource {
            val fields = mapOf(
                "device_id" to prefs.uuid,
                "Authtoken" to prefs.authenticationToken
            )
            authenticationRepository.logout(fields)
        }
        if (resource.isSuccess()) {
            prefs.deletePrefsData()
            userRepository.deleteUserFromLocalDatabase()
        }
        emit(resource)
        emit(Resource.Loading(false))
    }

    fun canShowActionMenu(): Boolean {
        return if (prefs.userRole?.actionsMenu != Menu.NONE) {
            true
        } else {
            prefs.previousUserRole?.actionsMenu != Menu.NONE
        }
    }

    fun getSchoolSettings() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            userRepository.getSchoolSettings()!!
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun isGlobalSchool(): Boolean {
        return prefs.schoolType == GLOBAL_SCHOOL_TYPE
    }

    fun isBlockedCallChild(): Boolean {
        return prefs.allowStudentsRequests == IS_ALLOW_STUDENTS_REQUESTS_BLOCKED
    }

    fun isAssessmentBlocked(): Boolean {
        return prefs.isAssessmentBlocked == IS_ASSESSMENT_BLOCKED
    }

    fun getNotificationUnseenCount() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryThirdPartyResource {
            notificationRepository.getNotificationCount(DEFAULT_CNS_PAGE)
        }
        if (resource.isSuccess()) {
            notificationUnseenCount = resource.element<NotificationWrapper>()?.unseenCount
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun updateNotificationStatus(uuid: String) {
        viewModelScope.launch(Dispatchers.IO) { settingsRepository.updateNotificationStatus(uuid) }
    }

    fun successPartnerVisibilitySettings(): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource =
            tryResource { authenticationRepository.googleLoginSettings() }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun wasTeacherRole(): Boolean {
        return prefs.mainRole == UserRole.TEACHER && prefs.userRole != UserRole.TEACHER
    }

    private companion object {

        private const val GLOBAL_SCHOOL_TYPE = "1"
        private const val DEFAULT_CNS_PAGE = 0
        private const val IS_ASSESSMENT_BLOCKED = "1"
        private const val IS_ALLOW_STUDENTS_REQUESTS_BLOCKED = "0"
    }
}
