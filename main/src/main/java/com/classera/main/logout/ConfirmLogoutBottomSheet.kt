package com.classera.main.logout

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.RatingBar
import androidx.fragment.app.FragmentManager
import com.classera.core.fragments.BaseBottomSheetDialogFragment
import com.classera.main.R

class ConfirmLogoutBottomSheet private constructor() : BaseBottomSheetDialogFragment() {

    private var callback: ((confirm: Boolean) -> Unit)? = null
    private var ratingBar: RatingBar? = null
    private var buttonSubmit: Button? = null

    override val layoutId: Int = R.layout.bottom_sheet_logout

    override fun enableDependencyInjection(): Boolean {
        return false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
    }

    private fun findViews() {
        ratingBar = view?.findViewById(R.id.rating_bar_bottom_sheet_rating)
        buttonSubmit = view?.findViewById(R.id.button_bottom_sheet_rating_submit)
    }

    private fun initListeners() {
        buttonSubmit?.setOnClickListener {
            dismiss()
            callback?.invoke(true)
        }
    }

    override fun onDetach() {
        callback?.invoke(false)
        super.onDetach()
    }

    override fun onDestroy() {
        callback?.invoke(false)
        super.onDestroy()
    }

    override fun onDestroyView() {
        callback?.invoke(false)
        super.onDestroyView()
    }

    companion object {

        fun show(supportFragmentManager: FragmentManager, callback: (confirm: Boolean) -> Unit) {
            val ratingBottomSheet = ConfirmLogoutBottomSheet()
            ratingBottomSheet.callback = callback
            ratingBottomSheet.show(supportFragmentManager, "")
        }
    }
}
