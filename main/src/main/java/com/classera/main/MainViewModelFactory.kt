package com.classera.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.authentication.AuthenticationRepository
import com.classera.data.repositories.notification.NotificationRepository
import com.classera.data.repositories.settings.SettingsRepository
import com.classera.data.repositories.user.UserRepository
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
class MainViewModelFactory @Inject constructor(
    private val userRepository: UserRepository,
    private val authenticationRepository: AuthenticationRepository,
    private val prefs: Prefs,
    private val notificationRepository: NotificationRepository,
    private val settingsRepository: SettingsRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel(
            userRepository,
            authenticationRepository,
            prefs,
            notificationRepository,
            settingsRepository
        ) as T
    }
}
