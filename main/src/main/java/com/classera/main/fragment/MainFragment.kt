package com.classera.main.fragment

import android.os.Bundle
import android.view.View
import androidx.lifecycle.observe
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.classera.core.fragments.BaseFragment
import com.classera.data.models.notification.NotificationWrapper
import com.classera.data.models.user.UserRole
import com.classera.data.network.errorhandling.Resource
import com.classera.data.prefs.Prefs
import com.classera.main.MainActivity
import com.classera.main.MainViewModel
import com.classera.main.R
import com.google.android.material.bottomnavigation.BottomNavigationView
import javax.inject.Inject


/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Mohamed Hamdan
 */
class MainFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: MainViewModel

    @Inject
    lateinit var prefs: Prefs

    private var bottomNavigationView: BottomNavigationView? = null
    private var navController: NavController? = null
    private var navHostFragment: NavHostFragment? = null

    override val layoutId: Int = R.layout.fragment_main

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navHostFragment =
            childFragmentManager.findFragmentById(R.id.fragment_main_navigation) as? NavHostFragment?

        bottomNavigationView = view.findViewById(R.id.bottom_navigation_view_fragment_main)

        initNavController()

        navController?.addOnDestinationChangedListener { _, destination, arguments ->
            (activity as MainActivity).onDestinationChangedListener(destination)

            var label = destination.label?.toString()
            if (label?.startsWith("{") == true) {
                label = label.replace(oldValue = "{", newValue = "")
                    .replace(oldValue = "}", newValue = "")
                label = arguments?.getString(label)
            }
            (activity as MainActivity).changeToolbarTitle(label)

            checkBottomNavigationView(destination)

        }

        getNotificationListAndCount()
    }


    private fun initNavController() {
        val navigation = resources.getIdentifier(
            viewModel.getBottomNavigationViewNavigationResource(),
            "navigation",
            context?.packageName
        )
        val graphInflater = navHostFragment?.navController?.navInflater
        val navGraph = graphInflater?.inflate(navigation)
        navController = navHostFragment?.navController
        navController?.graph = navGraph!!
    }

    private fun checkBottomNavigationView(destination: NavDestination) {
        if (destination.id == R.id.item_menu_activity_main_bottom_navigation_view_home
            && prefs.userRole == UserRole.DRIVER
        ) {
            bottomNavigationView?.visibility = View.GONE
        }

    }

    override fun onDestroyView() {
        bottomNavigationView = null
        super.onDestroyView()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        when {
            handleManagementRole() -> {
                bottomNavigationView?.menu?.clear()
                bottomNavigationView?.inflateMenu(R.menu.menu_activity_main_bottom_navigation_view_admin)
            }
            handleGeneralAndManagerRole() -> {
                bottomNavigationView?.menu?.clear()
                bottomNavigationView?.inflateMenu(R.menu.menu_activity_main_bottom_navigation_view_general)
            }
        }
        bottomNavigationView?.setupWithNavController(navController!!)
    }

    private fun handleManagementRole() = (prefs.userRole == UserRole.ADMIN)

    private fun handleGeneralAndManagerRole() = (
            prefs.userRole == UserRole.GENERAL || prefs.userRole == UserRole.MANAGER
                    || prefs.userRole == UserRole.TEACHER_SUPERVISOR)

    private fun getNotificationListAndCount() {
        viewModel.getNotificationUnseenCount().observe(this, this::handleNotificationResponse)
    }

    private fun handleNotificationResponse(resource: Resource) {
        when (resource) {
            is Resource.Success<*> -> {
                val unseenCount =
                    resource.element<NotificationWrapper>()?.unseenCount ?: DEFAULT_BADGE
                setNotificationUnseenCount(unseenCount)
            }
            is Resource.Error -> {
                setNotificationUnseenCount(DEFAULT_BADGE)
            }
        }
    }

    private fun setNotificationUnseenCount(unseenCount: Int) {
        val badge = bottomNavigationView?.getOrCreateBadge(
            R.id.item_menu_fragment_main_navigation_notification_list
        )

        badge?.isVisible = unseenCount != 0
        badge?.number = unseenCount
    }


    private companion object {

        private const val DEFAULT_BADGE = 0
    }
}
