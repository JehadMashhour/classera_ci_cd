package com.classera.main

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.classera.notigation.AnnouncementNavigationProcessor
import com.classera.notigation.AssignmentNavigationProcessor
import com.classera.notigation.AttachmentNavigationProcessor
import com.classera.notigation.DiscussionCommentNavigationProcessor
import com.classera.notigation.MailNavigationProcessor
import com.classera.notigation.NavigationHandlerImpl
import com.classera.notigation.TicketNavigationProcessor
import com.classera.notigation.VCRNavigationProcessor
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Module
abstract class MainActivityModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            activity: AppCompatActivity,
            factory: MainViewModelFactory
        ): MainViewModel {
            return ViewModelProvider(activity, factory)[MainViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideNavigationHandler(): com.classera.notigation.NavigationHandler {
            return NavigationHandlerImpl(
                setOf(
                    MailNavigationProcessor(),
                    AssignmentNavigationProcessor(),
                    VCRNavigationProcessor(),
                    AttachmentNavigationProcessor(),
                    TicketNavigationProcessor(),
                    DiscussionCommentNavigationProcessor(),
                    AnnouncementNavigationProcessor()
                )
            )
        }
    }

    @Binds
    abstract fun bindActivity(activity: MainActivity): AppCompatActivity
}
