package com.classera.surveys

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.ProgressBar
import android.widget.SearchView
import android.widget.Toast
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.onDebounceQueryTextChange
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import javax.inject.Inject

/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
@Screen("Surveys")
class SurveysFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: SurveysViewModel

    private var searchView: SearchView? = null
    private var progressBar: ProgressBar? = null
    private var adapter: SurveysAdapter? = null
    private var errorView: ErrorView? = null

    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null

    override val layoutId: Int = R.layout.fragment_surveys

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_surveys, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val searchMenuItem = menu.findItem(R.id.item_menu_fragment_surveys_search)
        searchView = (searchMenuItem.actionView as? SearchView?)
        searchView?.onDebounceQueryTextChange { getSurveys() }

        searchView?.setOnCloseListener {
            getSurveys()
            return@setOnCloseListener false
        }
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_surveys)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_surveys)
        errorView = view?.findViewById(R.id.error_view_fragment_surveys)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_surveys)
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            refreshSurveys()
        }

        viewModel.notifyAdapterItemLiveData.observe(this) { adapter?.notifyItemChanged(it) }

        viewModel.toastLiveData.observe(this) { message ->
            val stringMessage = if (message is Int) getString(message) else message as String
            Toast.makeText(context, stringMessage, Toast.LENGTH_LONG).show()
        }
        getSurveys()
    }

    private fun getSurveys(pageNumber: Int = DEFAULT_PAGE) {
        if (pageNumber == DEFAULT_PAGE) {
            adapter?.resetPaging()
        }
        viewModel.getListSurveys(
            pageNumber,
            searchView?.query
        )
            .observe(this, this::handleResource)
    }

    private fun refreshSurveys() {
        viewModel.refreshSurveys(searchView?.query)
            .observe(this, this::handleResource)
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        if (adapter == null) {
            initAdapter()
        } else {
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()
    }

    private fun initAdapter() {
        adapter = SurveysAdapter(viewModel)
        recyclerView?.adapter = adapter
        adapter?.setOnLoadMoreListener(::getSurveys)
    }

    private fun handleErrorResource(resource: Resource.Error) {
        if ((adapter?.getItemsCount() ?: 0) > 0) {
            Toast.makeText(context, resource.error.resourceMessage, Toast.LENGTH_LONG).show()
            return
        }
        errorView?.visibility = View.VISIBLE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getSurveys() }
        adapter?.finishLoading()
    }

    override fun onDestroyView() {
        progressBar = null
        recyclerView = null
        adapter = null
        errorView = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }
}
