package com.classera.surveys

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * Created by Rawan Al-Theeb on 2/18/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class SurveysFragmentModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: SurveysViewModelFactory
        ): SurveysViewModel {
            return ViewModelProvider(fragment, factory)[SurveysViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: SurveysFragment): Fragment
}
