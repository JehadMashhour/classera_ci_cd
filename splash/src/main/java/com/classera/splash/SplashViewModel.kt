package com.classera.splash

import androidx.lifecycle.liveData
import com.classera.core.Activities
import com.classera.core.BaseViewModel
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.settings.SettingsRepository
import kotlinx.coroutines.Dispatchers

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
class SplashViewModel(private val prefs: Prefs, private val settingsRepository: SettingsRepository) : BaseViewModel() {

    fun getIntentAction() = liveData {
        emit(if (prefs.isLoggedIn) Activities.Main else Activities.Login)
    }

    fun getAppVersion(appId: String?, version: String?) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource { settingsRepository.getAppVersion(appId, version) }
        emit((resource))
        emit(Resource.Loading(show = false))
    }
}
