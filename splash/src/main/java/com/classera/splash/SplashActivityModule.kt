package com.classera.splash

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
@Module
abstract class SplashActivityModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideViewModel(
            activity: AppCompatActivity,
            factory: SplashViewModelFactory
        ): SplashViewModel {
            return ViewModelProvider(activity, factory)[SplashViewModel::class.java]
        }
    }
    @Binds
    abstract fun bindActivity(activity: SplashActivity): AppCompatActivity
}
