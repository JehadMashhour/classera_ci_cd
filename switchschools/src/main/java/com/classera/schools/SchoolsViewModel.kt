package com.classera.schools

import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.mailbox.RecipientByRole
import com.classera.data.models.switchschools.School
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.switchschools.SwitchSchoolsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SchoolsViewModel(
    private val switchSchoolsRepository: SwitchSchoolsRepository,
    private val prefs: Prefs
) : BaseViewModel() {

    private var schools: MutableList<School> = mutableListOf()
    var currentSearchValue: String = ""
    private var filteredSchoolsList: List<School> = listOf()

    fun getSchoolsList() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource =
            tryResource { switchSchoolsRepository.getSwitchSchoolsList() }
        schools.clear()
        schools.addAll(resource.element<BaseWrapper<List<School>>>()?.data ?: mutableListOf())
        getFilteredSchoolList()
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun changeSelectedSchool(position: Int) = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            switchSchoolsRepository.changeSchool(filteredSchoolsList[position])
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getSchool(position: Int): School {
        return filteredSchoolsList[position]
    }

    fun getSchoolsCount(): Int {
        return filteredSchoolsList.size
    }

    fun getSelectedSchoolId(): String? {
        return prefs.schoolId
    }

    fun getFilteredSchoolList() {
        viewModelScope.launch {
            filteredSchoolsList = if (currentSearchValue.isEmpty())
                schools.filter {
                    it.schoolName?:"" == it.schoolName
                }
            else {
                schools.filter {
                    it.schoolName?.contains(currentSearchValue, true)?: false
                }
            }

        }
    }
}
