package com.classera.schools.sub

import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.schools.R
import com.classera.schools.databinding.RowSchoolBinding

class SubSchoolsAdapter(private val viewModel: SubSchoolsViewModel) :
    BasePagingAdapter<SubSchoolsAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowSchoolBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getSchoolCount()
    }

    inner class ViewHolder(binding: RowSchoolBinding) : BaseBindingViewHolder(binding) {

        private var buttonSubSchools: Button? = null

        init {
            buttonSubSchools = itemView.findViewById(R.id.button_row_school_sub_schools)
        }

        override fun bind(position: Int) {
            bind<RowSchoolBinding> {
                switchSchoolsItem = viewModel.getSchool(position)
                selectedSchoolId = viewModel.getSelectedSchoolId()
            }
            buttonSubSchools?.setOnClickListener { view ->
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    getOnItemClickListener()?.invoke(view, clickedPosition)
                }
            }
        }
    }
}
