package com.classera.schools.sub

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.models.switchschools.School
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.switchschools.SwitchSchoolsRepository
import javax.inject.Inject

class SubSchoolsViewModelFactory @Inject constructor(
    private val switchSchoolsRepository: SwitchSchoolsRepository,
    private val schools: List<School>,
    private val prefs: Prefs
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SubSchoolsViewModel(switchSchoolsRepository, schools, prefs) as T
    }
}

