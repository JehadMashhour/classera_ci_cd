package com.classera.profile.publicprofile.adapter

import android.view.ViewGroup
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.data.models.profile.PublicProfileWrapper
import com.classera.profile.databinding.EducationListRowItemBinding
import com.classera.profile.databinding.SkillsListRowItemBinding


class SkillsPublicProfileAdapter(
    private val publicProfileData: PublicProfileWrapper?
) : BaseAdapter<SkillsPublicProfileAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = SkillsListRowItemBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return publicProfileData?.profile?.skills?.size!!
    }

    inner class ViewHolder(binding: SkillsListRowItemBinding) : BaseBindingViewHolder(binding) {
        override fun bind(position: Int) {
            bind<SkillsListRowItemBinding> {
                this.skills = publicProfileData?.profile?.skills?.get(position)
            }
        }
    }
}
