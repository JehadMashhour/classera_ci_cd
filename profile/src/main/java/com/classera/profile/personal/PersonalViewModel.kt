package com.classera.profile.personal

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseViewModel
import com.classera.data.models.user.User
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.profile.ProfileRepository
import com.classera.data.repositories.user.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
class PersonalViewModel(
    private val profileRepository: ProfileRepository,
    private val userRepository: UserRepository,
    private val pref: Prefs
) : BaseViewModel(), LifecycleObserver {

    private var user: MutableLiveData<User>? = null

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        viewModelScope.launch(Dispatchers.IO) {
            user = MutableLiveData()
            user?.postValue(userRepository.getLocalUser().first()?.firstOrNull())
        }
    }

    @Suppress("LongParameterList")
    fun updateProfile(
        placeOfBirthCountry: String,
        address: String,
        cityOfBirth: String,
        bio: String,
        email: String,
        phoneNumber: String
    ) =
        liveData(Dispatchers.IO) {
            emit(Resource.Loading(show = true))
            val resource = tryNoContentResource {
                profileRepository.editUserProfile(
                    placeOfBirthCountry,
                    address,
                    cityOfBirth,
                    bio,
                    email,
                    phoneNumber
                )
            }
            emit(resource)
            emit(Resource.Loading(show = false))
        }

    fun getUser(): LiveData<User> {
        return user ?: MutableLiveData()
    }


    fun getUserId(): String? {
        return pref.userId
    }

    fun getRemoteUser() {
        viewModelScope.launch {
            //userRepository.deleteUserFromLocalDatabase()
            userRepository.getUserInfo()
        }
    }


    fun changeUserProfilePicture(filePath: String, fileType: String) = liveData {
        emit(Resource.Loading(show = true))
        val paramsList = ArrayList<MultipartBody.Part>()
        if (filePath.isNotBlank() || filePath.isNotEmpty()) {
            val file = File(filePath)
            val attachmentRequestBody = RequestBody.create(MediaType.parse(fileType), file)
            paramsList.add(
                MultipartBody.Part.createFormData(
                    IMAGE_PROFILE_KEY,
                    file.name,
                    attachmentRequestBody
                )
            )
        }
        val resource =
            tryNoContentResource { profileRepository.changeUserProfilePicture(paramsList) }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private companion object {

        private const val IMAGE_PROFILE_KEY = "file"
    }

}
