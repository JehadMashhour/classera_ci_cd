package com.classera.profile.experiences.addExperiences

import android.app.DatePickerDialog
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import android.widget.EditText
import android.widget.Button
import android.widget.ProgressBar
import android.widget.DatePicker
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.classera.core.fragments.BaseBottomSheetValidationDialogFragment
import com.classera.core.utils.android.observe
import com.classera.data.models.profile.Experiences
import com.classera.data.network.errorhandling.Resource
import com.classera.profile.R
import com.classera.profile.experiences.ExperiencesFragmentRefresh
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.textfield.TextInputLayout
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import javax.inject.Inject

/**
 * Created by Rawan Al-Theeb on 1/27/2021.
 * Classera
 * r.altheeb@classera.com
 */
class AddExperienceBottomSheet(
    private val fragment: ExperiencesFragmentRefresh,
    private val mode: Int,
    private val experience: Experiences?
) : BaseBottomSheetValidationDialogFragment(), DatePickerDialog.OnDateSetListener {

    @Inject
    lateinit var viewModel: AddExperienceViewModel

    private var buttonSubmit: Button? = null
    private var progressBar: ProgressBar? = null
    private var closeImageView: AppCompatImageView? = null
    private var textViewTitle: AppCompatTextView? = null

    @NotEmpty(message = "validation_bottom_sheet_add_experience_job_title")
    private var editTextJobTitle: EditText? = null

    @NotEmpty(message = "validation_bottom_sheet_add_experience_job_type")
    private var editTextJobType: EditText? = null

    @NotEmpty(message = "validation_bottom_sheet_add_experience_company_name")
    private var editTextCompanyName: EditText? = null

    @NotEmpty(message = "validation_bottom_sheet_add_experience_description")
    private var editTextDescription: EditText? = null

    @NotEmpty(message = "validation_bottom_sheet_add_experience_start_date")
    private var editTextStartDate: EditText? = null

    @NotEmpty(message = "validation_bottom_sheet_add_experience_end_date")
    private var editTextEndDate: EditText? = null

    private var editTextDate: EditText? = null
    private var currentDayCalendar = Calendar.getInstance()
    private var year: Int = currentDayCalendar.get(Calendar.YEAR)
    private var month: Int = currentDayCalendar.get(Calendar.MONTH)
    private var dayOfMonth: Int = currentDayCalendar.get(Calendar.DAY_OF_MONTH)

    override val layoutId: Int = R.layout.bottom_sheet_add_experiences

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findViews()
        initListener()
        prepareBottomSheet()
    }

    private fun findViews() {
        buttonSubmit = view?.findViewById(R.id.button_bottom_sheet_experiences_submit)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_submit)
        closeImageView = view?.findViewById(R.id.image_view_bottom_sheet_close)
        editTextJobTitle = view?.findViewById(R.id.edit_text_bottom_sheet_job_title)
        editTextJobType = view?.findViewById(R.id.edit_text_bottom_sheet_job_type)
        editTextCompanyName = view?.findViewById(R.id.edit_text_bottom_sheet_company_name)
        editTextDescription = view?.findViewById(R.id.edit_text_bottom_sheet_description)
        editTextStartDate = view?.findViewById(R.id.edit_text_bottom_sheet_start_date)
        editTextEndDate = view?.findViewById(R.id.edit_text_bottom_sheet_end_date)
        textViewTitle = view?.findViewById(R.id.text_view_bottom_sheet_sheet_title)
    }

    private fun prepareBottomSheet() {
        if (mode == MODE_MODIFY) {
            textViewTitle?.text = getString(R.string.bottom_sheet_add_experiences_title_edit)
            editTextJobTitle?.text = SpannableStringBuilder(experience?.jobTitle ?: "")
            editTextDescription?.text = SpannableStringBuilder(experience?.description ?: "")
            editTextJobType?.text = SpannableStringBuilder(experience?.jobType ?: "")
            editTextCompanyName?.text = SpannableStringBuilder(experience?.companyName ?: "")
            editTextStartDate?.text = SpannableStringBuilder(experience?.startDate ?: "")
            editTextEndDate?.text = SpannableStringBuilder(experience?.endDate ?: "")

        } else {
            textViewTitle?.text = getString(R.string.bottom_sheet_add_experiences_title_add)
        }
    }

    private fun initListener() {
        buttonSubmit?.setOnClickListener {
            validator.validate()
        }

        closeImageView?.setOnClickListener {
            behavior.state = BottomSheetBehavior.STATE_HIDDEN
        }

        editTextStartDate?.setOnClickListener {
            DatePickerDialog(
                requireContext(),
                R.style.AppTheme_PickerTheme,
                this,
                year,
                month,
                dayOfMonth
            ).show()
            editTextDate = editTextStartDate
        }
        editTextEndDate?.setOnClickListener {
            DatePickerDialog(
                requireContext(),
                R.style.AppTheme_PickerTheme,
                this,
                year,
                month,
                dayOfMonth
            ).show()
            editTextDate = editTextEndDate
        }
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        this.year = year
        this.month = month
        this.dayOfMonth = dayOfMonth
        val humanMonth = month + 1
        editTextDate?.setText(
            getString(
                R.string.text_fragment_add_experience_date_format,
                year,
                humanMonth,
                dayOfMonth
            )
        )
        removeError(editTextDate?.parent?.parent as TextInputLayout)
    }

    override fun onValidationSucceeded() {
        val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        var startDate: Date? = null
        var endDate: Date? = null

        try {
            startDate = dateFormat.parse(editTextStartDate?.text?.toString() ?: "")
            endDate = dateFormat.parse(editTextEndDate?.text?.toString() ?: "")
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val startDateCalendar: Calendar = Calendar.getInstance()
        startDateCalendar.time = startDate

        val endDateCalendar: Calendar = Calendar.getInstance()
        endDateCalendar.time = endDate

        if (startDateCalendar.after(endDateCalendar)) {
            Toast.makeText(
                context,
                getString(R.string.toast_bottom_sheet_add_experience_start_befor_end),
                Toast.LENGTH_LONG
            ).show()
        } else {
            viewModel.addUserExperiences(
                mode,
                experience?.id,
                editTextJobTitle?.text?.toString() ?: "",
                editTextJobType?.text?.toString() ?: "",
                editTextCompanyName?.text?.toString() ?: "",
                editTextStartDate?.text?.toString() ?: "",
                editTextEndDate?.text?.toString() ?: "",
                editTextDescription?.text?.toString() ?: ""
            ).observe(this, ::handleResource)
        }
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            buttonSubmit?.isEnabled = false
            buttonSubmit?.text = ""
        } else {
            progressBar?.visibility = View.VISIBLE
            buttonSubmit?.isEnabled = true
            buttonSubmit?.text = getString(R.string.submit)
        }
    }

    private fun handleSuccessResource() {
        if (mode == MODE_CREATE) {
            Toast.makeText(context, getString(R.string.saved), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, getString(R.string.edited), Toast.LENGTH_SHORT).show()
        }
        behavior.state = BottomSheetBehavior.STATE_HIDDEN
        fragment.refresh()
        dismiss()
    }

    private fun handleErrorResource(resource: Resource.Error) {
        Toast.makeText(context, resource.error.message, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroyView() {
        buttonSubmit = null
        progressBar = null
        closeImageView = null
        editTextJobTitle = null
        editTextDescription = null
        editTextJobType = null
        editTextCompanyName = null
        editTextStartDate = null
        editTextEndDate = null
        textViewTitle = null
        super.onDestroyView()
    }

    companion object {
        const val YEAR_PICKER_STARTING_YEAR = 1990
        const val MODE_CREATE = 0
        const val MODE_MODIFY = 1
    }
}


