package com.classera.profile.experiences


/**
 * Created by Rawan Al-Theeb on 1/28/2021.
 * Classera
 * r.altheeb@classera.com
 */
interface ExperiencesFragmentRefresh {

    fun refresh()
}
