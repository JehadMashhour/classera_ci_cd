package com.classera.profile.education

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.profile.Education
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.profile.ProfileRepository
import kotlinx.coroutines.Dispatchers

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
class EducationViewModel(
    private val profileRepository: ProfileRepository
): BaseViewModel() {

    private var educations: MutableList<Education> = mutableListOf()

    fun getEducation() =
        liveData(Dispatchers.IO) {
            emit(Resource.Loading(show = true))
            val resource = tryResource {
                profileRepository.getUserEducations()
            }

            educations.clear()
            educations.addAll(resource.element<BaseWrapper<List<Education>>>()?.data ?: mutableListOf())

            emit(resource)
            emit(Resource.Loading(show = false))
        }

    fun delete(position: Int) =
        liveData(Dispatchers.IO) {
            val resource = tryNoContentResource {
                profileRepository.deleteUserEducation(getEducation(position)?.id!!)
            }
            emit(resource)
        }

    fun removeFromList(position: Int) {
        educations.removeAt(position)
    }

    fun getEducation(position: Int): Education? {
        return educations[position]
    }
    fun getEducationsCount(): Int {
        return educations.size
    }

}
