package com.classera.profile.education.addEducation

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.profile.City
import com.classera.data.models.profile.CityResponse
import com.classera.data.models.profile.Country
import com.classera.data.models.profile.CountryResponse
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.profile.ProfileRepository
import com.classera.profile.education.addEducation.AddEducationBottomSheet.Companion.MODE_CREATE
import com.classera.profile.education.addEducation.AddEducationBottomSheet.Companion.MODE_MODIFY
import kotlinx.coroutines.Dispatchers
import java.lang.IllegalStateException

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
class AddEducationViewModel(
    private val profileRepository: ProfileRepository
) : BaseViewModel() {

    private var countryList = mutableListOf<Country>()
    private var cityList = mutableListOf<City>()

    @Suppress("LongParameterList")
    fun createEducation(
        id : String?,
        mode: Int,
        countryName: String,
        cityName: String,
        school: String,
        fromYear: String,
        toYear: String,
        degree: String,
        fieldOfStudy: String,
        description: String
    ): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryNoContentResource {

            when(mode){
                MODE_CREATE -> {
                    profileRepository.createUserEducation(
                        getCountryId(countryName),
                        getCityId(cityName),
                        school,
                        fromYear,
                        toYear,
                        degree,
                        fieldOfStudy,
                        description
                    )
                }

                MODE_MODIFY -> {
                    profileRepository.editUserEducation(
                        id,
                        getCountryId(countryName),
                        getCityId(cityName),
                        school,
                        fromYear,
                        toYear,
                        degree,
                        fieldOfStudy,
                        description
                    )
                }
                else -> throw IllegalStateException()
            }

        }

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getCountries(): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource { profileRepository.getCountries() }

        countryList.addAll(resource.element<BaseWrapper<CountryResponse>>()?.data?.countries ?: mutableListOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getCities(countryName: String): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            profileRepository.getCities(getCountryId(countryName))
        }

        cityList.addAll(resource.element<BaseWrapper<CityResponse>>()?.data?.cities ?: mutableListOf())

        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getCountryId(countryName: String): String {
        return countryList.find { it.nameEng == countryName }?.id!!
    }

    fun getCityId(cityName: String): String {
        return cityList.find { it.nameEng == cityName }?.id!!
    }

}
