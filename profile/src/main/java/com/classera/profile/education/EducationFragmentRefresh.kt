package com.classera.profile.education

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
interface EducationFragmentRefresh {

    fun refresh()

}
