package com.classera.profile.skill.addskill

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.repositories.profile.ProfileRepository
import com.classera.profile.skill.TYPE_INTEREST
import com.classera.profile.skill.TYPE_LANGUAGE
import com.classera.profile.skill.TYPE_SKILL
import kotlinx.coroutines.Dispatchers
import java.lang.IllegalStateException

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
class AddModifySkillViewModel(
    private val profileRepository: ProfileRepository
) : BaseViewModel() {

    @Suppress("ThrowsCount", "ComplexMethod")
    fun handle(
        type: Int,
        mode: Int,
        id: String?,
        skill: String?,
        description: String?
    ): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryNoContentResource {
            when (type) {
                TYPE_LANGUAGE -> {
                    when (mode) {
                        MODE_CREATE -> profileRepository.createUserLanguages(skill!!, description!!)
                        MODE_MODIFY -> profileRepository.editUserLanguages(id!!, skill!!, description!!)
                        else -> throw IllegalStateException()
                    }
                }
                TYPE_INTEREST -> {
                    when (mode) {
                        MODE_CREATE -> profileRepository.createUserInterests(skill!!, description!!)
                        MODE_MODIFY -> profileRepository.editUserInterests(id!!, skill!!, description!!)
                        else -> throw IllegalStateException()
                    }
                }
                TYPE_SKILL -> {
                    when (mode) {
                        MODE_CREATE -> profileRepository.createUserSkill(skill!!, description!!)
                        MODE_MODIFY -> profileRepository.editUserSkill(id!!, skill!!, description!!)
                        else -> throw IllegalStateException()
                    }
                }
                else -> throw IllegalStateException()
            }
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }
}
