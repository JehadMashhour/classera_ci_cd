package com.classera.profile.skill.addskill

import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.widget.AppCompatAutoCompleteTextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.observe
import com.classera.core.Screen
import com.classera.core.fragments.BaseBottomSheetValidationDialogFragment
import com.classera.core.utils.android.getStringOrElse
import com.classera.core.utils.android.hideKeyboard
import com.classera.data.network.errorhandling.Resource
import com.classera.data.prefs.Prefs
import com.classera.profile.R
import com.classera.profile.skill.SkillFragmentRefresh
import com.classera.profile.skill.TYPE_INTEREST
import com.classera.profile.skill.TYPE_LANGUAGE
import com.classera.profile.skill.TYPE_SKILL
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.textfield.TextInputLayout
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import javax.inject.Inject


/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */

@Screen("Add Modify Skill")
class AddModifySkillBottomSheet(
    private val refreshFragment: SkillFragmentRefresh,
    private val type: Int,
    private val mode: Int,
    private val skill: String?,
    private val description: String?,
    private val id: String?
) : BaseBottomSheetValidationDialogFragment(){

    @Inject
    lateinit var prefs: Prefs

    @Inject
    lateinit var viewModel: AddModifySkillViewModel

    private var buttonSubmit: Button? = null
    private var progressBar: ProgressBar? = null
    private var closeImageView: AppCompatImageView? = null
    private var textViewTitle: AppCompatTextView? = null
    private var textInputLayout: TextInputLayout? = null

    @NotEmpty (message = "validation_bottom_sheet_add_skill_skill")
    private var editTextSkill: EditText? = null

    @NotEmpty (message = "validation_bottom_sheet_add_education_description")
    private var editTextDescription: EditText? = null

    @NotEmpty (message = "validation_bottom_sheet_add_skill_proficiency")
    private var editTextProficiency: AppCompatAutoCompleteTextView? = null

    override val layoutId: Int = R.layout.bottom_sheet_skills

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findViews()
        initListeners()
        prepareBottomSheet()
    }

    private fun findViews() {
        buttonSubmit = view?.findViewById(R.id.button_bottom_sheet_skill_submit)
        editTextSkill = view?.findViewById(R.id.edit_text_bottom_sheet_skill)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_submit)
        closeImageView = view?.findViewById(R.id.image_view_bottom_sheet_close)
        textViewTitle = view?.findViewById(R.id.text_view_bottom_sheet_skill_title)
        textInputLayout = view?.findViewById(R.id.text_input_layout_bottom_sheet_skill)
        editTextDescription = view?.findViewById(R.id.edit_text_bottom_sheet_skill_description)
        editTextProficiency = view?.findViewById(R.id.edit_text_bottom_sheet_skill_level)
    }

    private fun prepareBottomSheet() {
        when (type){
            TYPE_INTEREST -> {
                prepareBottomSheetInterest()
            }
            TYPE_SKILL -> {
                prepareBottomSheetSkill()
            }
            TYPE_LANGUAGE -> {
                prepareBottomSheetLanguage()
            }
        }
    }

    private fun prepareBottomSheetInterest(){
        textInputLayout?.hint = getString(R.string.interest)
        editTextProficiency?.visibility = View.GONE

        if (mode == MODE_MODIFY){
            textViewTitle?.text = getString(R.string.edit_interest)
            editTextSkill?.text = SpannableStringBuilder(skill?:"")
            editTextDescription?.text = SpannableStringBuilder(description?:"")
        } else {
            textViewTitle?.text = getString(R.string.add_interest)
        }
    }

    private fun prepareBottomSheetLanguage(){
        textInputLayout?.hint = getString(R.string.language)
        editTextDescription?.visibility = View.GONE
        prepareDropDown()

        if (mode == MODE_MODIFY){
            textViewTitle?.text = getString(com.classera.profile.R.string.edit_language)
            editTextSkill?.text = SpannableStringBuilder(skill?:"")
        } else {
            textViewTitle?.text = getString(com.classera.profile.R.string.add_language)
        }
    }

    private fun prepareBottomSheetSkill(){
        textInputLayout?.hint = getString(R.string.skill)
        editTextDescription?.visibility = View.GONE
        prepareDropDown()

        if (mode == MODE_MODIFY){
            textViewTitle?.text = getString(R.string.edit_skill)
            editTextSkill?.text = SpannableStringBuilder(skill?:"")
        } else {
            textViewTitle?.text = getString(R.string.add_skill)
        }
    }

    private fun prepareDropDown() {
        val stringResourcesArray = if (type == TYPE_LANGUAGE) {
            resources.getStringArray(R.array.proficiency)
        } else {
            resources.getStringArray(R.array.skill_level)
        }
        val adapter = createMenuAdapter(stringResourcesArray)
        editTextProficiency?.text = SpannableStringBuilder(description?:"")
        editTextProficiency?.setAdapter(adapter)
    }

    private fun createMenuAdapter(array: Array<String>?) : ArrayAdapter<String>{
        return ArrayAdapter(
            context!!,
            R.layout.profile_drop_down_menu_item,
            array!!
        )
    }

    private fun initListeners() {
        buttonSubmit?.setOnClickListener {
            validator.validate()
        }

        closeImageView?.setOnClickListener {
            behavior.state = BottomSheetBehavior.STATE_HIDDEN
        }
    }

    override fun onValidationSucceeded() {
        activity?.hideKeyboard()
        val skill = editTextSkill?.text?.toString()
        val descriptionMain: String = if (type == TYPE_INTEREST) {
            editTextDescription?.text?.toString()!!
        } else {
            editTextProficiency?.text?.toString()!!
        }

        val description: String = getEnglishValues(descriptionMain)

        viewModel.handle(type, mode, id, skill, description).observe(this, this::handleResource)
    }

    private fun getEnglishValues(descriptionMain: String): String {
        return if (prefs.language == "ar" && type == TYPE_SKILL) {
            checkSkill( descriptionMain)
        } else if (prefs.language == "ar" && type == TYPE_LANGUAGE) {
            checkLanguage( descriptionMain)
        } else {
            descriptionMain
        }
    }

    private fun checkLanguage(descriptionMain: String): String {
        return when (descriptionMain) {
            "ابتدائي" -> "Elementary"
            "محدود" -> "Limited"
            "محترف جدا" -> "Full Professional"
            "محلي" -> "Advanced"
            "خبير" -> "Expert"
            else -> throw IllegalAccessException()
        }
    }

    private fun checkSkill(descriptionMain: String): String {
        return when (descriptionMain) {
            "مبتدئ" -> "Beginner"
            "موهوب" -> "Talented"
            "متقدم" -> "Skilled"
            "ماهر" -> "Advanced"
            "خبير" -> "Expert"
            else -> throw IllegalAccessException()
        }
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            buttonSubmit?.text = null
            buttonSubmit?.isEnabled = false
        } else {
            progressBar?.visibility = View.GONE
            buttonSubmit?.setText(R.string.submit)
            buttonSubmit?.isEnabled = true
        }
    }

    private fun handleSuccessResource() {
        behavior.state = BottomSheetBehavior.STATE_HIDDEN
        refreshFragment.refresh()
    }

    private fun handleErrorResource(resource: Resource.Error) {
        val message = context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }
}

const val MODE_CREATE = 0
const val MODE_MODIFY = 1
