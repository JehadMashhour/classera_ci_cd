package com.classera.profile.skill

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.profile.Interest
import com.classera.data.models.profile.Language
import com.classera.data.models.profile.Skill
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.profile.ProfileRepository
import kotlinx.coroutines.Dispatchers

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
class SkillsViewModel(
    private val profileRepository: ProfileRepository
) : BaseViewModel() {

    private var interests: MutableList<Interest> = mutableListOf()
    private var languages: MutableList<Language> = mutableListOf()
    private var skills: MutableList<Skill> = mutableListOf()

    fun getInfo() =
        liveData(Dispatchers.IO) {
            emit(Resource.Loading(show = true))

            val resourceInterest = tryResource {
                profileRepository.getUserInterests()
            }

            val resourceLanguage = tryResource {
                profileRepository.getUserLanguages()
            }

            val resourceSkill = tryResource {
                profileRepository.getUserSkills()
            }

            interests.clear()
            interests.addAll(resourceInterest.element<BaseWrapper<List<Interest>>>()?.data ?: mutableListOf())

            skills.clear()
            skills.addAll(resourceSkill.element<BaseWrapper<List<Skill>>>()?.data ?: mutableListOf())

            languages.clear()
            languages.addAll(resourceLanguage.element<BaseWrapper<List<Language>>>()?.data ?: mutableListOf())

            emit(resourceInterest)
            emit(resourceLanguage)
            emit(resourceSkill)

            emit(Resource.Loading(show = false))
        }

    fun delete(position: Int, type: Int) =
        liveData(Dispatchers.IO) {
            val resource = tryNoContentResource {
                when (type) {
                    TYPE_LANGUAGE -> profileRepository.deleteUserLanguages(languages[position].id!!)
                    TYPE_INTEREST -> profileRepository.deleteUserInterests(interests[position].id!!)
                    TYPE_SKILL -> profileRepository.deleteUserSkill(skills[position].id!!)
                    else -> throw IllegalStateException()
                }
            }
            emit(resource)
        }

    fun removeFromList(position: Int, type: Int) {
        when (type) {
            TYPE_SKILL -> skills.removeAt(position)
            TYPE_INTEREST -> interests.removeAt(position)
            TYPE_LANGUAGE -> languages.removeAt(position)
        }
    }

    fun getInterest(position: Int): Interest? {
        return interests[position]
    }

    fun getInterestsCount(): Int {
        return interests.size
    }

    fun getLanguage(position: Int): Language? {
        return languages[position]
    }

    fun getLanguagesCount(): Int {
        return languages.size
    }

    fun getSkill(position: Int): Skill? {
        return skills[position]
    }

    fun getSkillsCount(): Int {
        return skills.size
    }

}
