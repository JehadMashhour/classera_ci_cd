package com.classera.profile.skill

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
interface SkillFragmentRefresh {

    fun refresh()

}
