package com.classera.profile.skill

import android.view.ViewGroup
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.profile.databinding.SkillListRowItemBinding

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
class SkillsAdapter(
    private val viewModel: SkillsViewModel,
    private val type: Int
) : BaseAdapter<SkillsAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = SkillListRowItemBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        var count = 0 // default
        when (type){
            TYPE_SKILL -> {
                count = viewModel.getSkillsCount()
            }
            TYPE_LANGUAGE -> {
                count = viewModel.getLanguagesCount()
            }
            TYPE_INTEREST -> {
                count = viewModel.getInterestsCount()
            }
        }
        return count
    }

    fun removeItem(position: Int) {
        viewModel.removeFromList(position, type)
        notifyItemRemoved(position)
    }

    inner class ViewHolder(binding: SkillListRowItemBinding) : BaseBindingViewHolder(binding) {
        override fun bind(position: Int) {
            bind<SkillListRowItemBinding> {
                when (type){
                    TYPE_SKILL -> {
                        this.skill = viewModel.getSkill(position)?.skill
                        this.details = viewModel.getSkill(position)?.skillLevel
                    }
                    TYPE_LANGUAGE -> {
                        this.skill = viewModel.getLanguage(position)?.language
                        this.details = viewModel.getLanguage(position)?.proficiency
                    }
                    TYPE_INTEREST -> {
                        this.skill = viewModel.getInterest(position)?.interest
                        this.details = viewModel.getInterest(position)?.description
                    }
                }

            }
        }
    }
}

const val TYPE_SKILL = 0
const val TYPE_LANGUAGE = 1
const val TYPE_INTEREST = 2
