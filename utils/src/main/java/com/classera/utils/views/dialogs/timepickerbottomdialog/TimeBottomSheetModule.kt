package com.classera.utils.views.dialogs.timepickerbottomdialog

import androidx.fragment.app.Fragment
import dagger.Binds
import dagger.Module

@Module
abstract class TimeBottomSheetModule {



    @Binds
    abstract fun bindFragment(fragment: TimeBottomSheetFragment): Fragment
}
