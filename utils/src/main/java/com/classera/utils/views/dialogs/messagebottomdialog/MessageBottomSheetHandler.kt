package com.classera.utils.views.dialogs.messagebottomdialog

/**
 * Project: Classera
 * Created: 7/14/2021
 *
 * @author Jehad Abdalqader
 */
interface MessageBottomSheetHandler {
    fun onLeftButtonClicked()
    fun onRightButtonClicked()
}
