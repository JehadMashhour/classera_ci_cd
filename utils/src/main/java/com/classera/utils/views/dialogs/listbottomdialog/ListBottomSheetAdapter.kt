package com.classera.utils.views.dialogs.listbottomdialog

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.utils.databinding.RowListBottomSheetBinding

class ListBottomSheetAdapter(private val listBottomSheetViewModel: ListBottomSheetViewModel) :
    BaseAdapter<ListBottomSheetAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowListBottomSheetBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return listBottomSheetViewModel.getItemsCount()
    }

    inner class ViewHolder(binding: RowListBottomSheetBinding) :
        BaseBindingViewHolder(binding) {

        init {
            setItemViewClickListener()
        }

        private fun setItemViewClickListener() {
            itemView.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    listBottomSheetViewModel.toggleClickedSelectable(clickedPosition)
                    notifyDataSetChanged()
                }
            }
        }

        override fun bind(position: Int) {
            bind<RowListBottomSheetBinding> {
                this.selectable = listBottomSheetViewModel.getSelectable(position)
            }
        }

    }


}
