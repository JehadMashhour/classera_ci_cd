package com.classera.utils.views.fragments

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.classera.data.models.selection.Selectable
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class ListModule {

    @Module
    companion object {


        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: ListViewModelFactory
        ): ListViewModel {
            return ViewModelProvider(fragment, factory)[ListViewModel::class.java]
        }


        @Provides
        @JvmStatic
        fun provideSelectableList(fragment: Fragment): Array<out Selectable?> {
            val args by fragment.navArgs<ListFragmentArgs>()
            val selectables = args.selectableList

            selectables.map {
                it.apply {
                    selected = false
                }
            }.filter {
                it.id == args.selectedId && !args.selectedId.isNullOrEmpty()
            }.map {
                it.apply {
                    it.selected = true
                }
            }
            return selectables
        }


        @Provides
        @JvmStatic
        fun provideListBottomSheetAdapter(listViewModel: ListViewModel): ListAdapter {
            return ListAdapter(listViewModel)
        }
    }

    @Binds
    abstract fun bindFragment(fragment: ListFragment): Fragment

}
