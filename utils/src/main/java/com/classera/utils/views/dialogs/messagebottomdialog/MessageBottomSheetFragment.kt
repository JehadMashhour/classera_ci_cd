package com.classera.utils.views.dialogs.messagebottomdialog

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.classera.core.fragments.BaseBottomSheetDialogBindingFragment
import com.classera.utils.R
import com.classera.utils.databinding.FragmentMessageBottomSheetBinding
import com.classera.utils.views.dialogs.alertdialog.AlertDialogFragment
import java.util.*

/**
 * Project: Classera
 * Created: 6/13/2021
 *
 * @author Jehad Abdalqader
 */
class MessageBottomSheetFragment : BaseBottomSheetDialogBindingFragment(),
    MessageBottomSheetHandler {
    override val layoutId: Int = R.layout.fragment_message_bottom_sheet

    val args by navArgs<MessageBottomSheetFragmentArgs>()

    override fun isCancelable(): Boolean {
        return args.cancelable
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindData()
    }

    private fun bindData() {
        bind<FragmentMessageBottomSheetBinding> {
            this?.messageBottomSheetHandler = this@MessageBottomSheetFragment
            this?.messageBottomSheetFragmentArgs = args
        }
    }

    override fun onLeftButtonClicked() {
        findNavController().navigateUp()
        setResult(args.buttonLeftText)
    }

    override fun onRightButtonClicked() {
        findNavController().navigateUp()
        setResult(args.buttonRightText)
    }


    private fun setResult(buttonName: String?) {
        setFragmentResult(
            DEFAULT_REQUEST_KEY,
            bundleOf(BUTTON_CLICKED_NAME to buttonName)
        )
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        setFragmentResult(
            DISMISS_REQUEST_KEY,
            bundleOf(IS_DISMISSED to true)
        )
    }

    companion object {
        private const val DEFAULT_REQUEST_KEY = "default_request_key"
        private const val DISMISS_REQUEST_KEY = "dismiss_request_key"
        private const val KEY = "key"
        private const val IMAGE = "image"
        private const val TITLE = "title"
        private const val MESSAGE = "message"
        private const val BUTTON_LEFT_TEXT = "buttonLeftText"
        private const val BUTTON_RIGHT_TEXT = "buttonRightText"
        private const val CANCELABLE = "cancelable"
        private const val BUTTON_CLICKED_NAME = "buttonClicked"
        private const val IS_DISMISSED = "is_dismissed"
        fun show(
            fragment: Fragment,
            requestKey: String = DEFAULT_REQUEST_KEY,
            image: Int = -1,
            title: String? = null,
            message: String? = null,
            buttonLeftText: String? = null,
            buttonRightText: String? = null,
            cancelable: Boolean = true,
            dismissListener: ((dismissed: Boolean) -> Unit)? = null,
            listener: ((buttonName: String?) -> Unit)? = null,

        ) {

            fragment.setFragmentResultListener(DEFAULT_REQUEST_KEY) { _, bundle ->
                listener?.invoke(bundle.get(BUTTON_CLICKED_NAME) as String?)
            }

            fragment.setFragmentResultListener(DISMISS_REQUEST_KEY) { _, bundle ->
                dismissListener?.invoke(bundle.get(IS_DISMISSED) as Boolean)
            }

            fragment.findNavController().navigate(R.id.messageActionDirection,
                Bundle().apply {
                    putString(KEY, requestKey)
                    putInt(IMAGE, image)
                    putString(TITLE, title)
                    putString(MESSAGE, message)
                    putString(BUTTON_LEFT_TEXT, buttonLeftText)
                    putString(BUTTON_RIGHT_TEXT, buttonRightText)
                    putBoolean(CANCELABLE, cancelable)
                }
            )

        }
    }
}
