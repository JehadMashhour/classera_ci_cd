package com.classera.utils.views.dialogs.alertdialog

import androidx.fragment.app.Fragment
import dagger.Binds
import dagger.Module

@Module
abstract class AlertDialogModule {


    @Binds
    abstract fun bindFragment(fragment: AlertDialogFragment): Fragment
}
