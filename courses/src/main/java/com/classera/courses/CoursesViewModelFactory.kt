package com.classera.courses

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.courses.CoursesRepository
import javax.inject.Inject


/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
class CoursesViewModelFactory @Inject constructor(
    private val coursesRepository: CoursesRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CoursesViewModel(coursesRepository) as T
    }
}
