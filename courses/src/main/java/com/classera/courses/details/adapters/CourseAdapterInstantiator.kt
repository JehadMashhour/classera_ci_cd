package com.classera.courses.details.adapters

import com.classera.core.adapter.BaseAdapter
import com.classera.courses.details.CourseDetailsViewModel
import kotlin.reflect.KClass

/**
 * Project: Classera
 * Created: Jan 09, 2020
 *
 * @author Mohamed Hamdan
 */
class CourseAdapterInstantiator(private val kClass: KClass<*>) {

    fun getInstance(type: String, viewModel: CourseDetailsViewModel): BaseAdapter<*> {
        return kClass.java.constructors.first()?.newInstance(type, viewModel) as BaseAdapter<*>
    }
}
