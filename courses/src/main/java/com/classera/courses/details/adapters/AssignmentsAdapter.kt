package com.classera.courses.details.adapters

import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.classera.assignments.R
import com.classera.assignments.databinding.RowAssignmentsBinding
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.courses.details.CourseDetailsViewModel
import com.google.android.material.button.MaterialButton

/**
 * Project: Classera
 * Created: Jan 09, 2020
 *
 * @author Mohamed Hamdan
 */
class AssignmentsAdapter(
    type: String,
    private val viewModel: CourseDetailsViewModel
) : CourseDetailsAdapter<AssignmentsAdapter.ViewHolder>(type) {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowAssignmentsBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getAssignmentsCountBy(type)
    }

    inner class ViewHolder(binding: RowAssignmentsBinding) : BaseBindingViewHolder(binding) {

        private var viewAssignment: MaterialButton? = null
        private var startAssignment: MaterialButton? = null
        private var imageViewDetailsMenu: ImageView? = null

        init {
            viewAssignment = itemView.findViewById(R.id.button_row_assignment_second_action)
            startAssignment = itemView.findViewById(R.id.button_row_assignment_action)
            imageViewDetailsMenu = itemView.findViewById(R.id.image_view_row_assignment_details_menu)
        }

        override fun bind(position: Int) {

            viewAssignment?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    onItemClicked(it, this)
                }
            }

            startAssignment?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    onItemClicked(it, this)
                }
            }

            imageViewDetailsMenu?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    onItemClicked(it, this)
                }
            }

            bind<RowAssignmentsBinding> {
                assignmentsItem = viewModel.getAssignmentBy(type, position)
                currentRole = viewModel.getCurrentRole()
            }
        }
    }
}
