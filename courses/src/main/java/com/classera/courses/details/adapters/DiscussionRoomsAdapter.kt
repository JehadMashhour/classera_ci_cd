package com.classera.courses.details.adapters

import android.view.ViewGroup
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.courses.details.CourseDetailsViewModel
import com.classera.discussionrooms.databinding.RowDiscussionRoomBinding

/**
 * Project: Classera
 * Created: Jan 09, 2020
 *
 * @author Mohamed Hamdan
 */
class DiscussionRoomsAdapter(
    type: String,
    private val viewModel: CourseDetailsViewModel
) : CourseDetailsAdapter<DiscussionRoomsAdapter.ViewHolder>(type) {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowDiscussionRoomBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getDiscussionRoomsCount()
    }

    inner class ViewHolder(binding: RowDiscussionRoomBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowDiscussionRoomBinding> {
                discussionRoom = viewModel.getDiscussionRoomBy(position)
            }
        }
    }
}
