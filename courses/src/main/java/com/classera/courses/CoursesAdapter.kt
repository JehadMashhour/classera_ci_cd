package com.classera.courses

import android.view.ViewGroup
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.courses.databinding.RowCoursesBinding


/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */

class CoursesAdapter(
    private val viewModel: CoursesViewModel
) : BaseAdapter<CoursesAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowCoursesBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getCoursesCount()
    }

    inner class ViewHolder(binding: RowCoursesBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowCoursesBinding> { course = viewModel.getCourse(position) }
        }
    }
}
