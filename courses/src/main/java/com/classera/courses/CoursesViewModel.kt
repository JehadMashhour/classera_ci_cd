package com.classera.courses

import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.courses.Course
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.courses.CoursesRepository
import kotlinx.coroutines.Dispatchers

/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
class CoursesViewModel(
    private val coursesRepository: CoursesRepository
) : BaseViewModel() {

    private var courses: MutableList<Course> = mutableListOf()

    fun getCourses() = liveData(Dispatchers.IO) {

        emit(Resource.Loading(show = true))

        val resource =
            tryResource { coursesRepository.getCourses() }

        courses.clear()
        courses.addAll(resource.element<BaseWrapper<List<Course>>>()?.data ?: mutableListOf())
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getCourse(position: Int): Course? {
        return courses[position]
    }

    fun getCoursesCount(): Int {
        return courses.size
    }
}
