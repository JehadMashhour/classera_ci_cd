package com.classera.classvisits

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides


/**
 * Created by Rawan Al-Theeb on 2/12/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Module
abstract class ClassVisitsFragmentModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: ClassVisitsViewModelFactory
        ): ClassVisitsViewModel {
            return ViewModelProvider(fragment, factory)[ClassVisitsViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindFragment(fragment: ClassVisitsFragment): Fragment
}
