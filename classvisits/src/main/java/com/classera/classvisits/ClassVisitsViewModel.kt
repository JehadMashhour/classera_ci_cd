package com.classera.classvisits

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.classvisits.ClassVisit
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.classvisits.ClassVisitsRepository
import kotlinx.coroutines.Dispatchers


/**
 * Created by Rawan Al-Theeb on 2/12/2020.
 * Classera
 * r.altheeb@classera.com
 */
class ClassVisitsViewModel(private val classVisitsRepository: ClassVisitsRepository) : BaseViewModel() {

    private val _notifyAdapterItemLiveData = MutableLiveData<Int>()
    val notifyAdapterItemLiveData: LiveData<Int> = _notifyAdapterItemLiveData

    private val _toastLiveData = MutableLiveData<Any>()
    val toastLiveData: LiveData<Any> = _toastLiveData

    private var classVisits: MutableList<ClassVisit> = mutableListOf()

    fun getClassVisits(): LiveData<Resource> {
        return getClassVisits(true)
    }

    fun refreshClassVisits() = getClassVisits(false)

    private fun getClassVisits(
        showProgress: Boolean
    ) =
        liveData(Dispatchers.IO) {
            if (showProgress) {
                emit(Resource.Loading(show = true))
            }

            val resource = tryResource {
                classVisitsRepository.classVisits()
            }
            classVisits.clear()
            classVisits.addAll(
                resource.element<BaseWrapper<List<ClassVisit>>>()?.data ?: mutableListOf()
            )
            emit(resource)
            emit(Resource.Loading(show = false))
        }

    fun getClassVisit(position: Int): ClassVisit? {
        return classVisits[position]
    }

    fun getClassVisitCount(): Int {
        return classVisits.size
    }
}
