package com.classera.classvisits

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.classvisits.ClassVisitsRepository
import javax.inject.Inject


/**
 * Created by Rawan Al-Theeb on 2/12/2020.
 * Classera
 * r.altheeb@classera.com
 */
class ClassVisitsViewModelFactory @Inject constructor(private val classVisitsRepository: ClassVisitsRepository) :
    ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ClassVisitsViewModel(classVisitsRepository) as T
    }
}
