package com.classera.classvisits.add

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.classvisits.Assessment
import com.classera.data.models.classvisits.Teacher
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.classvisits.ClassVisitsRepository
import kotlinx.coroutines.Dispatchers


/**
 * Created by Rawan Al-Theeb on 2/13/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Suppress("LongParameterList")
class AddClassVisitViewModel(
    private val classVisitsRepository: ClassVisitsRepository
) : BaseViewModel() {

    private var teachers: MutableList<Teacher> = mutableListOf()
    private var assessments: MutableList<Assessment> = mutableListOf()

    fun getTeachers(): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            classVisitsRepository.getTeachers()
        }
        teachers.addAll(
            resource.element<BaseWrapper<List<Teacher>>>()?.data ?: mutableListOf()
        )
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getAssessments(): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            classVisitsRepository.getAssessments()
        }
        assessments.addAll(
            resource.element<BaseWrapper<List<Assessment>>>()?.data ?: mutableListOf()
        )
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun getTimeslots(
        date: String?,
        teacherId: String?
    ): LiveData<Resource> = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        val resource = tryResource {
            classVisitsRepository.getTimeslots(date, teacherId)
        }
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    fun onSaveClicked(
        date: String?,
        teacherId: String?,
        assessmentId: String?,
        privateVisit: String?,
        timeslotTitle: String?,
        timeslotId: String?
    ) = liveData {
        emit(Resource.Loading(show = true))
        val resource =
            startAddingClassVisit(date, teacherId, assessmentId, privateVisit, timeslotTitle, timeslotId)
        emit(resource)
        emit(Resource.Loading(show = false))
    }

    private suspend fun startAddingClassVisit(
        date: String?,
        teacherId: String?,
        assessmentId: String?,
        privateVisit: String?,
        timeslotTitle: String?,
        timeslotId: String?
    ): Resource =
        tryNoContentResource {
            classVisitsRepository.addClassVisit(
                date = date,
                teacherId = teacherId,
                assessmentId = assessmentId,
                privateVisit = privateVisit,
                timeslotTitle = timeslotTitle,
                timeslotId = timeslotId
            )
        }
}
