package com.classera.classvisits

import android.view.ViewGroup
import com.classera.classvisits.databinding.RowClassVisitBinding
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder


/**
 * Created by Rawan Al-Theeb on 2/12/2020.
 * Classera
 * r.altheeb@classera.com
 */
class ClassVisitsAdapter(
    private val viewModel: ClassVisitsViewModel
) : BaseAdapter<ClassVisitsAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowClassVisitBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getClassVisitCount()
    }

    inner class ViewHolder(binding: RowClassVisitBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowClassVisitBinding> { classVisitItem = viewModel.getClassVisit(position) }
        }
    }
}
