package com.classera.fcm

import dagger.Module

/**
 * Project: Classera
 * Created: Mar 04, 2020
 *
 * @author Mohamed Hamdan
 */
@Module
class FirebaseMessagingServiceModule
