package com.classera.di

import com.classera.data.BuildConfig
import com.classera.data.models.chat.moshi.ViewTypeAdapter
import com.classera.data.moshi.capitalize.CapitalizeAdapter
import com.classera.data.moshi.chattime.ChatTimeAdapter
import com.classera.data.moshi.duration.DurationAdapter
import com.classera.data.moshi.messagebody.MessageBodyAdapter
import com.classera.data.moshi.messages.MessageTypeAdapter
import com.classera.data.moshi.rate.RateAdapter
import com.classera.data.moshi.shortday.ShortDayNameAdapter
import com.classera.data.moshi.subquestions.SubQuestionsAdapter
import com.classera.data.moshi.timeago.TimeAgoAdapter
import com.classera.data.moshi.timestampago.TimeStampAgoAdapter
import com.classera.data.moshi.toStringNumber.ToStringNumberAdapter
import com.classera.data.moshi.todate.ToDateAdapter
import com.classera.data.moshi.toint.ToIntAdapter
import com.classera.data.moshi.trim.TrimAdapter
import com.classera.data.network.intercepters.AccessTokenInterceptor
import com.classera.data.network.intercepters.AuthenticationTokenInterceptor
import com.classera.data.network.intercepters.ChildIdnInterceptor
import com.classera.data.network.intercepters.LanguageInterceptor
import com.classera.data.network.intercepters.PaginationInterceptor
import com.classera.data.network.intercepters.SchoolTokenInterceptor
import com.classera.data.network.intercepters.SemesterTokenInterceptor
import com.classera.data.network.intercepters.W3WApiKeyInterceptor
import com.classera.data.network.intercepters.RefreshTokenInterceptor
import com.classera.data.network.intercepters.BundleIdInterceptor
import com.classera.data.network.intercepters.AppVersionInterceptor
import com.classera.data.network.intercepters.AppTypeInterceptor
import com.classera.data.network.intercepters.OnlineNowInterceptor
import com.classera.data.prefs.Prefs
import com.classera.di.qualifiers.Chat
import com.classera.di.qualifiers.Notification
import com.classera.di.qualifiers.OnlineNow
import com.classera.di.qualifiers.W3w
import com.google.gson.Gson
import com.itkacher.okhttpprofiler.OkHttpProfilerInterceptor
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.engineio.client.transports.WebSocket
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import tech.linjiang.pandora.Pandora
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Suppress("LongParameterList")
@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideOkHttp(
        accessTokenInterceptor: AccessTokenInterceptor,
        authenticationTokenInterceptor: AuthenticationTokenInterceptor,
        semesterAuthTokenInterceptor: SemesterTokenInterceptor,
        paginationInterceptor: PaginationInterceptor,
        schoolTokenInterceptor: SchoolTokenInterceptor,
        childIdnInterceptor: ChildIdnInterceptor,
        languageInterceptor: LanguageInterceptor,
        appTypeInterceptor: AppTypeInterceptor,
        refreshTokenInterceptor: RefreshTokenInterceptor,
        appBundleIdInterceptor: BundleIdInterceptor /* TO BE ADDED AFTER BE DEPLOY THE CODE */,
        appVersionInterceptor: AppVersionInterceptor /* TO BE ADDED AFTER BE DEPLOY THE CODE */
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(Pandora.get().interceptor)
            .addInterceptor(accessTokenInterceptor)
            .addInterceptor(semesterAuthTokenInterceptor)
            .addInterceptor(paginationInterceptor)
            .addInterceptor(authenticationTokenInterceptor)
            .addInterceptor(schoolTokenInterceptor)
            .addInterceptor(childIdnInterceptor)
            .addInterceptor(languageInterceptor)
            .addInterceptor(appTypeInterceptor)
            .addInterceptor(refreshTokenInterceptor)
            .addInterceptor(OkHttpProfilerInterceptor())
            .readTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .writeTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .connectTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .callTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .build()
    }


    @Provides
    @Singleton
    @W3w
    fun provideW3WOkHttp(w3WApiKeyInterceptor: W3WApiKeyInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(Pandora.get().interceptor)
            .addInterceptor(w3WApiKeyInterceptor)
            .addInterceptor(OkHttpProfilerInterceptor())
            .readTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .writeTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .connectTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .callTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .build()
    }

    @Provides
    @Singleton
    @Chat
    fun provideChatOkHttp(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(Pandora.get().interceptor)
            .addInterceptor(OkHttpProfilerInterceptor())
            .readTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .writeTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .connectTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .callTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .build()
    }

    @Provides
    @Singleton
    @Notification
    fun provideNotificationOkHttp(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(Pandora.get().interceptor)
            .addInterceptor(OkHttpProfilerInterceptor())
            .readTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .writeTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .connectTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .callTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .build()
    }

    @Provides
    @Singleton
    @OnlineNow
    fun provideOnlineNowOkHttp(onlineNowInterceptor: OnlineNowInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(Pandora.get().interceptor)
            .addInterceptor(onlineNowInterceptor)
            .addInterceptor(OkHttpProfilerInterceptor())
            .readTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .writeTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .connectTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .callTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient, moshi: Moshi): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.flavorData.apiBaseUrl)
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient())
            .build()
    }

    @Provides
    @Singleton
    @Chat
    fun provideChatRetrofit(
        @Chat okHttpClient: OkHttpClient,
        moshi: Moshi
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.flavorData.chatBaseUrl)
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
    }

    @Provides
    @Singleton
    fun provideMoshi(prefs: Prefs): Moshi {
        return Moshi.Builder()
            .addMyEnumAdapters()
            .add(CapitalizeAdapter())
            .add(ToIntAdapter())
            .add(RateAdapter())
            .add(ToDateAdapter())
            .add(ToStringNumberAdapter())
            .add(TimeAgoAdapter())
            .add(ChatTimeAdapter())
            .add(ViewTypeAdapter(prefs))
            .add(MessageTypeAdapter())
            .add(MessageBodyAdapter(Moshi.Builder().add(DurationAdapter()).build()))
            .add(ShortDayNameAdapter())
            .add(SubQuestionsAdapter(Gson()))
            .add(TrimAdapter())
            .add(KotlinJsonAdapterFactory())
            .add(TimeStampAgoAdapter())
            .add(DurationAdapter())
            .build()
    }

    @Provides
    @Singleton
    @W3w
    fun provideW3WRetrofit(
        @W3w okHttpClient: OkHttpClient,
        @W3w moshi: Moshi
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.flavorData.w3wBaseUrl)
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
    }

    @Provides
    @Singleton
    @W3w
    fun provideW3WMoshi(): Moshi {
        return Moshi.Builder()
            .build()
    }

    @Provides
    @Singleton
    @OnlineNow
    fun provideOnlineNowRetrofit(
        @OnlineNow okHttpClient: OkHttpClient,
        moshi: Moshi
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.flavorData.onlineNowBaseUrl)
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
    }

    @Provides
    @Singleton
    @Notification
    fun provideNotificationRetrofit(
        @Notification okHttpClient: OkHttpClient,
        moshi: Moshi
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.flavorData.notificationBaseUrl)
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
    }

    @Provides
    @Singleton
    fun provideSocket(prefs: Prefs): Socket {
        val options = IO.Options()
        options.path = BuildConfig.flavorData.socketPathUrl
        options.port = SOCKET_PORT
        options.reconnection = true
        options.transports = arrayOf(WebSocket.NAME)

        options.query =
            "userId=${prefs.userId}&source=me&school_id=${prefs.schoolId}&role_id=${prefs.userRole?.value}"
        return IO.socket(BuildConfig.flavorData.socketBaseUrl, options).apply { connect() }
    }

    companion object {

        private const val TIMEOUT_MINUTES = 1L
        private const val SOCKET_PORT = 80
    }
}
