package com.classera.di.qualifiers

import javax.inject.Qualifier

/**
 * Project: Classera
 * Created: SEP 20, 2020
 *
 * @author Saeed Halawani
 */
@Qualifier
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class Notification
