package com.classera.di

import com.classera.data.daos.announcements.RemoteAnnouncementsDao
import com.classera.data.daos.asessments.RemoteAssessmentsDao
import com.classera.data.daos.assignments.LocalLastQuestionDao
import com.classera.data.daos.assignments.RemoteAssignmentsDao
import com.classera.data.daos.attachment.RemoteAttachmentDao
import com.classera.data.daos.attendance.RemoteAttendancesDao
import com.classera.data.daos.authentication.RemoteAuthenticationDao
import com.classera.data.daos.behaviours.RemoteBehavioursDao
import com.classera.data.daos.calendar.RemoteCalendarDao
import com.classera.data.daos.callchild.RemoteCallChildDao
import com.classera.data.daos.chat.RemoteChatDao
import com.classera.data.daos.chat.RemoteChatServiceDao
import com.classera.data.daos.classvisits.RemoteClassVisitsDao
import com.classera.data.daos.courses.RemoteCoursesDao
import com.classera.data.daos.digitallibrary.RemoteDigitalLibraryDao
import com.classera.data.daos.discussions.RemoteDiscussionDao
import com.classera.data.daos.eportfolios.RemoteEPortfolioDao
import com.classera.data.daos.home.RemoteHomeDao
import com.classera.data.daos.home.RemoteOnlineNowDao
import com.classera.data.daos.homeloction.RemoteHomeLocationDao
import com.classera.data.daos.homeloction.RemoteW3WDao
import com.classera.data.daos.lectures.RemoteLecturesDao
import com.classera.data.daos.mailbox.LocalMailboxDao
import com.classera.data.daos.mailbox.RemoteMailboxDao
import com.classera.data.daos.mycard.RemoteMyCardDao
import com.classera.data.daos.notification.RemoteNotificationsDao
import com.classera.data.daos.parent.RemoteParentChildsDao
import com.classera.data.daos.profile.RemoteProfileDao
import com.classera.data.daos.profile.publicprofile.RemotePublicProfileDao
import com.classera.data.daos.reportcards.RemoteReportCardsDao
import com.classera.data.daos.schedule.RemoteScheduleDao
import com.classera.data.daos.settings.RemoteSettingsDao
import com.classera.data.daos.support.RemoteSupportDao
import com.classera.data.daos.surveys.RemoteSurveysDao
import com.classera.data.daos.switchroles.RemoteSwitchRolesDao
import com.classera.data.daos.switchschools.RemoteSwitchSchoolsDao
import com.classera.data.daos.switchsemester.RemoteSwitchSemestersDao
import com.classera.data.daos.user.LocalUserDao
import com.classera.data.daos.user.RemoteUserDao
import com.classera.data.daos.vcr.RemoteVcrDao
import com.classera.data.daos.weeklyplan.RemoteWeeklyPlanDao
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.announcements.AnnouncementsRepository
import com.classera.data.repositories.announcements.AnnouncementsRepositoryImpl
import com.classera.data.repositories.assessments.AssessmentRepositoryImpl
import com.classera.data.repositories.assessments.AssessmentsRepoistory
import com.classera.data.repositories.assignments.AssignmentsRepository
import com.classera.data.repositories.assignments.AssignmentsRepositoryImpl
import com.classera.data.repositories.attachment.AttachmentRepository
import com.classera.data.repositories.attachment.AttachmentRepositoryImpl
import com.classera.data.repositories.attendance.AttendanceRepository
import com.classera.data.repositories.attendance.AttendanceRepositoryImpl
import com.classera.data.repositories.authentication.AuthenticationRepository
import com.classera.data.repositories.authentication.AuthenticationRepositoryImpl
import com.classera.data.repositories.behaviours.BehavioursRepository
import com.classera.data.repositories.behaviours.BehavioursRepositoryImpl
import com.classera.data.repositories.calendar.CalendarRepository
import com.classera.data.repositories.calendar.CalendarRepositoryImpl
import com.classera.data.repositories.callchild.CallChildRepository
import com.classera.data.repositories.callchild.CallChildRepositoryImpl
import com.classera.data.repositories.chat.ChatBlockedUsersRepository
import com.classera.data.repositories.chat.ChatBlockedUsersRepositoryImpl
import com.classera.data.repositories.chat.ChatRepository
import com.classera.data.repositories.chat.ChatRepositoryImpl
import com.classera.data.repositories.chat.ChatServiceRepository
import com.classera.data.repositories.chat.ChatServiceRepositoryImpl
import com.classera.data.repositories.classvisits.ClassVisitsRepository
import com.classera.data.repositories.classvisits.ClassVisitsRepositoryImpl
import com.classera.data.repositories.courses.CoursesRepository
import com.classera.data.repositories.courses.CoursesRepositoryImpl
import com.classera.data.repositories.digitallibrary.DigitalLibraryRepository
import com.classera.data.repositories.digitallibrary.DigitalLibraryRepositoryImpl
import com.classera.data.repositories.discussions.DiscussionRoomsRepository
import com.classera.data.repositories.discussions.DiscussionRoomsRepositoryImpl
import com.classera.data.repositories.eportfolios.EPortfolioRepository
import com.classera.data.repositories.eportfolios.EPortfolioRepositoryImpl
import com.classera.data.repositories.home.HomeRepository
import com.classera.data.repositories.home.HomeRepositoryImpl
import com.classera.data.repositories.home.OnlineNowRepository
import com.classera.data.repositories.home.OnlineNowRepositoryImpl
import com.classera.data.repositories.homelocation.HomeLocationRepository
import com.classera.data.repositories.homelocation.HomeLocationRepositoryImpl
import com.classera.data.repositories.lectures.LecturesRepository
import com.classera.data.repositories.lectures.LecturesRepositoryImpl
import com.classera.data.repositories.mailbox.MailboxRepository
import com.classera.data.repositories.mailbox.MailboxRepositoryImpl
import com.classera.data.repositories.mycard.MyCardRepositoryImpl
import com.classera.data.repositories.mycard.MyCradRepository
import com.classera.data.repositories.notification.NotificationRepository
import com.classera.data.repositories.notification.NotificationRepositoryImpl
import com.classera.data.repositories.parent.ParentChildRepository
import com.classera.data.repositories.parent.ParentChildRepositoryImpl
import com.classera.data.repositories.profile.ProfileRepository
import com.classera.data.repositories.profile.ProfileRepositoryImpl
import com.classera.data.repositories.profile.publicprofile.PublicProfileRepository
import com.classera.data.repositories.profile.publicprofile.PublicProfileRepositoryImpl
import com.classera.data.repositories.reportcards.ReportCardsRepository
import com.classera.data.repositories.reportcards.ReportCardsRepositoryImpl
import com.classera.data.repositories.schedule.ScheduleRepository
import com.classera.data.repositories.schedule.ScheduleRepositoryImpl
import com.classera.data.repositories.settings.SettingsRepository
import com.classera.data.repositories.settings.SettingsRepositoryImpl
import com.classera.data.repositories.support.SupportRepository
import com.classera.data.repositories.support.SupportRepositoryImpl
import com.classera.data.repositories.surveys.SurveysRepository
import com.classera.data.repositories.surveys.SurveysRepositoryImpl
import com.classera.data.repositories.switchroles.SwitchRolesRepository
import com.classera.data.repositories.switchroles.SwitchRolesRepositoryImpl
import com.classera.data.repositories.switchschools.SwitchSchoolsRepository
import com.classera.data.repositories.switchschools.SwitchSchoolsRepositoryImpl
import com.classera.data.repositories.switchsemester.SwitchSemesterRepository
import com.classera.data.repositories.switchsemester.SwitchSemesterRepositoryImpl
import com.classera.data.repositories.user.UserRepository
import com.classera.data.repositories.user.UserRepositoryImpl
import com.classera.data.repositories.vcr.VcrRepository
import com.classera.data.repositories.vcr.VcrRepositoryImpl
import com.classera.data.repositories.weeklyplan.WeeklyPlanRepository
import com.classera.data.repositories.weeklyplan.WeeklyPlanRepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Suppress("TooManyFunctions")
@Module
object RepositoriesModule {

    @Provides
    @Singleton
    fun provideAuthenticationRepository(
        remoteAuthenticationDao: RemoteAuthenticationDao,
        userRepository: UserRepository,
        prefs: Prefs,
        settingsRepository: SettingsRepository
    ): AuthenticationRepository {
        return AuthenticationRepositoryImpl(
            remoteAuthenticationDao,
            userRepository,
            settingsRepository,
            prefs
        )
    }

    @Provides
    @Singleton
    fun provideUserRepository(
        remoteUserDao: RemoteUserDao,
        localUserDao: LocalUserDao,
        prefs: Prefs
    ): UserRepository {
        return UserRepositoryImpl(remoteUserDao, localUserDao, prefs)
    }

    @Provides
    @Singleton
    fun provideMyCradRepository(
        remoteMyCardDao: RemoteMyCardDao
    ): MyCradRepository {
        return MyCardRepositoryImpl(remoteMyCardDao)
    }

    @Provides
    @Singleton
    fun provideProfileRepository(
        remoteProfileDao: RemoteProfileDao
    ): ProfileRepository {
        return ProfileRepositoryImpl(remoteProfileDao)
    }

    @Provides
    @Singleton
    fun provideHomeRepository(
        remoteHomeDao: RemoteHomeDao
    ): HomeRepository {
        return HomeRepositoryImpl(remoteHomeDao)
    }

    @Provides
    @Singleton
    fun provideAsessmentRepository(
        remoteAsessmentsDao: RemoteAssessmentsDao
    ): AssessmentsRepoistory {
        return AssessmentRepositoryImpl(remoteAsessmentsDao)
    }

    @Provides
    @Singleton
    fun provideDigitalLibraryRepository(
        remoteDigitalLibraryDao: RemoteDigitalLibraryDao
    ): DigitalLibraryRepository {
        return DigitalLibraryRepositoryImpl(remoteDigitalLibraryDao)
    }

    @Provides
    @Singleton
    fun provideVcrRepository(
        remoteVcrDao: RemoteVcrDao,
        prefs: Prefs
    ): VcrRepository {
        return VcrRepositoryImpl(remoteVcrDao, prefs)
    }

    @Provides
    @Singleton
    fun provideAssignmentsRepository(
        remoteAssignmentsDao: RemoteAssignmentsDao,
        localLastQuestionDao: LocalLastQuestionDao
    ): AssignmentsRepository {
        return AssignmentsRepositoryImpl(remoteAssignmentsDao, localLastQuestionDao)
    }

    @Provides
    @Singleton
    fun provideAttachmentRepository(
        attachmentDao: RemoteAttachmentDao
    ): AttachmentRepository {
        return AttachmentRepositoryImpl(attachmentDao)
    }

    @Provides
    @Singleton
    fun provideCalendarRepository(
        remoteCalendarDao: RemoteCalendarDao
    ): CalendarRepository {
        return CalendarRepositoryImpl(remoteCalendarDao)
    }

    @Provides
    @Singleton
    fun provideWeeklyPlanRepository(remoteWeeklyPlanDao: RemoteWeeklyPlanDao): WeeklyPlanRepository {
        return WeeklyPlanRepositoryImpl(remoteWeeklyPlanDao)
    }

    @Provides
    @Singleton
    fun provideDiscussionRoomsRepository(remoteDiscussionDao: RemoteDiscussionDao): DiscussionRoomsRepository {
        return DiscussionRoomsRepositoryImpl(remoteDiscussionDao)
    }

    @Provides
    @Singleton
    fun provideBehavioursRepository(remoteBehavioursDao: RemoteBehavioursDao): BehavioursRepository {
        return BehavioursRepositoryImpl(remoteBehavioursDao)
    }

    @Provides
    @Singleton
    fun provideMailboxRepository(
        remoteMailboxDao: RemoteMailboxDao,
        localMailboxDao: LocalMailboxDao
    ): MailboxRepository {
        return MailboxRepositoryImpl(remoteMailboxDao, localMailboxDao)
    }

    @Provides
    @Singleton
    fun provideAnnouncementsRepository(remoteAnnouncementsDao: RemoteAnnouncementsDao): AnnouncementsRepository {
        return AnnouncementsRepositoryImpl(remoteAnnouncementsDao)
    }

    @Provides
    @Singleton
    fun provideCoursesRepository(
        remoteCoursesDao: RemoteCoursesDao,
        prefs: Prefs
    ): CoursesRepository {
        return CoursesRepositoryImpl(remoteCoursesDao, prefs)
    }

    @Provides
    @Singleton
    fun provideAttendanceRepository(remoteAttendancesDao: RemoteAttendancesDao): AttendanceRepository {
        return AttendanceRepositoryImpl(remoteAttendancesDao)
    }

    @Provides
    @Singleton
    fun provideHomeLocationRepository(
        remoteHomeLocationDao: RemoteHomeLocationDao,
        remoteW3WDao: RemoteW3WDao
    ): HomeLocationRepository {
        return HomeLocationRepositoryImpl(remoteHomeLocationDao, remoteW3WDao)
    }

    @Provides
    @Singleton
    fun provideChatRepository(remoteChatDao: RemoteChatDao, prefs: Prefs): ChatRepository {
        return ChatRepositoryImpl(remoteChatDao, prefs)
    }

    @Provides
    @Singleton
    fun provideChatServiceRepository(
        remoteChatServiceDao: RemoteChatServiceDao,
        prefs: Prefs,
        chatRepository: ChatRepository
    ): ChatServiceRepository {
        return ChatServiceRepositoryImpl(remoteChatServiceDao, chatRepository, prefs)
    }

    @Provides
    @Singleton
    fun provideChatBlockedUsersRepository(
        remoteChatDao: RemoteChatDao
    ): ChatBlockedUsersRepository {
        return ChatBlockedUsersRepositoryImpl(remoteChatDao)
    }

    @Provides
    @Singleton
    fun provideSwitchRolesRepository(
        remoteSwitchRolesDao: RemoteSwitchRolesDao, prefs: Prefs
    ): SwitchRolesRepository {
        return SwitchRolesRepositoryImpl(remoteSwitchRolesDao, prefs)
    }

    @Provides
    @Singleton
    fun provideScheduleRepository(remoteScheduleDao: RemoteScheduleDao): ScheduleRepository {
        return ScheduleRepositoryImpl(remoteScheduleDao)
    }

    @Provides
    @Singleton
    fun provideSwitchSchoolsRepository(
        remoteSwitchSchoolsDao: RemoteSwitchSchoolsDao,
        prefs: Prefs
    ): SwitchSchoolsRepository {
        return SwitchSchoolsRepositoryImpl(remoteSwitchSchoolsDao, prefs)
    }

    @Provides
    @Singleton
    fun provideEportfolioRepository(remoteEportfolioDao: RemoteEPortfolioDao): EPortfolioRepository {
        return EPortfolioRepositoryImpl(remoteEportfolioDao)
    }

    @Provides
    @Singleton
    fun provideSwitchSemestersRepository(
        remoteSwitchSemestersDao: RemoteSwitchSemestersDao, prefs: Prefs
    ): SwitchSemesterRepository {
        return SwitchSemesterRepositoryImpl(remoteSwitchSemestersDao, prefs)
    }

    @Provides
    @Singleton
    fun provideLecturesRepository(remoteLecturesDao: RemoteLecturesDao): LecturesRepository {
        return LecturesRepositoryImpl(remoteLecturesDao)
    }

    @Provides
    @Singleton
    fun provideSupportRepository(remoteSupportDao: RemoteSupportDao): SupportRepository {
        return SupportRepositoryImpl(remoteSupportDao)
    }

    @Provides
    @Singleton
    fun provideSurveysRepository(remoteSurveysDao: RemoteSurveysDao): SurveysRepository {
        return SurveysRepositoryImpl(remoteSurveysDao)
    }

    @Provides
    @Singleton
    fun provideReportCardsRepository(remoteReportCardsDao: RemoteReportCardsDao): ReportCardsRepository {
        return ReportCardsRepositoryImpl(remoteReportCardsDao)
    }

    @Provides
    @Singleton
    fun provideClassVisitsDao(remoteClassVisitsDao: RemoteClassVisitsDao): ClassVisitsRepository {
        return ClassVisitsRepositoryImpl(remoteClassVisitsDao)
    }

    @Provides
    @Singleton
    fun provideSettingsDao(remoteSettingsDao: RemoteSettingsDao, prefs: Prefs): SettingsRepository {
        return SettingsRepositoryImpl(remoteSettingsDao, prefs)
    }

    @Provides
    @Singleton
    fun provideParentChildsDao(remoteParentChildsDao: RemoteParentChildsDao): ParentChildRepository {
        return ParentChildRepositoryImpl(remoteParentChildsDao)
    }

    @Provides
    @Singleton
    fun providePublicProfileDao(remotePublicProfileDao: RemotePublicProfileDao): PublicProfileRepository {
        return PublicProfileRepositoryImpl(remotePublicProfileDao)
    }

    @Provides
    @Singleton
    fun provideNotificationListDao(
        remoteNotificationsDao: RemoteNotificationsDao,
        prefs: Prefs
    ): NotificationRepository {
        return NotificationRepositoryImpl(remoteNotificationsDao, prefs)
    }

    @Provides
    @Singleton
    fun provideOnlineNowDao(remoteOnlineNowDao: RemoteOnlineNowDao): OnlineNowRepository {
        return OnlineNowRepositoryImpl(remoteOnlineNowDao)
    }

    @Provides
    @Singleton
    fun provideCallChildDao(remoteCallChildDao: RemoteCallChildDao): CallChildRepository {
        return CallChildRepositoryImpl(remoteCallChildDao)
    }
}
