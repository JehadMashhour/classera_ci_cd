package com.classera.di

import android.app.Application
import com.classera.MyApplication
import dagger.Binds
import dagger.Module

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Module
abstract class AppModule {

    @Binds
    abstract fun provideApplication(application: MyApplication): Application
}
