package com.classera.di

import com.classera.data.daos.announcements.RemoteAnnouncementsDao
import com.classera.data.daos.asessments.RemoteAssessmentsDao
import com.classera.data.daos.assignments.LocalLastQuestionDao
import com.classera.data.daos.assignments.RemoteAssignmentsDao
import com.classera.data.daos.attachment.RemoteAttachmentDao
import com.classera.data.daos.attendance.RemoteAttendancesDao
import com.classera.data.daos.authentication.RemoteAuthenticationDao
import com.classera.data.daos.behaviours.RemoteBehavioursDao
import com.classera.data.daos.calendar.RemoteCalendarDao
import com.classera.data.daos.callchild.RemoteCallChildDao
import com.classera.data.daos.chat.RemoteChatDao
import com.classera.data.daos.chat.RemoteChatServiceDao
import com.classera.data.daos.classvisits.RemoteClassVisitsDao
import com.classera.data.daos.courses.RemoteCoursesDao
import com.classera.data.daos.digitallibrary.RemoteDigitalLibraryDao
import com.classera.data.daos.discussions.RemoteDiscussionDao
import com.classera.data.daos.eportfolios.RemoteEPortfolioDao
import com.classera.data.daos.home.RemoteHomeDao
import com.classera.data.daos.home.RemoteOnlineNowDao
import com.classera.data.daos.homeloction.RemoteHomeLocationDao
import com.classera.data.daos.homeloction.RemoteW3WDao
import com.classera.data.daos.lectures.RemoteLecturesDao
import com.classera.data.daos.mailbox.LocalMailboxDao
import com.classera.data.daos.mailbox.RemoteMailboxDao
import com.classera.data.daos.mycard.RemoteMyCardDao
import com.classera.data.daos.notification.RemoteNotificationsDao
import com.classera.data.daos.parent.RemoteParentChildsDao
import com.classera.data.daos.profile.RemoteProfileDao
import com.classera.data.daos.profile.publicprofile.RemotePublicProfileDao
import com.classera.data.daos.reportcards.RemoteReportCardsDao
import com.classera.data.daos.schedule.RemoteScheduleDao
import com.classera.data.daos.settings.RemoteSettingsDao
import com.classera.data.daos.support.RemoteSupportDao
import com.classera.data.daos.surveys.RemoteSurveysDao
import com.classera.data.daos.switchroles.RemoteSwitchRolesDao
import com.classera.data.daos.switchschools.RemoteSwitchSchoolsDao
import com.classera.data.daos.switchsemester.RemoteSwitchSemestersDao
import com.classera.data.daos.user.LocalUserDao
import com.classera.data.daos.user.RemoteUserDao
import com.classera.data.daos.vcr.RemoteVcrDao
import com.classera.data.daos.weeklyplan.RemoteWeeklyPlanDao
import com.classera.data.database.Database
import com.classera.di.qualifiers.Chat
import com.classera.di.qualifiers.OnlineNow
import com.classera.di.qualifiers.Notification
import com.classera.di.qualifiers.W3w
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Suppress("TooManyFunctions")
@Module
object DaosModule {

    @Provides
    @Singleton
    fun provideRemoteAuthenticationDao(retrofit: Retrofit): RemoteAuthenticationDao {
        return retrofit.create(RemoteAuthenticationDao::class.java)
    }

    @Provides
    @Singleton
    fun provideLocalUserDao(database: Database): LocalUserDao {
        return database.localUserDao()
    }

    @Provides
    @Singleton
    fun provideRemoteUserDao(retrofit: Retrofit): RemoteUserDao {
        return retrofit.create(RemoteUserDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemotAssessmentsDao(retrofit: Retrofit): RemoteAssessmentsDao {
        return retrofit.create(RemoteAssessmentsDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteMyCardDao(retrofit: Retrofit): RemoteMyCardDao {
        return retrofit.create(RemoteMyCardDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteVcrDao(retrofit: Retrofit): RemoteVcrDao {
        return retrofit.create(RemoteVcrDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteHomeDao(retrofit: Retrofit): RemoteHomeDao {
        return retrofit.create(RemoteHomeDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteDigitalLibraryDao(retrofit: Retrofit): RemoteDigitalLibraryDao {
        return retrofit.create(RemoteDigitalLibraryDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteAssignmentsDao(retrofit: Retrofit): RemoteAssignmentsDao {
        return retrofit.create(RemoteAssignmentsDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteAttachmentDao(retrofit: Retrofit): RemoteAttachmentDao {
        return retrofit.create(RemoteAttachmentDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteCalendarDao(retrofit: Retrofit): RemoteCalendarDao {
        return retrofit.create(RemoteCalendarDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteWeeklyPlanDao(retrofit: Retrofit): RemoteWeeklyPlanDao {
        return retrofit.create(RemoteWeeklyPlanDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteDiscussionDao(retrofit: Retrofit): RemoteDiscussionDao {
        return retrofit.create(RemoteDiscussionDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteBehavioursDao(retrofit: Retrofit): RemoteBehavioursDao {
        return retrofit.create(RemoteBehavioursDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteMailboxDao(retrofit: Retrofit): RemoteMailboxDao {
        return retrofit.create(RemoteMailboxDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteAnnouncementsDao(retrofit: Retrofit): RemoteAnnouncementsDao {
        return retrofit.create(RemoteAnnouncementsDao::class.java)
    }

    @Provides
    @Singleton
    fun provideLocalMailboxDao(database: Database): LocalMailboxDao {
        return database.localMailboxDao()
    }

    @Provides
    @Singleton
    fun provideRemoteCoursesDao(retrofit: Retrofit): RemoteCoursesDao {
        return retrofit.create(RemoteCoursesDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteAttendancesDao(retrofit: Retrofit): RemoteAttendancesDao {
        return retrofit.create(RemoteAttendancesDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteHomeLocationDao(retrofit: Retrofit): RemoteHomeLocationDao {
        return retrofit.create(RemoteHomeLocationDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteChatDao(retrofit: Retrofit): RemoteChatDao {
        return retrofit.create(RemoteChatDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteChatServiceDao(@Chat retrofit: Retrofit): RemoteChatServiceDao {
        return retrofit.create(RemoteChatServiceDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteW3WDao(@W3w retrofit: Retrofit): RemoteW3WDao {
        return retrofit.create(RemoteW3WDao::class.java)
    }

    @Provides
    @Singleton
    fun provideSwitchRolesDao(retrofit: Retrofit): RemoteSwitchRolesDao {
        return retrofit.create(RemoteSwitchRolesDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteScheduleDao(retrofit: Retrofit): RemoteScheduleDao {
        return retrofit.create(RemoteScheduleDao::class.java)
    }

    @Provides
    @Singleton
    fun provideSwitchSchoolsDao(retrofit: Retrofit): RemoteSwitchSchoolsDao {
        return retrofit.create(RemoteSwitchSchoolsDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteEportfolioDao(retrofit: Retrofit): RemoteEPortfolioDao {
        return retrofit.create(RemoteEPortfolioDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteProfileDao(retrofit: Retrofit): RemoteProfileDao {
        return retrofit.create(RemoteProfileDao::class.java)
    }

    @Provides
    @Singleton
    fun provideSwitchSemestersDao(retrofit: Retrofit): RemoteSwitchSemestersDao {
        return retrofit.create(RemoteSwitchSemestersDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteLecturesDao(retrofit: Retrofit): RemoteLecturesDao {
        return retrofit.create(RemoteLecturesDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteSupportDao(retrofit: Retrofit): RemoteSupportDao {
        return retrofit.create(RemoteSupportDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteSurveysDao(retrofit: Retrofit): RemoteSurveysDao {
        return retrofit.create(RemoteSurveysDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteReportCardsDao(retrofit: Retrofit): RemoteReportCardsDao {
        return retrofit.create(RemoteReportCardsDao::class.java)
    }

    @Provides
    @Singleton
    fun provideClassVisitsDao(retrofit: Retrofit): RemoteClassVisitsDao {
        return retrofit.create(RemoteClassVisitsDao::class.java)
    }

    @Provides
    @Singleton
    fun provideSettingsDao(retrofit: Retrofit): RemoteSettingsDao {
        return retrofit.create(RemoteSettingsDao::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteParentChildsDao(retrofit: Retrofit): RemoteParentChildsDao {
        return retrofit.create(RemoteParentChildsDao::class.java)
    }

    @Provides
    @Singleton
    fun providePublicProfileDao(retrofit: Retrofit): RemotePublicProfileDao {
        return retrofit.create(RemotePublicProfileDao::class.java)
    }

    @Provides
    @Singleton
    fun provideLocalLastQuestionDao(database: Database): LocalLastQuestionDao {
        return database.localLastQuestionDao()
    }

    @Provides
    @Singleton
    fun provideNotificationsDao(@Notification retrofit: Retrofit): RemoteNotificationsDao {
        return retrofit.create(RemoteNotificationsDao::class.java)
    }

    @Provides
    @Singleton
    fun provideOnlineNowDao(@OnlineNow retrofit: Retrofit): RemoteOnlineNowDao {
        return retrofit.create(RemoteOnlineNowDao::class.java)
    }

    @Provides
    @Singleton
    fun provideCallChildDao(retrofit: Retrofit): RemoteCallChildDao {
        return retrofit.create(RemoteCallChildDao::class.java)
    }
}
