package com.classera.di

import android.app.Application
import android.content.SharedPreferences
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.preference.PreferenceManager
import androidx.room.Room
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.classera.data.CoroutinesContextProvider
import com.classera.data.CoroutinesContextProviderImpl
import com.classera.data.binding.BindingAdapters
import com.classera.data.binding.BindingAdaptersImpl
import com.classera.data.database.Database
import com.classera.data.database.Database.Companion.DATABASE_VERSION_1
import com.classera.data.database.Database.Companion.DATABASE_VERSION_2
import com.classera.data.database.Database.Companion.DATABASE_VERSION_3
import com.classera.data.prefs.Prefs
import com.classera.data.prefs.PrefsImpl
import com.classera.main.R
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Module
object CommonModule {

    private const val DATABASE_NAME = "com.app.classera_v2"

    // This injection is a workaround for a known issue in Gradle build system https://github.com/google/dagger/issues/955
    @Provides
    @Singleton
    fun provideDummyObject(): String {
        return ""
    }

    @Provides
    @Singleton
    fun provideContextProvider(): CoroutinesContextProvider {
        return CoroutinesContextProviderImpl()
    }

    @Provides
    @Singleton
    fun provideSharedPreferences(context: Application): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    @Provides
    @Singleton
    fun providePrefs(sharedPreferences: SharedPreferences): Prefs {
        return PrefsImpl(sharedPreferences)
    }

    @Provides
    @Singleton
    fun provideBindingAdapters(): BindingAdapters {
        return BindingAdaptersImpl()
    }

    @Provides
    @Singleton
    fun provideCustomTabsIntent(context: Application): CustomTabsIntent {
        return CustomTabsIntent.Builder()
            .setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary))
            .build()
    }

    @Provides
    @Singleton
    fun provideDatabase(context: Application): Database {
        return Room.databaseBuilder(context, Database::class.java, DATABASE_NAME)
            .addMigrations(object : Migration(DATABASE_VERSION_1, DATABASE_VERSION_2) {
                override fun migrate(database: SupportSQLiteDatabase) {
                    database.execSQL("ALTER TABLE User ADD COLUMN studentBehaviorId TEXT")
                }
            })
            .addMigrations(object : Migration(DATABASE_VERSION_2, DATABASE_VERSION_3) {
                override fun migrate(database: SupportSQLiteDatabase) {
                    database.execSQL(
                        "CREATE TABLE IF NOT EXISTS user_last_question " +
                                "(questionId TEXT NOT NULL, " +
                                "userId TEXT NOT NULL," +
                                " examId TEXT NOT NULL, " +
                                "questionNumber TEXT NOT NULL, PRIMARY KEY (examId))"
                    )
                }
            })
            .build()
    }
}
