package com.classera.attendance

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.attendance.AttendanceRepository
import javax.inject.Inject

class AttendanceViewModelFactory @Inject constructor(
    private val attendanceRepository: AttendanceRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AttendanceViewModel(attendanceRepository) as T
    }
}
