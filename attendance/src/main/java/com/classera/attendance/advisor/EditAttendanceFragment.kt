@file:Suppress("DEPRECATION")

package com.classera.attendance.advisor

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.ArrayAdapter
import androidx.appcompat.widget.AppCompatAutoCompleteTextView
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.classera.attendance.R
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.hideKeyboard
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.onItemClicked
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.CompositeDateValidator
import com.google.android.material.datepicker.DateValidatorPointBackward
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.snackbar.Snackbar
import java.util.*
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Aug 15, 2020
 *
 * @author AhmeDroid
 */
@Screen("Advisor Attendance")
class EditAttendanceFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: EditAttendanceViewModel

    private var studentsAdapter: EditAttendanceStudentsAdapter? = null
    private var progressDialog: ProgressDialog? = null
    private val currentDayCalendar = Calendar.getInstance()
    private var year: Int = currentDayCalendar.get(Calendar.YEAR)
    private var month: Int = currentDayCalendar.get(Calendar.MONTH)
    private var dayOfMonth: Int = currentDayCalendar.get(Calendar.DAY_OF_MONTH)

    private var editTextDate: EditText? = null
    private var recyclerView: RecyclerView? = null
    private var linearLayoutContent: LinearLayout? = null
    private var progressBar: ProgressBar? = null
    private var errorView: ErrorView? = null
    private var autoCompleteTextViewSection: AppCompatAutoCompleteTextView? = null

    override val layoutId: Int = R.layout.fragment_edit_attendance

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_take_attendance, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initProgressDialog()
        findViews()
        initListeners()
        handleAttendanceDate()
        hadnleSectionObserver()
        initViews()
    }

    private fun findViews() {
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_take_attendance)
        editTextDate = view?.findViewById(R.id.edit_text_fragment_take_attendance_date)
        linearLayoutContent =
            view?.findViewById(R.id.linear_layout_fragment_take_attendance_content)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_take_attendance)
        errorView = view?.findViewById(R.id.error_view_fragment_take_attendance)
        autoCompleteTextViewSection =
            view?.findViewById(R.id.auto_complete_text_view_fragment_take_attendance_section)
    }

    @SuppressLint("SetTextI18n")
    private fun initListeners() {
        editTextDate?.setOnClickListener {

            val constraintsBuilder = CalendarConstraints.Builder()
            val validators: ArrayList<CalendarConstraints.DateValidator> = ArrayList()
            validators.add(DateValidatorPointBackward.before(Calendar.getInstance().timeInMillis))
            constraintsBuilder.setValidator(CompositeDateValidator.allOf(validators))

            val builder = MaterialDatePicker.Builder.datePicker()
                .setCalendarConstraints(constraintsBuilder.build())

            val datePicker = builder.build()

            datePicker.addOnPositiveButtonClickListener {
                val calendar = Calendar.getInstance()
                calendar.time = Date(it)
                this.year = calendar.get(Calendar.YEAR)
                this.month = calendar.get(Calendar.MONTH)
                this.dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
                editTextDate?.setText(
                    "${calendar.get(Calendar.YEAR)}- " +
                            "${calendar.get(Calendar.MONTH)}-${calendar.get(Calendar.DAY_OF_MONTH)}"
                )
                handleAttendanceDate()
            }

            datePicker.show(childFragmentManager, "datePickerTag")
        }

        autoCompleteTextViewSection?.setOnClickListener {
            hideKeyboard()
        }

        viewModel.statusChangedLiveData.observe(this) {
            studentsAdapter?.notifyItemChanged(it)
        }
    }

    private fun handleAttendanceDate() {
        val humanMonth = month + 1
        val date = getString(
            R.string.text_fragment_take_attendance_date_format,
            year,
            humanMonth,
            dayOfMonth
        )
        editTextDate?.setText(date)
        viewModel.setDate(year, month, dayOfMonth)
        if (viewModel.selectedSectionIndex != -1) {
            viewModel.getStudents(viewModel.selectedSectionIndex)
                .observe(this, ::handleStudentsResource)
        }
    }

    private fun hadnleSectionObserver() {
        viewModel.getSections().observe(this, ::handleSectionsResource)
    }

    private fun initProgressDialog() {
        progressDialog = ProgressDialog(requireContext())
        progressDialog?.setMessage(getString(R.string.please_wait))
    }

    private fun initViews() {
        onResult<Pair<String, String>>(UPDATE_UI)?.observe(this) { pair ->

            view?.let {
                Snackbar.make(it, R.string.success_left_with_permission, Snackbar.LENGTH_SHORT)
                    .show()
            }

            studentsAdapter?.updateStudentStatus(pair.second)

            autoCompleteTextViewSection?.setSelection(viewModel.selectedSectionIndex)

            viewModel.getStudents(viewModel.selectedSectionIndex, DEFAULT_PAGE)
        }
    }

    private fun handleSectionsResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSectionsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSectionsSuccessResource()
            }
            is Resource.Error -> {
                handleSectionsErrorResource(resource)
            }
        }
    }

    private fun handleSectionsLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            linearLayoutContent?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            linearLayoutContent?.visibility = View.VISIBLE
        }
    }

    private fun handleSectionsSuccessResource() {
        val sectionTitles = viewModel.getSectionTitles().toMutableList()
        sectionTitles.add(
            0, getString(R.string.label_row_filter_all)
        )

        val adapter = ArrayAdapter(
            requireContext(),
            R.layout.dropdown_menu_popup_item,
            sectionTitles.toList()
        )

        autoCompleteTextViewSection?.setAdapter(adapter)
        autoCompleteTextViewSection?.onItemClicked {
            if (it == viewModel.selectedSectionIndex) {
                return@onItemClicked
            }

            viewModel.selectedSectionIndex = it

            viewModel.getStudents(it, DEFAULT_PAGE).observe(this, ::handleStudentsResource)
        }
    }

    private fun handleStudentsResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleStudentsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleStudentsSuccessResource()
            }
            is Resource.Error -> {
                handleStudentsErrorResource(resource)
            }
        }
    }

    private fun handleStudentsLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressDialog?.show()
        } else {
            progressDialog?.dismiss()
        }
    }

    private fun handleStudentsSuccessResource() {

        if (viewModel.currentPage == DEFAULT_PAGE) {
            initStudentAdapter()
        } else {
            studentsAdapter?.notifyDataSetChanged()
        }
        studentsAdapter?.finishLoading()
    }

    private fun initStudentAdapter() {
        recyclerView?.visibility = View.VISIBLE
        studentsAdapter = EditAttendanceStudentsAdapter(viewModel, this) { position ->
            studentsAdapter?.notifyItemChanged(position)
            findNavController().navigate(
                EditAttendanceFragmentDirections.editAttendanceAttachmentDirection(
                    editTextDate?.text.toString(),
                    viewModel.getStudent(position)?.fullName,
                    viewModel.getStudent(position)?.studentId.orEmpty(),
                    viewModel.getSelectedSectionId().orEmpty()
                )
            )
        }

        recyclerView?.adapter = studentsAdapter

        studentsAdapter?.setOnLoadMoreListener {
            viewModel.getStudents(
                viewModel.selectedSectionIndex,
                it
            ).observe(this, ::handleStudentsResource)
        }
    }

    private fun handleStudentsErrorResource(resource: Resource.Error) {
        view?.let {
            Snackbar.make(it, resource.error.message.toString(), Snackbar.LENGTH_LONG).show()
        }
        initStudentAdapter()
    }

    private fun handleSectionsErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener {
            hadnleSectionObserver()
        }
    }

    companion object {

        const val UPDATE_UI = "UpdateUI"
    }
}
