@file:Suppress("DEPRECATION")

package com.classera.attendance.advisor

import android.app.ProgressDialog
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.Toast
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import com.classera.attendance.R
import com.classera.attendance.databinding.RowEditAttendanceBinding
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.data.models.user.AbsenceStatus
import com.classera.data.network.errorhandling.Resource

/**
 * Project: Classera
 * Created: Aug 15, 2020
 *
 * @author AhmeDroid
 */
class EditAttendanceStudentsAdapter(
    private val viewModel: EditAttendanceViewModel,
    private val lifecycleOwner: LifecycleOwner,
    private val onLeftWithPermissionClicked: (position: Int) -> Unit
) : BasePagingAdapter<EditAttendanceStudentsAdapter.ViewHolder>() {

    private var progressDialog: ProgressDialog? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        initProgressDialog()
    }

    private fun initProgressDialog() {
        progressDialog = ProgressDialog(context)
        progressDialog?.setMessage(context?.getString(R.string.please_wait))
    }

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowEditAttendanceBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int = viewModel.getItemsCount()

    fun updateStudentStatus(studentId: String) {
        viewModel.updateStudentStatus(studentId)
        notifyDataSetChanged()
    }

    inner class ViewHolder(binding: RowEditAttendanceBinding) : BaseBindingViewHolder(binding) {

        private var radioButtonPresent: RadioButton? = null
        private var radioButtonAbsent: RadioButton? = null
        private var radioButtonLate: RadioButton? = null
        private var radioButtonExcused: RadioButton? = null
        private var radioButtonLeftWithPermission: RadioButton? = null

        init {
            radioButtonPresent =
                itemView.findViewById(R.id.radio_button_row_take_attendance_present)
            radioButtonAbsent = itemView.findViewById(R.id.radio_button_row_take_attendance_absent)
            radioButtonLate = itemView.findViewById(R.id.radio_button_row_take_attendance_late)
            radioButtonExcused =
                itemView.findViewById(R.id.radio_button_row_take_attendance_excused)
            radioButtonLeftWithPermission =
                itemView.findViewById(R.id.radio_button_row_edit_attendance_left_with_permission)
        }

        override fun bind(position: Int) {

            if (viewModel.getStudent(position)?.absenceStatus == AbsenceStatus.LEFT_WITH_PERMISSION) {
                radioButtonAbsent?.isEnabled = false
                radioButtonExcused?.isEnabled = false
                radioButtonLate?.isEnabled = false
                radioButtonPresent?.isEnabled = false
                radioButtonLeftWithPermission?.isEnabled = false
            } else {
                radioButtonAbsent?.isEnabled = true
                radioButtonExcused?.isEnabled = true
                radioButtonLate?.isEnabled = true
                radioButtonPresent?.isEnabled = true
                radioButtonLeftWithPermission?.isEnabled = true
            }
            radioButtonPresent?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    changeStatus(viewModel.onPresentSelected(clickedPosition))
                }
            }

            radioButtonAbsent?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    changeStatus(viewModel.onAbsentSelected(clickedPosition))
                }
            }

            radioButtonLate?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    changeStatus(viewModel.onLateSelected(clickedPosition))
                }
            }

            radioButtonExcused?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    changeStatus(viewModel.onExcusedSelected(clickedPosition))
                }
            }

            radioButtonLeftWithPermission?.setOnClickListener {
                val clickedPosition = adapterPosition
                if (clickedPosition != RecyclerView.NO_POSITION) {
                    onLeftWithPermissionClicked(clickedPosition)
                }
            }

            bind<RowEditAttendanceBinding> {
                this.user = viewModel.getStudent(position)
            }
        }

        private fun changeStatus(liveData: LiveData<Resource>) {
            liveData.observe(lifecycleOwner, ::handleResource)
        }

        private fun handleResource(resource: Resource) {
            when (resource) {
                is Resource.Loading -> {
                    handleLoadingResource(resource)
                }
                is Resource.Error -> {
                    handleErrorResource(resource)
                }
            }
        }

        private fun handleLoadingResource(resource: Resource.Loading) {
            if (resource.show) {
                progressDialog?.show()
            } else {
                progressDialog?.dismiss()
            }
        }

        private fun handleErrorResource(resource: Resource.Error) {
            Toast.makeText(context, resource.error.resourceMessage, Toast.LENGTH_LONG).show()
        }
    }
}
