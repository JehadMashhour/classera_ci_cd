package com.classera.attendance.advisor.attachmentbottomsheet

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.classera.data.repositories.attendance.AttendanceRepository
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Aug 15, 2020
 *
 * @author AhmeDroid
 */
@Module
abstract class AttendanceAttachmentModule {
    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: AttendanceAttachmentViewModelFactory
        ): AttendanceAttachmentViewModel {
            return ViewModelProvider(fragment, factory)[AttendanceAttachmentViewModel::class.java]
        }

        @Provides
        @JvmStatic
        fun provideViewModelFactory(
            fragment: Fragment,
            attendanceRepository: AttendanceRepository
        ): AttendanceAttachmentViewModelFactory {
            val arguments by fragment.navArgs<AttendanceAttachmentBottomSheetFragmentArgs>()
            return AttendanceAttachmentViewModelFactory(
                arguments,
                attendanceRepository
            )
        }
    }

    @Binds
    abstract fun bindFragment(fragment: AttendanceAttachmentBottomSheetFragment): Fragment
}
