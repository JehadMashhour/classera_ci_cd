package com.classera.attendance.teacher

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.models.courses.Course
import com.classera.data.repositories.attendance.AttendanceRepository
import javax.inject.Inject

class TakeAttendanceViewModelFactory @Inject constructor(
    private val attendanceRepository: AttendanceRepository,
    private val course: Course?
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TakeAttendanceViewModel(attendanceRepository, course) as T
    }
}
