@file:Suppress("DEPRECATION")

package com.classera.attendance.teacher

import android.app.Activity
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.DatePicker
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionManager
import com.classera.attendance.R
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.hideKeyboard
import com.classera.core.utils.android.observe
import com.classera.core.utils.android.onTextChanged
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.google.android.material.textfield.TextInputLayout
import com.jaiselrahman.filepicker.activity.FilePickerActivity
import com.jaiselrahman.filepicker.config.Configurations
import com.jaiselrahman.filepicker.model.MediaFile
import java.util.*
import javax.inject.Inject


/**
 * Project: Classera
 * Created: Feb 15, 2020
 *
 * @author Mohamed Hamdan
 */
@Suppress("TooManyFunctions")
class TakeAttendanceFragment : BaseFragment(), DatePickerDialog.OnDateSetListener {

    @Inject
    lateinit var viewModel: TakeAttendanceViewModel

    private val args: TakeAttendanceFragmentArgs by navArgs()


    private var studentsAdapter: TakeAttendanceStudentsAdapter? = null
    private var progressDialog: ProgressDialog? = null
    private val currentDayCalendar = Calendar.getInstance()
    private var year: Int = currentDayCalendar.get(Calendar.YEAR)
    private var month: Int = currentDayCalendar.get(Calendar.MONTH)
    private var dayOfMonth: Int = currentDayCalendar.get(Calendar.DAY_OF_MONTH)

    private var editTextDate: EditText? = null
    private var recyclerView: RecyclerView? = null
    private var linearLayoutContent: LinearLayout? = null
    private var progressBar: ProgressBar? = null
    private var errorView: ErrorView? = null
    private var autoCompleteTextViewSection: AutoCompleteTextView? = null
    private var autoCompleteTextViewCourses: AutoCompleteTextView? = null
    private var autoCompleteTextViewTimeSlot: AutoCompleteTextView? = null
    private var textInputLayoutTimeSlot: TextInputLayout? = null
    private var coursesLayout: TextInputLayout? = null
    private var sectionLayout: TextInputLayout? = null
    private var scanFaceMenuItem: Menu? = null


    override val layoutId: Int = R.layout.fragment_take_attendance

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        this.scanFaceMenuItem = menu
        inflater.inflate(R.menu.menu_fragment_take_attendance, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initProgressDialog()
        findViews()
        handleKeyboardListener()
        initListeners()
        setDate()
        if (args.course == null)
            getCourses()
        else
            getSections()
    }

    private fun handleKeyboardListener() {
        autoCompleteTextViewCourses?.setOnClickListener {
            autoCompleteTextViewCourses?.hideKeyboard()
        }
        autoCompleteTextViewSection?.setOnClickListener {
            autoCompleteTextViewSection?.hideKeyboard()
        }
        autoCompleteTextViewTimeSlot?.setOnClickListener {
            autoCompleteTextViewTimeSlot?.hideKeyboard()
        }
    }

    private fun initProgressDialog() {
        progressDialog = ProgressDialog(requireContext())
        progressDialog?.setMessage(getString(R.string.please_wait))
    }

    private fun findViews() {
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_take_attendance)
        editTextDate = view?.findViewById(R.id.edit_text_fragment_take_attendance_date)
        linearLayoutContent =
            view?.findViewById(R.id.linear_layout_fragment_take_attendance_content)
        progressBar = view?.findViewById(R.id.progress_bar_fragment_take_attendance)
        errorView = view?.findViewById(R.id.error_view_fragment_take_attendance)
        autoCompleteTextViewCourses =
            view?.findViewById(R.id.auto_complete_text_view_fragment_take_attendance_courses)
        autoCompleteTextViewSection =
            view?.findViewById(R.id.auto_complete_text_view_fragment_take_attendance_section)
        coursesLayout = view?.findViewById(R.id.input_layout_courses)
        sectionLayout = view?.findViewById(R.id.input_layout_section)
        autoCompleteTextViewTimeSlot = view?.findViewById(
            R.id.auto_complete_text_view_fragment_take_attendance_time_slot
        )

        textInputLayoutTimeSlot =
            view?.findViewById(R.id.text_input_layout_fragment_take_attendance_time_slot)
        checkIfTheArgNotEmpty()
    }

    private fun checkIfTheArgNotEmpty() {
        if (args.course != null) {
            autoCompleteTextViewCourses?.setText(args.course?.courseTitle, false)
            autoCompleteTextViewCourses?.isEnabled = false
            coursesLayout?.helperText = null
            (activity as AppCompatActivity).supportActionBar?.title = args.course?.courseTitle
        }
    }

    private fun initListeners() {
        editTextDate?.setOnClickListener {
            val dialog = DatePickerDialog(
                requireContext(),
                R.style.AppTheme_PickerTheme,
                this,
                year,
                month,
                dayOfMonth
            )
            dialog.setOnShowListener {
                dialog.datePicker.maxDate = System.currentTimeMillis()
            }
            editTextDate?.hideKeyboard()
            dialog.show()
        }

        viewModel.noTimeSlotsLiveData.observe(this) {
            TransitionManager.beginDelayedTransition(textInputLayoutTimeSlot!!)
            textInputLayoutTimeSlot?.isErrorEnabled = true
            textInputLayoutTimeSlot?.error =
                getString(R.string.error_fragment_take_attendance_empty_time_slots)

            autoCompleteTextViewTimeSlot?.onTextChanged {
                autoCompleteTextViewTimeSlot?.hideKeyboard()
                textInputLayoutTimeSlot?.hideKeyboard()
                TransitionManager.beginDelayedTransition(textInputLayoutTimeSlot!!)
                textInputLayoutTimeSlot?.isErrorEnabled = false
            }
        }

        viewModel.statusChangedLiveData.observe(this) { studentsAdapter?.notifyItemChanged(it) }

        viewModel.newTimeSlotsLiveData.observe(this) {
            autoCompleteTextViewTimeSlot?.setText("")
            autoCompleteTextViewTimeSlot?.hideKeyboard()
            textInputLayoutTimeSlot?.hideKeyboard()
        }

        viewModel.statusResponseLiveData.observe(this) { message ->
            requireContext().let { context ->
                if(!(message.equals("Success", true)
                            || message.equals("نجاح", true)))
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        this.year = year
        this.month = month
        this.dayOfMonth = dayOfMonth
        setDate()
    }

    private fun setDate() {
        val humanMonth = month + 1
        val date = getString(
            R.string.text_fragment_take_attendance_date_format,
            year,
            humanMonth,
            dayOfMonth
        )
        editTextDate?.setText(date)
        viewModel.setDate(year, month, dayOfMonth)
        resetFields()
    }

    private fun resetFields() {
        if (args.course != null) {
            autoCompleteTextViewCourses?.setText(args.course?.courseTitle, false)
            autoCompleteTextViewSection?.setText("")
            autoCompleteTextViewTimeSlot?.setText("")
            recyclerView?.visibility = View.GONE
        } else {
            autoCompleteTextViewCourses?.setText("", false)
            autoCompleteTextViewSection?.setText("", false)
            autoCompleteTextViewTimeSlot?.setText("", false)
            recyclerView?.visibility = View.GONE
        }
    }

    private fun getCourses() {
        viewModel.getCourses().observe(this, ::handleCoursesResource)
    }

    private fun getSections() {
        viewModel.getSections().observe(this, ::handleSectionsResource)
    }

    private fun handleCoursesResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleCoursesLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleCoursesSuccessResource()
            }
            is Resource.Error -> {
                handleCoursesErrorResource(resource)
            }
        }
    }

    private fun handleCoursesLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            linearLayoutContent?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            linearLayoutContent?.visibility = View.VISIBLE
        }
    }


    private fun handleSectionsResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSectionsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSectionsSuccessResource()
            }
            is Resource.Error -> {
                handleSectionsErrorResource(resource)
            }
        }
    }

    private fun handleSectionsLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            linearLayoutContent?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            linearLayoutContent?.visibility = View.VISIBLE
        }
    }

    private fun handleCoursesSuccessResource() {
        initCoursesAdapter()
    }

    private fun handleSectionsSuccessResource() {
        clearSectionAndTimeSlotsFields()
        initSectionsAdapter()
    }

    private fun clearSectionAndTimeSlotsFields() {
        autoCompleteTextViewSection?.setText("")
        autoCompleteTextViewTimeSlot?.setText("")
        autoCompleteTextViewSection?.isEnabled = false
        sectionLayout?.isEnabled = false
        autoCompleteTextViewTimeSlot?.isEnabled = false
        textInputLayoutTimeSlot?.isEnabled = false
        recyclerView?.visibility = View.GONE
    }

    private fun initCoursesAdapter() {
        val adapter = ArrayAdapter(
            requireContext(),
            R.layout.dropdown_menu_popup_item,
            viewModel.getCoursesTitles()
        )
        autoCompleteTextViewCourses?.setAdapter(adapter)
        autoCompleteTextViewCourses?.setOnItemClickListener { _, _, position, _ ->
            hideMenuItem()
            autoCompleteTextViewCourses?.hideKeyboard()
            coursesLayout?.hideKeyboard()
            viewModel.onCourseSelected(position).observe(this, ::handleSectionsResource)
        }
    }

    private fun initSectionsAdapter() {
        sectionLayout?.isEnabled = true
        autoCompleteTextViewSection?.isEnabled = true
        val adapter = ArrayAdapter(
            requireContext(),
            R.layout.dropdown_menu_popup_item,
            viewModel.getSectionTitles()
        )
        autoCompleteTextViewSection?.setAdapter(adapter)
        autoCompleteTextViewSection?.setOnItemClickListener { _, _, position, _ ->
            hideMenuItem()
            autoCompleteTextViewSection?.hideKeyboard()
            sectionLayout?.hideKeyboard()
            autoCompleteTextViewTimeSlot?.setText("")
            recyclerView?.visibility = View.GONE
            textInputLayoutTimeSlot?.helperText = resources.getString(
                R.string.helper_text_fragment_take_attendance_time_slots
            )
            viewModel.onSectionSelected(position).observe(this, ::handleTimeSlotsResource)
        }
    }

    private fun handleTimeSlotsResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleTimeSlotsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleTimeSlotsSuccessResource()
            }
            is Resource.Error -> {
                handleTimeSlotsErrorResource(resource)
            }
        }
    }

    private fun handleTimeSlotsLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressDialog?.show()
        } else {
            progressDialog?.dismiss()
        }
    }

    private fun handleTimeSlotsSuccessResource() {
        initTimeSlotsAdapter()
    }

    private fun initTimeSlotsAdapter() {
        autoCompleteTextViewTimeSlot?.isEnabled = true
        textInputLayoutTimeSlot?.isEnabled = true
        val adapter = ArrayAdapter(
            requireContext(),
            R.layout.dropdown_menu_popup_item,
            viewModel.getTimeSlotTitles()
        )
        autoCompleteTextViewTimeSlot?.setAdapter(adapter)
        autoCompleteTextViewTimeSlot?.setOnItemClickListener { _, _, position, _ ->
            hideMenuItem()
            autoCompleteTextViewTimeSlot?.hideKeyboard()
            textInputLayoutTimeSlot?.hideKeyboard()
            textInputLayoutTimeSlot?.helperText = ""
            viewModel.timeSlotPosition = position
            viewModel.onTimeSlotSelected(position, DEFAULT_PAGE)
                .observe(this, ::handleStudentsResource)
        }
    }

    private fun handleStudentsResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleStudentsLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleStudentsSuccessResource()
            }
            is Resource.Error -> {
                handleStudentsErrorResource(resource)
            }
        }
    }

    private fun handleStudentsLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressDialog?.show()
            disableMenuItem()
        } else {
            progressDialog?.dismiss()
        }
    }

    private fun handleStudentsSuccessResource() {
        initStudentAdapter()
        initTimeSlotsAdapter()
        showMenuItem()
        enableMenuItem()
    }

    private fun hideMenuItem() {
        val scanMenuItem =
            scanFaceMenuItem?.findItem(R.id.item_menu_fragment_take_attendance_scan_faces)
        scanMenuItem?.isVisible = false
    }

    private fun showMenuItem() {
        val scanMenuItem =
            scanFaceMenuItem?.findItem(R.id.item_menu_fragment_take_attendance_scan_faces)
        scanMenuItem?.isVisible = true
    }

    private fun disableMenuItem() {
        val scanMenuItem =
            scanFaceMenuItem?.findItem(R.id.item_menu_fragment_take_attendance_scan_faces)
        scanMenuItem?.isEnabled = false
    }

    private fun enableMenuItem() {
        val scanMenuItem =
            scanFaceMenuItem?.findItem(R.id.item_menu_fragment_take_attendance_scan_faces)
        scanMenuItem?.isEnabled = true
    }

    private fun initStudentAdapter() {
        recyclerView?.visibility = View.VISIBLE
        studentsAdapter = TakeAttendanceStudentsAdapter(viewModel, this)
        studentsAdapter?.setOnLoadMoreListener {
            viewModel.onTimeSlotSelected(viewModel.timeSlotPosition ?: 0, it)
                .observe(this, ::handleStudentsResource)
        }
        recyclerView?.adapter = studentsAdapter
    }

    private fun handleStudentsErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
        recyclerView?.visibility = View.GONE
    }

    private fun handleTimeSlotsErrorResource(resource: Resource.Error) {
        Toast.makeText(requireContext(), resource.error.message, Toast.LENGTH_LONG).show()
    }

    private fun handleSectionsErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
    }

    private fun handleCoursesErrorResource(resource: Resource.Error) {
        errorView?.setError(resource)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_menu_fragment_take_attendance_scan_faces -> {
                requireContext().let {
                    val intent = Intent(it, FilePickerActivity::class.java)
                    intent.putExtra(
                        FilePickerActivity.CONFIGS, Configurations.Builder()
                            .setMaxSelection(-1)
                            .setCheckPermission(true)
                            .setShowFiles(false)
                            .setShowImages(true)
                            .setShowAudios(false)
                            .setShowVideos(false)
                            .enableImageCapture(true)
                            .setSkipZeroSizeFiles(true)
                            .build()
                    )
                    startActivityForResult(intent, ATTACH_ATTACHMENT_REQUEST_CODE)
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ATTACH_ATTACHMENT_REQUEST_CODE
            && resultCode == Activity.RESULT_OK && data != null
        ) {
            val images =
                data.getParcelableArrayListExtra<MediaFile>(FilePickerActivity.MEDIA_FILES)

            val attachmentPathList = ArrayList<String>()
            images?.forEachIndexed { index, value ->
                attachmentPathList.add(index, value?.path ?: "")
            }

            val attachmentMimeTypeList = ArrayList<String>()
            images?.forEachIndexed { index, value ->
                attachmentMimeTypeList.add(
                    index,
                    value?.mimeType ?: ""
                )
            }
            viewModel.takeAttendanceByFace(
                attachmentPathList, attachmentMimeTypeList
            ).observe(this, ::handleStudentsResource)

        }
    }

    private companion object {

        private const val ATTACH_ATTACHMENT_REQUEST_CODE = 104
    }
}
