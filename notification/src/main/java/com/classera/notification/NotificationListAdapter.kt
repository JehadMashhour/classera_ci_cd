package com.classera.notification

import android.view.ViewGroup
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.core.adapter.CNSPagingAdapter
import com.classera.notification.databinding.RowNotificationListBinding

class NotificationListAdapter(
    private val viewModel: NotificationListViewModel
) : CNSPagingAdapter<NotificationListAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowNotificationListBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getNotificationCount()
    }

    inner class ViewHolder(binding: RowNotificationListBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowNotificationListBinding> {
                this.notification = viewModel.getNotificationItem(position)
            }
        }
    }
}
