package com.classera.notification

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.notification.NotificationRepository
import com.classera.data.repositories.settings.SettingsRepository
import javax.inject.Inject

class NotificationListViewModelFactory @Inject constructor(
    private val notificationRepository: NotificationRepository,
    private val settingsRepository: SettingsRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            NotificationListViewModel::class.java -> NotificationListViewModel(
                notificationRepository,
                settingsRepository
            ) as T
            else -> throw IllegalAccessException("There is no view model called $modelClass")
        }
    }
}
