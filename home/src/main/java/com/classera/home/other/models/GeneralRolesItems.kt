package com.classera.home.other.models

import com.classera.data.models.user.UserRole
import com.classera.home.R


class GeneralRolesItems : OtherRolesItem() {

    override val userRole: UserRole
        get() = UserRole.GENERAL

    override val shortcutIcons: Array<Int>
        get() = arrayOf(
            R.drawable.ic_my_card,
            R.drawable.ic_announcements,
            R.drawable.ic_profile,
            R.drawable.ic_success_partners
        )

    override val shortcutColors: Array<Int>
        get() = arrayOf(
            R.color.color_my_card,
            R.color.color_announcements,
            R.color.color_profile,
            R.color.color_success_partners
        )

    override val shortcutLabels: Array<Int>
        get() = arrayOf(
            R.string.title_row_dashboard_shortcut_my_card,
            R.string.title_row_dashboard_shortcut_announcements,
            R.string.title_row_dashboard_shortcut_profile,
            R.string.title_row_dashboard_shortcut_success_partners
        )

    override val shortcutKeys: Array<String>
        get() = arrayOf(
            "my_card",
            "announcements",
            "profile",
            "success_partners"
        )

    override val directions: Map<String, Int>
        get() = mapOf(
            "my_card" to R.id.item_menu_activity_main_navigation_view_my_card,
            "announcements" to R.id.item_menu_activity_main_navigation_view_announcements,
            "profile" to R.id.item_menu_activity_main_navigation_view_profile,
            "success_partners" to R.id.item_menu_activity_main_navigation_view_success_partners
        )
}
