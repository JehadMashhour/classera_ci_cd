package com.classera.home.other.models

import com.classera.data.models.user.UserRole


abstract class OtherRolesItem {

    abstract val userRole: UserRole

    abstract val shortcutIcons: Array<Int>

    abstract val shortcutColors: Array<Int>

    abstract val shortcutLabels: Array<Int>

    abstract val shortcutKeys: Array<String>

    abstract val directions: Map<String, Int>
}
