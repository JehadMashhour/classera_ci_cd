package com.classera.home.other.models

import com.classera.data.models.user.UserRole
import com.classera.home.R

class ManagerRoleItems : OtherRolesItem() {

    override val userRole: UserRole
        get() = UserRole.MANAGER

    override val shortcutIcons: Array<Int>
        get() = arrayOf(
            R.drawable.ic_survey,
            R.drawable.ic_settings
        )

    override val shortcutColors: Array<Int>
        get() = arrayOf(
            R.color.color_events,
            R.color.color_behaviors
        )

    override val shortcutLabels: Array<Int>
        get() = arrayOf(
            R.string.title_row_dashboard_shortcut_survey,
            R.string.title_menu_activity_main_navigation_settings
        )

    override val shortcutKeys: Array<String>
        get() = arrayOf(
            "surveys",
            "settings"
        )

    override val directions: Map<String, Int>
        get() = mapOf(
            "surveys" to R.id.item_menu_activity_main_navigation_view_surveys,
            "settings" to R.id.item_menu_activity_main_navigation_view_settings
        )
}
