package com.classera.home.admin.schoolgroupactiveusers

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.home.admin.AdminHomeViewModel
import com.classera.home.admin.AdminHomeViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
@Module
abstract class AdminHomeSchoolGroupActiveUsersFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: AdminHomeViewModelFactory
        ): AdminHomeViewModel {
            return ViewModelProvider(fragment, factory)[AdminHomeViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindActivity(activity: AdminHomeSchoolGroupActiveUsersFragment): Fragment
}
