package com.classera.home.admin.allschoolsactiveusers

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.custom.views.QuickFilterView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.getStringOrElse
import com.classera.data.network.errorhandling.Resource
import com.classera.home.R
import com.classera.home.admin.AdminHomeViewModel
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
class AdminHomeAllSchoolActiveUsersFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: AdminHomeViewModel

    private var recyclerViewActiveUsers: RecyclerView? = null
    private var progressBarSchoolActiveUser: ProgressBar? = null
    var quickFilterView: QuickFilterView? = null

    override val layoutId: Int = R.layout.fragment_admin_home_all_school_active_users

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initFilter()
    }

    private fun findViews() {
        recyclerViewActiveUsers = view?.findViewById(R.id.recycler_view_admin_home_all_school_active_users)
        progressBarSchoolActiveUser =
            view?.findViewById(R.id.progress_bar_fragment_home_dashboard_all_school_active_users)
        quickFilterView = view?.findViewById(R.id.filter_view_admin_home_all_school_active_users)
    }

    private fun getAllSchoolActiveUsers(key: String) {
        viewModel.getAllSchoolActiveUsers(key).observe(this, this::handleAllSchoolActiveUsersResource)

    }

    private fun initFilter() {
        quickFilterView?.setAdapter(
            R.array.admin_dashboard_roles_entry_entries,
            R.array.admin_dashboard_roles_entry_values
        )
        quickFilterView?.setOnFilterSelectedListener(::getAllSchoolActiveUsers)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleAllSchoolActiveUsersResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleAllSchoolActiveUsersLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleAllSchoolActiveUsersSuccessResource()
            }
            is Resource.Error -> {
                handleAllSchoolActiveUsersErrorResource(resource)
            }
        }
    }

    private fun handleAllSchoolActiveUsersLoadingResource(resource: Resource.Loading) {
        if (resource.show) progressBarSchoolActiveUser?.visibility =
            View.VISIBLE else progressBarSchoolActiveUser?.visibility = View.GONE
    }

    private fun handleAllSchoolActiveUsersSuccessResource() {
        val adapter =
            AdminHomeAllSchoolActiveUsersAdapter(viewModel)
        recyclerViewActiveUsers?.adapter = adapter
    }

    private fun handleAllSchoolActiveUsersErrorResource(resource: Resource.Error) {
        val message = context?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        lifecycle.removeObserver(viewModel)
        recyclerViewActiveUsers = null
        progressBarSchoolActiveUser = null
        quickFilterView = null
    }
}
