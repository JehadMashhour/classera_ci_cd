package com.classera.home.admin.contentstatistic

import android.view.ViewGroup
import android.widget.ImageView
import androidx.cardview.widget.CardView
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.home.R
import com.classera.home.admin.AdminHomeViewModel
import com.classera.home.databinding.RowContentStatisticsBinding

/**
 * Project: Classera
 * Created: Dec 21, 2019
 *
 * @author Mohamed Hamdan
 */
class AdminHomeContentStatisticsAdapter(private val viewModel: AdminHomeViewModel) :
    BaseAdapter<AdminHomeContentStatisticsAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowContentStatisticsBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getContentStatisticsCount()
    }

    inner class ViewHolder(binding: RowContentStatisticsBinding) : BaseBindingViewHolder(binding) {
        private var imageViewContentStatisticIcon: ImageView? = null
        private var cardViewContentStatistic: CardView? = null

        init {
            imageViewContentStatisticIcon = itemView.findViewById(R.id.image_view_row_content_statistic_icon)
            cardViewContentStatistic = itemView.findViewById(R.id.card_view_row_content_statistic)
        }

        override fun bind(position: Int) {
            bind<RowContentStatisticsBinding> {
                /*val color = ContextCompat.getColor(context!!, viewModel.getCardColor(position))
                cardViewContentStatistic?.backgroundTintList = ColorStateList.valueOf(color)
*/
                statistics = viewModel.getContentStatistic(position)
            }
        }
    }
}
