package com.classera.home.admin.onlinenow

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.res.ResourcesCompat
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.OnlineNowValueFormatter
import com.classera.core.utils.android.observe
import com.classera.data.models.socket.OnlineNow
import com.classera.data.network.errorhandling.Resource
import com.classera.home.R
import com.classera.home.admin.AdminHomeViewModel
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.utils.MPPointF
import com.google.gson.Gson
import com.snapics.screenshot.Screenshot
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
@SuppressWarnings("MagicNumber")
class AdminHomeSchoolOnlineNowFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: AdminHomeViewModel

    @Inject
    lateinit var screenshot: Screenshot

    private var progressBarOnlineNow: ProgressBar? = null
    private var imageViewOnlineNowDownload: ImageView? = null
    private var pieChartOnlineNow: PieChart? = null
    private var textViewNoResults: AppCompatTextView? = null
    private var imageViewOnlineNowRefresh: ImageView? = null

    override val layoutId: Int = R.layout.fragment_admin_home_online_now

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initBarChart()
        initViewModelListeners()
        initViewListeners()
    }

    private fun findViews() {
        progressBarOnlineNow = view?.findViewById(R.id.progress_bar_fragment_home_dashboard_school_online_now)
        pieChartOnlineNow = view?.findViewById(R.id.bar_chart_admin_home_school_online_now)
        imageViewOnlineNowDownload = view?.findViewById(R.id.image_view_admin_home_school_online_now_download)
        imageViewOnlineNowRefresh = view?.findViewById(R.id.image_view_admin_home_school_online_now_refresh)
        textViewNoResults = view?.findViewById(R.id.label_admin_home_fragment_online_now_no_data)
    }

    private fun initBarChart() {
        pieChartOnlineNow?.setUsePercentValues(false)
        pieChartOnlineNow?.description?.isEnabled = false
        pieChartOnlineNow?.setExtraOffsets(5f, 10f, 40f, 10f)

        pieChartOnlineNow?.dragDecelerationFrictionCoef = 0.95f

        pieChartOnlineNow?.isDrawHoleEnabled = true
        pieChartOnlineNow?.setHoleColor(Color.WHITE)

        pieChartOnlineNow?.setTransparentCircleColor(Color.WHITE)
        pieChartOnlineNow?.setTransparentCircleAlpha(110)

        pieChartOnlineNow?.holeRadius = 58f
        pieChartOnlineNow?.transparentCircleRadius = 61f

        pieChartOnlineNow?.rotationAngle = 0f
        pieChartOnlineNow?.isRotationEnabled = true
        pieChartOnlineNow?.isHighlightPerTapEnabled = true

        pieChartOnlineNow?.animateY(1400, Easing.EaseInOutQuad)

        val legend = pieChartOnlineNow?.legend
        legend?.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        legend?.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
        legend?.orientation = Legend.LegendOrientation.VERTICAL
        legend?.setDrawInside(false)
        legend?.xEntrySpace = 7f
        legend?.yEntrySpace = 0f
        legend?.yOffset = 0f

        pieChartOnlineNow?.setDrawEntryLabels(false)
        pieChartOnlineNow?.setEntryLabelColor(Color.WHITE)
        pieChartOnlineNow?.setEntryLabelTextSize(12f)
    }

    private fun initViewModelListeners() {
        viewModel.getSchools().observe(this, this::handleSchoolsResource)
    }

    private fun handleSchoolsResource(resource: Resource) {
      if(resource is Resource.Success<*>){
          viewModel.getOnlineNow().observe(this, this::handleResource)
      }
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource(resource)
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBarOnlineNow?.visibility = View.VISIBLE
        } else {
            progressBarOnlineNow?.visibility = View.GONE
        }
    }

    private fun handleSuccessResource(resource: Resource.Success<*>) {
        val jsonData = Gson().toJson(resource.data)
        val response: OnlineNow = Gson().fromJson(jsonData, OnlineNow::class.java)
        var onlineNow = OnlineNow(
            administrators = response.administrators,
            parents = response.parents,
            teachers = response.teachers,
            students = response.students,
            managers = response.managers,
            advisers = response.advisers,
            supervisors = response.supervisors,
            kaganCoaches = response.kaganCoaches
        )
        handleOnlineNowSuccess(onlineNow)
    }

    private fun handleErrorResource(resource: Resource.Error) {
        pieChartOnlineNow?.visibility = View.GONE
        textViewNoResults?.visibility = View.VISIBLE
    }

    private fun initViewListeners() {
        imageViewOnlineNowDownload?.setOnClickListener {
            saveToGallery()
        }

        imageViewOnlineNowRefresh?.setOnClickListener {
            viewModel.getSchools().observe(this, this::handleSchoolsResource)
        }
    }

    private fun handleOnlineNowSuccess(onlineNow: OnlineNow) {

        val values = getValues(onlineNow)

        val dataSet =
            PieDataSet(values, getString(R.string.label_admin_home_fragment_online_now))
        dataSet.setDrawIcons(false)

        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0f, 40f)
        dataSet.selectionShift = 5f

        val colors = ArrayList<Int>()
        colors.add(ResourcesCompat.getColor(resources, R.color.color_admin_home_online_now_administrators, null))
        colors.add(ResourcesCompat.getColor(resources, R.color.color_admin_home_online_now_parents, null))
        colors.add(ResourcesCompat.getColor(resources, R.color.color_admin_home_online_now_teachers, null))
        colors.add(ResourcesCompat.getColor(resources, R.color.color_admin_home_online_now_students, null))
        colors.add(ResourcesCompat.getColor(resources, R.color.color_admin_home_online_now_managers, null))
        colors.add(ResourcesCompat.getColor(resources, R.color.color_admin_home_online_now_advisers, null))
        colors.add(ResourcesCompat.getColor(resources, R.color.color_admin_home_online_now_supervisors, null))
        colors.add(ResourcesCompat.getColor(resources, R.color.color_admin_home_online_now_kagan_coaches, null))

        dataSet.colors = colors

        val data = PieData(dataSet)
        data.setValueFormatter(OnlineNowValueFormatter())
        data.setValueTextSize(11f)
        data.setValueTextColor(Color.WHITE)

        if(onlineNow.totalCount() == 0){
            pieChartOnlineNow?.visibility = View.GONE
            textViewNoResults?.visibility = View.VISIBLE
            imageViewOnlineNowDownload?.visibility = View.GONE
        }else{
            pieChartOnlineNow?.visibility = View.VISIBLE
            textViewNoResults?.visibility = View.GONE
            imageViewOnlineNowDownload?.visibility = View.VISIBLE
            pieChartOnlineNow?.data = data
            pieChartOnlineNow?.highlightValues(null)
            pieChartOnlineNow?.centerText = (""+onlineNow.totalCount()).trim()
            pieChartOnlineNow?.setCenterTextSize(20f)
            pieChartOnlineNow?.setCenterTextColor(resources.getColor(R.color.colorPrimary))
            pieChartOnlineNow?.invalidate()

        }
    }

    @SuppressWarnings("LongMethod")
    private fun getValues(onlineNow: OnlineNow): List<PieEntry> {
        return ArrayList<PieEntry>().apply {
            add(
                PieEntry(
                    onlineNow.administrators?.toFloat()?:0f,
                    getString(R.string.label_admin_home_online_now_administrators),
                    ResourcesCompat.getDrawable(resources, R.drawable.transparent_background,
                        null)
                )
            )
            add(
                PieEntry(
                    onlineNow.parents?.toFloat()?:0f,
                    getString(R.string.label_admin_home_online_now_parents),
                    ResourcesCompat.getDrawable(resources, R.drawable.transparent_background,
                        null)
                )
            )
            add(
                PieEntry(
                    onlineNow.teachers?.toFloat()?:0f,
                    getString(R.string.label_admin_home_online_now_teachers),
                    ResourcesCompat.getDrawable(resources, R.drawable.transparent_background,
                        null)
                )
            )
            add(
                PieEntry(
                    onlineNow.students?.toFloat()?:0f,
                    getString(R.string.label_admin_home_online_now_students),
                    ResourcesCompat.getDrawable(resources, R.drawable.transparent_background,
                        null)
                )
            )
            add(
                PieEntry(
                    onlineNow.managers?.toFloat()?:0f,
                    getString(R.string.label_admin_home_online_now_managers),
                    ResourcesCompat.getDrawable(resources, R.drawable.transparent_background,
                        null)
                )
            )
            add(
                PieEntry(
                    onlineNow.advisers?.toFloat()?:0f,
                    getString(R.string.label_admin_home_online_now_advisers),
                    ResourcesCompat.getDrawable(resources, R.drawable.transparent_background,
                        null)
                )
            )
            add(
                PieEntry(
                    onlineNow.supervisors?.toFloat()?:0f,
                    getString(R.string.label_admin_home_online_now_supervisors),
                    ResourcesCompat.getDrawable(resources, R.drawable.transparent_background,
                        null)
                )
            )
            add(
                PieEntry(
                    onlineNow.kaganCoaches?.toFloat()?:0f,
                    getString(R.string.label_admin_home_online_now_kagan_coaches),
                    ResourcesCompat.getDrawable(resources, R.drawable.transparent_background,
                        null)
                )
            )
        }
    }

    private fun saveToGallery() {
        screenshot.take(this, pieChartOnlineNow!!)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        lifecycle.removeObserver(viewModel)
        pieChartOnlineNow = null
        progressBarOnlineNow = null
        imageViewOnlineNowDownload = null
    }
}
