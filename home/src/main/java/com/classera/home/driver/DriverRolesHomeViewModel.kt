package com.classera.home.driver

import android.app.Application
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.classera.core.BaseAndroidViewModel
import com.classera.data.models.home.DashboardShortcut
import com.classera.data.models.user.User
import com.classera.data.models.user.UserRole
import com.classera.data.network.errorhandling.Resource
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.user.UserRepository
import com.classera.home.other.models.FloorSupervisorRoleItems
import com.classera.home.other.models.GeneralRolesItems
import com.classera.home.other.models.ManagerRoleItems
import com.classera.home.other.models.OtherRolesItem
import com.classera.home.other.models.TeacherSupervisorRoleItems
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 * Project: Classera
 * Created: 8/16/2021
 *
 * @author Jehad Abdalqader
 */
class DriverRolesHomeViewModel(
    application: Application,
    private val userRepository: UserRepository,
    private val prefs: Prefs
) : BaseAndroidViewModel(application), LifecycleObserver {

    private val arguments = mutableMapOf<String, Bundle?>()

    private var shortcuts: List<DashboardShortcut>? = null

    private val _directionLiveData = MutableLiveData<Pair<Int, Bundle?>>()
    val directionLiveData: LiveData<Pair<Int, Bundle?>> = _directionLiveData

    private val _shortcutsLiveData = MutableLiveData<Boolean>()
    val shortcutsLiveData: LiveData<Boolean> = _shortcutsLiveData

    private val _userLiveData = MutableLiveData<User>()
    val userLiveData: LiveData<User> = _userLiveData

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        viewModelScope.launch(Dispatchers.IO) {
            userRepository.getLocalUser().collect { users ->
                val user = users?.firstOrNull()
                _userLiveData.postValue(user)
            }
        }
    }

    fun getShortcuts() = liveData(Dispatchers.IO) {
        emit(Resource.Loading(show = true))
        shortcuts = parseData()
        if (!shortcuts.isNullOrEmpty()) {
            _shortcutsLiveData.postValue(true)
        }
        emit(Resource.Loading(show = false))
    }

    private fun parseData(): List<DashboardShortcut>? {
        val result = mutableListOf<DashboardShortcut>()
        getOtherRolesItem().shortcutLabels.forEachIndexed { index, label ->
            val dashboardShortcut =
                DashboardShortcut(
                    label,
                    getOtherRolesItem().shortcutIcons[index],
                    ContextCompat.getColor(application, getOtherRolesItem().shortcutColors[index]),
                    MAX_PROGRESS,
                    Int.MIN_VALUE,
                    Int.MAX_VALUE,
                    getOtherRolesItem().shortcutKeys[index]
                )
            result.add(dashboardShortcut)
        }
        return result
    }

    fun getShortcutCount(): Int {
        return shortcuts?.size ?: 0
    }

    fun getShortcut(position: Int): DashboardShortcut? {
        return shortcuts?.get(position)
    }

    fun onShortcutClicked(position: Int) {
        val direction = getOtherRolesItem().directions[shortcuts?.get(position)?.key]
        if (direction != null) {
            _directionLiveData.value = direction to arguments[shortcuts?.get(position)?.key]
        }
    }

    private fun getOtherRolesItems(): List<OtherRolesItem> {
        return listOf(
            DriverRolesItems()
        )
    }

    private fun getOtherRolesItem(): OtherRolesItem {
        return getOtherRolesItems().first {
            it.userRole == prefs.userRole
        }
    }

    fun wasTeacherRole(): Boolean {
        return prefs.mainRole == UserRole.TEACHER
    }

    private companion object {

        private const val MAX_PROGRESS = 100
    }
}
