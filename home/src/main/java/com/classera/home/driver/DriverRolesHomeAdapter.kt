package com.classera.home.driver

import android.view.ViewGroup
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.home.databinding.RowDashboardShortcutBinding

/**
 * Project: Classera
 * Created: 8/16/2021
 *
 * @author Jehad Abdalqader
 */
class DriverRolesHomeAdapter(private val viewModel: DriverRolesHomeViewModel) :
    BaseAdapter<DriverRolesHomeAdapter.ViewHolder>() {

    init {
        disableAnimations()
    }

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowDashboardShortcutBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getShortcutCount()
    }

    inner class ViewHolder(binding: RowDashboardShortcutBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowDashboardShortcutBinding> {
                shortcut = viewModel.getShortcut(position)
            }
        }
    }
}
