package com.classera.home.driver

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.user.UserRepository
import javax.inject.Inject

/**
 * Project: Classera
 * Created: 8/16/2021
 *
 * @author Jehad Abdalqader
 */
class DriverRolesHomeViewModelFactory @Inject constructor(
    private val application: Application,
    private val userRepository: UserRepository,
    private val prefs: Prefs
) : ViewModelProvider.AndroidViewModelFactory(application) {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DriverRolesHomeViewModel(application, userRepository, prefs) as T
    }
}
