package com.classera.home.driver

import com.classera.data.models.user.UserRole
import com.classera.home.R
import com.classera.home.other.models.OtherRolesItem

/**
 * Project: Classera
 * Created: 8/16/2021
 *
 * @author Jehad Abdalqader
 */
class DriverRolesItems : OtherRolesItem()  {
    override val userRole: UserRole
        get() = UserRole.DRIVER

    override val shortcutIcons: Array<Int>
        get() = arrayOf(
            R.drawable.ic_profile
        )

    override val shortcutColors: Array<Int>
        get() = arrayOf(
            R.color.color_profile
        )

    override val shortcutLabels: Array<Int>
        get() = arrayOf(
            R.string.title_row_dashboard_shortcut_profile
        )

    override val shortcutKeys: Array<String>
        get() = arrayOf(
            "profile"
        )

    override val directions: Map<String, Int>
        get() = mapOf(
            "profile" to R.id.item_menu_activity_main_navigation_view_profile
        )
}
