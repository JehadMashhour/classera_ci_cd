package com.classera.home.teacher

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
@Module
abstract class TeacherHomeFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: TeacherHomeViewModelFactory
        ): TeacherHomeViewModel {
            return ViewModelProvider(fragment, factory)[TeacherHomeViewModel::class.java]
        }
    }

    @Binds
    abstract fun bindActivity(activity: TeacherHomeFragment): Fragment
}
