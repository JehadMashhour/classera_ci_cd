package com.classera.home.teacher

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.classera.data.repositories.home.HomeRepository
import com.classera.data.repositories.user.UserRepository
import com.classera.data.repositories.vcr.VcrRepository
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Odai Nazzal
 */
class TeacherHomeViewModelFactory @Inject constructor(
    private val application: Application,
    private val userRepository: UserRepository,
    private val homeRepository: HomeRepository,
    private val vcrRepository: VcrRepository,
) : ViewModelProvider.AndroidViewModelFactory(application) {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TeacherHomeViewModel(application, userRepository, homeRepository, vcrRepository) as T
    }
}
