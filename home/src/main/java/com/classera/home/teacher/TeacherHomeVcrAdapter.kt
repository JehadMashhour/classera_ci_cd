package com.classera.home.teacher

import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.home.R
import com.classera.home.databinding.RowDailyUpcomingVcrBinding

/**
 * Project: Classera
 * Created: Jan 22, 2020
 *
 * @author Mohamed Hamdan
 */
class TeacherHomeVcrAdapter(private val viewModel: TeacherHomeViewModel)
    : BaseAdapter<TeacherHomeVcrAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowDailyUpcomingVcrBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getVcrCount()
    }

    inner class ViewHolder(binding: RowDailyUpcomingVcrBinding) : BaseBindingViewHolder(binding) {




        override fun bind(position: Int) {
            bind<RowDailyUpcomingVcrBinding> {
                this.vcrItem = viewModel.getVcr(position)
            }

        }
    }
}
