package com.classera.home.student

import android.view.View
import android.view.ViewGroup

import android.widget.RelativeLayout
import com.classera.core.adapter.BaseAdapter
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.home.R

import com.classera.home.databinding.RowDailyUpcomingVcrBinding

/**
 * Created by Rawan Al-Theeb on 12/22/2019.
 * Classera
 * r.altheeb@classera.com
 */
class StudentHomeVcrAdapter(private val viewModel: StudentHomeViewModel)
    : BaseAdapter<StudentHomeVcrAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowDailyUpcomingVcrBinding.inflate(inflater!!, parent, false))
    }

    override fun getItemsCount(): Int {
        return viewModel.getVcrCount()
    }

    inner class ViewHolder(binding: RowDailyUpcomingVcrBinding) : BaseBindingViewHolder(binding) {




        override fun bind(position: Int) {
            bind<RowDailyUpcomingVcrBinding> {
                vcrItem = viewModel.getVcr(position)
            }
        }
    }
}
