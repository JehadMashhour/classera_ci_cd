package com.classera.data.network.intercepters

import com.classera.data.BuildConfig
import com.classera.data.network.IgnoreAppType
import okhttp3.Interceptor
import okhttp3.Response
import retrofit2.Invocation
import javax.inject.Inject

/**
 * Created by Saeed Halawani on 30/08/2020.
 * Classera
 * s.halawani@classer.com
 */
class AppTypeInterceptor @Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val invocation = request.tag(Invocation::class.java)
        val method = invocation?.method()

        if (method?.getAnnotation(IgnoreAppType::class.java) == null) {
            request = request.newBuilder()
                .addHeader(APP_TYPE_HEADER_NAME, BuildConfig.flavorData.appType)
                .build()
        }
        return chain.proceed(request)
    }

    private companion object {

        private const val APP_TYPE_HEADER_NAME = "App-Type"
    }
}
