package com.classera.data.network

/**
 * Project: Classera
 * Created: Dec 25, 2019
 *
 * @author Mohamed Hamdan
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION)
annotation class Pagination

const val DEFAULT_PAGE = 1
