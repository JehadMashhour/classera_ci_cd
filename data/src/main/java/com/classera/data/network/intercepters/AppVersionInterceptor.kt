package com.classera.data.network.intercepters

import com.classera.data.BuildConfig
import com.classera.data.network.IgnoreAppVersion
import okhttp3.Interceptor
import okhttp3.Response
import retrofit2.Invocation
import javax.inject.Inject

/**
 * Created by Saeed Halawani on 30/08/2020.
 * Classera
 * s.halawani@classer.com
 */
class AppVersionInterceptor @Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        val invocation = request.tag(Invocation::class.java)
        val method = invocation?.method()

        if (method?.getAnnotation(IgnoreAppVersion::class.java) == null) {
            request = request.newBuilder()
                .addHeader(APP_VERSION_HEADER_NAME, BuildConfig.flavorData.appVersionName)
                .build()
        }
        return chain.proceed(request)
    }

    private companion object {

        private const val APP_VERSION_HEADER_NAME = "X-app-version"
    }
}
