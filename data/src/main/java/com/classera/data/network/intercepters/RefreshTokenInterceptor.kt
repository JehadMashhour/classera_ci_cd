package com.classera.data.network.intercepters

import android.app.Application
import android.content.Intent
import com.classera.data.network.IgnoreRefreshToken
import com.classera.data.prefs.Prefs
import okhttp3.Interceptor
import okhttp3.Response
import retrofit2.Invocation
import javax.inject.Inject

@Suppress("MagicNumber")
class RefreshTokenInterceptor @Inject constructor(
    private val prefs: Prefs,
    private val applicationContext: Application
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)
        val invocation = request.tag(Invocation::class.java)
        val method = invocation?.method()
        val body = response.peekBody(2048).string()
        if (response.code() == ERROR_RESPONSE &&
            method?.getAnnotation(IgnoreRefreshToken::class.java) == null) {
            prefs.deletePrefsData()
            val intent = Intent("com.classera.splash.SplashActivity")
                .setPackage(applicationContext.packageName)
            intent.putExtra(ERROR_INTENT_EXTRA_NAME, body)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            applicationContext.startActivity(intent)
            return response
        }

        return response.newBuilder().build()
    }

    private companion object {

        private const val ERROR_RESPONSE = 401
        private const val ERROR_INTENT_EXTRA_NAME = "error"
    }
}
