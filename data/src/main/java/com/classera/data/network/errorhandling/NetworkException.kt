package com.classera.data.network.errorhandling

import com.classera.data.R

/**
 * Project: MoveFast
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Suppress("MagicNumber")
sealed class NetworkException(
    val resourceTitle: Int = 0,
    val resourceMessage: Int = 0,
    val code: Int?,
    message: String? = null,
    cause: Throwable? = null
) : RuntimeException(message, cause) {

    class Client(
        resourceTitle: Int,
        resourceMessage: Int,
        message: String?,
        cause: Throwable
    ) : NetworkException(resourceTitle, resourceMessage, 0, message, cause)

    class Server(
        resourceTitle: Int,
        resourceMessage: Int,
        message: String?,
        cause: Throwable
    ) : NetworkException(resourceTitle, resourceMessage, 0, message, cause)

    class Timeout(
        resourceTitle: Int,
        resourceMessage: Int,
        message: String?,
        cause: Throwable
    ) : NetworkException(resourceTitle, resourceMessage, 0, message, cause)

    class Internet(
        resourceTitle: Int,
        resourceMessage: Int,
        message: String?,
        cause: Throwable
    ) : NetworkException(resourceTitle, resourceMessage, 0, message, cause)

    class Unexpected(
        resourceTitle: Int,
        resourceMessage: Int,
        message: String?,
        cause: Throwable
    ) : NetworkException(resourceTitle, resourceMessage, 0, message, cause)

    class Business(message: String?, code: Int?) : NetworkException(
        R.string.err_network_unexpected_message,
        R.string.err_network_unexpected_message,
        code,
        message
    )

    class Update(
        resourceTitle: Int,
        resourceMessage: Int,
        message: String?,
        cause: Throwable
    ) : NetworkException(resourceTitle, resourceMessage, 0, message, cause)

    class Blocked(
        resourceTitle: Int,
        resourceMessage: Int,
        message: String?,
        cause: Throwable
    ) : NetworkException(resourceTitle, resourceMessage, 303, message, cause)

    class ForceUpdate(
        resourceTitle: Int,
        resourceMessage: Int,
        message: String?,
        cause: Throwable
    ) : NetworkException(resourceTitle, resourceMessage, 426, message, cause)
}
