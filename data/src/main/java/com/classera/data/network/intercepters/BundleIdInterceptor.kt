package com.classera.data.network.intercepters

import com.classera.data.BuildConfig
import com.classera.data.network.IgnoreAppBundleId
import okhttp3.Interceptor
import okhttp3.Response
import retrofit2.Invocation
import javax.inject.Inject

/**
 * Created by Saeed Halawani on 30/08/2020.
 * Classera
 * s.halawani@classer.com
 */
class BundleIdInterceptor @Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        val invocation = request.tag(Invocation::class.java)
        val method = invocation?.method()

        if (method?.getAnnotation(IgnoreAppBundleId::class.java) == null) {
            request = request.newBuilder()
                .addHeader(BUNDLE_ID_HEADER_NAME, BuildConfig.flavorData.bundleId)
                .build()
        }
        return chain.proceed(request)
    }

    private companion object {

        private const val BUNDLE_ID_HEADER_NAME = "X-bundle-Id"
    }
}
