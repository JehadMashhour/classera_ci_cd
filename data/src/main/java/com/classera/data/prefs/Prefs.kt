package com.classera.data.prefs

import com.classera.data.models.user.UserRole

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
interface Prefs {


    var isLoggedIn: Boolean

    var userProfilePicture: String?

    var userFullName: String?

    var userId: String?

    var creatorId: String?

    var userRole: UserRole?

    var mainRole: UserRole?

    var previousUserRole: UserRole?

    var authenticationToken: String?

    var semesterId: String?

    var semesterToken: String?

    var schoolId: String?

    var schoolToken: String?

    var chatPagination: String?

    var userStatus: String?

    var language: String

    var childId: String?

    var notificationEnabled: Boolean

    var isBlocked: Boolean

    var usersUploadEportfolios: String?

    var studentsCreateDiscussions: String?

    var studentsChangePhoneNumber: String?

    var studentsChangeEmail: String?

    var studentsChangeProfilePic: String?

    var allowParentAccessToSmartClass: String?

    var schoolChat: String?

    var childAvatar: String?

    var schoolType: String?

    var uuid:String?

    var selectedSemesterId: String?

    var selectedSchoolId: String?

    var isAssessmentBlocked: String?

    var allowStudentsRequests: String?

    fun deletePrefsData()
}
