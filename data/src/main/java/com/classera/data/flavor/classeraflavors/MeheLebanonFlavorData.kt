package com.classera.data.flavor.classeraflavors

class MeheLebanonFlavorData : ClasseraFlavorData() {
    override val production_api_baseUrl: String
        get() = "https://leb-api.classera.com"
    override val staging_api_baseUrl: String
        get() = "https://leb-api.classera.com"
    override val development_api_baseUrl: String
        get() = "https://leb-api.classera.com"
    override val production_web_baseUrl: String
        get() = "https://leb.classera.com/"
    override val staging_web_baseUrl: String
        get() = "https://leb.classera.com/"
    override val development_web_baseUrl: String
        get() = "https://leb.classera.com/"
}
