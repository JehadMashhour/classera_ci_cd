package com.classera.data.flavor

import com.classera.data.BuildConfig

@Suppress("VariableNaming")
interface BaseFlavorData {
    val appType: String
    val bundleId: String
        get() = "com.app.classera.${BuildConfig.FLAVOR_whitelabel}"
    val appVersionName: String
        get() = BuildConfig.VERSION_NAME.split("-")[0]
    val accessToken: String
        get() = "a3d8c21b54c8ad8e4cc0d7d066b476c2ce9da709"
    val googleServicesApiKey: String
        get() = "AIzaSyA1idd45fujOf0xOk5X0ybSIlTGQTUzW5U"
    val googleServicesClientId: String
        get() = "105625603354-j71bp59go0f6sckk9vv9bpi1qjvh3rj3.apps.googleusercontent.com"
    val youtubeKey: String
        get() = "AIzaSyAtN5wu0lYzP18da4sW00RrrLHnJQTj-zc"
    val w3wBaseUrl: String
        get() = "https://api.what3words.com/v3/"
    val w3wApiKey: String
        get() = "T5TU7WCU"
    val chatBaseUrl: String
        get() = "https://chat-az.classera.com/api/"
    val socketBaseUrl: String
        get() = "https://chat-az.classera.com"
    val socketPathUrl: String
        get() = "/socket.io/"
    val socketSource: String
        get() = "me"
    val onlineNowBaseUrl: String
        get() = "https://onlines.classera.com/"
    val notificationBaseUrl: String
        get() = "http://cns.classera.com/api/"

    val production_api_baseUrl: String
    val staging_api_baseUrl: String
    val development_api_baseUrl: String

    val apiBaseUrl: String
        get() =  when (BuildConfig.FLAVOR_server) {
            "production" ->  production_api_baseUrl
            "staging" ->  staging_api_baseUrl
            "development" ->  development_api_baseUrl
            else -> production_api_baseUrl
        }

    val production_web_baseUrl: String
    val staging_web_baseUrl: String
    val development_web_baseUrl: String

    val webBaseUrl: String
        get() =  when (BuildConfig.FLAVOR_server) {
            "production" ->  production_web_baseUrl
            "staging" ->  staging_web_baseUrl
            "development" ->  development_web_baseUrl
            else -> production_web_baseUrl
        }
}
