package com.classera.data.moshi.messagebody

import com.classera.data.models.chat.MessageBody
import com.squareup.moshi.FromJson
import com.squareup.moshi.Moshi
import com.squareup.moshi.ToJson

/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Mohamed Hamdan
 */
class MessageBodyAdapter(private val moshi: Moshi) {

    private val messageAdapter by lazy { moshi.adapter(MessageBody::class.java) }

    @ToJson
    fun toJson(@MessageBodyConverter value: MessageBody?): String? {
        return messageAdapter.toJson(value)
    }

    @FromJson
    @MessageBodyConverter
    fun fromJson(value: String?): MessageBody? {
        return if (value?.startsWith("{") == true) {
            messageAdapter.fromJson(value)
        } else {
            MessageBody(text = value)
        }
    }
}
