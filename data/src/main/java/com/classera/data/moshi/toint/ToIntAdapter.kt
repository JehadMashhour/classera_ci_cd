package com.classera.data.moshi.toint

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Mohamed Hamdan
 */
class ToIntAdapter {

    @ToJson
    fun toJson(@ToInt value: Int): String? {
        return value.toString()
    }

    @FromJson
    @ToInt
    fun fromJson(value: String?): Int {
        return value?.toInt() ?: 0
    }
}
