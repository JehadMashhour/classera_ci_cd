package com.classera.data.moshi.messages

import com.classera.data.models.chat.MessageTypeEnum
import com.classera.data.moshi.enums.Json
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Mohamed Hamdan
 */
class MessageTypeAdapter {

    @ToJson
    fun toJson(@MessageType value: MessageTypeEnum?): Int? {
        return value?.type
    }

    @FromJson
    @MessageType
    fun fromJson(value: Int?): MessageTypeEnum? {
        val type = MessageTypeEnum::class.java
        val values = type.enumConstants
        values?.forEach {
            val jsonAnnotation = type.getField(it.name).getAnnotation(Json::class.java)
            if (value.toString() == jsonAnnotation?.name) {
                return it
            }
        }
        return MessageTypeEnum.UNKNOWN
    }
}
