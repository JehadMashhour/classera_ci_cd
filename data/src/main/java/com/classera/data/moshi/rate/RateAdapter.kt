package com.classera.data.moshi.rate

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Mohamed Hamdan
 */
class RateAdapter {

    @ToJson
    fun toJson(@Rate value: String?): String? {
        return value
    }

    @FromJson
    @Rate
    fun fromJson(value: String?): String? {
        val symbols = DecimalFormatSymbols(Locale.ENGLISH)
        val decimalFormat = DecimalFormat("#.#", symbols)
        return decimalFormat.format(value?.toDoubleOrNull() ?: 0.0) ?: "0"
    }
}
