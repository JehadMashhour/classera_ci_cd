package com.classera.data.crypto

/**
 * Project: Classera
 * Created: 6/10/2021
 *
 * @author Jehad Abdalqader
 */

fun <K, V> Map<K, V>.removeNull(): Map<K, V?> {
    val result = mutableMapOf<K, V>()
    forEach {
        if (it.value != null) {
            result[it.key] = it.value
        }
    }
    return result
}
