package com.classera.data

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Odai Nazzal on 12/27/2019.
 * Classera
 * o.nazzal@classera.com
 */
fun Date?.toString(pattern: String, locale: Locale = Locale.ENGLISH): String? {
    return if (this != null) {
        val simpleDateFormat = SimpleDateFormat(pattern, locale)
        simpleDateFormat.format(this)
    } else {
        null
    }
}

fun String?.toDate(
    pattern: String = "yyyy-MM-dd HH:mm:ss",
    locale: Locale = Locale.ENGLISH
): Date? {
    return if (!this.isNullOrEmpty()) {
        val simpleDateFormat = SimpleDateFormat(pattern, locale)
        simpleDateFormat.parse(this)
    } else {
        null
    }
}

fun String?.add(where: Int, value: Int, format: String = "yyyy-MM-dd"): String? {
    val date = toDate(format)
    return if (date != null) {
        val calendar = Calendar.getInstance().apply {
            time = date
            add(where, value)
        }
        calendar.time.toString(format)
    } else {
        this
    }
}

fun Date.addTime(time: Date?): Date? {
    time?.let {
        val t = Calendar.getInstance()
        t.time = time
        val c = Calendar.getInstance()
        c.time = this
        c[Calendar.HOUR_OF_DAY] = t[Calendar.HOUR_OF_DAY]
        c[Calendar.MINUTE] = t[Calendar.MINUTE]
        c[Calendar.SECOND] = t[Calendar.SECOND]
        c[Calendar.MILLISECOND] = t[Calendar.MILLISECOND]
        return c.time
    }
    return null
}


fun Date.addDate(date: Date?): Date? {
    date?.let {
        val d = Calendar.getInstance()
        d.time = date
        val c = Calendar.getInstance()
        c.time = this
        c[Calendar.YEAR] = d[Calendar.YEAR]
        c[Calendar.DATE] = d[Calendar.DATE]
        c[Calendar.MONTH] = d[Calendar.MONTH]
        return c.time
    }
    return null
}



