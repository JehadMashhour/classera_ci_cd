package com.classera.data.language

import android.content.Context
import android.os.Build
import android.os.LocaleList
import com.classera.data.prefs.Prefs
import java.util.*

/**
 * Project: Classera
 * Created: Mar 04, 2020
 *
 * @author Mohamed Hamdan
 */
object LanguageUtils {

    fun applyLocalizationContext(baseContext: Context, prefs: Prefs): Context {
        val currentLocale = Locale(prefs.language)
        val context = LanguageContext(baseContext, prefs)
        val config = context.resources.configuration
        return when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.O -> {
                config.setLocale(currentLocale)
                val localeList = LocaleList(currentLocale)
                LocaleList.setDefault(localeList)
                config.setLocale(currentLocale)
                config.setLocales(localeList)
                config.setLayoutDirection(currentLocale)
                context.createConfigurationContext(config)
            }
            else -> {
                config.setLocale(currentLocale)
                config.locale = currentLocale
                config.setLayoutDirection(currentLocale)
                context.createConfigurationContext(config)
            }
        }
    }
}
