package com.classera.data.language

import android.content.Context
import android.content.ContextWrapper
import android.content.res.Resources
import android.os.Build
import android.os.LocaleList
import android.util.DisplayMetrics
import com.classera.data.prefs.Prefs
import java.util.*

/**
 * Project: Classera
 * Created: Mar 24, 2020
 *
 * @author Mohamed Hamdan
 */
class LanguageContext(base: Context, private val prefs: Prefs) : ContextWrapper(base) {

    override fun getResources(): Resources {
        val locale = Locale(prefs.language)
        val configuration = super.getResources().configuration
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            configuration.setLocales(LocaleList(locale))
        }
        configuration.setLocale(locale)
        configuration.setLayoutDirection(locale)
        val metrics: DisplayMetrics = super.getResources().displayMetrics
        return Resources(assets, metrics, configuration)
    }
}
