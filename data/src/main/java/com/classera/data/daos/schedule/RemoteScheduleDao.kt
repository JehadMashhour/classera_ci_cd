package com.classera.data.daos.schedule

import com.classera.data.models.BaseWrapper
import com.classera.data.models.schedule.Schedule
import com.classera.data.network.IncludeChildId
import retrofit2.http.GET

/**
 * Created by Odai Nazzal on 1/12/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
interface RemoteScheduleDao {

    @IncludeChildId
    @GET("timeslots/student_schedule")
    suspend fun getStudentSchedule(): BaseWrapper<List<Schedule>>
}
