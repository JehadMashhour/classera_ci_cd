package com.classera.data.daos.courses

import com.classera.data.models.BaseWrapper
import com.classera.data.models.courses.PreparationFilterWrapper
import com.classera.data.models.courses.Course
import com.classera.data.models.courses.CourseDetailsWrapper
import com.classera.data.network.IncludeChildId
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap

/**
 * Created by Odai Nazzal on 1/7/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
interface RemoteCoursesDao {

    @IncludeChildId
    @GET("courses/get_cours_list")
    suspend fun getCourses(): BaseWrapper<List<Course>>

    @IncludeChildId
    @GET("courses/browse_content_v2")
    suspend fun getCourseDetails(
        @QueryMap query: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<CourseDetailsWrapper>

    @GET("courses/manage_content")
    suspend fun getTeacherCourseDetails(
        @QueryMap query: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<CourseDetailsWrapper>

    @GET("preparations/get_preparations_by_course")
    suspend fun getPreparation(
        @Query("course_id")
        courseId: String?
    ): BaseWrapper<List<PreparationFilterWrapper>>
}
