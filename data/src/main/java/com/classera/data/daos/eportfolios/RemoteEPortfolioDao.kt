package com.classera.data.daos.eportfolios

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.eportfolio.EPortfolioWraper
import com.classera.data.models.eportfolio.add.SharingLevelWrapper
import com.classera.data.models.eportfolio.add.TypeWrapper
import com.classera.data.models.eportfolio.details.EPortfolioComment
import com.classera.data.models.eportfolio.details.EPortfolioDetailsWraper
import com.classera.data.network.IncludeChildId
import com.classera.data.network.Pagination
import okhttp3.MultipartBody
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Query
import retrofit2.http.QueryMap

/**
 * Created by Odai Nazzal on 1/14/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
interface RemoteEPortfolioDao {

    @IncludeChildId
    @Pagination
    @GET("public_profiles/eportfolios")
    suspend fun getStudentEPortfolios(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<List<EPortfolioWraper>>

    @GET("public_profiles/view_post")
    suspend fun getStudentEPortfolioDetails(@Query("post_id") postId: String?): BaseWrapper<EPortfolioDetailsWraper>

    @Pagination
    @GET("public_profiles/comments")
    suspend fun getEPortfolioComments(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<List<EPortfolioComment>>

    @FormUrlEncoded
    @POST("public_profiles/save_post_comment")
    suspend fun addEPortfolioComment(@FieldMap fields: Map<String, String?>): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("public_profiles/delete_post")
    suspend fun deleteEPortfolio(@FieldMap fields: Map<String, String?>): NoContentBaseWrapper

    @GET("public_profiles/eportfolio_types")
    suspend fun getType():BaseWrapper<List<TypeWrapper>>

    @GET("public_profiles/sharing_types")
    suspend fun getSharingLevel():BaseWrapper<List<SharingLevelWrapper>>

    @Multipart
    @POST("public_profiles/save_post")
    suspend fun uploadAttachment(@Part parts: List<MultipartBody.Part>)
}
