package com.classera.data.daos.home

import com.classera.data.models.BaseWrapper
import com.classera.data.models.home.StudentHomeResponse
import com.classera.data.models.home.TeacherHomeResponse
import com.classera.data.models.home.admin.DayAbsences
import com.classera.data.models.home.admin.SmsUsage
import com.classera.data.models.home.admin.Statistic
import com.classera.data.models.home.admin.TopScoresSchool
import com.classera.data.models.home.admin.TopScoresSchoolGroup
import com.classera.data.models.home.admin.TopSectionScore
import com.classera.data.network.IncludeChildId
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 * Created by Odai Nazzal on 12/19/2019.
 * Classera
 * o.nazzal@classera.com
 */
interface RemoteHomeDao {

    @IncludeChildId
    @GET("users/student_home")
    suspend fun getStudentHome(): BaseWrapper<StudentHomeResponse>

    @GET("teachers/teacher_home")
    suspend fun getTeacherHome(): BaseWrapper<TeacherHomeResponse>

    @GET("Statistics/content_statistics")
    suspend fun getContentStatistics(): BaseWrapper<List<Statistic>>

    @GET("Statistics/get_dashboard_total_absences")
    suspend fun getDashboardTotalAbsences(): BaseWrapper<List<DayAbsences>>

    @GET("statistics/top_sections_scores")
    suspend fun getTopSectionsScores(@QueryMap fields: Map<String, String?>): BaseWrapper<List<TopSectionScore>>

    @GET("statistics/top_scores_school")
    suspend fun getTopScoresSchool(@QueryMap fields: Map<String, String?>): BaseWrapper<List<TopScoresSchool>>

    @GET("statistics/top_scores_schoolgroup")
    suspend fun getTopScoresSchoolGroup(@QueryMap fields: Map<String, String?>): BaseWrapper<List<TopScoresSchoolGroup>>

    @GET("statistics/top_scores_all_schools")
    suspend fun getTopScoresAllSchools(@QueryMap fields: Map<String, String?>): BaseWrapper<List<TopScoresSchoolGroup>>

    @GET("statistics/get_ambassadors")
    suspend fun getAmbassadors(): BaseWrapper<List<TopScoresSchool>>

    @GET("statistics/sms_usage")
    suspend fun getSmsUsage(): BaseWrapper<List<SmsUsage>>

}
