package com.classera.data.daos.weeklyplan

import com.classera.data.models.BaseWrapper
import com.classera.data.models.weeklyplan.WeeklyPlanWrapper
import com.classera.data.network.IncludeChildId
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 * Created by Odai Nazzal on 12/27/2019.
 * Classera
 * o.nazzal@classera.com
 */
interface RemoteWeeklyPlanDao {

    @IncludeChildId
    @GET("courses/weekly_study_plan")
    suspend fun getWeeklyPlanCalendar(@QueryMap fields: Map<String, String?>): BaseWrapper<WeeklyPlanWrapper>
}
