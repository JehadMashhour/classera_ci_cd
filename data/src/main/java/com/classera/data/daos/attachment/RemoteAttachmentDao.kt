package com.classera.data.daos.attachment

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.attachment.AttachmentCommentsWrapper
import com.classera.data.models.attachment.AttachmentData
import com.classera.data.models.digitallibrary.Attachment
import com.classera.data.models.rating.RatingResult
import com.classera.data.network.CustomTimeout
import com.classera.data.network.Pagination
import okhttp3.MultipartBody
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.QueryMap
import java.util.concurrent.TimeUnit

/**
 * Created by Odai Nazzal on 12/19/2019.
 * Classera
 * o.nazzal@classera.com
 */

const val UPLOAD_ATTACHMENT_TIME = 5

interface RemoteAttachmentDao {

    @GET("attachments/get_attachment_details")
    suspend fun getAttachmentDetails(@QueryMap fields: Map<String, String?>): BaseWrapper<Attachment>

    @Pagination
    @GET("attachments/get_attachment_comments")
    suspend fun getAttachmentComments(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<AttachmentCommentsWrapper>

    @FormUrlEncoded
    @POST("attachments/add_reaction")
    suspend fun addReaction(@FieldMap fields: Map<String, @JvmSuppressWildcards Any?>): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("attachments/add_attachment_comment")
    suspend fun addAttachmentComment(@FieldMap fields: Map<String, String?>): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("ratings/add_rating")
    suspend fun addRate(@FieldMap fields: Map<String, String?>): BaseWrapper<RatingResult>


    @GET("attachments/get_upload_attachment_settings")
    suspend fun getAttachmentData(@QueryMap fields: Map<String, String?>): BaseWrapper<AttachmentData>

    @CustomTimeout(UPLOAD_ATTACHMENT_TIME, TimeUnit.MINUTES)
    @Multipart
    @POST("attachments/upload_attachment")
    suspend fun uploadAttachment(@Part parts: List<MultipartBody.Part>): NoContentBaseWrapper
}
