package com.classera.data.daos.support

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.support.CommentWrapper
import com.classera.data.models.support.SupportDataWrapper
import com.classera.data.models.support.SupportWrapper
import com.classera.data.network.Pagination
import okhttp3.MultipartBody
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Part
import retrofit2.http.Query
import retrofit2.http.QueryMap


interface RemoteSupportDao {

    @Pagination
    @GET("support_claims/get_all_tickets")
    suspend fun getSupport(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<List<SupportWrapper>>

    @Pagination
    @GET("support_claims/get_all_tickets")
    suspend fun getSupportByType(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<List<SupportWrapper>>

    @GET("support_claims/get_ticket_details")
    suspend fun getSupportDetails(@Query("ticket_id") ticketId: String?): BaseWrapper<SupportWrapper>

    @Pagination
    @GET("support_claims/comments")
    suspend fun getSupportComments(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<List<CommentWrapper>>

    @FormUrlEncoded
    @POST("support_claims/add_comment")
    suspend fun addSupportComment(@FieldMap fields: Map<String, String?>): NoContentBaseWrapper

    @Multipart
    @POST("support_claims/tickets")
    suspend fun addSupportTicket(@Part parts: List<MultipartBody.Part>)

    @GET("support_claims/support_ticket_types")
    suspend fun getSupportTypes(): BaseWrapper<List<SupportDataWrapper>>

    @GET("support_claims/support_modules")
    suspend fun getSupportModules(): BaseWrapper<List<SupportDataWrapper>>

    @GET("support_claims/support_priorities")
    suspend fun getSupportPriorities(): BaseWrapper<List<SupportDataWrapper>>

    @GET("support_claims/support_statuses")
    suspend fun getSupportStatuses(): BaseWrapper<List<SupportDataWrapper>>

    @FormUrlEncoded
    @PUT("support_claims/support_ticket_types")
    suspend fun updateSupportTicketType(
        @FieldMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): NoContentBaseWrapper

    @FormUrlEncoded
    @PUT("support_claims/support_modules")
    suspend fun updateSupportTicketModules(
        @FieldMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): NoContentBaseWrapper

    @FormUrlEncoded
    @PUT("support_claims/support_priorities")
    suspend fun updateSupportTicketPriority(
        @FieldMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): NoContentBaseWrapper

    @FormUrlEncoded
    @PUT("support_claims/support_statuses")
    suspend fun updateSupportTicketStatus(
        @FieldMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): NoContentBaseWrapper
}
