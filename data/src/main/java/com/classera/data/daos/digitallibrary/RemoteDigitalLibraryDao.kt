package com.classera.data.daos.digitallibrary

import com.classera.data.models.BaseWrapper
import com.classera.data.models.digitallibrary.Attachment
import com.classera.data.network.IncludeChildId
import com.classera.data.network.Pagination
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 * Created by Odai Nazzal on 12/19/2019.
 * Classera
 * o.nazzal@classera.com
 */
interface RemoteDigitalLibraryDao {

    @IncludeChildId
    @Pagination
    @GET("DigitalLibraries/get_student_library")
    suspend fun getStudentLibrary(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<List<Attachment>>

    @IncludeChildId
    @Pagination
    @GET("attachments/student_attachments")
    suspend fun getStudentAttachments(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<List<Attachment>>
}
