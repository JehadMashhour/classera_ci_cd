package com.classera.data.daos.homeloction

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.homelocation.ThreeWordsWraper
import com.classera.data.network.IncludeChildId
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST


/**
 * Created by Rawan Al-Theeb on 1/7/2020.
 * Classera
 * r.altheeb@classera.com
 */
interface RemoteHomeLocationDao {

    @IncludeChildId
    @GET("ThreeWords/get_w3w/")
    suspend fun getW3W(): BaseWrapper<ThreeWordsWraper>

    @FormUrlEncoded
    @POST("ThreeWords/save_w3w")
    suspend fun saveW3W(@FieldMap fields: Map<String, String?>): NoContentBaseWrapper
}
