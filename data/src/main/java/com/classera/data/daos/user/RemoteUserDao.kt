package com.classera.data.daos.user

import com.classera.data.models.BaseWrapper
import com.classera.data.models.calendar.teacher.eventusers.UserSelectionRole
import com.classera.data.models.user.SchoolSettings
import com.classera.data.models.user.User
import com.classera.data.network.IncludeChildId
import retrofit2.http.GET
import retrofit2.http.Url

/**
 * Created by Odai Nazzal on 12/16/2019.
 * Classera
 * o.nazzal@classera.com
 */
interface RemoteUserDao {

    @GET("users/get_user_info")
    suspend fun getUserInfo(): BaseWrapper<User>

    @GET
    suspend fun getUserRoles(@Url url: String?): BaseWrapper<List<UserSelectionRole>>

    @GET("settings/get_school_settings")
    suspend fun getSchoolSettings(): BaseWrapper<SchoolSettings>
}
