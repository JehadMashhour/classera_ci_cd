package com.classera.data.daos.switchsemester

import com.classera.data.models.BaseWrapper
import com.classera.data.models.switchsemester.Semester
import com.classera.data.models.switchsemester.SemesterWrapper
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST


interface RemoteSwitchSemestersDao {

    @GET("schools/semester_list")
    suspend fun getSemesters(
    ): BaseWrapper<List<Semester>>

    @FormUrlEncoded
    @POST("users/generate_jws_for_semester_id")
    suspend fun generateSemesterToken(@FieldMap fields: Map<String, String?>): BaseWrapper<SemesterWrapper>
}
