package com.classera.data.daos.asessments

import com.classera.data.models.BaseWrapper
import com.classera.data.models.assessments.AssessmentResponse
import com.classera.data.models.assessments.AssessmentsDetails
import com.classera.data.network.IncludeChildId
import com.classera.data.network.Pagination
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 * Created by Odai Nazzal on 12/16/2019.
 * Classera
 * o.nazzal@classera.com
 */
interface RemoteAssessmentsDao {

    @IncludeChildId
    @Pagination
    @GET("Assessments/get_assessments")
    suspend fun getAssessmentsList(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<AssessmentResponse>

    @IncludeChildId
    @GET("Assessments/get_assessment_detail")
    suspend fun getAssessmentsDetails(@QueryMap fields: Map<String, String?>): BaseWrapper<AssessmentsDetails>
}
