package com.classera.data.daos.callchild

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.callchildren.*
import retrofit2.http.*

interface RemoteCallChildDao {

    @GET("parents/children")
    suspend fun getParentChildList(): BaseWrapper<List<ChildStudent>>

    @FormUrlEncoded
    @POST("Users/call_child")
    suspend fun callChild(@FieldMap fields: Map<String, String?>): BaseWrapper<Request>

    @FormUrlEncoded
    @POST("Users/change_call_status")
    suspend fun changeCallStatus(@FieldMap fields: Map<String, String?>): NoContentBaseWrapper

    // This API for Driver role
    @GET("Studentsdrivers/personal_driver_students")
    suspend fun getPersonalDriverStudents(): BaseWrapper<List<DriverStudent>>

    @GET("Studentsdrivers/personal_drivers")
    suspend fun getPersonalDrivers(): BaseWrapper<List<Driver>>

    @GET("callChild/get_status")
    suspend fun getCallStatus(@QueryMap fields: Map<String, String?>): BaseWrapper<CallStatus>

}
