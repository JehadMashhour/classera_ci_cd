package com.classera.data.daos.profile.publicprofile

import com.classera.data.models.BaseWrapper
import com.classera.data.models.profile.PublicProfileWrapper
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface RemotePublicProfileDao {

    @GET("profiles/public_profile")
    suspend fun getPublicProfile(@QueryMap fields: Map<String, String?>): BaseWrapper<PublicProfileWrapper>
}
