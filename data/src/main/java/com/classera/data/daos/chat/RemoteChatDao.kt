package com.classera.data.daos.chat

import com.classera.data.models.BasePaginationWrapper
import com.classera.data.models.BaseWrapper
import com.classera.data.models.chat.Block
import com.classera.data.models.chat.Role
import com.classera.data.models.chat.Thread
import com.classera.data.models.chat.ThreadUser
import com.classera.data.models.user.User
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap

/**
 * Created by Odai Nazzal on 12/19/2019. Modified by Lana Manaseer
 * Classera
 * o.nazzal@classera.com
 */
interface RemoteChatDao {

    @GET("ChatsUsers/user_list")
    suspend fun getThreads(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BasePaginationWrapper<List<Thread>>

    @GET("ChatsUsers/user_list")
    suspend fun getGroupThreads(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BasePaginationWrapper<List<ThreadUser>>

    @GET("DynamicRoles/get_roles")
    suspend fun getRoles(): BaseWrapper<List<Role>>

    @GET("ChatsUsers/get_block_users")
    suspend fun getBlockedUsers(): BaseWrapper<List<User>>

    @GET("ChatsUsers/check_blocked/{blockUserId}")
    suspend fun checkBlockedUser(
        @Path("blockUserId") blockUserId: String?
    ): BaseWrapper<Block>

    @FormUrlEncoded
    @POST("ChatsUsers/block_users")
    suspend fun toggleUserBlockStatus(
        @FieldMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<Block>

    @GET("ChatsUsers/list_group_participants")
    suspend fun getListGroupParticipants(
        @Query("thread_id") threadID: String?
    ): BaseWrapper<List<ThreadUser>>

    @FormUrlEncoded
    @POST("users/get_users_baisc_details")
    suspend fun getUsersBasicDetails(
        @FieldMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<List<User>>

}
