package com.classera.data.daos.notification

import com.classera.data.models.notification.NotificationWrapper
import com.classera.data.network.CNSPagination
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface RemoteNotificationsDao {

    @CNSPagination
    @GET("v1/users/{id}/notifications")
    suspend fun getNotificationList(
        @Path("id") id: String?,
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): NotificationWrapper

    @CNSPagination
    @GET("v1/users/{id}/notifications/unseenCount")
    suspend fun getNotificationCount(
        @Path("id") id: String?,
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): NotificationWrapper
}
