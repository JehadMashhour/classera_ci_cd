package com.classera.data.daos.switchroles

import com.classera.data.models.BaseWrapper
import com.classera.data.models.switchroles.AuthWrapper
import com.classera.data.models.switchroles.Role
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST


interface RemoteSwitchRolesDao {

    @GET("users/get_user_roles")
    suspend fun getRoles(): BaseWrapper<List<Role>>

    @FormUrlEncoded
    @POST("users/generate_jws_for_user_role")
    suspend fun generateJwtToken(@FieldMap fields: Map<String, String?>): BaseWrapper<AuthWrapper>
}
