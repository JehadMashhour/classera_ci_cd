package com.classera.data.daos.mailbox

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.mailbox.DraftWrapper
import com.classera.data.models.mailbox.InboxDetailsResponse
import com.classera.data.models.mailbox.InboxResponse
import com.classera.data.models.mailbox.InboxSearchResponse
import com.classera.data.models.mailbox.MessageRoleResponse
import com.classera.data.models.mailbox.OutboxDetailsResponse
import com.classera.data.models.mailbox.OutboxResponse
import com.classera.data.models.mailbox.OutboxSearchResponse
import com.classera.data.models.mailbox.RecipientByRole
import com.classera.data.models.mailbox.Trash
import com.classera.data.models.mailbox.School
import com.classera.data.network.Pagination
import okhttp3.MultipartBody
import retrofit2.http.GET
import retrofit2.http.QueryMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.FieldMap
import retrofit2.http.Part
import retrofit2.http.Multipart

/**
 * Project: Classera
 * Created: Jan 6, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
interface RemoteMailboxDao {

    @FormUrlEncoded
    @POST("messages/permanently_delete")
    suspend fun deletePermanently(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

    @FormUrlEncoded
    @POST("messages/return_to_inbox")
    suspend fun returnToBox(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

    @Pagination
    @GET("messages/get_trash_list")
    suspend fun getTrash(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<List<Trash>>

    @Pagination
    @GET("messages/get_inbox_messages")
    suspend fun getInbox(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<InboxResponse>

    @Pagination
    @GET("messages/get_outbox_messages")
    suspend fun getOutbox(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<OutboxResponse>

    @FormUrlEncoded
    @POST("messages/move_to_trash")
    suspend fun deleteMessage(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

    @Pagination
    @GET("messages/search_inbox_messages")
    suspend fun searchInbox(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<InboxSearchResponse>

    @Pagination
    @GET("Messages/search_outbox_messages")
    suspend fun searchOutbox(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<OutboxSearchResponse>

    @GET("messages/get_message_inbox_details")
    suspend fun getInboxDetails(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<InboxDetailsResponse>

    @GET("messages/get_message_outbox_details")
    suspend fun getOutboxDetails(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<OutboxDetailsResponse>

    @FormUrlEncoded
    @POST("messages/reply_message")
    suspend fun replyMessage(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

    @GET("messages/get_message_role")
    suspend fun getMessageRoles(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<List<MessageRoleResponse>>

    @Multipart
    @POST("messages/send_message")
    suspend fun sendMessage(@Part parts: List<MultipartBody.Part>): NoContentBaseWrapper

    @GET("messages/get_user_by_role")
    suspend fun getUserByRole(@QueryMap fields: Map<String, String?>): BaseWrapper<List<RecipientByRole>>

    @Pagination
    @GET("messages/get_draft")
    suspend fun getDraftMessages(
        @QueryMap fields: Map<String, @JvmSuppressWildcards Any?>
    ): BaseWrapper<List<DraftWrapper>>

    @GET("messages/get_schools_list_user")
    suspend fun getSchoolList(): BaseWrapper<List<School>>
}
