package com.classera.data.daos.classvisits

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.classvisits.Assessment
import com.classera.data.models.classvisits.ClassVisit
import com.classera.data.models.classvisits.Teacher
import com.classera.data.models.classvisits.Timeslot
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.QueryMap


/**
 * Created by Rawan Al-Theeb on 2/12/2020.
 * Classera
 * r.altheeb@classera.com
 */
interface RemoteClassVisitsDao {
    @GET("supervisors/class_visits")
    suspend fun classVisits() : BaseWrapper<List<ClassVisit>>

    @GET("supervisors/tsupv_teachers")
    suspend fun getTeachers(): BaseWrapper<List<Teacher>>

    @GET("supervisors/tsupv_school_assessments")
    suspend fun getAssessments(): BaseWrapper<List<Assessment>>

    @GET("supervisors/get_lecture_timeslots_by_date")
    suspend fun getTimeslots(
        @QueryMap fields: Map<String, String?>
    ): BaseWrapper<List<Timeslot>>

    @FormUrlEncoded
    @POST("supervisors/add_class_visit")
    suspend fun addClassVisit(
        @FieldMap fields: Map<String, String?>
    ): NoContentBaseWrapper

}
