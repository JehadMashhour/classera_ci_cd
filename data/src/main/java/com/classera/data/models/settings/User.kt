package com.classera.data.models.settings


import com.squareup.moshi.Json

data class User(
    @Json(name = "full_name")
    val fullName: String? = "",
    @Json(name = "id")
    val id: String? = "",
    @Json(name = "lang")
    val lang: String? = "",
    @Json(name = "main_account_id")
    val mainAccountId: Any? = Any(),
    @Json(name = "number")
    val number: String? = "",
    @Json(name = "restrict_login")
    val restrictLogin: Boolean? = false,
    @Json(name = "role_id")
    val roleId: String? = "",
    @Json(name = "school_id")
    val schoolId: String? = "",
    @Json(name = "username")
    val username: String? = ""
)
