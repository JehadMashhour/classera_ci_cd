package com.classera.data.models.assignments

import android.os.Parcelable
import com.classera.data.R
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Assignment(

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "title")
    val title: String? = null,

    @Json(name = "status")
    val status: AssignmentStatus? = null,

    @Json(name = "course_title")
    val courseTitle: String? = null,

    @Json(name = "publishing_datetime")
    val publishingDatetime: String? = null,

    @Json(name = "due_datetime")
    val dueDatetime: String? = null,

    @Json(name = "cutoff_datetime")
    val cutoffDatetime: String? = null,

    @Json(name = "mark")
    val mark: String? = null,

    @Json(name = "assessment_id")
    val assessmentId: String? = null,

    @Json(name = "issue_certificates")
    val issueCertificates: Boolean? = null,

    @Json(name = "certificate_template_id")
    val certificateTemplateId: String? = null,

    @Json(name = "randomize")
    val randomize: String? = null,

    @Json(name = "view_answers")
    val viewAnswers: Boolean? = null,

    @Json(name = "teacher_user_id")
    val teacherUserId: String? = null,

    @Json(name = "submission_mark")
    val submissionMark: String? = null,

    @Json(name = "submission_id")
    val submissionId: String? = null,

    @Json(name = "rate")
    val rate: Float? = null,

    @Json(name = "score")
    val score: Float = 0f,

    @Json(name = "total_mark")
    val totalMark: String? = null,

    @Json(name = "student_average")
    val studentAverage: Double? = null,

    @Json(name = "section_average")
    val sectionAverage: Double? = null,

    @Json(name = "type")
    val type: String? = null,

    @Json(name = "comments")
    val comments: String? = null,

    @Json(name = "accept_submission_attachment")
    val acceptSubmissionAttachment: Boolean? = null,

    @Json(name = "question_time")
    val questionTime: String? = null,

    @Json(name = "show_responses")
    val showResponses: Boolean? = null,

    @Json(name = "show_hints")
    val showHints: Boolean? = null,

    @Json(name = "course_id")
    val courseId: String? = null,

    @Json(name = "created")
    val created: String? = null,

    @Json(name = "duration")
    val duration: Long? = null,

    @Json(name = "maxMark")
    val maxMark: String? = null,

    @Json(name = "multiple_submissions")
    val multipleSubmissions: Boolean? = null,

    @Json(name = "max_submissions")
    val maxSubmissions: String? = null,

    @Json(name = "student_submissions")
    val studentSubmissions: Int? = null,

    @Json(name = "password")
    val password: Boolean? = null,

    @Json(name = "published")
    val published: Boolean? = null,

    @Json(name = "teacher_name")
    val teacherName: String? = null,

    @Json(name = "Question")
    val questions: List<Question>? = null,

    @Json(name = "training_course_passing_percentage")
    val passingPercentage: String? = null,

    @Json(name = "preparation_id")
    val preparationId: String? = null,

    @Json(name = "allow_multiple_submissions")
    val allowMultipleSubmissions: Boolean? = null,

    @Json(name = "view_answers_after_cutoff")
    val viewAnswersAfterCutOff: Boolean? = null,

    @Json(name = "max_submissions_allowed")
    val maxSubmissionsAllowed: String? = null,

    @Json(name = "time_limit")
    val timeLimit: String? = null,

    @Json(name = "can_edit")
    val canEdit: Boolean? = null,

    @Json(name = "face_verification")
    val faceVerification: Boolean? = null

) : Parcelable {

    fun getStatusLabel(): Int {
        return status?.label ?: R.string.button_row_assignment_action_launch
    }

    fun viewStartAgainButton(): Boolean {
        return status == AssignmentStatus.START_AGAIN
    }
}
