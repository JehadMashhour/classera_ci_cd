package com.classera.data.models.home

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TeacherHomeResponse(

    @Json(name = "unviewed_events_count")
    val unviewedEventsCount: Int = 0,

    @Json(name = "events_percentage")
    val eventsPercentage: Int = 0
)
