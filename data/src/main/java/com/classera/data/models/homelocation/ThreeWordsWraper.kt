package com.classera.data.models.homelocation


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ThreeWordsWraper(
    @Json(name = "ThreeWord")
    val threeWord: ThreeWord? = null
)
