package com.classera.data.models.parent


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ParentChildWrapper(
    @Json(name = "id")
    val id: String? = null,
    @Json(name = "level")
    val level: String? = null,
    @Json(name = "name")
    val name: String? = null,
    @Json(name = "profile_picture")
    val profilePicture: String? = null,
    @Json(name = "request_id")
    val requestId: Any? = Any(),
    @Json(name = "score")
    val score: Score? = Score(),
    @Json(name = "status")
    val status: Any? = Any(),
    @Json(name = "status_name")
    val statusName: Any? = Any(),
    @Json(name = "student_id")
    val studentId: String? = null,
    @Json(name = "w3w")
    val w3w: String? = null
)
