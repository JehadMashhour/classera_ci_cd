package com.classera.data.models.announcements


import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class AnnouncementWrapper(

    @Json(name = "Announcement")
    val announcement: Announcement? = null
) : Parcelable
