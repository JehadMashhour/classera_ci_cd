package com.classera.data.models.mailbox

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Project: Classera
 * Created: Nov 18, 2020
 *
 * @author Khaled Mohammad
 */
@JsonClass(generateAdapter = true)
data class School(

    @Json(name = "school_id")
    val id: String? = null,

    @Json(name = "school_name")
    val name: String? = null
)
