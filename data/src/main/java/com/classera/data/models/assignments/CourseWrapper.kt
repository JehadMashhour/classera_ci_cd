package com.classera.data.models.assignments


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CourseWrapper(

    @Json(name = "Course")
    val course: AssignmentCourse? = null
)
