package com.classera.data.models.vcr.lecturetimeslotpreparation


import com.classera.data.models.selection.Selectable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

/**
 * Created by Rawan Al-Theeb on 2/13/2020.
 * Classera
 * r.altheeb@classera.com
 */
@JsonClass(generateAdapter = true)
@Parcelize
data class Timeslot(
    @Json(name = "id")
    override val id: String?,

    @Json(name = "title")
    override val title: String?,

    override var selected: Boolean = false

) : Selectable
