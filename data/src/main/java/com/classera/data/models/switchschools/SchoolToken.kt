package com.classera.data.models.switchschools

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SchoolToken(

    @Json(name = "token")
    val token: String? = ""
)
