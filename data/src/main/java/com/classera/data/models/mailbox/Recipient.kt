package com.classera.data.models.mailbox

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Recipient(

    @Json(name = "recipient_id")
    val recipientId: String? = null,

    @Json(name = "message_id")
    val messageId: String? = null,

    @Json(name = "first_name")
    val firstName: String? = null,

    @Json(name = "family_name")
    val familyName: String? = null,

    @Json(name = "image_url")
    val imageUrl: String? = null

)
