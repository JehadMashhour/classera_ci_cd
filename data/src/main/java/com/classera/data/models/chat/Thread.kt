package com.classera.data.models.chat

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

/**
 * Created by Odai Nazzal on 1/11/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
@Parcelize
@JsonClass(generateAdapter = true)
data class Thread(

    @Json(name = "status")
    val status: UserStatus? = null,

    @Json(name = "user_id")
    val userId: String? = null,

    @Json(name = "thread_id")
    val threadId: String? = null,

    @Json(name = "unread")
    val unread: String? = null,

    @Json(name = "last_message")
    var lastMessage: Message? = null,

    @Json(name = "photo_filename")
    val photoFilename: String? = null,

    @Json(name = "full_name")
    val fullName: String? = null,

    @Json(name = "username")
    val username: String? = null,

    @Json(name = "group")
    val group: Boolean? = null
) : Parcelable
