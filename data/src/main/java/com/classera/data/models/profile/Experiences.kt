package com.classera.data.models.profile


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Experiences(
    @Json(name = "company_name")
    val companyName: String? = null,

    @Json(name = "description")
    val description: String? = null,

    @Json(name = "end_date")
    val endDate: String? = null,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "job_title")
    val jobTitle: String? = null,

    @Json(name = "Job_type")
    val jobType: String? = null,

    @Json(name = "start_date")
    val startDate: String? = null
)
