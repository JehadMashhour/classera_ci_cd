package com.classera.data.models.mailbox

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DraftWrapper(

    @Json(name = "body")
    val body: String? = null,

    @Json(name = "created")
    val created: String? = null,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "image_url")
    val imageUrl: String? = null,

    @Json(name = "priority")
    val priority: String? = null,

    @Json(name = "title")
    val title: String? = null,

    @Json(name = "type")
    val type: String? = null,

    @Json(name = "sender_id")
    val senderId: String? = null,

    @Json(name = "full_name")
    val fullName: String? = null
)
