package com.classera.data.models.notification

import com.google.gson.annotations.SerializedName

data class AnnouncementNotificationBody(

    @SerializedName("announcementId")
    val announcementId: String? = null,

    @SerializedName("title")
    val title: String? = null
)

