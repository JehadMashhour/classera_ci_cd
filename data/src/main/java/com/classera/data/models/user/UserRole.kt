package com.classera.data.models.user

import android.view.Menu
import com.classera.data.R
import com.classera.data.moshi.enums.Json

/**
 *
 * Project: Classera
 * Created: Jan 08, 2020
 *
 * @author Mohamed Hamdan
 */
enum class UserRole(
    val value: String,
    val menu: Int,
    val actionsMenu: Int,
    val bottomNavigationViewNavigation: String,
    val navigationViewNavigation: String
) {

    @Json(name = "6")
    STUDENT(
        value = "6",
        menu = R.menu.menu_activity_main_student_navigation_view,
        actionsMenu = Menu.NONE,
        bottomNavigationViewNavigation = "navigation_student_fragment_main",
        navigationViewNavigation = "navigation_student_activity_main"
    ),

    @Json(name = "4")
    TEACHER(
        value = "4",
        menu = R.menu.menu_activity_main_teacher_navigation_view,
        actionsMenu = R.menu.menu_teacher_more_actions,
        bottomNavigationViewNavigation = "navigation_teacher_fragment_main",
        navigationViewNavigation = "navigation_teacher_activity_main"
    ),

    @Json(name = "3")
    GUARDIAN(
        value = "3",
        menu = R.menu.menu_activity_main_guardian_navigation_view,
        actionsMenu = Menu.NONE,
        bottomNavigationViewNavigation = "navigation_guardian_fragment_main",
        navigationViewNavigation = "navigation_guardian_activity_main"
    ),

    @Json(name = "11")
    TEACHER_SUPERVISOR(
        value = "11",
        menu = R.menu.menu_activity_main_teacher_supervisor_navigation_view,
        actionsMenu = R.menu.menu_teacher_more_actions,
        bottomNavigationViewNavigation = "navigation_other_roles_fragment_main",
        navigationViewNavigation = "navigation_other_roles_activity_main"
    ),

    @Json(name = "2")
    ADMIN(
        value = "2",
        menu = R.menu.menu_activity_main_admin_navigation_view,
        actionsMenu = R.menu.menu_teacher_more_actions,
        bottomNavigationViewNavigation = "navigation_admin_fragment_main",
        navigationViewNavigation = "navigation_admin_activity_main"
    ),

    @Json(name = "8")
    MANAGER(
        value = "8",
        menu = R.menu.menu_activity_main_manager_navigation_view,
        actionsMenu = R.menu.menu_teacher_more_actions,
        bottomNavigationViewNavigation = "navigation_other_roles_fragment_main",
        navigationViewNavigation = "navigation_other_roles_activity_main"
    ),

    @Json(
        name = "12",
        alternatives = ["9"]
    )
    FLOOR_SUPERVISOR(
        value = "12",
        menu = R.menu.menu_activity_main_other_roles_navigation_view,
        actionsMenu = R.menu.menu_teacher_more_actions,
        bottomNavigationViewNavigation = "navigation_other_roles_fragment_main",
        navigationViewNavigation = "navigation_other_roles_activity_main"
    ),

    @Json(name = "99")
    UNKNOWN(
        value = "99",
        menu = R.menu.menu_unknown_navigation_view,
        actionsMenu = Menu.NONE,
        bottomNavigationViewNavigation = "navigation_unknown_fragment_main",
        navigationViewNavigation = "navigation_unknown_activity_main"
    ),

    @Json(name = "21")
    DRIVER(
        value = "21",
        menu = R.menu.menu_activity_main_driver_roles_navigation_view,
        actionsMenu = Menu.NONE,
        bottomNavigationViewNavigation = "navigation_driver_roles_fragment_main",
        navigationViewNavigation = "navigation_driver_roles_activity_main"
    ),

    @Json(
        name = "29",
        alternatives = ["14", "7", "18", "17", "22", "26", "25", "13", "19", "28",
            "16", "10", "23", "15", "5", "24"]
    )
    GENERAL(
        value = "29",
        menu = R.menu.menu_activity_main_other_roles_navigation_view,
        actionsMenu = R.menu.menu_teacher_more_actions,
        bottomNavigationViewNavigation = "navigation_other_roles_fragment_main",
        navigationViewNavigation = "navigation_other_roles_activity_main"
    ),
}
