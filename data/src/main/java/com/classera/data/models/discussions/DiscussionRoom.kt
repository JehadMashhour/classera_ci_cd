package com.classera.data.models.discussions

import android.os.Parcelable
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.classera.data.moshi.timeago.TimeAgo
import com.squareup.moshi.Json
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

/**
 * Created by Odai Nazzal on 12/30/2019.
 * Classera
 *
 * o.nazzal@classera.com
 */
@Parcelize
data class DiscussionRoom(

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "teacher_image")
    val teacherImage: String? = null,

    @Json(name = "teacher_name")
    val teacherName: String? = null,

    @Json(name = "title")
    val title: String? = null,

    @Json(name = "creator_id")
    val creatorId: String? = null,

    @TimeAgo
    @Json(name = "created")
    val created: String? = null,

    @Json(name = "instruction")
    val instruction: String? = null,

    @Json(name = "comments_counts")
    val commentsCounts: String? = null,

    @Json(name = "closed")
    val closed: Boolean = false,

    @Json(name = "hide_comments")
    val hideComments: Boolean = false

) : BaseObservable(), Parcelable {

    @get:Bindable
    @IgnoredOnParcel
    @Transient

    var deleting: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.deleting)
        }
}
