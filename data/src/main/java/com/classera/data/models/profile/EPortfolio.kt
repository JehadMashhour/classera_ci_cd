package com.classera.data.models.profile


import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class EPortfolio(

    val test: String = "Portfolio",

    @Json(name = "attachment_id")
    val attachmentId: String? = null,

    @Json(name = "brief")
    val brief: String? = null,

    @Json(name = "course_id")
    val courseId: String? = null,

    @Json(name = "created")
    val created: String? = null,

    @Json(name = "date")
    val date: String? = null,

    @Json(name = "deleted")
    val deleted: Boolean? = false,

    @Json(name = "details")
    val details: String? = null,

    @Json(name = "eportfolio")
    val eportfolio: Boolean? = false,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "image")
    val image: String? = null,

    @Json(name = "level_id")
    val levelId: String? = null,

    @Json(name = "modified")
    val modified: String? = null,

    @Json(name = "preparation_id")
    val preparationId: String? = null,

    @Json(name = "reference_id")
    val referenceId: String? = null,

    @Json(name = "reference_sub_type")
    val referenceSubType: String? = null,

    @Json(name = "reference_type")
    val referenceType: String? = null,

    @Json(name = "school_id")
    val schoolId: String? = null,

    @Json(name = "semester_id")
    val semesterId: String? = null,

    @Json(name = "sharing_status")
    val sharingStatus: String? = null,

    @Json(name = "status")
    val status: String? = null,

    @Json(name = "teacher_action")
    val teacherAction: String? = null,

    @Json(name = "title")
    val title: String? = null,

    @Json(name = "total_views")
    val totalViews: String? = null,

    @Json(name = "type_id")
    val typeId: String? = null,

    @Json(name = "user_id")
    val userId: String? = null,

    @Json(name = "user_role_id")
    val userRoleId: String? = null
) : Parcelable
