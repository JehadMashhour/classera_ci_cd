package com.classera.data.models.attachment


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AttachmentData(

    @Json(name = "allowed")
    val allowed: List<String?>? = null,

    @Json(name = "is_premium")
    val isPremium: Boolean? = null,

    @Json(name = "lectures")
    val lectures: List<Lecture>? = null,

    @Json(name = "libraries")
    val libraries: List<Library>? = null,

    @Json(name = "parent_school_name")
    val parentSchoolName: String? = null,

    @Json(name = "preparations")
    val preparations: List<Preparation>? = null,

    @Json(name = "sharing_status")
    val sharingStatus: SharingStatus? = null,

    @Json(name = "students")
    val students: List<Student>? = null,

    @Json(name = "upload_from")
    val uploadFrom: List<Int>? = null

)
