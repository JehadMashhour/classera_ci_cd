package com.classera.data.models.notification

import com.google.gson.annotations.SerializedName

data class MailboxNotificationBody(

    @SerializedName("messageId")
    var messageId: String? = null,

    @SerializedName("title")
    var title: String? = null
)

