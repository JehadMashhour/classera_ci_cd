package com.classera.data.models.vcr.vendor

import android.content.Context
import android.content.res.Resources
import android.os.Parcelable
import com.classera.data.R
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize
import java.lang.reflect.Field

@JsonClass(generateAdapter = true)
@Parcelize
data class Durations(

    @field:Json(name = "45")
    val jsonMember45: String? = null,

    @field:Json(name = "35")
    val jsonMember35: String? = null,

    @field:Json(name = "80")
    val jsonMember80: String? = null,

    @field:Json(name = "60")
    val jsonMember60: String? = null,

    @field:Json(name = "40")
    val jsonMember40: String? = null,

    @field:Json(name = "30")
    val jsonMember30: String? = null
) : Parcelable {

    fun getList(context: Context): List<Duration> {
        this.let {
            val fields = it::class.java.declaredFields
            return fields
                .filter { f -> f?.getAnnotation(Json::class.java) != null }
                .mapIndexed { _: Int, field: Field? ->
                    Duration(
                        field?.getAnnotation(Json::class.java)?.name,
                        field?.get(it).toString()
                    )
                }
        }
    }
}
