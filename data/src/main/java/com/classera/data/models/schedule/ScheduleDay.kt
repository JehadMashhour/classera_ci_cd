package com.classera.data.models.schedule


import com.squareup.moshi.Json

data class ScheduleDay(

    @Json(name = "day")
    val day: String? = "",

    @Json(name = "slots")
    val slots: List<ScheduleSlot?>? = listOf()
)
