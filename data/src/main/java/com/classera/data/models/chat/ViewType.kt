package com.classera.data.models.chat

/**
 * Created by Odai Nazzal on 1/11/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
enum class ViewType(var senderId: String? = null) {

    RIGHT,

    LEFT
}
