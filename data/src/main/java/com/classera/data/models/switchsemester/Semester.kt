package com.classera.data.models.switchsemester


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Semester(

    @Json(name = "current")
    val current: Boolean? = false,

    @Json(name = "end_date")
    val endDate: String? = "",

    @Json(name = "semester_id")
    val semesterId: String? = "",

    @Json(name = "start_date")
    val startDate: String? = "",

    @Json(name = "title")
    val title: String? = "",

    @Json(name = "weeks_number")
    val weeksNumber: Any? = Any(),

    @Json(name = "year_id")
    val yearId: String? = ""
)
