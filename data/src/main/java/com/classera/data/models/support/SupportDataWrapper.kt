package com.classera.data.models.support


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SupportDataWrapper(
    @Json(name = "id")
    val id: Int? = 0,
    @Json(name = "title")
    val title: String? = null
)
