package com.classera.data.models.callchildren

import android.os.Parcelable


/**
 * Project: Classera
 * Created: 8/18/2021
 *
 * @author Jehad Abdalqader
 */
abstract class BaseStudent : Parcelable {
    abstract val id: String?
    abstract val studentName: String?
    abstract val profileImage: String?
    abstract var status: String?
    abstract val statusName: String?
    abstract var requestId: String?
    abstract var isLoading: Boolean?

    val callStatusTypes: CallStatusTypes
        get() = CallStatusTypes.getCallStatusType(status)

    val canCall: Boolean
        get() = when (callStatusTypes) {
            CallStatusTypes.UNKNOWN,
            CallStatusTypes.ARRIVED,
            CallStatusTypes.CANCELED -> {
                true
            }
            else -> false
        }
    val isCalling: Boolean
        get() = callStatusTypes == CallStatusTypes.ACTIVE

    val isDismissed: Boolean
        get() = callStatusTypes == CallStatusTypes.DISMISSED
}



