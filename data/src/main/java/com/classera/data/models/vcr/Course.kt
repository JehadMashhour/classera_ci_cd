package com.classera.data.models.vcr


import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Course(

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "title")
    val title: String? = null

): Parcelable

