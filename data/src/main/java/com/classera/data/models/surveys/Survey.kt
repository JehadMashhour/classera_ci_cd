package com.classera.data.models.surveys

import com.classera.data.moshi.timeago.TimeAgo
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Survey(
    @Json(name = "AssessmentType")
    val assessmentType: String? = null,

    @TimeAgo
    @Json(name = "created")
    val created: String? = null,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "is_template")
    val isTemplate: String? = null,

    @Json(name = "ParentTitle")
    val parentTitle: String? = null,

    @Json(name = "title")
    val title: String? = null
)
