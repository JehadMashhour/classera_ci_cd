package com.classera.data.models.chat

import android.view.View
import com.classera.data.R

/**
 * Project: Classera
 * Created: Mar 10, 2020
 *
 * @author Mohamed Hamdan
 */
enum class MessageMediaType(
    val pattern: Regex?,
    val senderLayout: Int,
    val receiverLayout: Int
) {

    IMAGE(
        pattern = ("(bmp|dds|exr|gif|heic|heif|ico|iff|jng|jp2|jpeg|jpg|mng|pbm" +
                "|pcd|pcx|pgm|png|ppm|psd|ras|sgi|targa|tga|tif|tiff|wbmp|webp|xbm|xpm)")
            .toRegex(RegexOption.IGNORE_CASE),
        senderLayout = R.layout.row_message_image_sender,
        receiverLayout = R.layout.row_message_image_receiver
    ),

    VIDEO(
        pattern = "(webm|mpg|mp2|mpeg|mpe|mpv|ogg|mp4|m4p|m4v|avi|wmv|mov|qt|flv|swf|avchd)"
            .toRegex(RegexOption.IGNORE_CASE),
        senderLayout = R.layout.row_message_video_sender,
        receiverLayout = R.layout.row_message_video_receiver
    ),

    AUDIO(
        pattern = "(mp3|wma|wav|mp2|aac|ac3|au|ogg|flac|m4a)".toRegex(RegexOption.IGNORE_CASE),
        senderLayout = R.layout.row_message_audio_sender,
        receiverLayout = R.layout.row_message_audio_receiver
    ),

    DOCUMENT(
        pattern = "(doc|docx|pdf|pptx|xlsx|oxps|xps)".toRegex(RegexOption.IGNORE_CASE),
        senderLayout = R.layout.row_message_document_sender,
        receiverLayout = R.layout.row_message_document_receiver
    ),

    OTHERS(
        pattern = null,
        senderLayout = R.layout.row_message_others_sender,
        receiverLayout = R.layout.row_message_others_receiver
    ),

    UNKNOWN(null, View.NO_ID, View.NO_ID)
}
