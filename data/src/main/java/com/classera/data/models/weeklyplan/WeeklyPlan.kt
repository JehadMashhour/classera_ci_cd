package com.classera.data.models.weeklyplan

import com.classera.data.moshi.shortday.ShortDayName
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Created by Odai Nazzal on 12/27/2019.
 * Classera
 * o.nazzal@classera.com
 */

@JsonClass(generateAdapter = true)
data class WeeklyPlan(

    @ShortDayName
    @Json(name = "week_day")
    val weekDay: String? = null,

    @Json(name = "day_date")
    val dayDate: String? = null,

    @Json(name = "preparations")
    val preparations: List<WeeklyPlanPreparationWrapper>? = null
)
