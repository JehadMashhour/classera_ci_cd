package com.classera.data.models.user

import com.squareup.moshi.Json

/**
 * Project: Classera
 * Created: Feb 15, 2020
 *
 * @author Mohamed Hamdan
 */
private const val PRESENT_STATUS = 0
private const val ABSENT_STATUS = 1
private const val LATE_STATUS = 3
private const val EXCUSED_STATUS = ABSENT_STATUS
private const val LEFT_WITH_PERMISSION_STATUS = 5

enum class AbsenceStatus(val value: Int) {

    @Json(name = "P")
    PRESENT(PRESENT_STATUS),

    @Json(name = "A")
    ABSENT(ABSENT_STATUS),

    @Json(name = "L")
    LATE(LATE_STATUS),

    @Json(name = "E")
    EXCUSED(EXCUSED_STATUS),

    @Json(name = "LP")
    LEFT_WITH_PERMISSION(LEFT_WITH_PERMISSION_STATUS);
}
