package com.classera.data.models.assessments

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Assessment(

    @Json(name = "assessment_id")
    val assessmentId: String? = null,

    @Json(name = "assessment_item_title")
    val assessmentItemTitle: String? = null,

    @Json(name = "assessment_type_id")
    val assessmentTypeId: String? = null,

    @Json(name = "comment")
    val comment: String? = null,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "indicator_id")
    val indicatorId: String? = null,

    @Json(name = "indicator_title")
    val indicatorTitle: String? = null,

    @Json(name = "item_id")
    val itemId: String? = null,

    @Json(name = "level")
    val level: String? = null,

    @Json(name = "student_id")
    val studentId: String? = null,

    @Json(name = "assessment_type_title")
    val assessmentTypeTitle: String? = null,

    @Json(name = "assessment_level_title")
    val assessmentLevelTitle: String? = null,

    @Json(name = "created")
    val created: String? = null,

    @Json(name = "title")
    val title: String? = null
)
