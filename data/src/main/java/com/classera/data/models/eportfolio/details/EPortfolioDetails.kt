package com.classera.data.models.eportfolio.details

import android.os.Parcelable
import androidx.databinding.BaseObservable
import com.classera.data.moshi.timeago.TimeAgo
import com.classera.data.moshi.toint.ToInt
import com.classera.data.toDate
import com.classera.data.toString
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EPortfolioDetails(

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "title")
    val title: String? = null,

    @Json(name = "brief")
    val brief: String? = null,

    @Json(name = "details")
    val details: String? = null,

    @Json(name = "user_id")
    val userId: String? = null,

    @Json(name = "user_role_id")
    val userRoleId: String? = null,

    @Json(name = "attachment_id")
    val attachmentId: String? = null,

    @TimeAgo
    @Json(name = "created")
    val created: String? = null,

    @Json(name = "modified")
    val modified: String? = null,

    @ToInt
    @Json(name = "total_views")
    val totalViews: Int = 0,

    @Json(name = "image_url")
    val imageUrl: String? = null,

    @Json(name = "cover")
    val cover: String? = null,

    @Json(name = "avatar")
    val avatar: String? = null,

    @Json(name = "file_url")
    val attachmentUrl: String? = null,

    @Json(name = "course_id_info_id")
    val courseInfoId: String? = null,

    @Json(name = "type_id_info_id")
    val typeInfoId: String? = null,

    @Json(name = "sharing_status_info_id")
    val sharingInfoLevel: String? = null

) : BaseObservable(), Parcelable {
    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun getDate(): String? {
        return modified?.toDate("yyyy-MM-dd")?.toString("yyyy-MM-dd")
    }
}
