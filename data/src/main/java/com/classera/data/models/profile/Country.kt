package com.classera.data.models.profile

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Country (

    @Json(name = "ID")
    val id: String? = null,

    @Json(name = "Name_English")
    val nameEng: String? = null,

    @Json(name = "Name_Arabic")
    val nameAra: String? = null,

    @Json(name = "Name_French")
    val nameFre: String? = null,

    @Json(name = "Code")
    val code: String? = null,

    @Json(name = "Code2")
    val code2: String? = null,

    @Json(name = "Continent")
    val continent: String? = null,

    @Json(name = "Region")
    val region: String? = null,

    @Json(name = "Phone_Code")
    val phoneCode: String? = null,

    @Json(name = "Mobile_Length")
    val mobileLength: String? = null

)
