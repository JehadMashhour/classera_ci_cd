package com.classera.data.models.vcr.vcrconfirm


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class VcrConfirmResponse(
    @Json(name = "session_url")
    val sessionUrl: String? = null,

    @Json(name = "passcode")
    val passcode: String? = null
)
