package com.classera.data.models.assignments

/**
 * Project: Classera
 * Created: May 18, 2020
 *
 * @author Kahled Mohammad
 */
enum class PublishToEnum {
    SECTIONS,

    STUDENTS
}
