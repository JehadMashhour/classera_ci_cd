package com.classera.data.models.settings


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SettingsWrapper(
    @Json(name = "auth_token")
    val authToken: String? = null,
    @Json(name = "login")
    val login: Login? = Login()
)
