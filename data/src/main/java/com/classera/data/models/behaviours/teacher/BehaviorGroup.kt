package com.classera.data.models.behaviours.teacher

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BehaviorGroup(
    @Json(name = "id")
    val id: String? = null,

    @Json(name = "title")
    val title: String? = null,

    @Json(name = "type")
    val type: String? = null,

    @Json(name = "school_id")
    val schoolId: String? = null,

    @Json(name = "teacher_enabled")
    val teacherEnabled: Boolean? = null,

    @Json(name = "created")
    val created: String? = null

)

