package com.classera.data.models.rating

import com.squareup.moshi.Json

/**
 * Project: Classera
 * Created: Jun 04, 2020
 *
 * @author Mohamed Hamdan
 */
data class RatingResult(

    @Json(name = "newRating")
    val newRating: String? = null,

    @Json(name = "newAverage")
    val newAverage: String? = null,

    @Json(name = "newCount")
    val newCount: String? = null
)
