package com.classera.data.models.user

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


/**
 * Created by Rawan Al-Theeb on 3/9/2020.
 * Classera
 * r.altheeb@classera.com
 */
@JsonClass(generateAdapter = true)
data class SchoolSettings(

    @Json(name = "absences_letter")
    val absencesLetter: String? = null,

    @Json(name = "absences_letter_excused")
    val absencesLetterExcused: String? = null,

    @Json(name = "absences_notification")
    val absencesNotification: String? = null,

    @Json(name = "absences_notification_excused")
    val absencesNotificationExcused: String? = null,

    @Json(name = "allow_discussion_board")
    val allowDiscussionBoard: String? = null,

    @Json(name = "allow_hijri_date")
    val allowHijriDate: String? = null,

    @Json(name = "allow_internally_tickets")
    val allowInternallyTickets: String? = null,

    @Json(name = "allow_managers_add_announcements")
    val allowManagersAddAnnouncements: String? = null,

    @Json(name = "allow_others_change_login_name")
    val allowOthersChangeLoginName: String? = null,

    @Json(name = "allow_others_change_password")
    val allowOthersChangePassword: String? = null,

    @Json(name = "allow_parent_access_to_smart_class")
    val allowParentAccessToSmartClass: String? = null,

    @Json(name = "allow_parents_change_phone_number")
    val allowParentsChangePhoneNumber: String? = null,

    @Json(name = "allow_reset_password_via_sms")
    val allowResetPasswordViaSms: String? = null,

    @Json(name = "allow_school_chat")
    val allowSchoolChat: String? = null,

    @Json(name = "allow_smart_mate_generation")
    val allowSmartMateGeneration: String? = null,

    @Json(name = "allow_students_change_email")
    val allowStudentsChangeEmail: String? = null,

    @Json(name = "allow_students_change_face")
    val allowStudentsChangeFace: String? = null,

    @Json(name = "allow_students_change_login_name")
    val allowStudentsChangeLoginName: String? = null,

    @Json(name = "allow_students_change_password")
    val allowStudentsChangePassword: String? = null,

    @Json(name = "allow_students_change_phone_number")
    val allowStudentsChangePhoneNumber: String? = null,

    @Json(name = "allow_students_change_profile_pic")
    val allowStudentsChangeProfilePic: String? = null,

    @Json(name = "allow_students_create_discussions")
    val allowStudentsCreateDiscussions: String? = null,

    @Json(name = "allow_students_create_topics_in_discussions")
    val allowStudentsCreateTopicsInDiscussions: String? = null,

    @Json(name = "allow_students_requests")
    val allowStudentsRequests: String? = null,

    @Json(name = "allow_teacher_to_link_preparation_standards")
    val allowTeacherToLinkPreparationStandards: Int? = null,

    @Json(name = "allow_teachers_change_login_name")
    val allowTeachersChangeLoginName: String? = null,

    @Json(name = "allow_teachers_change_password")
    val allowTeachersChangePassword: String? = null,

    @Json(name = "allow_tsupv_approve_preparation")
    val allowTsupvApprovePreparation: String? = null,

    @Json(name = "allow_tsupv_show_messages_between_teachers_and_students")
    val allowTsupvShowMessagesBetweenTeachersAndStudents: String? = null,

    @Json(name = "allow_users_open_publicprofile")
    val allowUsersOpenPublicprofile: String? = null,

    @Json(name = "allow_users_upload_eportfolios")
    val allowUsersUploadEportfolios: String? = null,

    @Json(name = "allow_users_upload_posts")
    val allowUsersUploadPosts: String? = null,

    @Json(name = "approved_attachments_in_library_only")
    val approvedAttachmentsInLibraryOnly: String? = null,

    @Json(name = "enabled")
    val enabled: String? = null,

    @Json(name = "graduates_club_allow")
    val graduatesClubAllow: String? = null,

    @Json(name = "hide_weekends_in_weekly_plan")
    val hideWeekendsInWeeklyPlan: String? = null,

    @Json(name = "implementation_phase")
    val implementationPhase: String? = null,

    @Json(name = "is_lite")
    val isLite: String? = null,

    @Json(name = "is_mawhiba_olympics_exam_school")
    val isMawhibaOlympicsExamSchool: String? = null,

    @Json(name = "is_mawhiba_scan_exam_school")
    val isMawhibaScanExamSchool: String? = null,

    @Json(name = "is_muqararat_school")
    val isMuqararatSchool: String? = null,

    @Json(name = "is_premium")
    val isPremium: String? = null,

    @Json(name = "lectures_absences_notification")
    val lecturesAbsencesNotification: String? = null,

    @Json(name = "lectures_absences_notification_excused")
    val lecturesAbsencesNotificationExcused: String? = null,

    @Json(name = "manager_approve_content_publish")
    val managerApproveContentPublish: String? = null,

    @Json(name = "max_number_of_replies_discussion_rooms")
    val maxNumberOfRepliesDiscussionRooms: String? = null,

    @Json(name = "normal_english_curriculum")
    val normalEnglishCurriculum: String? = null,

    @Json(name = "parents_registration_open")
    val parentsRegistrationOpen: String? = null,

    @Json(name = "publicprofiles_privecy_level_id")
    val publicprofilesPrivecyLevelId: String? = null,

    @Json(name = "registration_open")
    val registrationOpen: String? = null,

    @Json(name = "send_to_school_group_staff")
    val sendToSchoolGroupStaff: String? = null,

    @Json(name = "show_american_question_bank")
    val showAmericanQuestionBank: String? = null,

    @Json(name = "show_weekends_in_schedule")
    val showWeekendsInSchedule: String? = null,

    @Json(name = "students_section_messages")
    val studentsSectionMessages: String? = null,

    @Json(name = "summer_period")
    val summerPeriod: String? = null,

    @Json(name = "unit_plan_allow")
    val unitPlanAllow: String? = null,

    @Json(name = "type")
    val schoolType: String? = null,

    @Json(name = "is_assessment_blocked")
    val isAssessmentBlock: String? = null,

    @Json(name = "allow_admin_to_add_vcr")
    val allowAdminToAddVcr: Boolean? = null,

    @Json(name = "is_Kherkom")
    val isKherkom: Boolean? = null
)
