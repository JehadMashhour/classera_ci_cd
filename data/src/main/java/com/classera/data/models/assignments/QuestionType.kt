package com.classera.data.models.assignments

import com.classera.data.moshi.enums.Json

/**
 * Project: Classera
 * Created: Mar 10, 2020
 *
 * @author Mohamed Hamdan
 */
@Suppress("UNCHECKED_CAST")
enum class QuestionType(val className: Class<*>, val canShowStatus: Boolean) {

    @Json(name = "tfq")
    TRUE_FALSE(
        Class.forName("com.classera.assignments.solve.types.truefalsetype.TrueFalseFragment"),
        true
    ),

    @Json(name = "mcq")
    MULTIPLE_CHOICE(
        Class.forName("com.classera.assignments.solve.types.multiplechoicetype.MultipleChoiceFragment"),
        true
    ),

    @Json(name = "short")
    ESSAY(
        Class.forName("com.classera.assignments.solve.types.essaytype.EssayFragment"),
        false
    ),

    @Json(name = "matching")
    MATCHING(
        Class.forName("com.classera.assignments.solve.types.matchingtype.MatchingFragment"),
        true
    ),

    @Json(name = "hotspot")
    HOTSPOT(
        Class.forName("com.classera.assignments.solve.types.hotspottype.HotspotFragment"),
        true
    ),

    @Json(name = "fillblank")
    FILL_BLANK(
        Class.forName("com.classera.assignments.solve.types.fillblanktype.FillBlankFragment"),
        false
    ),

    @Json(name = "msa")
    MULTIPLE_SELECT(
        Class.forName("com.classera.assignments.solve.types.multipleselecttype.MultipleSelectFragment"),
        false
    ),

    @Json(name = "NA")
    UNKNOWN(
        Class.forName("com.classera.assignments.solve.types.truefalsetype.TrueFalseFragment"),
        true
    )
}
