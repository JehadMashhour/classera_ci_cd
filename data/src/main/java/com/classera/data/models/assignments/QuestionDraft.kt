package com.classera.data.models.assignments


import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class QuestionDraft(

    @Json(name = "answer")
    val answer: String? = null,

    @Json(name = "question_id")
    val questionId: Int? = null
) : Parcelable
