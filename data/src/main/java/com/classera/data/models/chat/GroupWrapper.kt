package com.classera.data.models.chat

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

/**
 * Created on 11/03/2020.
 * Classera
 *
 * @author Lana Manaseer
 */
@Parcelize
@JsonClass(generateAdapter = true)
class GroupWrapper(

    @Json(name = "success")
    val success: Boolean? = null,

    @Json(name = "thread_id")
    val threadId: String? = null,

    @Json(name = "creator_id")
    val creatorId: String? = null,

    @Json(name = "new")
    val new: Boolean? = null

) : Parcelable
