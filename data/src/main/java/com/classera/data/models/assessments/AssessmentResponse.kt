package com.classera.data.models.assessments

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AssessmentResponse(

    @Json(name = "Assessmentes")
    val assessments: List<Assessment>? = null
)
