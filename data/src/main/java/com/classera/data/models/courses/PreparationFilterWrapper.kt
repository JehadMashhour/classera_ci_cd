package com.classera.data.models.courses

import android.os.Parcelable
import com.classera.data.models.filter.Filterable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

/**
 * Project: Classera
 * Created: NOV 01, 2020
 *
 * @author Saeed Halawani
 */
@Parcelize
@JsonClass(generateAdapter = true)
data class PreparationFilterWrapper(

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "title")
    val preparationTitle: String? = null,

    @Transient
    override var selected: Boolean = false
) : Parcelable, Filterable {

    override val title: String?
        get() = preparationTitle

    override val filterId: String?
        get() = id
}
