package com.classera.data.models.schedule

import com.squareup.moshi.Json

data class Schedule(
    @Json(name = "timeslot_id")
    val timeSlotId: String? = "",

    @Json(name = "timeslot_name")
    val timeSlotName: String? = "",

    @Json(name = "timeslots")
    val timeSlots: List<ScheduleDay?>? = listOf()
)
