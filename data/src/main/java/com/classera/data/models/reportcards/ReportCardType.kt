package com.classera.data.models.reportcards

import com.classera.data.R
import com.classera.data.moshi.enums.Json

/**
 * Project: Classera
 * Created: Dec 31, 2019
 *
 * @author Mohamed Hamdan
 */
enum class ReportCardType(val icon: Int, val url: String? = null) {

    @Json(name = "0", alternatives = ["2"])
    PDF(R.drawable.ic_pdf, "ReportCards/student_download_pdf_from_mobile"),

    @Json(name = "1")
    DYNAMIC_REPORT(R.drawable.ic_website, "users/student_report_details"),

    UNKNOWN(R.drawable.ic_pdf)
}
