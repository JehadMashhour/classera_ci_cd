package com.classera.data.models.home.admin

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Project: classeraandroidv3
 * Created: Mar 06, 2020
 *
 * @author Odai Nazzal
 */
@JsonClass(generateAdapter = true)
data class DayAbsences(

    @Json(name = "day")
    val day: Int,

    @Json(name = "count")
    val count: Int
)
