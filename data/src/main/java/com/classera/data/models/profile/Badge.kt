package com.classera.data.models.profile

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

/**
 * Project: Classera
 * Created: Mar 08, 2020
 *
 * @author Mohamed Hamdan
 */
@Parcelize
@JsonClass(generateAdapter = true)
data class Badge(

    @Json(name = "id")
    var id: String? = null,

    @Json(name = "title")
    val title: String? = null,

    @Json(name = "url")
    val url: String? = null
) : Parcelable
