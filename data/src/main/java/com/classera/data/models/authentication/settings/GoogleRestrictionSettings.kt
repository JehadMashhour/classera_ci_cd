package com.classera.data.models.authentication.settings

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Project: Classera
 * Created: AUG 20, 2020
 *
 * @author Saeed Halawani
 */
@JsonClass(generateAdapter = true)
data class GoogleRestrictionSettings(

    @Json(name = "hide_google_login")
    val hideGoogleLogin: Boolean? = false,

    @Json(name = "hide_success_partner")
    val hideSuccessPartner: Boolean? = false
)
