package com.classera.data.models.digitallibrary

import android.view.View
import com.classera.data.R
import com.classera.data.moshi.enums.Json

/**
 * Project: Classera
 * Created: Dec 23, 2019
 *
 * @author Mohamed Hamdan
 */
enum class AttachmentTypes(val icon: Int) {

    @Json(name = "Vimeo")
    VIMEO(View.NO_ID),

    @Json(name = "YouTube")
    YOUTUBE(View.NO_ID),

    @Json(
        name = "video/3gpp",
        alternatives = [
            "video/avi",
            "video/flash",
            "video/flv",
            "video/mp4",
            "video/mpeg",
            "video/mpeg4",
            "video/mpg",
            "video/mspowerpoint",
            "video/msword",
            "video/quicktime",
            "video/webm",
            "video/x-flv",
            "video/x-m4v",
            "video/x-matroska",
            "video/x-ms-asf",
            "video/x-ms-wmv",
            "video/x-msvideo",
            "video/avc"
        ]
    )
    VIDEO(View.NO_ID),

    @Json(
        name = "ams"
    )
    STREAM(View.NO_ID),

    @Json(
        name = "application/Scorm",
        alternatives = ["Bgame", "swf"]
    )
    GAME(R.drawable.ic_game),

    @Json(
        name = "application/javascript",
        alternatives = [
            "application/HTML5",
            "application/json",
            "application/postscript",
            "application/vnd.sun.xml.base",
            "application/vnd.wordprocessing-openxml",
            "application/x-javascript",
            "application/xhtml+xml",
            "text/xml"
        ]
    )
    CODE(R.drawable.ic_code),

    @Json(
        name = "application/jpg",
        alternatives = [
            "application/png",
            "application/x-jpg",
            "image/CR2",
            "image/bmp",
            "image/gif",
            "image/heic",
            "image/jpeg",
            "image/jpg",
            "image/jps",
            "image/pjpeg",
            "image/png",
            "image/svg+xml",
            "image/tiff",
            "image/vnd.ms-dds",
            "image/vnd.ms-photo",
            "image/webp",
            "image/x-bmp",
            "image/x-emf",
            "image/x-icon",
            "image/x-ms-bmp",
            "image/x-png",
            "image/x-xwindowdump",
            "jpg"
        ]
    )
    IMAGE(View.NO_ID),

    @Json(
        name = "application/mspowerpoint",
        alternatives = [
            "application/powerpoint",
            "application/vnd.ms-powerpoint",
            "application/vnd.ms-powerpoint.12",
            "application/vnd.ms-powerpoint.presentation.12",
            "application/vnd.ms-powerpoint.presentation.macroEnabled.12",
            "application/vnd.ms-powerpoint.slideshow.macroEnabled.12",
            "application/vnd.openxmlformats-officedocument.presentationml.template",
            "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            "application/vnd.openxmlformats-officedocument.presentationml",
            "application/wps-office.pptx",
            "application/x-mspowerpoint",
            "ppt/pptx"
        ]
    )
    SHOW(R.drawable.ic_presentation),

    @Json(
        name = "application/MindMapleemm",
        alternatives = [
            "application/binary",
            "application/com.matchware.mindview",
            "application/download",
            "application/epub+zip",
            "application/force-download",
            "application/imindmap",
            "application/mindpreview",
            "application/octect-stream",
            "application/octet-stream",
            "application/octetstream",
            "application/rar",
            "application/vnd.android.package-archive",
            "application/vnd.geogebra.file",
            "application/vnd.xmind.workbook",
            "application/wlmoviemaker",
            "application/x-bittorrent",
            "application/x-chrome-extension",
            "application/x-compressed",
            "application/x-diskcopy",
            "application/x-download",
            "application/x-endnote-library",
            "application/x-gzip",
            "application/x-iwork-keynote-sffkey",
            "application/x-iwork-keynote-sffnumbers",
            "application/x-iwork-pages-sffpages",
            "application/x-ms-download",
            "application/x-msaccess",
            "application/x-msdownload",
            "application/x-rar-compressed",
            "application/x-rar",
            "application/x-vlc-plugin",
            "application/x-zip-compressed",
            "application/zip",
            "binary/octet-stream"
        ]
    )
    PROGRAM(R.drawable.ic_program),

    @Json(
        name = "application/CDFV2-unknown",
        alternatives = [
            "application/doc",
            "application/kswps",
            "application/msaccess",
            "application/msonenote",
            "application/msword",
            "application/rtf",
            "application/vnd.ms-excel",
            "application/vnd.ms-excel.sheet.macroEnabled.12",
            "application/vnd.ms-office",
            "vnd.ms-publisher",
            "application/vnd.ms-wpl",
            "application/vnd.ms-word.template.macroEnabled.12",
            "application/vnd.ms-word.document.macroEnabled.12",
            "application/vnd.ms-word.document.12",
            "application/vnd.ms-word",
            "application/vnd.ms-xpsdocument",
            "application/vnd.oasis.opendocument.spreadsheet",
            "application/vnd.oasis.opendocument.presentation-template",
            "application/vnd.oasis.opendocument.presentation",
            "application/vnd.oasis.opendocument.graphics",
            "application/vnd.oasis.opendocument.database",
            "application/vnd.oasis.opendocument.text",
            "application/vnd.oasis.opendocument.text-template",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.template",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "application/vnd.openxmlformats-officedocument.wordprocessingml",
            "application/vnd.openxmlformats-officedocument.word",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "application/vnd.xls",
            "application/wps-office.doc",
            "application/wps-office.docx",
            "application/x-msexcel",
            "application/x-smarttech-notebook",
            "text/css",
            "text/html",
            "text/plain",
            "text/richtext",
            "text/rtf",
            "text/x-ms-contact",
            "text/x-ms-odc",
            "text/x-vcard",
            "plain/text",
            "doc/doc",
            "doc/docx",
            "message/rfc822"
        ]
    )
    DOCUMENT(R.drawable.ic_document),

    @Json(
        name = "application/ogg",
        alternatives = [
            "audio/3ga",
            "audio/3gpp",
            "audio/aac",
            "audio/aac-adts",
            "audio/aiff",
            "audio/amr",
            "audio/basic",
            "audio/m4a",
            "audio/mp3",
            "audio/mp4",
            "audio/mpeg",
            "audio/ogg",
            "audio/scpls",
            "audio/vnd.dlna.adts",
            "audio/wav",
            "audio/x-m4a",
            "audio/x-ms-wax",
            "audio/x-ms-wma",
            "audio/x-pn-realaudio",
            "audio/x-wav"
        ]
    )
    AUDIO(R.drawable.ic_headset),

    @Json(
        name = "application/x-mswebsite",
        alternatives = [
            "application/x-webarchive",
            "weblink"
        ]
    )
    WEBSITE(R.drawable.ic_website),

    @Json(name = "application/x-shockwave-flash")
    FLASH(R.drawable.ic_flash),

    @Json(
        name = "application/pdf",
        alternatives = [
            "application/acrobat",
            "application/x-pdf"
        ]
    )
    PDF(R.drawable.ic_document),

    UNKNOWN(R.drawable.ic_material),

    @Json(
        name = "external_resource"
    )
    EXTERNAL_RESOURCES(View.NO_ID)
}
