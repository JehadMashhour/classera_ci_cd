package com.classera.data.models.callchildren

import com.squareup.moshi.Json


/**
 * Created by Rawan Al-Theeb on 8/17/2021.
 * Classera
 * r.altheeb@classera.com
 */

data class Request (

    @Json(name = "requestId")
    val requestId: String? = null
)
