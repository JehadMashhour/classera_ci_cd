package com.classera.data.models.switchsemester

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SemesterWrapper(

    @Json(name = "token")
    val token: String? = ""
)
