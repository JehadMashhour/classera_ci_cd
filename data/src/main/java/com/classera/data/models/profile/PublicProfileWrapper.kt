package com.classera.data.models.profile


import android.os.Parcelable
import com.classera.data.models.profile.Profile
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class PublicProfileWrapper(

    @Json(name = "about_me")
    val aboutMe: String? = null,

    @Json(name = "archived")
    val archived: String? = null,

    @Json(name = "date_of_birth")
    val dateOfBirth: String? = null,

    @Json(name = "email")
    val email: String? = null,

    @Json(name = "facebook_account")
    val facebookAccount: String? = null,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "instagram_account")
    val instagramAccount: String? = null,

    @Json(name = "is_student")
    val isStudent: Boolean? = false,

    @Json(name = "linkedin_account")
    val linkedinAccount: String? = null,

    @Json(name = "lives_in")
    val livesIn: String? = null,

    @Json(name = "name")
    val name: String? = null,

    @Json(name = "noor_leakage_type_id")
    val noorLeakageTypeId: String? = null,

    @Json(name = "noor_supervision_major_id")
    val noorSupervisionMajorId: String? = null,

    @Json(name = "place_of_birth")
    val placeOfBirth: String? = null,

    @Json(name = "profile")
    val profile: Profile? = null,

    @Json(name = "profile_picture")
    val profilePicture: String? = null,

    @Json(name = "role_id")
    val roleId: String? = null,

    @Json(name = "school_name")
    val schoolName: String? = null,

    @Json(name = "showing_born_in_public_profile")
    val showingBornInPublicProfile: String? = null,

    @Json(name = "showing_email_in_public_profile")
    val showingEmailInPublicProfile: String? = null,

    @Json(name = "showing_facebook_in_public_profile")
    val showingFacebookInPublicProfile: String? = null,

    @Json(name = "showing_instagram_in_public_profile")
    val showingInstagramInPublicProfile: String? = null,

    @Json(name = "showing_linkedin_in_public_profile")
    val showingLinkedinInPublicProfile: String? = null,

    @Json(name = "showing_twitter_in_public_profile")
    val showingTwitterInPublicProfile: String? = null,

    @Json(name = "social_status")
    val socialStatus: String? = null,

    @Json(name = "study_status_id")
    val studyStatusId: String? = null,

    @Json(name = "teacher_lectures")
    val teacherLectures: List<String?>? = null,

    @Json(name = "terms_and_conditions")
    val termsAndConditions: Boolean? = false,

    @Json(name = "twitter_account")
    val twitterAccount: String? = null,

    @Json(name = "user_id")
    val userId: String? = null
) : Parcelable
