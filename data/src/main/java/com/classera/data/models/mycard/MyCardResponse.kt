package com.classera.data.models.mycard

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Rawan Altheeb
 */
@JsonClass(generateAdapter = true)
data class MyCardResponse(

    @Json(name = "ClasseraCardsUsers")
    val classeraCardsUsers: ClasseraCardsUsers? = null
)
