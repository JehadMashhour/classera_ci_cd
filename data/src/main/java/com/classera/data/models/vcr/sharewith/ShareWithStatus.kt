package com.classera.data.models.vcr.sharewith

import com.classera.data.models.selection.Selectable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class ShareWithStatus(
    val shareWithStatusTypes: ShareWithStatusTypes? = null,

    override val id: String?,

    override val title: String?,

    override var selected: Boolean = false

) : Selectable
