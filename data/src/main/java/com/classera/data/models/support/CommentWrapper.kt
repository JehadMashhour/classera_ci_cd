package com.classera.data.models.support

import android.os.Parcelable
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.squareup.moshi.Json
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CommentWrapper(

    @Json(name = "created")
    val created: String? = null,

    @Json(name = "full_name")
    val fullName: String? = null,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "post_content")
    val postContent: String? = null,

    @Json(name = "post_user_photo")
    val postUserPhoto: String? = null,

    @Json(name = "comment_attachment")
    val commentAttachment: List<String>? = null,

    @Transient
    val isFailed: Boolean = false,

    @Transient
    val isLoading: Boolean = false

) : BaseObservable(), Parcelable {

    @get:Bindable
    @IgnoredOnParcel
    @Transient
    var progress: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.progress)
        }

    @get:Bindable
    @IgnoredOnParcel
    @Transient
    var state: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.state)
        }
}
