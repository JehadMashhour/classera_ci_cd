package com.classera.data.models.profile

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SkillResponse(

    @Json(name = "UsersSkill")
    val skill: Skill? = null
)
