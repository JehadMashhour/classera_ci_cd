package com.classera.data.models.notification

import com.google.gson.annotations.SerializedName

data class NotificationData(

    @SerializedName("message")
    var message: String? = null,

    @SerializedName("title")
    var title: String? = null
)

