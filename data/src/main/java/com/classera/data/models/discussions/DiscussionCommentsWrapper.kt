package com.classera.data.models.discussions

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Project: Classera
 * Created: Jan 06, 2020
 *
 * @author Mohamed Hamdan
 */
@JsonClass(generateAdapter = true)
data class DiscussionCommentsWrapper(

    @Json(name = "comments")
    val comments: List<DiscussionComment>
)
