package com.classera.data.models.notification

import com.google.gson.annotations.SerializedName

data class DiscussionCommentNotificationBody(

    @SerializedName("room_id")
    val roomId: String? = null,

    @SerializedName("postId")
    val postId: String? = null,

    @SerializedName("post_comment_id")
    val commentId: String? = null,

    @SerializedName("room_title")
    val roomTitle: String? = null,

    @SerializedName("post_title")
    val postTitle: String? = null,

    @SerializedName("comment_title")
    val commentTitle: String? = null
)
