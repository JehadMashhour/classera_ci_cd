package com.classera.data.models.home

/**
 * Project: Classera
 * Created: Dec 21, 2019
 *
 * @author Mohamed Hamdan
 */
data class DashboardShortcut(

    val title: Int,

    val icon: Int,

    val color: Int,

    val progress: Int,

    val count: Int,

    val order: Int?,

    val key: String?
)
