package com.classera.data.models.discussions

import android.os.Parcelable
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.classera.data.moshi.timeago.TimeAgo
import com.classera.data.moshi.toint.ToInt
import com.classera.data.moshi.trim.Trim
import com.squareup.moshi.Json
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

/**
 * Project: Classera
 * Created: Jan 05, 2020
 *
 * @author Mohamed Hamdan
 */
@Parcelize
data class DiscussionPost(

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "full_name")
    val fullName: String? = null,

    @Json(name = "creator_id")
    val creatorId: String? = null,

    @Trim
    @Json(name = "post_content")
    val postContent: String? = null,

    @Json(name = "post_user_photo")
    val postUserPhoto: String? = null,

    @ToInt
    @Json(name = "comments_count")
    val commentsCount: Int = 0,

    @TimeAgo
    @Json(name = "created")
    val created: String? = null,

    @Json(name = "approved")
    val approved: Boolean = false

): BaseObservable(), Parcelable {

    @get:Bindable
    @IgnoredOnParcel
    @Transient

    var deleting: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.deleting)
        }
}
