package com.classera.data.models.schedule.local

data class LocalScheduleDay(val name: String?, val data: List<LocalSchedule>?)
