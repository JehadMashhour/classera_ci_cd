package com.classera.data.models.weeklyplan

import com.classera.data.toDate
import com.classera.data.toString
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Created by Odai Nazzal on 12/27/2019.
 * Classera
 * o.nazzal@classera.com
 */

@JsonClass(generateAdapter = true)
data class WeeklyPlanWrapper(

    @Json(name = "week_preprations")
    val plans: List<WeeklyPlan>? = null
) {

    fun getMonthName(): String? {
        val firstMonthDate = plans?.first()?.dayDate?.toDate("yyyy-MM-dd")
        val secondMonthDate = plans?.last()?.dayDate?.toDate("yyyy-MM-dd")

        val firstMonthName = firstMonthDate?.toString("dd MMM yyyy")
        val secondMonthName = secondMonthDate?.toString("dd MMM yyyy")

        return setOf(firstMonthName, secondMonthName).joinToString(separator = " - ") { it ?: "" }
    }
}
