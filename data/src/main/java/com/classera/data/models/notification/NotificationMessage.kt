package com.classera.data.models.notification

import android.os.Build
import android.os.Parcel
import android.os.Parcelable
import android.text.Html
import androidx.core.text.HtmlCompat
import com.google.gson.annotations.SerializedName
@Suppress("EmptySecondaryConstructor")
data class NotificationMessage(

    @SerializedName("source")
    var source: String? = null,

    @SerializedName("event")
    var event: String? = null,

    @SerializedName("sender")
    var sender: String? = null,

    @SerializedName("title")
    var title: String? = null,

    @SerializedName("body")
    var body: Any? = null,

    @SerializedName("message")
    var message: String? = null,

    @SerializedName("uuid")
    var uuid: String? = null,

    @SerializedName("created")
    var created: String? = null,

    @SerializedName("seen")
    var seen: Int? = null

) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        TODO("body"),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int
    ) {
    }

    fun getCleanedMessage(): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(message ?: "", HtmlCompat.FROM_HTML_MODE_LEGACY).toString().trim()
        } else {
            Html.fromHtml(message ?: "").toString().trim()
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(source)
        parcel.writeString(event)
        parcel.writeString(sender)
        parcel.writeString(title)
        parcel.writeString(message)
        parcel.writeString(uuid)
        parcel.writeString(created)
        parcel.writeValue(seen)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NotificationMessage> {
        override fun createFromParcel(parcel: Parcel): NotificationMessage {
            return NotificationMessage(parcel)
        }

        override fun newArray(size: Int): Array<NotificationMessage?> {
            return arrayOfNulls(size)
        }
    }
}

