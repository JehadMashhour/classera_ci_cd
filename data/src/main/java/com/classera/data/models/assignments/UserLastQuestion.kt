package com.classera.data.models.assignments

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


/**
 * Created by Rawan Al-Theeb on 7/5/2020.
 * Classera
 * r.altheeb@classera.com
 */
@Entity(tableName = "user_last_question")
@JsonClass(generateAdapter = true)
data class UserLastQuestion(

    @Json(name = "question_id")
    var questionId: String = "",

    @Json(name = "user_id")
    var userId: String = "",

    @PrimaryKey
    @Json(name = "exam_id")
    var examId: String = "",

    @Json(name = "question_number")
    var questionNumber: String = ""
)
