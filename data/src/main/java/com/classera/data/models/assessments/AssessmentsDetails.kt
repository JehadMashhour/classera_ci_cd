package com.classera.data.models.assessments


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AssessmentsDetails(

    @Json(name = "assessment")
    val assessments: List<Assessment?>? = null,

    @Json(name = "hide_percentage")
    val hidePercentage: Boolean? = null,

    @Json(name = "mark")
    val mark: String? = null,

    @Json(name = "percentage")
    val percentage: Double? = null,

    @Json(name = "student_mark")
    val studentMark: Double? = null
) {

    fun isPercentageHidden(): Boolean {
        return hidePercentage ?: false
    }

    fun isStudentMarkGreaterThanFifty(): Boolean {
        return studentMark ?: 0.0 >= PASSING_MARK
    }

    fun isPercentageGreaterThanFifty(): Boolean {
        return percentage ?: 0.0 >= PASSING_MARK
    }

    private companion object {

        private const val PASSING_MARK = 50.0
    }
}
