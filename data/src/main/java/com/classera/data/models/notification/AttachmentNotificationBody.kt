package com.classera.data.models.notification

import com.google.gson.annotations.SerializedName

data class AttachmentNotificationBody(

    @SerializedName("attachment_id")
    val attachmentId: String? = null,

    @SerializedName("title")
    val title: String? = null
)
