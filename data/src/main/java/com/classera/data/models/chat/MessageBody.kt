package com.classera.data.models.chat

import android.os.Parcelable
import com.classera.data.moshi.duration.Duration
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

/**
 * Project: Classera
 * Created: Mar 10, 2020
 *
 * @author Mohamed Hamdan
 */
@JsonClass(generateAdapter = true)
@Parcelize
data class MessageBody(

    @Json(name = "url")
    val url: String? = null,

    @Json(name = "thread_id")
    val threadId: String? = null,

    @Json(name = "message_id")
    val messageId: String? = null,

    @Json(name = "from_id")
    val fromId: String? = null,

    @Json(name = "type")
    val type: Int? = null,

    @Json(name = "source")
    val source: String? = null,

    @Json(name = "sent_date")
    val sentDate: String? = null,

    @Transient
    val text: String? = null,

    @Duration
    @Json(name = "duration")
    var duration: String? = null
) : Parcelable {

    @Suppress("ReturnCount")
    fun getMediaType(): MessageMediaType {
        val extension = getFileExtension()
        val values = MessageMediaType.values()
        for (type in values) {
            if (type.pattern?.matches(extension.toString()) == true) {
                return type
            }
        }
        if (extension != null) {
            return MessageMediaType.OTHERS
        }
        return MessageMediaType.UNKNOWN
    }

    fun getFileExtension(): String? {
        return url?.substring(url.lastIndexOf('.') + 1)
    }

    fun getFileName(): String? {
        return url?.substring(url.lastIndexOf('/') + 1)
    }
}
