package com.classera.data.models.chat.moshi

import com.squareup.moshi.JsonQualifier

/**
 * Created by Odai Nazzal on 1/11/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class ViewType
