package com.classera.data.models.mycard


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Rawan Altheeb
 */

@JsonClass(generateAdapter = true)
data class ClasseraCardsUsers(

    @Json(name = "card_number")
    val cardNumber: String? = null,

    @Json(name = "card_url")
    val cardUrl: String? = null,

    @Json(name = "class")
    val classX: String? = null,

    @Json(name = "created")
    val created: String? = null,

    @Json(name = "id")
    val id: String? = null,

    @Json(name = "linked_by")
    val linkedBy: String? = null,

    @Json(name = "modified")
    val modified: String? = null,

    @Json(name = "source")
    val source: String? = null,

    @Json(name = "status")
    val status: String? = null,

    @Json(name = "user_id")
    val userId: String? = null
)
