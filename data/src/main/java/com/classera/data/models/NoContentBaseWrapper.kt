package com.classera.data.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
open class NoContentBaseWrapper(

    @Json(name = "success")
    val success: Boolean? = null,

    @Json(name = "message")
    val message: String? = null,

    @Json(name = "code")
    val code: Int? = null,

    @Json(name = "url")
    val url: String? = null
)
