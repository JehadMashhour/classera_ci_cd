package com.classera.data.models.schedule

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Created by Odai Nazzal on 1/12/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
@JsonClass(generateAdapter = true)
data class ScheduleSlot(

    @Json(name = "timeslot_id")
    val timeSlotId: String? = null,

    @Json(name = "course_title")
    val courseTitle: String? = null,

    @Json(name = "level_title")
    val levelTitle: String? = null,

    @Json(name = "section_title")
    val sectionTitle: String? = null,

    @Json(name = "From")
    val from: String? = null,

    @Json(name = "To")
    val to: String? = null,

    @Transient
    var color: Int? = null
)
