package com.classera.data.database.converters

import androidx.room.TypeConverter
import com.classera.data.models.level.LevelTitle
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types

/**
 * Project: Classera
 * Created: Dec 17, 2019
 *
 * @author Mohamed Hamdan
 */
class LevelTitleListConverter {

    private val levelTitleAdapter = Moshi.Builder()
        .build()
        .adapter<List<LevelTitle>>(Types.newParameterizedType(List::class.java, LevelTitle::class.java))

    @TypeConverter
    fun fromCountryLangList(value: List<LevelTitle>): String {
        return levelTitleAdapter.toJson(value)
    }

    @TypeConverter
    fun toCountryLangList(value: String): List<LevelTitle>? {
        return levelTitleAdapter.fromJson(value)
    }
}
