package com.classera.data.glide.baseurl

import com.bumptech.glide.load.Options
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.ModelLoader
import com.bumptech.glide.load.model.stream.BaseGlideUrlLoader
import com.classera.data.BuildConfig
import java.io.InputStream

/**
 * Project: Classera
 * Created: Dec 26, 2019
 *
 * @author Mohamed Hamdan
 */
class BaseUrlLoader(urlLoader: ModelLoader<GlideUrl, InputStream>) : BaseGlideUrlLoader<String>(urlLoader) {

    override fun handles(url: String): Boolean {
        return !url.startsWith("http")
    }

    public override fun getUrl(url: String?, width: Int, height: Int, options: Options): String? {
        return BuildConfig.flavorData.apiBaseUrl + url
    }
}
