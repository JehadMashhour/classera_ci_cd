package com.classera.data.glide.youtube

import androidx.annotation.NonNull
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.ModelLoader
import com.bumptech.glide.load.model.ModelLoaderFactory
import com.bumptech.glide.load.model.MultiModelLoaderFactory
import java.io.InputStream

/**
 * Project: Classera
 * Created: Dec 26, 2019
 *
 * @author Mohamed Hamdan
 */
class YoutubeUrlLoaderFactory : ModelLoaderFactory<String, InputStream> {

    @NonNull
    override fun build(multiFactory: MultiModelLoaderFactory): ModelLoader<String, InputStream> {
        val urlLoader = multiFactory.build(GlideUrl::class.java, InputStream::class.java)
        return YoutubeUrlLoader(urlLoader)
    }

    override fun teardown() {
        // No Impl
    }
}
