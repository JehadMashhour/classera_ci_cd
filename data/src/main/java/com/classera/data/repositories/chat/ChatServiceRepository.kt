package com.classera.data.repositories.chat

import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.chat.GroupWrapper
import com.classera.data.models.chat.MessagesWrapper
import com.classera.data.models.chat.Status
import com.classera.data.models.chat.ThreadUser
import com.classera.data.models.chat.UserStatusWrapper
import okhttp3.MultipartBody

/**
 * Created by Odai Nazzal on 12/19/2019, Modified by Lana Manaseer
 * Classera
 *
 * o.nazzal@classera.com
 */
interface ChatServiceRepository {

    suspend fun getUserStatus(): UserStatusWrapper<Status>

    suspend fun updateUserStatus(status: String): UserStatusWrapper<Status>

    suspend fun getListMessages(threadId: String?, pageState: String): MessagesWrapper

    suspend fun uploadAttachment(parts: List<MultipartBody.Part>)

    suspend fun createGroup(name: String?, participantList: MutableList<ThreadUser>?): GroupWrapper

    suspend fun deleteGroup(threadID: String?): NoContentBaseWrapper

    suspend fun updateGroupName(name: String?, threadID: String?): NoContentBaseWrapper

    suspend fun deleteParticipants(threadID: String?, participantList: List<ThreadUser>?): NoContentBaseWrapper

    suspend fun addParticipants(threadID: String?, participantList: Array<ThreadUser>?): NoContentBaseWrapper
}
