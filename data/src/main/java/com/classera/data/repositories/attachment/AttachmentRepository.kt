package com.classera.data.repositories.attachment

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.attachment.AttachmentCommentsWrapper
import com.classera.data.models.attachment.AttachmentData
import com.classera.data.models.digitallibrary.Attachment
import com.classera.data.models.rating.RatingResult
import okhttp3.MultipartBody

/**
 * Created by Odai Nazzal on 12/19/2019.
 * Classera
 * o.nazzal@classera.com
 */
interface AttachmentRepository {

    suspend fun getAttachmentDetails(attachmentId: String?): BaseWrapper<Attachment>

    suspend fun getAttachmentComments(attachmentId: String?, page: Int): BaseWrapper<AttachmentCommentsWrapper>

    suspend fun addLike(attachmentId: String?): NoContentBaseWrapper

    suspend fun addUnderstand(attachmentId: String?): NoContentBaseWrapper

    suspend fun addRate(attachmentId: String?, value: String, attachmentAuthorId: String?): BaseWrapper<RatingResult>

    suspend fun addAttachmentComment(attachmentId: String?, title: String?, text: String?): NoContentBaseWrapper

    suspend fun getAttachmentData(courseId: String?, type: String?): BaseWrapper<AttachmentData>

    suspend fun uploadAttachment(parts: List<MultipartBody.Part>): NoContentBaseWrapper
}
