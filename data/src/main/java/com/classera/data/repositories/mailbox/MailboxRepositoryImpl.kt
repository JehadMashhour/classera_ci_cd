package com.classera.data.repositories.mailbox

import com.classera.data.daos.mailbox.LocalMailboxDao
import com.classera.data.daos.mailbox.RemoteMailboxDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.mailbox.DraftWrapper
import com.classera.data.models.mailbox.InboxDetailsResponse
import com.classera.data.models.mailbox.InboxResponse
import com.classera.data.models.mailbox.InboxSearchResponse
import com.classera.data.models.mailbox.MessageRoleResponse
import com.classera.data.models.mailbox.OutboxDetailsResponse
import com.classera.data.models.mailbox.OutboxResponse
import com.classera.data.models.mailbox.OutboxSearchResponse
import com.classera.data.models.mailbox.RecipientByRole
import com.classera.data.models.mailbox.Trash
import com.classera.data.models.mailbox.School
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody

/**
 * Project: Classera
 * Created: Jan 6, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
class MailboxRepositoryImpl(
    private val remoteMailboxDao: RemoteMailboxDao,
    private val localMailboxDao: LocalMailboxDao
) : MailboxRepository {

    override suspend fun returnToBox(msgId: String): NoContentBaseWrapper {
        val fields = mapOf(
            "message_id" to msgId
        )

        return remoteMailboxDao.returnToBox(fields)
    }

    override suspend fun deletePermanently(msgId: String): NoContentBaseWrapper {
        val fields = mapOf(
            "message_id" to msgId
        )

        return remoteMailboxDao.deletePermanently(fields)
    }

    override suspend fun getTrash(pageNumber: Int, searchText: String): BaseWrapper<List<Trash>> {
        val fields = mapOf(
            "p" to pageNumber,
            "text" to searchText
        )

        return remoteMailboxDao.getTrash(fields)
    }

    override suspend fun getMessageRoles(schoolId: String?): BaseWrapper<List<MessageRoleResponse>> {
        return if (schoolId != null) {
            remoteMailboxDao.getMessageRoles(mapOf("school_id" to schoolId))

        } else {
            remoteMailboxDao.getMessageRoles(mapOf())
        }
    }

    override fun getAllRecipients(): Flow<List<RecipientByRole>?> {
        return localMailboxDao.getAllRecipients()
    }

    override suspend fun addRecipient(recipientUser: RecipientByRole) {
        localMailboxDao.insert(recipientUser)
    }

    override suspend fun deleteRecipient(recipientUser: RecipientByRole) {
        localMailboxDao.delete(recipientUser)
    }

    override suspend fun deleteAllRecipient() {
        localMailboxDao.deleteAllData()
    }

    override suspend fun replyMessage(
        msgId: String,
        msgTitle: String,
        recipientId: String,
        msgBody: String
    ): NoContentBaseWrapper {
        val fields = mapOf(
            "message_id" to msgId,
            "message_title" to msgTitle,
            "recipient_id" to recipientId,
            "message_body" to msgBody
        )

        return remoteMailboxDao.replyMessage(fields)
    }

    override suspend fun sendMessage(parts: List<MultipartBody.Part>): NoContentBaseWrapper {
        return remoteMailboxDao.sendMessage(parts)
    }

    override suspend fun getInboxDetails(msgId: String): BaseWrapper<InboxDetailsResponse> {
        val fields = mapOf(
            "message_id" to msgId
        )

        return remoteMailboxDao.getInboxDetails(fields)
    }

    override suspend fun getOutboxDetails(msgId: String): BaseWrapper<OutboxDetailsResponse> {
        val fields = mapOf(
            "message_id" to msgId
        )

        return remoteMailboxDao.getOutboxDetails(fields)
    }

    override suspend fun searchInbox(
        searchText: String,
        pageNumber: Int
    ): BaseWrapper<InboxSearchResponse> {
        val fields = mapOf(
            "p" to pageNumber,
            "text" to searchText
        )
        return remoteMailboxDao.searchInbox(fields)
    }

    override suspend fun deleteMessage(msgId: String): NoContentBaseWrapper {
        val fields = mapOf(
            "message_id" to msgId
        )
        return remoteMailboxDao.deleteMessage(fields)
    }

    override suspend fun getInbox(pageNumber: Int): BaseWrapper<InboxResponse> {
        val fields = mapOf(
            "p" to pageNumber
        )

        return remoteMailboxDao.getInbox(fields)
    }

    override suspend fun getOutbox(
        searchText: String,
        pageNumber: Int
    ): BaseWrapper<OutboxResponse> {
        val fields = mapOf(
            "p" to pageNumber,
            "text" to searchText
        )

        return remoteMailboxDao.getOutbox(fields)
    }

    override suspend fun getUserByRole(
        roleId: String?,
        schoolId: String?
    ): BaseWrapper<List<RecipientByRole>> {
        val fields = mapOf(
            "to_role_ids" to roleId,
            "school_id" to schoolId
        )

        return remoteMailboxDao.getUserByRole(fields)
    }

    override suspend fun getDraftList(
        pageNumber: Int,
        text: CharSequence?
    ): BaseWrapper<List<DraftWrapper>> {
        val fields = mutableMapOf<String, Any?>("p" to pageNumber)
        return if (text?.isNotEmpty() == true) {
            fields["text"] = text
            remoteMailboxDao.getDraftMessages(fields)
        } else {
            remoteMailboxDao.getDraftMessages(fields)
        }
    }

    override suspend fun getSchoolList(): BaseWrapper<List<School>> {
        return remoteMailboxDao.getSchoolList()
    }

    override suspend fun deleteAllRecipientInSchool(schoolId: String?) {
        localMailboxDao.deleteAllRecipientInSchool(schoolId)
    }

}

const val TYPE_INBOX = "INBOX"
const val TYPE_OUTBOX = "OUTBOX"
