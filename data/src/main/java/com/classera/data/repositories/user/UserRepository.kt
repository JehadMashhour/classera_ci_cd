package com.classera.data.repositories.user

import com.classera.data.models.BaseWrapper
import com.classera.data.models.calendar.teacher.eventusers.UserSelectionRole
import com.classera.data.models.user.SchoolSettings
import com.classera.data.models.user.User
import kotlinx.coroutines.flow.Flow
import retrofit2.http.Url

/**
 * Created by Odai Nazzal on 12/16/2019.
 * Classera
 * o.nazzal@classera.com
 */
interface UserRepository {

    suspend fun getUserInfo(): BaseWrapper<User>

    suspend fun getLocalUser(): Flow<List<User>?>

    suspend fun deleteUserFromLocalDatabase()

    suspend fun getUserRoles(@Url url: String?): BaseWrapper<List<UserSelectionRole>>

    suspend fun getSchoolSettings(): BaseWrapper<SchoolSettings>
}
