package com.classera.data.repositories.mycard

import com.classera.data.daos.mycard.RemoteMyCardDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.mycard.MyCardResponse


/**
 * Project: Classera
 * Created: Dec 18, 2019
 *
 * @author Rawan Altheeb
 */
class MyCardRepositoryImpl(private val remoteMyCardDao: RemoteMyCardDao) : MyCradRepository {

    override suspend fun getUserCard(): BaseWrapper<MyCardResponse> {
        return remoteMyCardDao.getUserCard()
    }
}
