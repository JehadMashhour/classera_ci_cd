package com.classera.data.repositories.homelocation

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.homelocation.ThreeWordsWraper
import com.classera.data.models.homelocation.w3w.W3WResponse


/**
 * Created by Rawan Al-Theeb on 1/7/2020.
 * Classera
 * r.altheeb@classera.com
 */
interface HomeLocationRepository {

    suspend fun getW3W(): BaseWrapper<ThreeWordsWraper>

    suspend fun saveW3W(w3w: String?): NoContentBaseWrapper

    suspend fun convertToCoordinates(words: String): W3WResponse

    suspend fun convertTo3wa(coordinates: String): W3WResponse
}
