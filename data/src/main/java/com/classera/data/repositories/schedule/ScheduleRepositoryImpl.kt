package com.classera.data.repositories.schedule

import com.classera.data.daos.schedule.RemoteScheduleDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.schedule.Schedule

/**
 * Created by Odai Nazzal on 1/12/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
class ScheduleRepositoryImpl(private val remoteScheduleDao: RemoteScheduleDao) : ScheduleRepository {

    override suspend fun getStudentSchedule(): BaseWrapper<List<Schedule>> {
        return remoteScheduleDao.getStudentSchedule()
    }
}

