package com.classera.data.repositories.authentication

import com.classera.data.BuildConfig
import com.classera.data.daos.authentication.RemoteAuthenticationDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.authentication.forgetusername.ForgetUserNameResponse
import com.classera.data.models.authentication.forgotpassword.ForgetPasswordResponse
import com.classera.data.models.authentication.login.LoginResponse
import com.classera.data.models.authentication.settings.GoogleRestrictionSettings
import com.classera.data.models.authentication.verificationcode.VerificationCodeResponse
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.settings.SettingsRepository
import com.classera.data.repositories.user.UserRepository

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
@Suppress("ReturnCount", "MagicNumber","NestedBlockDepth")
class AuthenticationRepositoryImpl(
    private val remoteAuthenticationDao: RemoteAuthenticationDao,
    private val userRepository: UserRepository,
    private val settingsRepository: SettingsRepository,
    private val prefs: Prefs
) : AuthenticationRepository {

    override suspend fun loginBy(username: String?, password: String?): BaseWrapper<LoginResponse> {
        val fields = mapOf(
            "username" to username,
            "password" to password,
            "app_type" to BuildConfig.flavorData.appType
        )

        var loginResponse = remoteAuthenticationDao.login(fields = fields)
        if (loginResponse.data != null && loginResponse.success == true) {
            if (loginResponse.data?.userData?.user?.isNotAllowedToLogin() != true) {

                if (loginResponse.success == true) {
                    prefs.isLoggedIn = true
                    prefs.userId = loginResponse.data?.userData?.user?.id
                    prefs.userRole = loginResponse.data?.userData?.user?.roleId
                    prefs.authenticationToken = loginResponse.data?.authToken
                    prefs.schoolId = loginResponse.data?.userData?.user?.schoolId
                    val enableNotification = settingsRepository.notificationRegistration()
                    if (loginResponse.success == true) {
                        loginResponse = loginResponse.copy(
                            success = enableNotification.success,
                            message = enableNotification.message
                        )
                    } else {
                        prefs.isLoggedIn = false
                    }
                } else {
                    return loginResponse
                }
            }
        } else {
            return loginResponse
        }
        return loginResponse
    }

    override suspend fun logout(fields: Map<String, String?>): NoContentBaseWrapper {
        return remoteAuthenticationDao.logout(fields)
    }

    override suspend fun forgetPassword(username: String?): BaseWrapper<ForgetPasswordResponse> {
        val fields = mapOf("username" to username)
        return remoteAuthenticationDao.forgetPassword(fields)
    }

    override suspend fun checkVerificationCode(
        code: String?,
        username: String?
    ): BaseWrapper<VerificationCodeResponse> {
        val fields = mapOf(
            "code" to code,
            "username" to username
        )
        return remoteAuthenticationDao.checkVerificationCode(fields)
    }

    override suspend fun forgetUsername(number: String?): BaseWrapper<ForgetUserNameResponse> {
        val fields = mapOf("number" to number)
        return remoteAuthenticationDao.forgetUsername(fields)
    }

    override suspend fun googleLogin(android: String?): BaseWrapper<LoginResponse> {
        val fields = mapOf(
            "ANDROID" to android
        )

        var loginResponse = remoteAuthenticationDao.login(fields = fields)
        if (loginResponse.data != null && loginResponse.success == true) {
            if (loginResponse.data?.userData?.user?.isNotAllowedToLogin() != true) {

                if (loginResponse.success == true) {
                    prefs.isLoggedIn = true
                    prefs.userId = loginResponse.data?.userData?.user?.id
                    prefs.userRole = loginResponse.data?.userData?.user?.roleId
                    prefs.authenticationToken = loginResponse.data?.authToken
                    prefs.schoolId = loginResponse.data?.userData?.user?.schoolId
                    val enableNotification = settingsRepository.notificationRegistration()
                    if (loginResponse.success == true) {
                        loginResponse = loginResponse.copy(
                            success = enableNotification.success,
                            message = enableNotification.message
                        )
                    } else {
                        prefs.isLoggedIn = false
                    }
                } else {
                    return loginResponse
                }
            }
        } else {
            return loginResponse
        }
        return loginResponse
    }

    override suspend fun googleLoginSettings(): BaseWrapper<GoogleRestrictionSettings> {
        return remoteAuthenticationDao.getGoogleLoginSettings()
    }
}
