package com.classera.data.repositories.home

import com.classera.data.models.socket.OnlineNow
import retrofit2.http.FieldMap

/**
 * Created by Saeed Halawani on 17/1/2019.
 * Classera
 * s.halawani@classera.com
 */
interface OnlineNowRepository {

    suspend fun getOnlineNow(@FieldMap fields: Map<String, String?>): OnlineNow
}
