package com.classera.data.repositories.chat

import com.classera.data.BuildConfig
import com.classera.data.daos.chat.RemoteChatServiceDao
import com.classera.data.getFormalUserName
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.chat.GroupWrapper
import com.classera.data.models.chat.Message
import com.classera.data.models.chat.MessageBody
import com.classera.data.models.chat.MessageTypeEnum
import com.classera.data.models.chat.MessagesWrapper
import com.classera.data.models.chat.Status
import com.classera.data.models.chat.ThreadUser
import com.classera.data.models.chat.UserStatusWrapper
import com.classera.data.prefs.Prefs
import okhttp3.MultipartBody

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan, Modified by Lana Manaseer
 */
class ChatServiceRepositoryImpl(
    private val remoteChatServiceDao: RemoteChatServiceDao,
    private val chatRepository: ChatRepository,
    private val prefs: Prefs
) : ChatServiceRepository {

    override suspend fun getUserStatus(): UserStatusWrapper<Status> {
        val fields = mutableMapOf("source" to BuildConfig.flavorData.socketSource)
        val userStatusResponse = remoteChatServiceDao.getUserStatus(prefs.userId, fields)
        if (userStatusResponse.status == true) {
            prefs.userStatus = userStatusResponse.result?.status?.name
        }
        return userStatusResponse
    }

    override suspend fun updateUserStatus(status: String): UserStatusWrapper<Status> {
        val fields = mutableMapOf(
            "user_id" to prefs.userId,
            "status" to status,
            "source" to BuildConfig.flavorData.socketSource
        )
        return remoteChatServiceDao.updateUserStatus(fields)
    }

    override suspend fun getListMessages(
        threadId: String?,
        pageState: String
    ): MessagesWrapper {
        val fields = mutableMapOf("source" to BuildConfig.flavorData.socketSource)
        val messages = remoteChatServiceDao.getListMessages(threadId, pageState, fields)
        val users = chatRepository.getListGroupParticipants(threadId).data

        handleUserProfilePictures(messages.result, users)
        handleAddedUsers(messages.result, users)
        handleRemovedUsers(messages.result)

        return messages
    }

    private fun handleUserProfilePictures(messages: List<Message>?, users: List<ThreadUser>?) {
        messages?.forEach { message ->
            val user = users?.firstOrNull { user ->
                user.userId == message.fromId
            }

            message.senderPicture = user?.photoFilename
            message.senderName = user?.fullName.getFormalUserName()
        }
    }

    private fun handleAddedUsers(result: List<Message>?, users: List<ThreadUser>?) {
        result?.forEach { message ->
            users?.firstOrNull { it.userId == message.fromId && message.type == MessageTypeEnum.ADDED }
                ?.fullName?.let { fullName ->
                message.body = MessageBody(text = fullName)
            }
        }
    }

    private suspend fun handleRemovedUsers(result: List<Message>?) {
        val userIds = result?.filter { it.type == MessageTypeEnum.REMOVED }?.map { it.fromId }?.distinct()
        val users = chatRepository.getUsersBasicDetails(userIds).data

        result?.forEach { message ->
            users?.firstOrNull { it.userId == message.fromId && message.type == MessageTypeEnum.REMOVED }
                ?.fullName?.let { fullName ->
                message.body = MessageBody(text = fullName)
            }
        }
    }

    override suspend fun uploadAttachment(parts: List<MultipartBody.Part>) {
        remoteChatServiceDao.uploadAttachment(parts)
    }

    override suspend fun createGroup(
        name: String?,
        participantList: MutableList<ThreadUser>?
    ): GroupWrapper {
        val fields = mutableMapOf(
            "creator_id" to prefs.userId,
            "name" to name,
            "source" to BuildConfig.flavorData.socketSource
        )
        participantList?.forEachIndexed { index, participant ->
            fields["participants[$index]"] = participant.userId
        }
        return remoteChatServiceDao.createGroup(fields)
    }

    override suspend fun deleteParticipants(
        threadID: String?,
        participantList: List<ThreadUser>?
    ): NoContentBaseWrapper {
        val fields = mutableMapOf(
            "creator_id" to prefs.userId,
            "thread_id" to threadID,
            "source" to BuildConfig.flavorData.socketSource
        )
        participantList?.forEachIndexed { index, participant ->
            fields["participants[$index]"] = participant.userId
        }
        return remoteChatServiceDao.deleteParticipants(fields)
    }

    override suspend fun deleteGroup(threadID: String?): NoContentBaseWrapper {
        val fields = mutableMapOf(
            "creator_id" to prefs.userId,
            "thread_id" to threadID,
            "source" to BuildConfig.flavorData.socketSource
        )
        return remoteChatServiceDao.deleteGroup(fields)
    }

    override suspend fun updateGroupName(name: String?, threadID: String?): NoContentBaseWrapper {
        val fields = mutableMapOf(
            "creator_id" to prefs.userId,
            "thread_id" to threadID,
            "name" to name,
            "source" to BuildConfig.flavorData.socketSource
        )
        return remoteChatServiceDao.updateGroupName(fields)
    }

    override suspend fun addParticipants(
        threadID: String?,
        participantList: Array<ThreadUser>?
    ): NoContentBaseWrapper {
        val fields = mutableMapOf(
            "creator_id" to prefs.userId,
            "thread_id" to threadID,
            "source" to BuildConfig.flavorData.socketSource
        )
        participantList?.forEachIndexed { index, participant ->
            fields["participants[$index]"] = participant.userId
        }
        return remoteChatServiceDao.addParticipants(fields)
    }

}
