package com.classera.data.repositories.parent

import com.classera.data.models.BaseWrapper
import com.classera.data.models.parent.ParentChildWrapper

interface ParentChildRepository {

    suspend fun getStudents(): BaseWrapper<List<ParentChildWrapper>>
}
