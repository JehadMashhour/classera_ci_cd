package com.classera.data.repositories.switchschools

import com.classera.data.daos.switchschools.RemoteSwitchSchoolsDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.switchschools.School
import com.classera.data.models.switchschools.SchoolToken
import com.classera.data.prefs.Prefs

class SwitchSchoolsRepositoryImpl(
    private val remoteSwitchSchoolsDao: RemoteSwitchSchoolsDao,
    private val prefs: Prefs
) : SwitchSchoolsRepository {

    override suspend fun getSwitchSchoolsList(): BaseWrapper<List<School>> {
        return remoteSwitchSchoolsDao.getSchools()
    }

    override suspend fun changeSchool(school: School?): BaseWrapper<SchoolToken> {
        val token = remoteSwitchSchoolsDao.changeSchool(school?.id)
        if (token.success == true) {
            prefs.schoolId = school?.id
            prefs.schoolToken = token.data?.token
        }
        return token
    }
}
