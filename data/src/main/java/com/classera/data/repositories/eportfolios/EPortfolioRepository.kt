package com.classera.data.repositories.eportfolios

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.eportfolio.EPortfolioWraper
import com.classera.data.models.eportfolio.add.SharingLevelWrapper
import com.classera.data.models.eportfolio.add.TypeWrapper
import com.classera.data.models.eportfolio.details.EPortfolioComment
import com.classera.data.models.eportfolio.details.EPortfolioDetailsWraper
import okhttp3.MultipartBody

/**
 * Created by Odai Nazzal on 1/14/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
interface EPortfolioRepository {

    suspend fun getStudentEPortfolios(pageNumber: Int): BaseWrapper<List<EPortfolioWraper>>

    suspend fun getStudentEPortfolioDetails(ePortfolioId: String?): BaseWrapper<EPortfolioDetailsWraper>

    suspend fun getEPortfolioComments(ePortfolioId: String?, page: Int): BaseWrapper<List<EPortfolioComment>>

    suspend fun addEPortfolioComment(ePortfolioId: String?,text: String?): NoContentBaseWrapper

    suspend fun deleteEport(ePortfolioId: String?): NoContentBaseWrapper

    suspend fun getType():BaseWrapper<List<TypeWrapper>>

    suspend fun getSharingLevel():BaseWrapper<List<SharingLevelWrapper>>

    suspend fun uploadAttachment(parts: List<MultipartBody.Part>)
}
