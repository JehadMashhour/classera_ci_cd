package com.classera.data.repositories.surveys

import com.classera.data.models.BaseWrapper
import com.classera.data.models.surveys.Survey


/**
 * Created by Rawan Al-Theeb on 2/18/2020.
 * Classera
 * r.altheeb@classera.com
 */
interface SurveysRepository {
    suspend fun getListSurveys(
        pageNumber: Int,
        text: CharSequence? = null
    ): BaseWrapper<List<Survey>>
}
