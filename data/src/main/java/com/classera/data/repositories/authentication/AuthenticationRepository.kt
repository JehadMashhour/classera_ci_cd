package com.classera.data.repositories.authentication

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.authentication.forgetusername.ForgetUserNameResponse
import com.classera.data.models.authentication.forgotpassword.ForgetPasswordResponse
import com.classera.data.models.authentication.login.LoginResponse
import com.classera.data.models.authentication.settings.GoogleRestrictionSettings
import com.classera.data.models.authentication.verificationcode.VerificationCodeResponse

/**
 * Project: Classera
 * Created: Dec 9, 2019
 *
 * @author Mohamed Hamdan
 */
interface AuthenticationRepository {

    suspend fun loginBy(username: String?, password: String?): BaseWrapper<LoginResponse>

    suspend fun logout(fields: Map<String, String?>): NoContentBaseWrapper

    suspend fun forgetPassword(username: String?): BaseWrapper<ForgetPasswordResponse>

    suspend fun checkVerificationCode(
        code: String?,
        username: String?
    ): BaseWrapper<VerificationCodeResponse>

    suspend fun forgetUsername(number: String?): BaseWrapper<ForgetUserNameResponse>

    suspend fun googleLogin(android: String?): BaseWrapper<LoginResponse>

    suspend fun googleLoginSettings(): BaseWrapper<GoogleRestrictionSettings>
}
