package com.classera.data.repositories.reportcards

import com.classera.data.models.BaseWrapper
import com.classera.data.models.reportcards.ReportCardsWrapper
import com.classera.data.models.reportcards.reportcardsdetails.ReportCardDetails
import okhttp3.ResponseBody


interface ReportCardsRepository {

    suspend fun getReportCardsList(
    ): BaseWrapper<ReportCardsWrapper>

    suspend fun getReportCardDetails(
        url: String? = null,
        reportId: String? = null
    ): BaseWrapper<ReportCardDetails>

    suspend fun downloadFile(url: String?): ResponseBody
}
