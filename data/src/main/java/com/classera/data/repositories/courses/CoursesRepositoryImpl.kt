package com.classera.data.repositories.courses

import com.classera.data.daos.courses.RemoteCoursesDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.courses.PreparationFilterWrapper
import com.classera.data.models.courses.Course
import com.classera.data.models.courses.CourseDetailsWrapper
import com.classera.data.prefs.Prefs

/**
 * Created by Odai Nazzal on 1/7/2020.
 * Classera
 *
 * o.nazzal@classera.com
 */
class CoursesRepositoryImpl(private val coursesDao: RemoteCoursesDao, private val prefs: Prefs) :
    CoursesRepository {

    override suspend fun getCourses(): BaseWrapper<List<Course>> {
        return coursesDao.getCourses()
    }

    override suspend fun getCourseDetails(
        courseId: String?,
        teacherId: String?,
        preprationId: String?
    ): BaseWrapper<CourseDetailsWrapper> {
        val query = mapOf(
            "course_id" to courseId,
            "teacher_user_id" to (teacherId ?: prefs.userId),
            "preparation_id" to preprationId
        )
        return if (teacherId != null) {
            coursesDao.getCourseDetails(query)
        } else {
            coursesDao.getTeacherCourseDetails(query)
        }
    }

    override suspend fun getPreparation(courseId: String?): BaseWrapper<List<PreparationFilterWrapper>> {
        return coursesDao.getPreparation(courseId)
    }
}
