package com.classera.data.repositories.surveys

import com.classera.data.daos.surveys.RemoteSurveysDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.surveys.Survey


/**
 * Created by Rawan Al-Theeb on 2/18/2020.
 * Classera
 * r.altheeb@classera.com
 */
class SurveysRepositoryImpl(private val remoteSurveysDao: RemoteSurveysDao) : SurveysRepository {

    override suspend fun getListSurveys(pageNumber: Int, text: CharSequence?): BaseWrapper<List<Survey>> {
        val fields = mapOf(
            "p" to pageNumber,
            "text" to (text ?: "")
        )
        return remoteSurveysDao.getListSurveys(fields)
    }
}
