package com.classera.data.repositories.home

import com.classera.data.daos.home.RemoteOnlineNowDao
import com.classera.data.models.socket.OnlineNow
import retrofit2.http.FieldMap

/**
 * Created by Saeed Halawani on 17/1/2019.
 * Classera
 * s.halawani@classera.com
 */
class OnlineNowRepositoryImpl(private val remoteOnlineNowDao: RemoteOnlineNowDao
) : OnlineNowRepository {

    override suspend fun getOnlineNow(@FieldMap fields: Map<String, String?>): OnlineNow {
        return remoteOnlineNowDao.getOnlineNow(fields)
    }

}
