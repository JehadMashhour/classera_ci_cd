package com.classera.data.repositories.digitallibrary

import com.classera.data.models.BaseWrapper
import com.classera.data.models.digitallibrary.Attachment

/**
 * Created by Odai Nazzal on 12/19/2019.
 * Classera
 * o.nazzal@classera.com
 */
interface DigitalLibraryRepository {

    suspend fun getStudentLibrary(
        pageNumber: Int,
        filter: String? = null,
        text: CharSequence? = null
    ): BaseWrapper<List<Attachment>>

    suspend fun getStudentAttachments(
        pageNumber: Int,
        filter: String,
        text: CharSequence?,
        courseId: String? = null
    ): BaseWrapper<List<Attachment>>
}
