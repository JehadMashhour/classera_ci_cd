package com.classera.data.repositories.weeklyplan

import com.classera.data.models.BaseWrapper
import com.classera.data.models.weeklyplan.WeeklyPlanWrapper

/**
 * Created by Odai Nazzal on 12/27/2019.
 * Classera
 * o.nazzal@classera.com
 */
interface WeeklyPlanRepository {

    suspend fun getUserWeeklyPlan(from: String? = null, to: String? = null): BaseWrapper<WeeklyPlanWrapper>
}
