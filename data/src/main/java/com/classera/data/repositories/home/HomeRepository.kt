package com.classera.data.repositories.home

import com.classera.data.models.BaseWrapper
import com.classera.data.models.home.StudentHomeResponse
import com.classera.data.models.home.TeacherHomeResponse
import com.classera.data.models.home.admin.DayAbsences
import com.classera.data.models.home.admin.SmsUsage
import com.classera.data.models.home.admin.Statistic
import com.classera.data.models.home.admin.TopScoresSchool
import com.classera.data.models.home.admin.TopScoresSchoolGroup
import com.classera.data.models.home.admin.TopSectionScore

/**
 * Created by Odai Nazzal on 12/19/2019.
 * Classera
 * o.nazzal@classera.com
 */
interface HomeRepository {

    suspend fun getStudentHome(): BaseWrapper<StudentHomeResponse>

    suspend fun getTeacherHome(): BaseWrapper<TeacherHomeResponse>

    suspend fun getContentStatistics(): BaseWrapper<List<Statistic>>

    suspend fun getDashboardTotalAbsences(): BaseWrapper<List<DayAbsences>>

    suspend fun getTopSectionsScores(schoolId: String?): BaseWrapper<List<TopSectionScore>>

    suspend fun getTopScoresSchool(roleId: String?): BaseWrapper<List<TopScoresSchool>>

    suspend fun getTopScoresSchoolGroup(roleId: String?): BaseWrapper<List<TopScoresSchoolGroup>>

    suspend fun getTopScoresAllSchools(roleId: String?): BaseWrapper<List<TopScoresSchoolGroup>>

    suspend fun getAmbassadors(): BaseWrapper<List<TopScoresSchool>>

    suspend fun getSmsUsage(): BaseWrapper<List<SmsUsage>>


}
