package com.classera.data.repositories.behaviours

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.behaviours.Behaviour
import com.classera.data.models.behaviours.teacher.BehaviorActionsWraper
import com.classera.data.models.behaviours.teacher.BehaviorGroup
import com.classera.data.models.user.User
import okhttp3.MediaType

/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
interface BehavioursRepository {

    // positive param. for filter
    suspend fun getStudentBehaviours(
        pageNumber: Int,
        filter: String? = null,
        text: CharSequence? = null
    ): BaseWrapper<List<Behaviour>>

    suspend fun getTeacherBehaviours(
        pageNumber: Int, filter: String? = null,
        text: CharSequence? = null
    ): BaseWrapper<List<Behaviour>>

    suspend fun getBehaviourGroups(behaviorType: String): BaseWrapper<List<BehaviorGroup>>

    suspend fun getBehaviourActions(behaviorGroupId: String?): BaseWrapper<BehaviorActionsWraper>

    suspend fun getStudentsByCourse(courseId: String?): BaseWrapper<List<User>>

    suspend fun deleteBehavior(studentId: String?, studentBehaviorId: String?): NoContentBaseWrapper

    suspend fun addBehaviour(
        fields: Map<String, Any?>, attachmentPath: String,
        mediaType: String
    ): NoContentBaseWrapper

    suspend fun editBehaviour(
        fields: Map<String, Any?>, attachmentPath: String,
        mediaType: String
    ): NoContentBaseWrapper
}
