package com.classera.data.repositories.support

import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.support.CommentWrapper
import com.classera.data.models.support.SupportDataWrapper
import com.classera.data.models.support.SupportWrapper
import okhttp3.MultipartBody

interface SupportRepository {

    suspend fun getSupport(
        searchText: CharSequence?,
        pageNumber: Int,
        text: CharSequence? = null
    ): BaseWrapper<List<SupportWrapper>>

    suspend fun getSupportDetails(
        ticketId: String?
    ): BaseWrapper<SupportWrapper>

    suspend fun getSupportComments(
        ticketId: String?,
        pageNumber: Int
    ): BaseWrapper<List<CommentWrapper>>

    suspend fun addComment(ticketId: String?, body: String?): NoContentBaseWrapper

    suspend fun addSupportTicket(parts: List<MultipartBody.Part>)

    suspend fun getSupportTypes(): BaseWrapper<List<SupportDataWrapper>>

    suspend fun getSupportModules(): BaseWrapper<List<SupportDataWrapper>>

    suspend fun getSupportPriority(): BaseWrapper<List<SupportDataWrapper>>

    suspend fun getSupportStatuses(): BaseWrapper<List<SupportDataWrapper>>

    suspend fun updateSupportTicketStatus(ticketId: String?, statusId: String?): NoContentBaseWrapper

    suspend fun updateSupportTicketModule(ticketId: String?, moduleId: String?): NoContentBaseWrapper

    suspend fun updateSupportTicketType(ticketId: String?, typeId: String?): NoContentBaseWrapper

    suspend fun updateSupportTicketPriority(ticketId: String?, statusId: String?): NoContentBaseWrapper

}
