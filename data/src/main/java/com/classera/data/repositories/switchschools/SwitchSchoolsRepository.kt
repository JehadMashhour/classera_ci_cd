package com.classera.data.repositories.switchschools

import com.classera.data.models.BaseWrapper
import com.classera.data.models.switchschools.School
import com.classera.data.models.switchschools.SchoolToken

interface SwitchSchoolsRepository {

    suspend fun getSwitchSchoolsList(): BaseWrapper<List<School>>

    suspend fun changeSchool(school: School?): BaseWrapper<SchoolToken>
}
