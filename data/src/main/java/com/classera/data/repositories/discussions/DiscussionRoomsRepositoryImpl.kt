package com.classera.data.repositories.discussions

import com.classera.data.daos.discussions.RemoteDiscussionDao
import com.classera.data.models.BaseWrapper
import com.classera.data.models.NoContentBaseWrapper
import com.classera.data.models.discussions.DiscussionCommentsWrapper
import com.classera.data.models.discussions.DiscussionPost
import com.classera.data.models.discussions.DiscussionRoom
import com.classera.data.models.discussions.RoomDetails
import com.classera.data.models.discussions.Student

/**
 * Created by Odai Nazzal on 12/30/2019.
 * Classera
 *
 * o.nazzal@classera.com
 */
class DiscussionRoomsRepositoryImpl(private val remoteDiscussionDao: RemoteDiscussionDao) : DiscussionRoomsRepository {


    override suspend fun deleteDiscussionComment(commentId: String?, type: String?): NoContentBaseWrapper {
        val fields = mapOf(
            "post_id" to commentId,
            "type" to type
        )
        return remoteDiscussionDao.deleteDiscussionComment(fields)
    }

    override suspend fun getStudentDiscussions(
        pageNumber: Int,
        text: CharSequence?
    ): BaseWrapper<List<DiscussionRoom>> {
        val fields = mutableMapOf<String, Any?>("p" to pageNumber)

        return if (text?.isNotEmpty() == true) {
            fields["text"] = text
            remoteDiscussionDao.searchStudentDiscussions(fields)
        } else {
            remoteDiscussionDao.getStudentDiscussions(fields)
        }
    }

    override suspend fun getDiscussionDetails(
        roomId: String?,
        pageNumber: Int,
        text: CharSequence?
    ): BaseWrapper<List<DiscussionPost>> {
        val fields = mutableMapOf(
            "p" to pageNumber,
            "post_id" to roomId,
            "room_id" to roomId

        )

        return if (text?.isNotEmpty() == true) {
            fields["text"] = text.toString()
            remoteDiscussionDao.searchDiscussionPosts(fields)
        } else {
            remoteDiscussionDao.getDiscussionPosts(fields)
        }
    }

    override suspend fun getDiscussionComments(
        postId: String?,
        pageNumber: Int
    ): BaseWrapper<DiscussionCommentsWrapper> {
        val fields = mutableMapOf(
            "p" to pageNumber,
            "post_id" to postId
        )
        return remoteDiscussionDao.getDiscussionComments(fields)
    }

    override suspend fun addComment(postId: String?, text: String?): NoContentBaseWrapper {
        val fields = mutableMapOf(
            "post_id" to postId,
            "type" to "1",
            "text" to text
        )
        return remoteDiscussionDao.addComment(fields)
    }

    override suspend fun getStudents(): BaseWrapper<List<Student>> {
        return remoteDiscussionDao.getStudents()
    }

    override suspend fun addRoom(
        title: String?,
        instructions: String?,
        hideComments: String?,
        lectures: List<String?>?,
        users: List<String?>?
    ): NoContentBaseWrapper {
        val fields = mutableMapOf(
            "title" to title,
            "instruction" to instructions,
            "hide_comments" to hideComments
        )
        lectures?.forEachIndexed { index, value -> fields["lectures[$index]"] = value }
        users?.forEachIndexed { index, value -> fields["users[$index]"] = value }

        return remoteDiscussionDao.addRoom(fields)
    }

    override suspend fun addPost(
        roomId: String?,
        text: String?
    ): NoContentBaseWrapper {
        val fields = mutableMapOf(
            "post_id" to roomId,
            "text" to text
        )
        return remoteDiscussionDao.addPost(fields)
    }

    override suspend fun deleteRoom(roomId: String?): NoContentBaseWrapper {
        val fields =  mapOf(
            "room_id" to roomId
        )
        return remoteDiscussionDao.deleteRoom(fields)
    }

    override suspend fun getRoomDetails(roomId: String?): BaseWrapper<RoomDetails> {
        val fields = mapOf(
            "room_id" to roomId
        )
        return remoteDiscussionDao.getRoomDetails(fields)
    }

    override suspend fun approveTopic(postId: String?, approve: String?): NoContentBaseWrapper {
        val fields = mapOf(
            "post_id" to postId,
            "approve" to approve
        )
        return remoteDiscussionDao.approveTopic(fields)
    }

    override suspend fun editRoomSettings(roomId: String?, close: String?, approve: String?): NoContentBaseWrapper {
        val fields = mapOf(
            "room_id" to roomId,
            "close" to close,
            "approve" to approve
        )
        return remoteDiscussionDao.editRoomSettings(fields)
    }

    override suspend fun editRoom(
        title: String?,
        instructions: String?,
        hideComments: String?,
        roomId : String?,
        lectures: List<String?>?,
        users: List<String?>?
    ): NoContentBaseWrapper {
        val fields = mutableMapOf(
            "title" to title,
            "instruction" to instructions,
            "hide_comments" to hideComments,
            "room_id" to roomId
        )
        lectures?.forEachIndexed { index, value -> fields["lectures[$index]"] = value }
        users?.forEachIndexed { index, value -> fields["users[$index]"] = value }

        return remoteDiscussionDao.editRoom(fields)
    }
}
