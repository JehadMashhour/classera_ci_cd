package com.classera.notigation

import androidx.navigation.NavController

class NavigationHandlerImpl(private val processors: Set<NavigationProcessor>) :
    NavigationHandler {

    override fun process(
        event: String,
        notificationBody: String?,
        navController: NavController?
    ): Boolean {
        processors.forEach {
            if (it.matches(event)) {
                it.execute(event)
                navController?.let { navController ->
                    it.executeWithNavController(notificationBody, navController)
                }
                return true
            }
        }
        return false
    }
}
