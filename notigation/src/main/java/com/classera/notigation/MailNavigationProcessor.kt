package com.classera.notigation

import android.os.Bundle
import androidx.navigation.NavController
import com.classera.data.models.notification.MailboxNotificationBody
import com.google.gson.Gson

class MailNavigationProcessor : NavigationProcessor {

    override fun matches(event: String): Boolean {
        return event.equals("mail", true)
    }

    override fun executeWithNavController(notificationBody: String?, navController: NavController) {
        val notificationMailboxBody =
            Gson().fromJson(notificationBody, MailboxNotificationBody::class.java)
        navController.navigate(R.id.item_menu_activity_main_navigation_view_mailbox, null)
        navController.navigate(
            R.id.item_menu_activity_main_navigation_view_mailbox_details,
            Bundle().apply {
                putString("msgId", notificationMailboxBody.messageId)
                putString("title", notificationMailboxBody.title)
                putString("type", "INBOX")
            })
    }
}
