package com.calssera.behaviours.add

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import com.calssera.behaviours.databinding.RowStudentBinding
import com.classera.data.models.user.User

/**
 * Project: Classera
 * Created: Feb 19, 2020
 *
 * @author Mohamed Hamdan
 */
class StudentsAdapter(private var students: List<User>) : BaseAdapter(), Filterable {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        val viewHolder: ViewHolder
        if (view == null) {
            val binding = RowStudentBinding.inflate(LayoutInflater.from(parent?.context), parent, false)
            view = binding.root

            viewHolder = ViewHolder(binding)
            view.tag = viewHolder
        } else {
            viewHolder = view.tag as ViewHolder
        }
        viewHolder.bind(position)
        return view
    }

    override fun getItem(position: Int) = students[position].name

    override fun getItemId(position: Int) = position.toLong()

    override fun getCount() = students.size

    override fun getFilter(): Filter {
        return object : Filter() {

            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filteredItems = students.filter { it.name?.contains(constraint.toString(), true) == true }
                val filterResult = FilterResults()
                filterResult.count = filteredItems.size
                filterResult.values = filteredItems
                return filterResult
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                students = results?.values as List<User>
            }
        }
    }

    private inner class ViewHolder(private val binding: RowStudentBinding) {

        fun bind(position: Int) {
            binding.user = students[position]
            binding.executePendingBindings()
        }
    }
}
