package com.calssera.behaviours

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.ProgressBar
import android.widget.SearchView
import android.widget.Toast
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.calssera.behaviours.student.BehavioursStudentFragmentDirections
import com.classera.core.Screen
import com.classera.core.custom.views.ErrorView
import com.classera.core.custom.views.QuickFilterView
import com.classera.core.fragments.BaseFragment
import com.classera.core.utils.android.onDebounceQueryTextChange
import com.classera.data.models.behaviours.Behaviour
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource

/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
@Screen("Behaviours")
abstract class BehavioursFragment : BaseFragment() {

    private var searchView: SearchView? = null
    private var progressBar: ProgressBar? = null
    private var errorView: ErrorView? = null

    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var filterView: QuickFilterView? = null

    override val layoutId: Int = R.layout.fragment_behaviours

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    abstract fun getBehaviorViewModel(): BehavioursViewModel

    abstract fun getBehaviorAdapter(): BehavioursAdapter<*>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initFilter()
        initListeners()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_behaviours, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val searchMenuItem = menu.findItem(R.id.item_menu_fragment_behaviours_search)
        searchView = (searchMenuItem.actionView as? SearchView?)
        searchView?.onDebounceQueryTextChange { getStudentBehaviours() }

        searchView?.setOnCloseListener {
            getStudentBehaviours()
            return@setOnCloseListener false
        }
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_behaviours)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_behaviours)
        errorView = view?.findViewById(R.id.error_view_fragment_behaviours)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_behaviours)
        filterView = view?.findViewById(R.id.filter_view_fragment_behaviours)
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            refreshBehaviours()
        }

        getBehaviorViewModel().notifyAdapterItemLiveData.observe(this) {
            getBehaviorAdapter().notifyItemChanged(
                it
            )
        }

        getBehaviorViewModel().toastLiveData.observe(this) { message ->
            val stringMessage = if (message is Int) getString(message) else message as String
            Toast.makeText(context, stringMessage, Toast.LENGTH_LONG).show()
        }
        getStudentBehaviours()
    }

    private fun getStudentBehaviours(pageNumber: Int = DEFAULT_PAGE) {
        if (pageNumber == DEFAULT_PAGE) {
            getBehaviorAdapter().resetPaging()
        }
        getBehaviorViewModel().getUserBehaviours(
            searchView?.query,
            filterView?.getSelectedFilterKey(),
            pageNumber
        )
            .observe(this, this::handleResource)
    }

    private fun refreshBehaviours() {
        getBehaviorViewModel().refreshBehaviours(
            searchView?.query,
            filterView?.getSelectedFilterKey()
        )
            .observe(this, this::handleResource)
    }

    private fun initFilter() {
        filterView?.setAdapter(
            R.array.behaviours_filter_entries,
            R.array.behaviours_filter_entry_values
        )
        filterView?.setOnFilterSelectedListener {
            recyclerView?.scrollToPosition(0)
            getStudentBehaviours()
        }
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource() {
        recyclerView?.adapter = getBehaviorAdapter()
        getBehaviorAdapter().setOnLoadMoreListener(::getStudentBehaviours)
        handleAdapterListeners()
    }

    fun navigateToBehaviourDetails(view: View, behavioursResponse: Behaviour?) {
        behavioursResponse?.let {
            val direction = BehavioursStudentFragmentDirections.behaviorsDetailsDirection(
                behavioursResponse.behaviorTitle,
                behavioursResponse
            )
            view.findNavController().navigate(direction)
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        if ((getBehaviorAdapter().getItemsCount()) > 0) {
            Toast.makeText(context, resource.error.resourceMessage, Toast.LENGTH_LONG).show()
            return
        }
        errorView?.visibility = View.VISIBLE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getStudentBehaviours() }
        getBehaviorAdapter().finishLoading()
    }

    override fun onDestroyView() {
        progressBar = null
        recyclerView = null
        errorView = null
        filterView = null
        swipeRefreshLayout = null
        super.onDestroyView()
    }

    abstract fun handleAdapterListeners()
}
