package com.calssera.behaviours.student

import com.calssera.behaviours.BehavioursViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.behaviours.Behaviour
import com.classera.data.repositories.behaviours.BehavioursRepository

/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
class BehavioursStudentViewModel(private val behavioursRepository: BehavioursRepository) : BehavioursViewModel() {


    override suspend fun getBehaviours(
        text: CharSequence?,
        filter: String?,
        pageNumber: Int
    ): BaseWrapper<List<Behaviour>> {
        return behavioursRepository.getStudentBehaviours(pageNumber, filter, text)
    }

    override fun deleteBehaviour(position: Int) {
        throw IllegalAccessException("The student should not be able to delete a Behaviour")
    }
}
