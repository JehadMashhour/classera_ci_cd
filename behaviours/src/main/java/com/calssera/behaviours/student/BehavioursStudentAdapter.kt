package com.calssera.behaviours.student

import android.view.ViewGroup
import com.calssera.behaviours.BehavioursAdapter
import com.calssera.behaviours.BehavioursViewModel
import com.calssera.behaviours.databinding.RowBehavioursBinding
import com.classera.core.adapter.BaseBindingViewHolder


/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */

class BehavioursStudentAdapter(
    private val viewModel: BehavioursViewModel
) : BehavioursAdapter<BehavioursStudentAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowBehavioursBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getBehavioursCount()
    }

    inner class ViewHolder(binding: RowBehavioursBinding) : BaseBindingViewHolder(binding) {

        override fun bind(position: Int) {
            bind<RowBehavioursBinding> { behavioursItem = viewModel.getBehaviour(position) }
        }
    }
}
