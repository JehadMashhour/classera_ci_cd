package com.calssera.behaviours.student

import androidx.navigation.fragment.findNavController
import com.calssera.behaviours.BehavioursAdapter
import com.calssera.behaviours.BehavioursFragment
import com.calssera.behaviours.BehavioursViewModel
import com.classera.data.models.user.UserRole
import com.classera.data.prefs.Prefs
import javax.inject.Inject

class BehavioursStudentFragment : BehavioursFragment() {

    @Inject
    lateinit var viewModel: BehavioursStudentViewModel

    @Inject
    lateinit var adapter: BehavioursStudentAdapter

    @Inject
    lateinit var prefs: Prefs

    override fun getBehaviorViewModel(): BehavioursViewModel {
        return viewModel
    }

    override fun getBehaviorAdapter(): BehavioursAdapter<*> {
        return adapter
    }

    override fun handleAdapterListeners() {
        adapter.setOnItemClickListener { view, position ->
            val behavioursResponse = viewModel.getBehaviour(position)
            behavioursResponse?.let {
                findNavController().navigate(
                    BehavioursStudentFragmentDirections.behaviorsDetailsDirection(
                        behavioursResponse.behaviorTitle,
                        behavioursResponse
                    ).apply {
                        role = prefs.userRole ?: com.classera.data.models.user.UserRole.STUDENT
                    }
                )
            }
        }
    }
}

