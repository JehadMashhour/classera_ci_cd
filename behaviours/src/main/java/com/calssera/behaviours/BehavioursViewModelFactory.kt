package com.calssera.behaviours

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.calssera.behaviours.add.AddBehaviourViewModel
import com.calssera.behaviours.student.BehavioursStudentViewModel
import com.calssera.behaviours.teacher.BehavioursTeacherViewModel
import com.classera.data.prefs.Prefs
import com.classera.data.repositories.behaviours.BehavioursRepository
import com.classera.data.repositories.courses.CoursesRepository
import javax.inject.Inject


/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
class BehavioursViewModelFactory @Inject constructor(
    private val behavioursRepository: BehavioursRepository,
    private val coursesRepository: CoursesRepository,
    private val prefs: Prefs
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            BehavioursStudentViewModel::class.java -> {
                BehavioursStudentViewModel(behavioursRepository) as T
            }
            BehavioursTeacherViewModel::class.java -> {
                BehavioursTeacherViewModel(behavioursRepository) as T
            }
            AddBehaviourViewModel::class.java -> {
                AddBehaviourViewModel(behavioursRepository, coursesRepository, prefs) as T
            }
            else -> {
                throw IllegalAccessException("There is no view model called $modelClass")
            }
        }
    }
}
