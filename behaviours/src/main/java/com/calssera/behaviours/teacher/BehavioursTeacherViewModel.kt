package com.calssera.behaviours.teacher

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.calssera.behaviours.BehavioursViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.behaviours.Behaviour
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.repositories.behaviours.BehavioursRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Created by Rawan Al-Theeb on 12/29/2019.
 * Classera
 * r.altheeb@classera.com
 */
class BehavioursTeacherViewModel(private val behavioursRepository: BehavioursRepository) :
    BehavioursViewModel() {

    private val _notifyItemRemovedLiveData = MutableLiveData<Int>()
    val notifyItemRemovedLiveData: LiveData<Int> = _notifyItemRemovedLiveData

    override suspend fun getBehaviours(
        text: CharSequence?,
        filter: String?,
        pageNumber: Int
    ): BaseWrapper<List<Behaviour>> {
        return behavioursRepository.getTeacherBehaviours(pageNumber, filter, text)
    }

    override fun deleteBehaviour(position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val behaviour = getBehaviour(position)
            behaviour?.deleting = true

            val studentId = behaviour?.studentId
            val studentBehaviorId = behaviour?.id
            val resource = tryNoContentResource {
                behavioursRepository.deleteBehavior(
                    studentId,
                    studentBehaviorId
                )
            }
            if (resource.isSuccess()) {
                deleteItem(position)
                _notifyItemRemovedLiveData.postValue(position)
                return@launch
            }
            behaviour?.deleting = false
        }
    }
}
