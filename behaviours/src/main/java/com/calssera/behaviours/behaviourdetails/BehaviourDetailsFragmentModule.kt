package com.calssera.behaviours.behaviourdetails

import androidx.fragment.app.Fragment
import dagger.Binds
import dagger.Module

@Module
abstract class BehaviourDetailsFragmentModule {
    @Binds
    abstract fun bindFragment(fragment: BehaviourDetailsFragment): Fragment
}
