package com.classera.mailbox.compose

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.FragmentManager
import com.classera.core.fragments.BaseBottomSheetDialogFragment
import com.classera.mailbox.R


/**
 * Created by Rawan Al-Theeb on 2/9/2021.
 * Classera
 * r.altheeb@classera.com
 */
class BackComposeBottomSheet private constructor() : BaseBottomSheetDialogFragment() {

    private var callback: ((option: String) -> Unit)? = null
    private var buttonSaveDraft: Button? = null
    private var buttonDeleteDraft: Button? = null
    private var buttonContinue: Button? = null

    override val layoutId: Int = R.layout.back_compose_bottom_sheet

    override fun enableDependencyInjection(): Boolean {
        return false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()
    }

    private fun findViews() {
        buttonSaveDraft = view?.findViewById(R.id.button_bottom_sheet_save_draft)
        buttonDeleteDraft = view?.findViewById(R.id.button_bottom_sheet_delete_draft)
        buttonContinue = view?.findViewById(R.id.button_bottom_sheet_continue_sharing)
    }

    private fun initListeners() {
        buttonSaveDraft?.setOnClickListener {
            dismiss()
            callback?.invoke(SAVE)
        }

        buttonDeleteDraft?.setOnClickListener {
            dismiss()
            callback?.invoke(DELETE)
        }

        buttonContinue?.setOnClickListener {
            dismiss()
        }
    }

    companion object {

        fun show(supportFragmentManager: FragmentManager, callback: (option: String) -> Unit) {
            val backComposeBottomSheet = BackComposeBottomSheet()
            backComposeBottomSheet.callback = callback
            backComposeBottomSheet.show(supportFragmentManager, "")
        }

        const val SAVE = "save"
        const val DELETE = "delete"
    }
}
