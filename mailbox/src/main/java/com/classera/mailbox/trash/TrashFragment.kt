package com.classera.mailbox.trash


import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Screen
import com.classera.core.custom.callbacks.SwipeToRestoreDeleteCallback
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.mailbox.MailboxViewPagerFragment
import com.classera.mailbox.R
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Jan 25, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
@Screen("Trash")
class TrashFragment : BaseFragment(), MailboxViewPagerFragment{

    @Inject
    lateinit var viewModel: TrashViewModel

    private var progressBar: ProgressBar? = null
    private var errorView: ErrorView? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null

    private var adapter: TrashAdapter? = null
    private var swipeToRestoreDeleteBehavior: SwipeToRestoreDeleteCallback? = null

    override val layoutId: Int = R.layout.fragment_mailbox_view_pager

    private var searchValue: String = ""
        set(value) {
            field = value
            getTrash(DEFAULT_PAGE)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(viewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()

        viewModel.getTrash(searchValue, DEFAULT_PAGE).observe(this, this::handleResource)
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_mailbox)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_mailbox)
        errorView = view?.findViewById(R.id.error_view_fragment_mailbox)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_mailbox)

        swipeToRestoreDeleteBehavior = SwipeToRestoreDeleteCallback(
            0,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT,
            context!!
        )
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            refreshOutbox()
        }
    }

    private fun refreshOutbox() {
        viewModel.refreshTrash(searchValue)
            .observe(this, this::handleResource)
    }

    private fun initAdapter() {
        recyclerView?.recycledViewPool?.clear()
        adapter = TrashAdapter(viewModel)
        recyclerView?.adapter = adapter
        adapter?.setOnLoadMoreListener(::getTrash)

        // swipe to delete and restore
        initSwipeToRestoreDeleteListener()
    }

    private fun getTrash(pageNumber: Int = DEFAULT_PAGE) {
        if (pageNumber == DEFAULT_PAGE) {
            adapter?.resetPaging()
        }
        viewModel.getTrash(
            searchValue,
            pageNumber
        )
            .observe(this, this::handleResource)
    }

    private fun initSwipeToRestoreDeleteListener() {
        val itemTouchHelper = ItemTouchHelper(swipeToRestoreDeleteBehavior!!)
        itemTouchHelper.attachToRecyclerView(recyclerView)
        // delete
        swipeToRestoreDeleteBehavior?.getDeleteSwiped()?.observe(this) {
            if (it){
                requireContext().let {context->
                    AlertDialog.Builder(context)
                        .setTitle(R.string.title_delete_mailboix_dialog)
                        .setMessage(R.string.message_delete_mailbox_dialog_permanently)
                        .setPositiveButton(R.string.button_positive_delete_discussion_dialog)
                        { _, _ ->
                            viewModel.deleteMessage(swipeToRestoreDeleteBehavior?.position!!)
                                .observe(this, this::handleDeleteResource)
                            swipeToRestoreDeleteBehavior?.updateDeleteSwipe(false)
                        }
                        .setNegativeButton(R.string.button_negative_delete_mailbox_dialog) { _, _ ->
                            getTrash(DEFAULT_PAGE)
                        }
                        .setCancelable(false)
                        .show()
                }

            }
        }
        // restore
        swipeToRestoreDeleteBehavior?.getRestoreSwiped()?.observe(this) {
            if (it){
                requireContext().let {context->
                    AlertDialog.Builder(context)
                        .setTitle(R.string.title_restore_mailboix_dialog)
                        .setMessage(R.string.message_restore_mailbox_dialog)
                        .setPositiveButton(R.string.button_positive_delete_discussion_dialog)
                        { _, _ ->
                            viewModel.restoreMessage(swipeToRestoreDeleteBehavior?.position!!)
                                .observe(this, this::handleRestoreResource)
                            swipeToRestoreDeleteBehavior?.updateDeleteSwipe(false)
                        }
                        .setNegativeButton(R.string.button_negative_delete_mailbox_dialog) { _, _ ->
                            getTrash(DEFAULT_PAGE)
                        }
                        .setCancelable(false)
                        .show()
                }

            }
        }
    }

    private fun handleRestoreResource(resource: Resource) {
        if (resource is Resource.Error){
            Toast.makeText(context, resource.error.message, Toast.LENGTH_LONG).show()
            adapter!!.notifyItemChanged(swipeToRestoreDeleteBehavior?.position!!)
        } else {
            Toast.makeText(context, getString(R.string.message_restore_successfully), Toast.LENGTH_LONG).show()
            adapter!!.removeItem(swipeToRestoreDeleteBehavior?.position!!)
        }
    }

    private fun handleDeleteResource(resource: Resource) {
        if (resource is Resource.Error){
            Toast.makeText(context, resource.error.message, Toast.LENGTH_LONG).show()
            adapter!!.notifyItemChanged(swipeToRestoreDeleteBehavior?.position!!)
        } else {
            Toast.makeText(context, getString(R.string.message_deleted_successfully), Toast.LENGTH_LONG).show()
            adapter!!.removeItem(swipeToRestoreDeleteBehavior?.position!!)
        }
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleSuccessResource() {
        if (adapter == null) {
            initAdapter()
        } else {
            recyclerView?.recycledViewPool?.clear()
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.visibility = View.VISIBLE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getTrash() }
        adapter?.finishLoading()
    }

    override fun search(searchValue: String) {
        this.searchValue = searchValue
    }

    override fun onResume() {
        searchValue = ""
        viewModel.getTrash(searchValue, DEFAULT_PAGE).observe(this, this::handleResource)
        super.onResume()
    }

    override fun onDestroyView() {
        swipeRefreshLayout = null
        adapter = null
        errorView = null
        progressBar = null
        recyclerView = null
        super.onDestroyView()
    }

    companion object {
        fun newInstance(): Fragment {
            return TrashFragment()
        }
    }
}
