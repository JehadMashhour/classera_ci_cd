package com.classera.mailbox.trash

import android.view.ViewGroup
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.mailbox.databinding.TrashListRowItemBinding

/**
 * Project: Classera
 * Created: Jan 10, 2019
 *
 * @author Abdulrhman Hasan Agha
 */
class TrashAdapter(
    private val viewModel: TrashViewModel
) : BasePagingAdapter<TrashAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = TrashListRowItemBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getTrashCount()
    }

    fun removeItem(position: Int) {
        viewModel.removeFromList(position)
        notifyItemRemoved(position)
    }

    inner class ViewHolder(binding: TrashListRowItemBinding) : BaseBindingViewHolder(binding) {
        override fun bind(position: Int) {
            bind<TrashListRowItemBinding> {
                this.trash = viewModel.getSpecificTrash(position)
                this.user = viewModel.getLocalUser()
            }
        }
    }

}
