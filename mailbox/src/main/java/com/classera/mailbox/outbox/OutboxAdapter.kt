package com.classera.mailbox.outbox

import android.view.ViewGroup
import com.classera.core.adapter.BaseBindingViewHolder
import com.classera.core.adapter.BasePagingAdapter
import com.classera.mailbox.databinding.OutboxListRowItemBinding

/**
 * Project: Classera
 * Created: Jan 10, 2019
 *
 * @author Abdulrhman Hasan Agha
 */
class OutboxAdapter(
    private val viewModel: OutboxViewModel
) : BasePagingAdapter<OutboxAdapter.ViewHolder>() {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = OutboxListRowItemBinding.inflate(inflater!!, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemsCount(): Int {
        return viewModel.getOutboxCount()
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    fun removeItem(position: Int) {
        viewModel.removeFromList(position)
        notifyItemRemoved(position)
    }

    inner class ViewHolder(binding: OutboxListRowItemBinding) : BaseBindingViewHolder(binding) {
        override fun bind(position: Int) {
            bind<OutboxListRowItemBinding> {
                if(viewModel.getOutboxCount() != 0) {
                    this.outbox = viewModel.getSpecificOutbox(position)
                }
            }
        }
    }

}
