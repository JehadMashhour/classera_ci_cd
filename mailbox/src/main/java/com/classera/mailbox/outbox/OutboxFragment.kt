package com.classera.mailbox.outbox


import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.classera.core.Screen
import com.classera.core.custom.callbacks.SwipeToDeleteCallback
import com.classera.core.custom.views.ErrorView
import com.classera.core.fragments.BaseFragment
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.repositories.mailbox.TYPE_OUTBOX
import com.classera.mailbox.MailboxFragmentDirections
import com.classera.mailbox.MailboxViewPagerFragment
import com.classera.mailbox.R
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Jan 10, 2019
 *
 * @author Abdulrhman Hasan Agha
 */
@Screen("Outbox")
class OutboxFragment : BaseFragment(), MailboxViewPagerFragment {

    @Inject
    lateinit var viewModel: OutboxViewModel

    private var progressBar: ProgressBar? = null
    private var errorView: ErrorView? = null
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null

    private var adapter: OutboxAdapter? = null
    private var swipeToDeleteBehavior: SwipeToDeleteCallback? = null

    override val layoutId: Int = R.layout.fragment_mailbox_view_pager

    private var searchValue: String = ""
        set(value) {
            field = value
            getOutbox(DEFAULT_PAGE)
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews()
        initListeners()

        viewModel.getOutbox(searchValue, DEFAULT_PAGE).observe(this, this::handleResource)
    }

    private fun findViews() {
        progressBar = view?.findViewById(R.id.progress_bar_fragment_mailbox)
        recyclerView = view?.findViewById(R.id.recycler_view_fragment_mailbox)
        errorView = view?.findViewById(R.id.error_view_fragment_mailbox)
        swipeRefreshLayout = view?.findViewById(R.id.swipe_refresh_layout_fragment_mailbox)

        swipeToDeleteBehavior = SwipeToDeleteCallback(
            0,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT,
            context!!,
            recyclerView
        )

        recyclerView?.itemAnimator = null
    }

    private fun initListeners() {
        swipeRefreshLayout?.setOnRefreshListener {
            refreshOutbox()
        }
    }

    private fun refreshOutbox() {
        viewModel.refreshOutbox(searchValue)
            .observe(this, this::handleResource)
    }

    private fun initAdapter() {
        recyclerView?.recycledViewPool?.clear()
        adapter = OutboxAdapter(viewModel)
        recyclerView?.adapter = adapter
        adapter?.setOnLoadMoreListener(::getOutbox)

        // swipe to delete
        initSwipeToDeleteListener()

        // click listener
        adapter?.setOnItemClickListener { _, position ->
            if (adapter?.isLoading == false || adapter?.hasLoadedAllItems() == true) {
                val msgId = viewModel.getSpecificOutbox(position)?.id!!
                val title = viewModel.getSpecificOutbox(position)?.title!!
                val type = TYPE_OUTBOX
                findNavController().navigate(
                    MailboxFragmentDirections.mailboxDetailsDirection(
                        msgId,
                        type,
                        title
                    )
                )
            }
        }
    }

    private fun getOutbox(pageNumber: Int = DEFAULT_PAGE) {
        if (pageNumber == DEFAULT_PAGE) {
            adapter?.resetPaging()
        }
        viewModel.getOutbox(
            searchValue,
            pageNumber
        )
            .observe(this, this::handleResource)
    }

    private fun initSwipeToDeleteListener() {
        val itemTouchHelper = ItemTouchHelper(swipeToDeleteBehavior!!)
        itemTouchHelper.attachToRecyclerView(recyclerView)
        swipeToDeleteBehavior!!.getSwiped()!!.observe(this) {
            if (it) {
                AlertDialog.Builder(context!!)
                    .setTitle(R.string.title_delete_mailboix_dialog)
                    .setMessage(R.string.message_delete_mailbox_dialog)
                    .setPositiveButton(R.string.button_positive_delete_discussion_dialog)
                    { _, _ ->
                        viewModel.deleteMessage(swipeToDeleteBehavior?.position!!)
                            .observe(this, this::handleDeleteResource)
                        swipeToDeleteBehavior!!.updateSwipe(false)
                    }
                    .setNegativeButton(R.string.button_negative_delete_mailbox_dialog) { _, _ ->
                        getOutbox(DEFAULT_PAGE)
                    }
                    .setCancelable(false)
                    .show()
            }
        }
    }

    private fun handleDeleteResource(resource: Resource) {
        if (resource is Resource.Error) {
            Toast.makeText(context, resource.error.message, Toast.LENGTH_LONG).show()
            adapter!!.notifyItemChanged(swipeToDeleteBehavior?.position!!)
        } else {
            Toast.makeText(context, getString(R.string.message_move_to_trash_successfully), Toast.LENGTH_LONG).show()
            adapter!!.removeItem(swipeToDeleteBehavior?.position!!)
        }
    }

    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource()
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleSuccessResource() {
        if (adapter == null) {
            initAdapter()
        } else {
            recyclerView?.recycledViewPool?.clear()
            adapter?.notifyDataSetChanged()
        }
        adapter?.finishLoading()
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            errorView?.visibility = View.GONE
            progressBar?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
            recyclerView?.stopScroll()
        } else {
            progressBar?.visibility = View.GONE
            swipeRefreshLayout?.isRefreshing = false
            recyclerView?.visibility = View.VISIBLE

        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        errorView?.visibility = View.VISIBLE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener { getOutbox() }
        adapter?.finishLoading()
    }

    override fun search(searchValue: String) {
        this.searchValue = searchValue
    }

    override fun onResume() {
        searchValue = ""
        viewModel.getOutbox(searchValue, DEFAULT_PAGE).observe(this, this::handleResource)
        super.onResume()
    }

    override fun onDestroyView() {
        swipeRefreshLayout = null
        adapter = null
        errorView = null
        progressBar = null
        recyclerView = null
        super.onDestroyView()
    }

    companion object {
        fun newInstance(): Fragment {
            return OutboxFragment()
        }
    }
}
