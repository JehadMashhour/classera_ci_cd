package com.classera.mailbox.outbox

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.mailbox.MailboxViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Project: Classera
 * Created: Jan 10, 2019
 *
 * @author Abdulrhman Hasan Agha
 */
@Module
abstract class OutboxFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: MailboxViewModelFactory
        ): OutboxViewModel {
            return ViewModelProvider(fragment, factory)[OutboxViewModel::class.java]
        }

    }

    @Binds
    abstract fun bindActivity(activity: OutboxFragment): Fragment

}
