package com.classera.mailbox.draft

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.classera.mailbox.MailboxViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class DraftFragmentModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideViewModel(
            fragment: Fragment,
            factory: MailboxViewModelFactory
        ): DraftViewModel {
            return ViewModelProvider(fragment, factory)[DraftViewModel::class.java]
        }

    }

    @Binds
    abstract fun bindActivity(activity: DraftFragment): Fragment
}
