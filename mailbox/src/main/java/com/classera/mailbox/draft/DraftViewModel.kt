package com.classera.mailbox.draft

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.classera.core.BaseViewModel
import com.classera.data.models.BaseWrapper
import com.classera.data.models.mailbox.DraftWrapper
import com.classera.data.network.DEFAULT_PAGE
import com.classera.data.network.errorhandling.Resource
import com.classera.data.network.errorhandling.tryNoContentResource
import com.classera.data.network.errorhandling.tryResource
import com.classera.data.repositories.mailbox.MailboxRepository
import kotlinx.coroutines.Dispatchers

class DraftViewModel(private val mailboxRepository: MailboxRepository) : BaseViewModel() {

    private var draftMessagesList: MutableList<DraftWrapper?>? = mutableListOf()

    fun getDraftMessagesList(text: CharSequence?, pageNumber: Int): LiveData<Resource> {
        return getDraftMessagesList(text, pageNumber, pageNumber == DEFAULT_PAGE)
    }

    private fun getDraftMessagesList(text: CharSequence?, pageNumber: Int, showProgress: Boolean) =
        liveData(Dispatchers.IO) {
            if (showProgress) {
                emit(Resource.Loading(show = true))
            }
            val resource = tryResource {
                mailboxRepository.getDraftList(pageNumber, text = text)
            }
            if (pageNumber == DEFAULT_PAGE) {
                draftMessagesList?.clear()
            }
            draftMessagesList?.addAll(
                resource.element<BaseWrapper<List<DraftWrapper>>>()?.data ?: mutableListOf()
            )
            emit(resource)
            emit(Resource.Loading(show = false))
        }

    fun getDraftMessagesListCount(): Int {
        return draftMessagesList?.size ?: 0
    }

    fun getDraftItem(position: Int): DraftWrapper? {
        return draftMessagesList?.get(position)
    }

    fun deleteMessage(position: Int) =
        liveData(Dispatchers.IO) {
            val resource = tryNoContentResource {
                mailboxRepository.deletePermanently(draftMessagesList?.get(position)?.id?:"")
            }
            emit(resource)
        }

    fun removeFromList(position: Int) {
        draftMessagesList?.removeAt(position)
    }
}
