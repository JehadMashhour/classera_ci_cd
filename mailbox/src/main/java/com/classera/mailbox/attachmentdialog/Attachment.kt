package com.classera.mailbox.attachmentdialog

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.classera.mailbox.BR

data class Attachment(val url: String?) : BaseObservable() {

    val name: String
        get() = url!!.split("?")[0].substring(url.lastIndexOf("/") + 1)

    @get:Bindable
    var progress: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.progress)
        }

    @get:Bindable
    var state: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.state)
        }
}
