package com.classera.mailbox.recipients

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.widget.AppCompatAutoCompleteTextView
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import com.classera.core.Screen
import com.classera.core.activities.BaseBindingActivity
import com.classera.core.custom.views.CustomChip
import com.classera.core.custom.views.ErrorView
import com.classera.core.utils.android.getStringOrElse
import com.classera.data.models.BaseWrapper
import com.classera.data.models.mailbox.MessageRoleResponse
import com.classera.data.models.mailbox.RecipientByRole
import com.classera.data.models.user.UserRole
import com.classera.data.network.errorhandling.Resource
import com.classera.mailbox.R
import com.classera.mailbox.databinding.ActivityRecipientBinding
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.textfield.TextInputLayout
import javax.inject.Inject

/**
 * Project: Classera
 * Created: Jan 14, 2020
 *
 * @author Abdulrhman Hasan Agha
 */
@Screen("Recipient")
class RecipientActivity : BaseBindingActivity() {

    @Inject
    lateinit var viewModel: RecipientViewModel

    private var roleChipGroup: ChipGroup? = null
    private var recipientChipGroup: ChipGroup? = null
    private var recipientRecyclerview: RecyclerView? = null
    private var progressBar: ProgressBar? = null
    private var errorView: ErrorView? = null
    private var scrollView: NestedScrollView? = null
    private var searchEditText: AppCompatEditText? = null
    private var textViewSelectRole: AppCompatTextView? = null
    private var textViewComposeTo: AppCompatTextView? = null
    private var autoCompleteSchool: AppCompatAutoCompleteTextView? = null

    private var adapter: RecipientAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipient)

        title = getString(R.string.title_menu_slect_recipient)

        findViews()
        getLocalRecipient()
        initListeners()
        handleSchoolIfStudent()
    }

    private fun handleSchoolIfStudent() {
        if (prefs.userRole == UserRole.STUDENT) {
            (autoCompleteSchool?.parent?.parent as TextInputLayout).visibility = View.GONE
            viewModel.getMessageRole().observe(this, this::handleMessageRoleResource)
        } else {
            viewModel.getSchoolList().observe(this, this::handelSchoolListResource)
        }
    }

    private fun getLocalRecipient() {
        val recipients =
            intent.extras?.getParcelableArrayList<RecipientByRole>("recipients")?: mutableListOf<RecipientByRole>()
        recipients.forEach {
            addChip(it!!)
        }

        viewModel.localRecipientUsers = recipients
    }

    private fun initListeners() {
        roleChipGroup?.setOnCheckedChangeListener { _, id ->
            val chip = roleChipGroup?.findViewById<Chip>(id)
            viewModel.currentRole = chip?.text.toString()
            viewModel.currentRoleId = chip?.tag.toString()
            viewModel.getUserByRole(viewModel.currentRoleId).observe(this, this::handleResource)
        }


        searchEditText?.addTextChangedListener(object : TextWatcher {
            @Suppress("EmptyFunctionBlock")
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            @Suppress("EmptyFunctionBlock")
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(value: Editable?) {
                viewModel.currentSearchValue = value.toString().trim()
                viewModel.getFilteredRemoteRecipientUsers()
                adapter?.notifyDataSetChanged()
            }
        })

        autoCompleteSchool?.setOnItemClickListener { _, _, position, _ ->
            viewModel.setSelectedSchool(position)
            if(prefs.selectedSchoolId != viewModel.getSelectedSchool()?.id) {
                viewModel.deleteAllRecipientInSchool()
            }
            viewModel.getMessageRole().observe(this, this::handleMessageRoleResource)
            roleChipGroup?.visibility = View.VISIBLE
            textViewSelectRole?.visibility = View.VISIBLE
            recipientChipGroup?.visibility = View.VISIBLE
            textViewComposeTo?.visibility = View.VISIBLE
            recipientRecyclerview?.visibility = View.GONE
            recipientChipGroup?.removeAllViews()
            bind<ActivityRecipientBinding> {
                this?.role = null
            }
        }
    }

    private fun initSchoolsAdapter() {
        val adapter = ArrayAdapter(
            this,
            R.layout.dropdown_menu_popup_item,
            viewModel.getSchoolListTitles()
        )
        autoCompleteSchool?.setAdapter(adapter)
    }

    @Suppress("UNCHECKED_CAST")
    private fun handleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleSuccessResource(resource as Resource.Success<BaseWrapper<List<RecipientByRole>>>)
            }
            is Resource.Error -> {
                handleErrorResource(resource)
            }
        }
    }

    private fun handleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            recipientChipGroup?.isEnabled = false
            progressBar?.visibility = View.VISIBLE
        } else {
            progressBar?.visibility = View.GONE
            recipientRecyclerview?.visibility = View.VISIBLE
        }
    }

    private fun handleSuccessResource(success: Resource.Success<BaseWrapper<List<RecipientByRole>>>) {
        if(success?.data?.data?.size?:0 > 0) {
            initAdapter()
        }
        bind<ActivityRecipientBinding> {
            this?.role = ""
        }
    }

    private fun handleErrorResource(resource: Resource.Error) {
        val message =
            this?.getStringOrElse(resource.error.resourceMessage, resource.error.message)
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    private fun findViews() {
        roleChipGroup = findViewById(R.id.role_chip_group)
        recipientChipGroup = findViewById(R.id.recipient_chip_group)
        recipientRecyclerview = findViewById(R.id.recipient_rv)
        progressBar = findViewById(R.id.progress_bar_activity_recipient)
        errorView = findViewById(R.id.error_view_activity_recipient)
        scrollView = findViewById(R.id.scrollview)
        searchEditText = findViewById(R.id.search_edit_text)
        autoCompleteSchool = findViewById(R.id.auto_complete_text_activity_recipient_school)
        textViewSelectRole = findViewById(R.id.select_role_text)
        textViewComposeTo = findViewById(R.id.compose_to)
    }

    private fun handelSchoolListResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleSchoolListLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                initSchoolsAdapter()
            }
            is Resource.Error -> {
                Toast.makeText(this, resource.error.message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun handleSchoolListLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
        } else {
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleMessageRoleResource(resource: Resource) {
        when (resource) {
            is Resource.Loading -> {
                handleMessageRoleLoadingResource(resource)
            }
            is Resource.Success<*> -> {
                handleMessageRoleSuccessResource(resource)
            }
            is Resource.Error -> {
                handleMessageRoleErrorResource(resource)
            }
        }
    }

    private fun handleMessageRoleSuccessResource(resource: Resource) {
        progressBar?.visibility = View.GONE
        errorView?.visibility = View.GONE
        scrollView?.visibility = View.VISIBLE
        if (prefs.userRole == UserRole.STUDENT) {
            roleChipGroup?.visibility = View.VISIBLE
            textViewSelectRole?.visibility = View.VISIBLE
            recipientChipGroup?.visibility = View.VISIBLE
            textViewComposeTo?.visibility = View.VISIBLE
            recipientRecyclerview?.visibility = View.VISIBLE
        }
        prepareRoleChipGroup(resource)
    }

    private fun handleMessageRoleLoadingResource(resource: Resource.Loading) {
        if (resource.show) {
            progressBar?.visibility = View.VISIBLE
            errorView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
        }
    }

    private fun handleMessageRoleErrorResource(resource: Resource.Error) {
        errorView?.visibility = View.VISIBLE
        errorView?.setError(resource)
        errorView?.setOnRetryClickListener {
            viewModel.getMessageRole().observe(this, this::handleMessageRoleResource)
        }
    }

    private fun prepareRoleChipGroup(resource: Resource) {
        roleChipGroup?.removeAllViews()
        resource.element<BaseWrapper<List<MessageRoleResponse>>>()?.data!!.forEach {
            val chip = CustomChip.createCheckableChip(
                this,
                it.role?.title ?: "Unknown",
                it.role?.id ?: ""
            )

            roleChipGroup?.addView(chip)
        }
    }

    private fun initAdapter() {
        recipientRecyclerview?.visibility = View.VISIBLE
        adapter = RecipientAdapter(viewModel)
        recipientRecyclerview?.adapter = adapter

        adapter?.setOnItemClickListener { _, position ->
            prefs.selectedSchoolId = viewModel.getSelectedSchool()?.id
            val recipient = viewModel.getFilteredRemoteRecipientUsers()[position]

            if (recipient.checked) {
                viewModel.deleteRecipient(recipient)
                recipient.checked = false
                removeChip(recipient)
            } else {
                viewModel.addRecipient(position)
                recipient.checked = true
                addChip(recipient)
            }

            adapter?.notifyDataSetChanged()
        }
        recipientChipGroup?.isEnabled = true
    }

    private fun addChip(recipient: RecipientByRole) {
        val chip = CustomChip.createCancellableChip(
            this,
            recipient.fullName ?: "Unknown",
            recipient.id
        )

        chip.setOnCloseIconClickListener {
            recipient.checked = false
            viewModel.deleteRecipient(recipient)
            recipientChipGroup?.removeView(chip)

            adapter?.notifyDataSetChanged()
        }

        recipientChipGroup?.addView(chip)
    }

    private fun removeChip(recipient: RecipientByRole) {
        val chip = recipientChipGroup?.findViewWithTag<Chip>(recipient.id)
        recipientChipGroup?.removeView(chip)
    }

    override fun onDestroy() {
        roleChipGroup = null
        recipientChipGroup = null
        recipientRecyclerview = null
        progressBar = null
        errorView = null
        scrollView = null
        searchEditText = null
        super.onDestroy()
    }

}
